! Macros used to define a number of array operations

#undef MKARR1DN
#undef MKARR2DN
#undef MKARR3DN
#undef MKARR4DN
#undef MKSARR2DN
#undef MKSARR3DN
#undef MKINTARR
#undef MKINTARR3

#undef IF_BLOCK
#undef IF_BLOCK_ELSE
#undef IF_BLOCK_END

#if !defined(KOUNT_OPTION)
#  define KOUNT_OPTION .true.
#endif

#if !defined(PACKING_OPTION)
#  define PACKING_OPTION .true.
#endif

! This block is used for declaring the Phys_arrays derived type components in the agcm_types module
#if defined(PHYARRDCL)

#  define IF_BLOCK(OPTION)
#  define IF_BLOCK_ELSE
#  define IF_BLOCK_END

#  if defined(_POINTER_ASSIGNMENT) || defined(ECCCGEM)
#    define ALLOCABLE_ pointer
#  else
#    define ALLOCABLE_ allocatable
#  endif

#  define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:), ALLOCABLE_ :: PNAME
#  define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:), ALLOCABLE_ :: PNAME
#  define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:,:), ALLOCABLE_ :: PNAME
#  define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:,:,:), ALLOCABLE_ :: PNAME

#  define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:), ALLOCABLE_ :: PNAME
#  define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:,:), ALLOCABLE_ :: PNAME

! This block is used for declaring the PAK arrays in the compak module
#elif defined(PAKARRDCL)

#  define IF_BLOCK(OPTION)
#  define IF_BLOCK_ELSE
#  define IF_BLOCK_END

#  define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:), pointer :: PTAR
#  define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:), pointer :: PTAR
#  define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:,:), pointer :: PTAR
#  define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:,:,:), pointer :: PTAR

#  define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:), pointer :: PTAR
#  define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) real, dimension(:,:,:), pointer :: PTAR

#else

#  define IF_BLOCK(OPTION) if ( OPTION ) then
#  define IF_BLOCK_ELSE else
#  define IF_BLOCK_END end if

! This block is used for the optional _POINTER_ASSIGNMENT setup for the spectral model
#  if defined(POINT_)

#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI) => PTAR(ioff1:ioffg)
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK) => PTAR(ioff1:ioffg,:)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK,1:NN) => PTAR(ioff1:ioffg,:,:)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK,1:NN,1:NM) => PTAR(ioff1:ioffg,:,:,:)

#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK)) ; pa%PNAME(il1:il2,1:NK) = SPREAD(PTAR(ioffz+il1z,:),1,nil)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK,NN)) ; pa%PNAME(il1:il2,1:NK,1:NN) = SPREAD(PTAR(ioffz+il1z,:,:),1,nil)

! This block is used for pointing the various Physics arrays to starting location on the GEM bus
#  elif defined(BUSPOINT_)

#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI) => PBUS(n_ ## PNAME:)
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK) => PBUS(n_ ## PNAME:)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK,1:NN) => PBUS(n_ ## PNAME:)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK,1:NN,1:NM) => PBUS(n_ ## PNAME:)

#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK) => PBUS(n_ ## PNAME:)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) nullify(pa%PNAME); pa%PNAME(1:NI,1:NK,1:NN) => PBUS(n_ ## PNAME:)

! This block is used for assigning the Physics arrays to the equivalent PAK arrays for the spectral model
#  elif defined(ASSIGN_)
#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) pa%PNAME(il1:il2) = PTAR(ioff1:ioff2)
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) pa%PNAME(il1:il2,1:NK) = PTAR(ioff1:ioff2,:)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) pa%PNAME(il1:il2,1:NK,1:NN) = PTAR(ioff1:ioff2,:,:)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) pa%PNAME(il1:il2,1:NK,1:NN,1:NM) = PTAR(ioff+il1:ioff+il2,:,:,:)
#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) pa%PNAME(il1:il2,1:NK) = SPREAD(PTAR(ioffz+il1z,:),1,nil)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) pa%PNAME(il1:il2,1:NK,1:NN) = SPREAD(PTAR(ioffz+il1z,:,:),1,nil)

! This block is used for packing the Physics arrays back into the equivalent PAK arrays for the spectral model
#  elif defined(PACK_)
#    if defined(_POINTER_ASSIGNMENT)
#      define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME)
#      define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME)
#      define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME)
#      define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME)
#    else
#      define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) PTAR(ioff1:ioff2) = pa%PNAME(il1:il2)
#      define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) PTAR(ioff1:ioff2,1:NK) = pa%PNAME(il1:il2,1:NK)
#      define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) PTAR(ioff1:ioff2,1:NK,1:NN) = pa%PNAME(il1:il2,1:NK,1:NN)
#      define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) PTAR(ioff1:ioff2,1:NK,1:NN,1:NM) = pa%PNAME(il1:il2,1:NK,1:NN,1:NM)
#    endif
#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) PTAR(ioffz+il1z,1:NK) = pa%PNAME(il1,1:NK)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) PTAR(ioffz+il1z,1:NK,1:NN) = pa%PNAME(il1,1:NK,1:NN)

! This block is used for allocating the array sizes for Physics arrays in the spectral model
#  elif defined(ALLOC_)
#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI))
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK))
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK,NN))
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK,NN,NM))
#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK))
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) allocate(pa%PNAME(NI,NK,NN))

! This block is used for deallocating the array sizes for Physics arrays in the spectral model
#  elif defined(DEALLOC_)
#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) deallocate(pa%PNAME)
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) deallocate(pa%PNAME)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) deallocate(pa%PNAME)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) deallocate(pa%PNAME)
#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) deallocate(pa%PNAME)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) deallocate(pa%PNAME)

! This block is used for defining the PAK array index on the long spec PAK bus
#  elif defined(PAKINDLEN_)

#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) n_ ## PNAME = pak_array_len + 1 ; pak_array_len = pak_array_len + ip0j
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) n_ ## PNAME = pak_array_len + 1 ; pak_array_len = pak_array_len + ip0j * NK
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) n_ ## PNAME = pak_array_len + 1 ; pak_array_len = pak_array_len + ip0j * NK * NN
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) n_ ## PNAME = pak_array_len + 1 ; pak_array_len = pak_array_len + ip0j * NK * NN * NM

#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) n_ ## PNAME = pak_array_len + 1 ; pak_array_len = pak_array_len + ip0jz * NK
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) n_ ## PNAME = pak_array_len + 1 ; pak_array_len = pak_array_len + ip0jz * NK * NN

! This block is used for pointing the various PAK arrays to starting location on the Spectral long PAK array
#  elif defined(PAKPOINT_)

#    define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) nullify(PTAR); PTAR(1:ip0j) => PAK_ARRAY(n_ ## PNAME:)
#    define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) nullify(PTAR); PTAR(1:ip0j,1:NK) => PAK_ARRAY(n_ ## PNAME:)
#    define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) nullify(PTAR); PTAR(1:ip0j,1:NK,1:NN) => PAK_ARRAY(n_ ## PNAME:)
#    define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) nullify(PTAR); PTAR(1:ip0j,1:NK,1:NN,1:NM) => PAK_ARRAY(n_ ## PNAME:)

#    define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) nullify(PTAR); PTAR(1:ip0jz,1:NK) => PAK_ARRAY(n_ ## PNAME:)
#    define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) nullify(PTAR); PTAR(1:ip0jz,1:NK,1:NN) => PAK_ARRAY(n_ ## PNAME:)

#  endif

#endif

! This block is used for declaring the Physics arrays' bus indices on the GEM bus (phybus module)
#if defined(PHYBUSINDDCL)

#  define IF_BLOCK(OPTION)
#  define IF_BLOCK_ELSE
#  define IF_BLOCK_END

#  define MKARR1DN(PNAME,PTAR,NI,ONAME,DECR,PBUS,INIT,INAME) integer :: n_ ## PNAME = 0
#  define MKARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) integer :: n_ ## PNAME = 0
#  define MKARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) integer :: n_ ## PNAME = 0
#  define MKARR4DN(PNAME,PTAR,NI,NK,NN,NM,ONAME,DECR,PBUS,INIT,INAME) integer :: n_ ## PNAME = 0
#  define MKSARR2DN(PNAME,PTAR,NI,NK,ONAME,DECR,PBUS,INIT,INAME) integer :: n_ ## PNAME = 0
#  define MKSARR3DN(PNAME,PTAR,NI,NK,NN,ONAME,DECR,PBUS,INIT,INAME) integer :: n_ ## PNAME = 0
#endif

