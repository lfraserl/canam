!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
module msizes
  !=======================================================================
  !     * aug 14,2018 - m. lazare. - remove unused iflm.
  !     * nov 1,2017  - j. cole.   - add variables to handle zonal fields
  !     * apr 18,2015 - f. majaess.- add "ntlkd" definition to better
  !     *                            handle no lake tiles in trinfo10
  !     *                            namelists.
  !     * feb 12,2015 - m. lazare. - add tiling dimensions based on
  !                                  namelist output (for gcm18+).
  !     *                          - remove ilevp2 (unused), backstitched.
  !     * dec 15,2003 - m. lazare. - parms13b split into msizes,psizes.
  !     * dec 10,2003 - m. lazare. - nintsw now hard-coded.
  !     *                          - $nnode$ -> $nnode_a$.
  !     *                          - ntracn,ntraca added.
  !     *                          - ntraca used instead of ntrac in nlev1e.
  !     * dec 03,2003 - l. solheim - consolodation of "parmsub" parameters
  !                                  into a single module
  !=======================================================================
  !
  implicit none

  !     * number of tracers
  integer :: ntrac
  integer :: ntraca
  integer :: ntracn
  !
  !     * number of mosaic tiles per grid cell and
  !     * total number of tiles.
  !
  integer :: ntld
  integer :: ntlk
  integer :: ntwt
  integer :: im
  integer :: ntlkd

  !     * number of model levels
  integer :: ilev
  integer :: ilevp1
  integer :: ilevp2
  !     * number of moisture levels
  integer :: levs

  !     * number of longitudes on the physics grid
  integer :: lonsl

  !     * physics "chained latitude" dimension
  !     * ilg = lonp+1        for sublatitude case
  !     * ilg = ilgsl*nlatj+1 for chained latitude case
  integer :: ilg
  !
  !     * physics tile gathered array dimension
  !
  integer :: ilgl
  integer :: ilgk
  integer :: ilgw
  integer :: ilgm

  !     * number of longitudes on the zonal grid (obviously 1 for the spectral model)
  integer, parameter :: lonsz = 1

  !     * zonal mean latitude dimension
  integer :: ilgz

  !**********The following grid sizes are only used in the spectral model *********!
  !*
  !********************************************************************************!
  !     * number of smp nodes ...use mpi for nnode>1
  integer :: nnodex

  !.....physical dimension sizes
  !     * number of longitudes on the physics grid
  integer :: ilgsl

  !     * number of longitudes on the dynamics grid
  integer :: lonsld
  integer :: ilgsld

  !     * number of gaussian latitudes on physics grid
  integer :: nlat
  integer :: ilat

  !     * number of gaussian latitudes on dymanics grid
  integer :: nlatd
  integer :: ilatd

  !.....parameters related to using multi- or sub- latitudes

  !     * grid points per slice in physics
  integer :: lonp

  !     * grid points per slice in dynamics
  integer :: lond

  !     * number of chained physics latitudes or 1 for sublatitude cases
  integer :: nlatj

  !     * number of chained dymanics latitudes or 1 for sublatitude cases
  integer :: nlatjd

  integer :: nlatjm

  !     * dynamics "chained latitude" dimension
  !     * ilgd = lond+1          for sublatitude case
  !     * ilgd = ilgsld*nlatjd+1 for chained latitude case
  integer :: ilgd

  !     * physics "chained latitude" dimension
  integer :: ilg_tp

  !     * dynamics "chained latitude" dimension
  integer :: ilg_td

  !     * used to dimension arrays in "GR1" common
  integer :: idlm

  !     * first dimension of physics "pak" arrays
  integer :: ip0j

  !     * first dimension of zonal "pak" arrays
  integer :: ip0jz

  !     * first dimension of dynamics "pak" arrays
  integer :: dp0j

  integer :: lon_tp

  integer :: lon_td

  !     * number of iterations in physics latitude loop
  integer :: ntask_p

  !     * number of iterations in dynamics latitude loop
  integer :: ntask_d

  integer :: ntask_m

  !     * number of iterations in physics latitude loop for sublatitude case
  integer :: ntask_tp

  !     * number of iterations in dynamics latitude loop for sublatitude case
  integer :: ntask_td

  !.....spectral parameters

  !     * first dimension of complex (fourier) arrays
  integer :: ilh
  integer :: ilhd

  !     * total spectral triangle sizes
  integer :: lmtotal
  integer :: latotal
  integer :: iram

  !     * local (to each node) spectral sizes
  integer :: lm
  integer :: lmp1
  integer :: la

  !     * complex :: work space dimensions (local to each node)
  integer :: rl
  integer :: rs

  !     * length of idat data buffer in common/icom/
  !ignoreLint(5)
  integer :: ip0f

  !     * size of gll work array
  integer :: ngll

  !
  !     * ctem parameters from base file.
  !
  !
  !     lndcvrmod ! = 12345 means continuously update land cover
  !                   from 1850 to 2100
  !               ! = 1990 or any other year means use land cover
  !               !   for july of that year all the time.
  !
  !     lndcvr_offset ! = offset to add to model year to get calendar
  !                   !   year to use for land cover when lndcvrmod = 12345
  !
  !     initpool  ! = -ve means, initialize pools from coupler restart file
  !               ! = +ve means, initialize pools as below even if there are
  !                   values in the restart file.
  !               ! = 0 means, initialize pools from zero
  !               ! = 1850 or any other value means initialize pools from that year.
  !               !   of course, the relevant file must be present.
  !
  integer :: initpool    ! Used only in spectral
  integer :: lndcvrmod
  integer :: lndcvr_offset

end module msizes
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
