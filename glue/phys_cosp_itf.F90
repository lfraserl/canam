!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
 module phys_cosp_itf
  use cosp_defs
  implicit none

  integer, dimension(lev_cosp_max) :: llidar !<
  integer, dimension(lev_cosp_max) :: lradar !<
  integer, dimension(lev_cosp_max) :: llidartemp !<
  contains

! cosp output
   subroutine canam_cosp_output(ioff, il1, il2, ilev)
      implicit none

      integer, intent(in) :: il1
      integer, intent(in) :: il2
      integer, intent(in) :: ilev
      integer, intent(in) :: ioff

      integer :: i
      integer :: ibox
      integer :: ip
      integer :: isr
      integer :: it
      integer :: ize
      integer :: n

! put the accumulating arrays from cosp into the output arrays
    
      do i = il1, il2

! isccp fields
         if (lalbisccp) &
         o_albisccp(ioff+i) = albisccp(i)

         if (ltauisccp) &
         o_tauisccp(ioff+i) = tauisccp(i)

         if (lpctisccp) &
         o_pctisccp(ioff+i) = pctisccp(i)

         if (lcltisccp) &
         o_cltisccp(ioff+i) = cltisccp(i)


         if (lmeantbisccp) &
         o_meantbisccp(ioff+i) = meantbisccp(i)

         if (lmeantbclrisccp) &
         o_meantbclrisccp(ioff+i) = meantbclrisccp(i)

         if (lisccp_sim) &
         o_sunl(ioff+i) = sunl(i)

! calipso fields

         if (lclhcalipso) then
            o_clhcalipso(ioff+i)    = clhcalipso(i)
            ocnt_clhcalipso(ioff+i) = cnt_clhcalipso(i)
         end if

         if (lclmcalipso) then
            o_clmcalipso(ioff+i)    = clmcalipso(i)
            ocnt_clmcalipso(ioff+i) = cnt_clmcalipso(i)
         end if

         if (lcllcalipso) then
            o_cllcalipso(ioff+i)    = cllcalipso(i)
            ocnt_cllcalipso(ioff+i) = cnt_cllcalipso(i)
         end if

         if (lcltcalipso) then
            o_cltcalipso(ioff+i)    = cltcalipso(i)
            ocnt_cltcalipso(ioff+i) = cnt_cltcalipso(i)
         end if

         if (lclhcalipsoliq) then
            o_clhcalipsoliq(ioff+i)    = clhcalipsoliq(i)
            ocnt_clhcalipsoliq(ioff+i) = cnt_clhcalipsoliq(i)
         end if

         if (lclmcalipsoliq) then
            o_clmcalipsoliq(ioff+i)    = clmcalipsoliq(i)
            ocnt_clmcalipsoliq(ioff+i) = cnt_clmcalipsoliq(i)
         end if

         if (lcllcalipsoliq) then
            o_cllcalipsoliq(ioff+i)    = cllcalipsoliq(i)
            ocnt_cllcalipsoliq(ioff+i) = cnt_cllcalipsoliq(i)
         end if

         if (lcltcalipsoliq) then
            o_cltcalipsoliq(ioff+i)    = cltcalipsoliq(i)
            ocnt_cltcalipsoliq(ioff+i) = cnt_cltcalipsoliq(i)
         end if

         if (lclhcalipsoice) then
            o_clhcalipsoice(ioff+i)    = clhcalipsoice(i)
            ocnt_clhcalipsoice(ioff+i) = cnt_clhcalipsoice(i)
         end if

         if (lclmcalipsoice) then
            o_clmcalipsoice(ioff+i)    = clmcalipsoice(i)
            ocnt_clmcalipsoice(ioff+i) = cnt_clmcalipsoice(i)
         end if

         if (lcllcalipsoice) then
            o_cllcalipsoice(ioff+i)    = cllcalipsoice(i)
            ocnt_cllcalipsoice(ioff+i) = cnt_cllcalipsoice(i)
         end if

         if (lcltcalipsoice) then
            o_cltcalipsoice(ioff+i)    = cltcalipsoice(i)
            ocnt_cltcalipsoice(ioff+i) = cnt_cltcalipsoice(i)
         end if

         if (lclhcalipsoun) then
            o_clhcalipsoun(ioff+i)    = clhcalipsoun(i)
            ocnt_clhcalipsoun(ioff+i) = cnt_clhcalipsoun(i)
         end if

         if (lclmcalipsoun) then
            o_clmcalipsoun(ioff+i)    = clmcalipsoun(i)
            ocnt_clmcalipsoun(ioff+i) = cnt_clmcalipsoun(i)
         end if

         if (lcllcalipsoun) then
            o_cllcalipsoun(ioff+i)    = cllcalipsoun(i)
            ocnt_cllcalipsoun(ioff+i) = cnt_cllcalipsoun(i)
         end if

         if (lcltcalipsoun) then
            o_cltcalipsoun(ioff+i)    = cltcalipsoun(i)
            ocnt_cltcalipsoun(ioff+i) = cnt_cltcalipsoun(i)
         end if


! cloudsat+calipso fields
         if (lcltlidarradar) then
            o_cltlidarradar(ioff+i)    = cltlidarradar(i)
            ocnt_cltlidarradar(ioff+i) = cnt_cltlidarradar(i)
         end if
      end do


! several special fields > 1d

! isccp
      if (lclisccp) then
         do ip = 1, 7           ! nptop
            do it = 1, 7        ! ntaucld
               do i = il1, il2
                  o_clisccp(ioff+i,it,ip) = clisccp(i,it,ip)

               end do
            end do
         end do
      end if

! parasol
      if (lparasolrefl) then
         do ip = 1, parasol_nrefl
            do i = il1, il2
               o_parasol_refl(ioff+i,ip)    =  parasol_refl(i,ip)
               ocnt_parasol_refl(ioff+i,ip) =  cnt_parasol_refl(i,ip)
            end do
         end do
      end if

! 3d fields that might be interpolated to special levels or not
      if (use_vgrid) then       ! interpolate to specific heights
         do n = 1, nlr
            do i = il1, il2
               if (lclcalipso) then
                  o_clcalipso(ioff+i,n)    = clcalipso(i,n)
                  ocnt_clcalipso(ioff+i,n) = cnt_clcalipso(i,n)
               end if

               if (lclcalipsoliq) then
                  o_clcalipsoliq(ioff+i,n) = clcalipsoliq(i,n)
                  ocnt_clcalipsoliq(ioff+i,n) = cnt_clcalipsoliq(i,n)
               end if

               if (lclcalipsoice) then
                  o_clcalipsoice(ioff+i,n) = clcalipsoice(i,n)
                  ocnt_clcalipsoice(ioff+i,n) = cnt_clcalipsoice(i,n)
               end if

               if (lclcalipsoun) then
                  o_clcalipsoun(ioff+i,n) = clcalipsoun(i,n)
                  ocnt_clcalipsoun(ioff+i,n) = cnt_clcalipsoun(i,n)
               end if

               if (lclcalipso2) then
                  o_clcalipso2(ioff+i,n)    = clcalipso2(i,n)
                  ocnt_clcalipso2(ioff+i,n) = cnt_clcalipso2(i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  o_cosp_height_mask(ioff+i,n) = cosp_height_mask(i,n)
               end if
            end do
         end do
      else
         do n = 1, ilev
            do i = il1, il2
               if (lclcalipso) then
                   o_clcalipso(ioff+i,n) =  clcalipso(i,n)
                   ocnt_clcalipso(ioff+i,n) = cnt_clcalipso(i,n)
               end if

               if (lclcalipsoliq) then
                  o_clcalipsoliq(ioff+i,n) = clcalipsoliq(i,n)
                  ocnt_clcalipsoliq(ioff+i,n) = cnt_clcalipsoliq(i,n)
               end if

               if (lclcalipsoice) then
                  o_clcalipsoice(ioff+i,n) = clcalipsoice(i,n)
                  ocnt_clcalipsoice(ioff+i,n) = cnt_clcalipsoice(i,n)
               end if

               if (lclcalipsoun) then
                  o_clcalipsoun(ioff+i,n) = clcalipsoun(i,n)
                  ocnt_clcalipsoun(ioff+i,n) = cnt_clcalipsoun(i,n)
               end if

               if (lclcalipso2) then
                   o_clcalipso2(ioff+i,n) = clcalipso2(i,n)
                   ocnt_clcalipso2(ioff+i,n) = cnt_clcalipso2(i,n)
               end if

               if (lcfaddbze94 .or. lcfadlidarsr532) then
                  o_cosp_height_mask(ioff+i,n) = cosp_height_mask(i,n)
               end if
            end do
         end do
      end if

         do n = 1, lidar_ntemp
            do i = il1, il2
               if (lclcalipsotmp) then
                  o_clcalipsotmp(ioff+i,n) = clcalipsotmp(i,n)
                  ocnt_clcalipsotmp(ioff+i,n) = cnt_clcalipsotmp(i,n)
               end if

               if (lclcalipsotmpliq) then
                  o_clcalipsotmpliq(ioff+i,n) = clcalipsotmpliq(i,n)
                  ocnt_clcalipsotmpliq(ioff+i,n) = &
                                        cnt_clcalipsotmpliq(i,n)
               end if

               if (lclcalipsotmpice) then
                  o_clcalipsotmpice(ioff+i,n) = clcalipsotmpice(i,n)
                  ocnt_clcalipsotmpice(ioff+i,n) = &
                                        cnt_clcalipsotmpice(i,n)
               end if

               if (lclcalipsotmpun) then
                  o_clcalipsotmpun(ioff+i,n) = clcalipsotmpun(i,n)
                  ocnt_clcalipsotmpun(ioff+i,n) = &
                                         cnt_clcalipsotmpun(i,n)
               end if
            end do ! i
         end do ! n

! cfads (2d histograms)

! calipso

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, nlr
                  do i = il1, il2
                     o_cfad_lidarsr532(ioff+i,n,isr) = &
                     cfad_lidarsr532(i,n,isr)
                  end do
               end do
            end do
         end if
      else
         if (lcfadlidarsr532) then
            do isr = 1, sr_bins
               do n = 1, ilev
                  do i = il1, il2
                     o_cfad_lidarsr532(ioff+i,n,isr) = &
                     cfad_lidarsr532(i,n,isr)
                  end do
               end do
            end do
         end if
      end if

! cloudsat

      if (use_vgrid) then       ! interpolate to specific heights
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, nlr
                  do i = il1, il2
                     o_cfad_dbze94(ioff+i,n,ize)= &
                     cfad_dbze94(i,n,ize)
                  end do
               end do
            end do
         end if
      else
         if (lcfaddbze94) then
            do ize = 1, dbze_bins
               do n = 1, ilev
                  do i = il1, il2
                     o_cfad_dbze94(ioff+i,n,ize) = &
                     cfad_dbze94(i,n,ize)
                  end do
               end do
            end do
         end if
      end if
      if (lceres_sim) then
         do ip = 1, nceres
            do i = il1, il2
               o_ceres_cf(ioff+i,ip)    = ceres_cf(i,ip)
               o_ceres_cnt(ioff+i,ip)   = ceres_cnt(i,ip)
               o_ceres_ctp(ioff+i,ip)   = ceres_ctp(i,ip)
               o_ceres_tau(ioff+i,ip)   = ceres_tau(i,ip)
               o_ceres_lntau(ioff+i,ip) = ceres_lntau(i,ip)
               o_ceres_lwp(ioff+i,ip)   = ceres_lwp(i,ip)
               o_ceres_iwp(ioff+i,ip)   = ceres_iwp(i,ip)
               o_ceres_cfl(ioff+i,ip)   = ceres_cfl(i,ip)
               o_ceres_cfi(ioff+i,ip)   = ceres_cfi(i,ip)
               o_ceres_cntl(ioff+i,ip)  = ceres_cntl(i,ip)
               o_ceres_cnti(ioff+i,ip)  = ceres_cnti(i,ip)
               o_ceres_rel(ioff+i,ip)   = ceres_rel(i,ip)
               o_ceres_cdnc(ioff+i,ip)  = ceres_cdnc(i,ip)
               o_ceres_clm(ioff+i,ip)   = ceres_clm(i,ip)
               o_ceres_cntlm(ioff+i,ip) = ceres_cntlm(i,ip)
            end do ! i
         end do ! ip
      end if
      if (lceres_sim .and. lswath) then
         do ip = 1, nceres
            do i = il1, il2
               os_ceres_cf(ioff+i,ip)    = s_ceres_cf(i,ip)
               os_ceres_cnt(ioff+i,ip)   = s_ceres_cnt(i,ip)
               os_ceres_ctp(ioff+i,ip)   = s_ceres_ctp(i,ip)
               os_ceres_tau(ioff+i,ip)   = s_ceres_tau(i,ip)
               os_ceres_lntau(ioff+i,ip) = s_ceres_lntau(i,ip)
               os_ceres_lwp(ioff+i,ip)   = s_ceres_lwp(i,ip)
               os_ceres_iwp(ioff+i,ip)   = s_ceres_iwp(i,ip)
               os_ceres_cfl(ioff+i,ip)   = s_ceres_cfl(i,ip)
               os_ceres_cfi(ioff+i,ip)   = s_ceres_cfi(i,ip)
               os_ceres_cntl(ioff+i,ip)  = s_ceres_cntl(i,ip)
               os_ceres_cnti(ioff+i,ip)  = s_ceres_cnti(i,ip)
               os_ceres_rel(ioff+i,ip)   = s_ceres_rel(i,ip)
               os_ceres_cdnc(ioff+i,ip)  = s_ceres_cdnc(i,ip)
               os_ceres_clm(ioff+i,ip)   = s_ceres_clm(i,ip)
               os_ceres_cntlm(ioff+i,ip) = s_ceres_cntlm(i,ip)
            end do ! i
         end do ! ip
      end if

! misr
      if (lclmisr) then
         do i = il1, il2
            o_misr_cldarea(ioff+i)   = misr_cldarea(i)
            o_misr_mean_ztop(ioff+i) = misr_mean_ztop(i)
         end do ! i

         do ip = 1,misr_n_cth
            do i = il1, il2
               o_dist_model_layertops(ioff+i,ip) = &
                                     dist_model_layertops(i,ip)
            end do ! i
         end do ! ip

         ibox = 1
         do ip = 1,misr_n_cth
            do it = 1, 7
               do i = il1, il2
                  o_fq_misr_tau_v_cth(ioff+i,ibox) = &
                                     fq_misr_tau_v_cth(i,ibox)
               end do ! i
               ibox = ibox + 1
            end do ! it
         end do ! ip
      end if ! lclmisr

! modis
      if (lcltmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_total_mean(ioff+i) = &
                              modis_cloud_fraction_total_mean(i)
         end do ! i
      end if

      if (lclwmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_water_mean(ioff+i) = &
                              modis_cloud_fraction_water_mean(i)
         end do ! i
      end if

      if (lclimodis) then
         do i = il1, il2
            o_modis_cloud_fraction_ice_mean(ioff+i) = &
                              modis_cloud_fraction_ice_mean(i)
         end do ! i
      end if

      if (lclhmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_high_mean(ioff+i) = &
                              modis_cloud_fraction_high_mean(i)
         end do ! i
      end if

      if (lclmmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_mid_mean(ioff+i) = &
                              modis_cloud_fraction_mid_mean(i)
         end do ! i
      end if

      if (lcllmodis) then
         do i = il1, il2
            o_modis_cloud_fraction_low_mean(ioff+i) = &
                              modis_cloud_fraction_low_mean(i)
         end do ! i
      end if

      if (ltautmodis) then
         do i = il1, il2
            o_modis_optical_thickness_total_mean(ioff+i) = &
                            modis_optical_thickness_total_mean(i)
         end do ! i
      end if

      if (ltauwmodis) then
         do i = il1, il2
            o_modis_optical_thickness_water_mean(ioff+i) = &
                            modis_optical_thickness_water_mean(i)
         end do ! i
      end if

      if (ltauimodis) then
         do i = il1, il2
            o_modis_optical_thickness_ice_mean(ioff+i) = &
                            modis_optical_thickness_ice_mean(i)
         end do ! i
      end if

      if (ltautlogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_total_logmean(ioff+i) = &
                        modis_optical_thickness_total_logmean(i)
         end do ! i
      end if

      if (ltauwlogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_water_logmean(ioff+i) = &
                         modis_optical_thickness_water_logmean(i)
         end do ! i
      end if

      if (ltauilogmodis) then
         do i = il1, il2
            o_modis_optical_thickness_ice_logmean(ioff+i) = &
                          modis_optical_thickness_ice_logmean(i)
         end do ! i
      end if

      if (lreffclwmodis) then
         do i = il1, il2
            o_modis_cloud_particle_size_water_mean(ioff+i) = &
                        modis_cloud_particle_size_water_mean(i)
         end do ! i
      end if

      if (lreffclimodis) then
         do i = il1, il2
            o_modis_cloud_particle_size_ice_mean(ioff+i) = &
                        modis_cloud_particle_size_ice_mean(i)
         end do ! i
      end if

      if (lpctmodis) then
         do i = il1, il2
            o_modis_cloud_top_pressure_total_mean(ioff+i) = &
                       modis_cloud_top_pressure_total_mean(i)
         end do ! i
      end if

      if (llwpmodis) then
         do i = il1, il2
            o_modis_liquid_water_path_mean(ioff+i) = &
                                 modis_liquid_water_path_mean(i)
         end do ! i
      end if

      if (liwpmodis) then
         do i = il1, il2
            o_modis_ice_water_path_mean(ioff+i) = &
                                 modis_ice_water_path_mean(i)
         end do ! i
      end if

      if (lclmodis) then
         ibox = 1
         do ip = 1, a_nummodispressurebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
          o_modis_optical_thickness_vs_cloud_top_pressure(ioff+i,ibox)= &
              modis_optical_thickness_vs_cloud_top_pressure(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrimodis) then
         ibox = 1
         do ip = 1, a_nummodisrefficebins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                      o_modis_optical_thickness_vs_reffice(ioff+i,ibox)= &
                         modis_optical_thickness_vs_reffice(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (lcrlmodis) then
         ibox = 1
         do ip = 1, a_nummodisreffliqbins
            do it = 1, a_nummodistaubins+1
               do i = il1, il2
                      o_modis_optical_thickness_vs_reffliq(ioff+i,ibox)= &
                         modis_optical_thickness_vs_reffliq(i,ibox)
                end do ! i
                ibox = ibox + 1
             end do ! it
          end do ! ip
      end if

      if (llcdncmodis) then
         do i = il1, il2
            o_modis_liq_cdnc_mean(ioff+i) = &
                                 modis_liq_cdnc_mean(i)
            o_modis_liq_cdnc_gcm_mean(ioff+i) = &
                                 modis_liq_cdnc_gcm_mean(i)
            o_modis_cloud_fraction_warm_mean(ioff+i) = &
                      modis_cloud_fraction_warm_mean(i)
         end do ! i
      end if
   end subroutine canam_cosp_output

 end module phys_cosp_itf
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

