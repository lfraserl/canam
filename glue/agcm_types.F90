!> Contains gcm18 types that hold configureable items
module agcm_types_mod

 implicit none; private

!> Holds prognostic and diagnostic arrays used in the CCCma physics
 type, public :: phys_arrays_type

#  define PHYARRDCL
#  include "array_operations.inc"
#  include "phys_arrays.inc"

 end type phys_arrays_type

 type, public :: phys_options_type

   real    :: co2_mult
   integer :: ilaun
   integer :: relax_lake
   integer :: rad_flux_nstrm  !< RT flux approximation type; => 0 (Default) Legacy method; => 2 for 2-stream approximation; => 4 for 4-stream approximation.
   logical :: carbon
   logical :: envelop
   logical :: mam
   logical :: cslm
   logical :: transient_aerosol_emissions
   logical :: emists
   logical :: explvol
   logical :: pla
   logical :: pam
   logical :: agcm_classic
   logical :: cfc11_effective
   logical :: co2_1ppy
   logical :: co2_rev1ppy
   logical :: isnoalb
   logical :: specified_co2
   logical :: with_solvar
   logical :: use_mcica
   logical :: transient_ozone_concentrations
   logical :: salbtable
   logical :: salbform1
   logical :: specified_anncycle_co2_lowlev
   logical :: decoupled_bgc_co2
   logical :: decoupled_rad_co2
   logical :: decay_strat_aerosol !< Switch to control if the stratospheric aerosols decay from initial value \f$[unitless]\f$
   logical :: use_tke             !< Switch to control whether the TKE scheme is active or not \f$[unitless]\f$
   logical :: solar_refraction    !< Switch to control if the direct-beam is refracted in solar radiative transfer \f$[unitless]\f$
   logical :: zero_thermal_source !< Switch to control if the thermal sources in thermal radiative transfer \f$[unitless]\f$
   logical :: gas_chem !< Switch to run with trace gas chemistry \f$[T,F]\f$
   logical :: new_duinit !< Switch to control if using updated version of routine to initialize soil fields for dust emission \f$[T,F]\f$
   logical :: cslm_mod
   logical :: with_2022_emissions !< Use 2022 updated non-transient aerosol emission file
   logical :: use_gem
! Character string indicating the choice of non-orographic gravity-wave drag
! scheme. Presently the three options are SCINOCCA, HINES and MEDKLA (Medvedev-
! Klaassen). The default is SCINOCCA.
   character (len=10) :: scheme_nogwd
! Character string indicating the choice of radiative transfer scheme.
! Current options are 'RADDRIV10' and 'RADDRIV11'
   character (len=16) :: scheme_rad

   contains

   procedure, public :: initialize => initialize_phys_options
 end type phys_options_type

!> Contains various dimensions associated with arrays in phys_arrays. Default values should NOT be set here
 type, public :: phys_dims_type

  ! Defined in cppdef_sizes.h
   integer :: ntld
   integer :: levoz
   integer :: levox
   integer :: ntlk
   integer :: ntwt

  ! Derived from other variables
   integer :: im

  ! Defined based on configuration
   integer :: levsa

   contains

   procedure, public :: initialize => initialize_phys_dims
 end type phys_dims_type

!> Control type for diagnostics of physics
 type, public :: phys_diag_type
  ! Diagnostic switches for the atmospheric module
   logical :: switch_cosp_diag              ! Switch for COSP diagnostics
   logical :: switch_pbl_diag               ! Switch for boundary layer diagnostics

   logical :: radforce
   logical :: tprhs
   logical :: qprhs
   logical :: uprhs
   logical :: vprhs
   logical :: qconsav
   logical :: tprhsc
   logical :: qprhsc
   logical :: uprhsc
   logical :: vprhsc
   logical :: xconsav
   logical :: xprhs
   logical :: xprhsc
   logical :: xtraconv
   logical :: xtrachem
   logical :: xtrapla1
   logical :: xtrapla2
   logical :: xtrals
   logical :: aodpth
   logical :: aodpth_masked
   logical :: xtradust
   logical :: x01
   logical :: x02
   logical :: x03
   logical :: x04
   logical :: x05
   logical :: x06
   logical :: x07
   logical :: x08
   logical :: x09
   logical :: pfrc
   logical :: cdncfrc
   logical :: amradfrc
   logical :: ssradfrc
   logical :: dsradfrc
   logical :: bcradfrc
   logical :: ocradfrc
   logical :: rad_flux_profs
   logical :: canam_mach
   integer :: radforce_trop_idx
   character(len=10) :: radforce_scenario !< character variable to identify the set of perturbations to use for radiative forcings \f$[unitless]\f$

   !> COSP variables
   ! By default these ISCCP simulator options are true when COSP turned on
   logical :: Lisccp_sim
   logical :: Lalbisccp        ! Cloud albedo
   logical :: Lcltisccp        ! Total cloud fraction
   logical :: Lpctisccp        ! Cloud top pressure
   logical :: Ltauisccp        ! Cloud optical thickness (linear average)
   logical :: Lclisccp         ! Cloud top pressure/optical thickness frequency

   ! CALIPSO parameters
   ! By default CALIPSO is turned off
   logical :: Llidar_sim
   logical :: Lclcalipso         ! Cloud amount profile from lidar
   logical :: Lclcalipsoliq      ! Cloud phase profiles from lidar
   logical :: Lclcalipsoice
   logical :: Lclcalipsoun
   logical :: Lclcalipsotmp
   logical :: Lclcalipsotmpliq
   logical :: Lclcalipsotmpice
   logical :: Lclcalipsotmpun
   logical :: Lclhcalipso        ! High cloud fraction lidar
   logical :: Lclmcalipso        ! Middle cloud fraction lidar
   logical :: Lcllcalipso        ! Low cloud fraction lidar
   logical :: Lcltcalipso        ! Total cloud fraction lidar
   logical :: Lclhcalipsoliq     ! Liquid, ice and undefined cloud fractions
   logical :: Lcllcalipsoliq
   logical :: Lclmcalipsoliq
   logical :: Lcltcalipsoliq
   logical :: Lclhcalipsoice
   logical :: Lcllcalipsoice
   logical :: Lclmcalipsoice
   logical :: Lcltcalipsoice
   logical :: Lclhcalipsoun
   logical :: Lcllcalipsoun
   logical :: Lclmcalipsoun
   logical :: Lcltcalipsoun
   logical :: LparasolRefl       ! Parasol reflectances at 5 angles
   logical :: LcfadLidarsr532    ! Backscatter-height histogram

   ! CloudSat
   ! By default CloudSat is turned off
   ! !!!WARNING!!! Turning this on is VERY expensive
   logical :: Lradar_sim
   logical :: Lclcalipso2       ! Cloud fraction detected by lidar but not radar
   logical :: Lcltlidarradar    ! Total cloud detected by lidar and radar
   logical :: LcfadDbze94       ! Radar reflectivity-height histogram

   ! MISR
   ! By default the MISR simulator is turned off
   logical :: Lmisr_sim
   logical :: LclMISR          ! .true. turns on all of the MISR output

   ! MODIS
   ! By default the MODIS simulator is turned off
   logical :: Lmodis_sim
   logical :: Lcltmodis        ! Total cloud fraction
   logical :: Lclwmodis        ! Water cloud fraction
   logical :: Lclimodis        ! Ice cloud fraction
   logical :: Lclhmodis        ! High cloud fraction
   logical :: Lclmmodis        ! Middle cloud fraction
   logical :: Lcllmodis        ! Low cloud fraction
   logical :: Ltautmodis       ! Total optical thickness (linear)
   logical :: Ltauwmodis       ! Water optical thickness (linear)
   logical :: Ltauimodis       ! Ice optical thickness (linear)
   logical :: Ltautlogmodis    ! Total optical thickness (log)
   logical :: Ltauwlogmodis    ! Water optical thickness (log)
   logical :: Ltauilogmodis    ! Ice optical thickness (log)
   logical :: Lreffclwmodis    ! Liquid particle size
   logical :: Lreffclimodis    ! Ice particle size
   logical :: Lpctmodis        ! Cloud top pressure
   logical :: Llwpmodis        ! Liquid water path
   logical :: Liwpmodis        ! Ice water path
   logical :: Lclmodis         ! Pressure-tau histogram
   logical :: Lcrimodis        ! Tau-ice particle radius histogram
   logical :: Lcrlmodis        ! Tau-liquid particle radius histogram

   logical :: Llcdncmodis      ! Liquid cloud droplet number concentration
   logical :: Lsimplereffmodis ! How to compute effective radii in the MODIS simulator
   logical :: Ltwotaumodis     ! How optical thicknesses are passed to the MODIS simulator

   ! CERES simulator
   logical :: Lceres_sim       ! If set to .true. then Lisccp_sim is also set to .true.
                               ! and all of the output from the simulator is saved.

   ! Extra fields
   logical :: use_vgrid        ! True interpolates vertical profiles to heights for Calipso and CloudSat
   logical :: Lswath           ! Sampling along an orbital "swath" (dummy variable for now)
   logical :: Linstant         ! Instantaneous sampling of the fields

   ! COSP calling frequency, can be multiples of ISRAD
   integer :: ISCOSP


   !> planetary boundary layer parameters

   ! Switches for the planetary boundary layer
   logical :: switch_pbl_state_heights       ! Switch to turn on
   logical :: switch_pbl_wind_gust           ! Switch to turn on diagnostic estimates of wind gusts

   ! Parameters for the planetary boundary layer
   integer :: iz_pbl_thermo_state      ! Number of height levels for boundary layer state diagnostics on thermodyamic variables
   integer :: iz_pbl_momentum_state    ! Number of height levels for boundary layer state diagnostics on momentum variables

   real, allocatable, dimension(:) :: z_pbl_thermo_state     ! Heights above ground at which to evaluate the thermodynamic boundary layer state
   real, allocatable, dimension(:) :: z_pbl_momentum_state   ! Heights above ground at which to evaluate the momentum boundary layer

   contains
   ! Initialization of the diagnostic class
   procedure, public :: initialize => initialize_phys_diag
 end type phys_diag_type

  !> Contains runtime options and various dimensions used throughout the physics
 type, public :: phys_config_type
   type(phys_options_type), pointer :: options !< Runtime configurable options
   type(phys_dims_type)   , pointer :: dims    !< Dimensions of arrays in the physics
   type(phys_diag_type)   , pointer :: diag    !< options for diagnostics in CanAM
   contains

   procedure, public :: initialize => initialize_config
 end type phys_config_type

! Module variables
 type(phys_dims_type),    public, target, save :: phys_dims
 type(phys_options_type), public, target, save :: phys_options
 type(phys_diag_type),    public, target, save :: phys_diag
 contains

!> Read in dimensions associated with all arrays in phys_arrays.
subroutine initialize_phys_dims( self, nml_unit, mynode )
  use psizes_19, only : ntld, ntlk, ntwt, levoz, levox, ioxtyp, ioztyp, &
                        initpool, lndcvrmod, lndcvr_offset, &
                        im, levsa, nxloc, ntlkd
  implicit none

  class(phys_dims_type), intent(inout) :: self     !< phys_arrays_dims type to be initialized
  integer,               intent(in)    :: nml_unit !< File unit of an already opened namelist
  integer,               intent(in)    :: mynode   !< MPI CPU tile

  ! Defined in a namelist, set default values here
  ntld   = 1
  levoz  = 49
  levox  = 19
  ntlk   = 3
  ntwt   = 2
  ioxtyp = 1
  ioztyp = 6

  initpool      = -1000
  lndcvrmod     = 1850
  lndcvr_offset = 0

  namelist /phys_dims/ ntld, levoz, levox, ntlk, ntwt, ioxtyp, ioztyp

  namelist /ctem_params/ initpool, lndcvrmod, lndcvr_offset

  rewind(nml_unit)
  read(nml_unit, phys_dims)
  read(nml_unit, ctem_params)

  if (mynode == 0) then
     write(6, *) "Final model's Physics dimensions settings:"
     write(6, nml = phys_dims)
     write(6, *) "Final CTEM parameters:"
     write(6, nml = ctem_params)
  end if

  ! Derived fom other variables
  !     * total number of tiles.
  im = ntld + ntlk + ntwt
  ntlkd = max(ntlk,1)

  ! Set fields from namelist
  self%ntld = ntld
  self%levoz = levoz
  self%levox = levox
  self%ntlk = ntlk
  self%ntwt = ntwt

  self%im = im

  ! Configuration dependent model sizes and checks
  if (phys_options%explvol) levsa = 70
  self%levsa = levsa

  if (phys_options%use_mcica .or. phys_diag%switch_cosp_diag) nxloc = 150

end subroutine initialize_phys_dims

!> Set runtime configurable parameters affecting phys_arrays. Currently these are being set via CPP keys, but
!! there is hope that they may actually be set from a namelist
subroutine initialize_phys_options( self, nml_unit, mynode )
  implicit none

  class(phys_options_type), intent(inout) :: self !< Contains all configurable parameters affecting phys_arrays
  integer,                  intent(in   ) :: nml_unit !< Handle to the namelist file
  integer,                  intent(in   ) :: mynode   !< MPI CPU tile

  real    :: co2_mult = 1.0
  integer :: ilaun = 0
  integer :: relax_lake = 0
  integer :: rad_flux_nstrm = 0
  logical :: carbon = .false.
  logical :: envelop = .true.
  logical :: mam = .false.      ! Use high-top 80-level configuration
  logical :: cslm = .false.
  logical :: transient_aerosol_emissions = .false.
  logical :: emists = .false.
  logical :: explvol = .false.
  logical :: pla = .false.
  logical :: pam = .false.
  logical :: agcm_classic = .false.
  logical :: cfc11_effective = .false.
  logical :: co2_1ppy = .false.
  logical :: co2_rev1ppy = .false.
  logical :: isnoalb = .false.
  logical :: specified_co2 = .false.
  logical :: with_solvar = .false.
  logical :: use_mcica = .false.
  logical :: transient_ozone_concentrations = .false.
  logical :: salbtable = .false.
  logical :: salbform1 = .false.
  logical :: specified_anncycle_co2_lowlev = .false.
  logical :: decoupled_bgc_co2 = .false.
  logical :: decoupled_rad_co2 = .false.
  logical :: decay_strat_aerosol = .false. ! Default is .false., no decay.
  logical :: solar_refraction = .true.     ! Default is .true., refract direct-beam in solar radiative transfer
  logical :: zero_thermal_source = .false. ! Default is .false., include thermal sources in thermal radiative transfer
  logical :: gas_chem = .false.            ! Default is .false., do not run gas chemistry
  logical :: use_tke = .false.             ! Default is .false., do not use tke
  logical :: new_duinit = .false.          ! Default is .false., do not use updated version of routine to initialize soil fields for dust emission
  logical :: cslm_mod = .false.
  logical :: with_2022_emissions = .false. ! Use 2022 updated non-transient aerosol emission file

  logical :: use_gem = .false.

  !--- Set the default value for the non-orographic GWD scheme
  character(len=10) :: scheme_nogwd = "SCINOCCA"     ! Default is Scinocca. Other options are
                         ! Hines (HINES) or Medvedev-Klaassen (MEDKLA)
  !--- Set the default radiative transfer scheme
  character(len=16) :: scheme_rad = "RADDRIV10"      ! Other option is 'RADDRIV11'


  namelist / phys_options / &
    carbon, ilaun, mam, cslm, transient_aerosol_emissions, emists, explvol, agcm_classic, &
    pla, pam, cfc11_effective, isnoalb, co2_mult, co2_1ppy, co2_rev1ppy, &
    with_solvar, use_mcica, transient_ozone_concentrations, salbform1, salbtable, specified_co2, &
    specified_anncycle_co2_lowlev, decoupled_bgc_co2, decoupled_rad_co2, &
    decay_strat_aerosol, use_tke, solar_refraction, zero_thermal_source, &
    gas_chem, scheme_nogwd, scheme_rad, rad_flux_nstrm, new_duinit, with_2022_emissions, &
    envelop, relax_lake, cslm_mod, use_gem

  rewind(nml_unit)
  read(nml_unit, nml=phys_options)

  if (mynode == 0) then
    write(6, *) "Final model's Physics options:"
    write(6, nml = phys_options)
  end if

  self%co2_mult = co2_mult
  self%rad_flux_nstrm = rad_flux_nstrm
  self%carbon = carbon
  self%mam = mam
  self%envelop = envelop
  self%ilaun = ilaun
  self%cslm = cslm
  self%relax_lake = relax_lake
  self%cslm_mod = cslm_mod
  self%transient_aerosol_emissions = transient_aerosol_emissions
  self%emists = emists
  self%explvol = explvol
  self%pla = pla
  self%pam = pam
  self%agcm_classic = agcm_classic
  self%cfc11_effective = cfc11_effective
  self%co2_1ppy = co2_1ppy
  self%co2_rev1ppy = co2_rev1ppy
  self%isnoalb = isnoalb
  self%specified_co2 = specified_co2
  self%with_solvar = with_solvar
  self%use_mcica = use_mcica
  self%transient_ozone_concentrations = transient_ozone_concentrations
  self%salbtable = salbtable
  self%salbform1 = salbform1
  self%specified_anncycle_co2_lowlev = specified_anncycle_co2_lowlev
  self%decoupled_bgc_co2 = decoupled_bgc_co2
  self%decoupled_rad_co2 = decoupled_rad_co2
  self%decay_strat_aerosol = decay_strat_aerosol
  self%solar_refraction = solar_refraction
  self%zero_thermal_source = zero_thermal_source
  self%gas_chem = gas_chem
  self%use_tke = use_tke
  self%new_duinit = new_duinit
  self%with_2022_emissions = with_2022_emissions
  self%use_gem = use_gem
  self%scheme_nogwd = scheme_nogwd
  self%scheme_rad = scheme_rad
  !
  ! * Consistency check for annual cycle CO2 in lowest level CPP directive.
  !
  if (self%specified_anncycle_co2_lowlev) then
     if (self%specified_co2) then
        if (mynode == 0) write(6, *)'AGCM_TYPES_MOD: INCOMPATIBLE DUELING SPECIFIED CO2 !! '
        call xit('AGCM_TYPES_MOD', -1)
     end if
     if (mynode == 0) write(6, *)'AGCM_TYPES_MOD: FREE-CO2 WITH SPECIFIED ANNUAL CYCLE LOWEST LEVEL'
  end if
  !
  ! * Consistency check for surface albedo options.
  !
  if (self%salbform1 .and. self%salbtable) then
     if (mynode == 0) write(6, *)'AGCM_TYPES_MOD: BOTH SALBTABLE AND SALBFORM1 ARE DEFINED.'
     call xit('AGCM_TYPES_MOD', -2)
  end if

  if (.not. self%transient_aerosol_emissions) call xit('AGCM_TYPES_MOD', -3)
  if (.not. self%transient_ozone_concentrations) call xit('AGCM_TYPES_MOD', -4)

  return
end subroutine initialize_phys_options

subroutine initialize_phys_diag( self, nml_unit, mynode )
  use psizes_19, only : izpblm, izpblt
  implicit none

  class(phys_diag_type), intent(inout) :: self      !< Contains all configurable parameters affecting phys_diag
  integer,               intent(in   ) :: nml_unit  !< Handle to the namelist file
  integer,               intent(in   ) :: mynode    !< MPI CPU tileg

  integer :: istat
  !> Switches for diagnostics which are all set to false by default

  logical :: switch_cosp_diag  = .false.
  logical :: switch_pbl_diag   = .false.

  logical :: radforce = .false.
  logical :: tprhs = .false.
  logical :: qprhs = .false.
  logical :: uprhs = .false.
  logical :: vprhs = .false.
  logical :: qconsav = .false.
  logical :: tprhsc = .false.
  logical :: qprhsc = .false.
  logical :: uprhsc = .false.
  logical :: vprhsc = .false.
  logical :: xconsav = .false.
  logical :: xprhs = .false.
  logical :: xprhsc = .false.
  logical :: xtraconv = .false.
  logical :: xtrachem = .false.
  logical :: xtrapla1 = .false.
  logical :: xtrapla2 = .false.
  logical :: xtrals = .false.
  logical :: aodpth = .false.
  logical :: aodpth_masked = .false.
  logical :: xtradust = .false.
  logical :: x01 = .false.
  logical :: x02 = .false.
  logical :: x03 = .false.
  logical :: x04 = .false.
  logical :: x05 = .false.
  logical :: x06 = .false.
  logical :: x07 = .false.
  logical :: x08 = .false.
  logical :: x09 = .false.
  logical :: pla = .false.
  logical :: pam = .false.
  logical :: pfrc = .false.
  logical :: cdncfrc = .false.
  logical :: amradfrc = .false.
  logical :: ssradfrc = .false.
  logical :: dsradfrc = .false.
  logical :: bcradfrc = .false.
  logical :: ocradfrc = .false.
  logical :: rad_flux_profs = .false.
  logical :: canam_mach = .false.
  integer :: radforce_trop_idx = 0
  character(len=10) :: radforce_scenario = ' '

  ! Namelist for diagnostic switches
  namelist / diag_switches / &
           switch_cosp_diag, switch_pbl_diag, &
           radforce, radforce_scenario, radforce_trop_idx, tprhs, qprhs, uprhs, vprhs, &
           qconsav, tprhsc, qprhsc, uprhsc, vprhsc, xconsav, &
           xprhs, xprhsc, xtraconv, xtrachem, xtrapla1, xtrapla2, xtrals, aodpth, &
           xtradust, x01, x02, x03, x04, x05, x06, x07, x08, x09, pfrc, rad_flux_profs, &
           cdncfrc, amradfrc, ssradfrc, dsradfrc, bcradfrc, ocradfrc, aodpth_masked, canam_mach


  !> Parameters for COSP
  ! ISCCP simulator
  ! By default these ISCCP simulator options are true when COSP turned on
  logical :: Lisccp_sim =.false.
  logical :: Lalbisccp  =.true. ! Cloud albedo
  logical :: Lcltisccp  =.true. ! Total cloud fraction
  logical :: Lpctisccp  =.true. ! Cloud top pressure
  logical :: Ltauisccp  =.true. ! Cloud optical thickness (linear average)
  logical :: Lclisccp   =.true. ! Cloud top pressure/optical thickness frequency

  ! CALIPSO
  ! By default CALIPSO is turned off
  logical :: Llidar_sim    =.false.
  logical :: Lclcalipso       =.true.      ! Cloud amount profile from lidar
  logical :: Lclcalipsoliq    =.true.      ! Cloud phase profiles from lidar
  logical :: Lclcalipsoice    =.true.
  logical :: Lclcalipsoun     =.true.
  logical :: Lclcalipsotmp    =.false.
  logical :: Lclcalipsotmpliq =.false.
  logical :: Lclcalipsotmpice =.false.
  logical :: Lclcalipsotmpun  =.false.
  logical :: Lclhcalipso      =.true.     ! High cloud fraction lidar
  logical :: Lclmcalipso      =.true.     ! Middle cloud fraction lidar
  logical :: Lcllcalipso      =.true.     ! Low cloud fraction lidar
  logical :: Lcltcalipso      =.true.     ! Total cloud fraction lidar
  logical :: Lclhcalipsoliq   =.true.     ! Liquid, ice and undefined cloud fractions
  logical :: Lcllcalipsoliq   =.true.
  logical :: Lclmcalipsoliq   =.true.
  logical :: Lcltcalipsoliq   =.true.
  logical :: Lclhcalipsoice   =.true.
  logical :: Lcllcalipsoice   =.true.
  logical :: Lclmcalipsoice   =.true.
  logical :: Lcltcalipsoice   =.true.
  logical :: Lclhcalipsoun    =.true.
  logical :: Lcllcalipsoun    =.true.
  logical :: Lclmcalipsoun    =.true.
  logical :: Lcltcalipsoun    =.true.
  logical :: LparasolRefl     =.true.    ! Parasol reflectances at 5 angles
  logical :: LcfadLidarsr532  =.false.   ! Backscatter-height histogram

  ! CloudSat
  ! By default CloudSat is turned off
  ! !!!WARNING!!! Turning this on is VERY expensive
  logical :: Lradar_sim     =.false.
  logical :: Lclcalipso2    =.true.   ! Cloud fraction detected by lidar but not radar
  logical :: Lcltlidarradar =.true.   ! Total cloud detected by lidar and radar
  logical :: LcfadDbze94    =.false.  ! Radar reflectivity-height histogram

  ! MISR
  ! By default the MISR simulator is turned off
  logical :: Lmisr_sim =.false.
  logical :: LclMISR   =.true.    ! .true. turns on all of the MISR output

  ! MODIS
  ! By default the MODIS simulator is turned off
  logical :: Lmodis_sim       =.false.
  logical :: Lcltmodis        =.true.   ! Total cloud fraction
  logical :: Lclwmodis        =.true.   ! Water cloud fraction
  logical :: Lclimodis        =.true.   ! Ice cloud fraction
  logical :: Lclhmodis        =.true.   ! High cloud fraction
  logical :: Lclmmodis        =.true.   ! Middle cloud fraction
  logical :: Lcllmodis        =.true.   ! Low cloud fraction
  logical :: Ltautmodis       =.true.   ! Total optical thickness (linear)
  logical :: Ltauwmodis       =.true.   ! Water optical thickness (linear)
  logical :: Ltauimodis       =.true.   ! Ice optical thickness (linear)
  logical :: Ltautlogmodis    =.true.   ! Total optical thickness (log)
  logical :: Ltauwlogmodis    =.true.   ! Water optical thickness (log)
  logical :: Ltauilogmodis    =.true.   ! Ice optical thickness (log)
  logical :: Lreffclwmodis    =.true.   ! Liquid particle size
  logical :: Lreffclimodis    =.true.   ! Ice particle size
  logical :: Lpctmodis        =.true.   ! Cloud top pressure
  logical :: Llwpmodis        =.true.   ! Liquid water path
  logical :: Liwpmodis        =.true.   ! Ice water path
  logical :: Lclmodis         =.true.   ! Pressure-tau histogram
  logical :: Lcrimodis        =.true.   ! Tau-ice particle radius histogram
  logical :: Lcrlmodis        =.true.   ! Tau-liquid particle radius histogram

  logical :: Llcdncmodis      =.true.   ! Liquid cloud droplet number concentration
  logical :: Lsimplereffmodis =.true.   ! How to compute effective radii in the MODIS simulator
  logical :: Ltwotaumodis     =.true.   ! How optical thicknesses are passed to the MODIS simulator

  ! CERES simulator
  logical :: Lceres_sim=.false. ! If set to .true. then Lisccp_sim is also set to .true.
                                ! and all of the output from the simulator is saved.

  ! Extra fields
  logical :: use_vgrid=.true. ! True interpolates vertical profiles to heights for Calipso and CloudSat
  logical :: Lswath=.false.   ! Sampling along an orbital "swath" (dummy variable for now)
  logical :: Linstant=.false. ! Instantaneous sampling of the fields

  ! COSP calling frequency, can be multiples of ISRAD
  integer :: ISCOSP=4  ! Conflict with icosp in agcm_phys_init ???

  namelist / cosp_diag_param / &
           Lisccp_sim, Lalbisccp, Lcltisccp, Lpctisccp, Ltauisccp, Lclisccp,    &
           Llidar_sim, Lclcalipso, Lclcalipsoliq, Lclcalipsoice, Lclcalipsoun,  &
           Lclcalipsotmp, Lclcalipsotmpliq, Lclcalipsotmpice, Lclcalipsotmpun,  &
           Lclhcalipso, Lclmcalipso, Lcllcalipso, Lcltcalipso, Lclhcalipsoliq,  &
           Lcllcalipsoliq, Lclmcalipsoliq, Lcltcalipsoliq,                      &
           Lclhcalipsoice, Lcllcalipsoice, Lclmcalipsoice,                      &
           Lcltcalipsoice, Lclhcalipsoun, Lcllcalipsoun, Lclmcalipsoun,         &
           Lcltcalipsoun, LparasolRefl, LcfadLidarsr532,                        &
           Lradar_sim, Lclcalipso2, Lcltlidarradar, LcfadDbze94, Lmisr_sim,     &
           LclMISR, Lmodis_sim, Lcltmodis, Lclwmodis, Lclimodis, Lclhmodis,     &
           Lclmmodis, Lcllmodis, Ltautmodis, Ltauwmodis, Ltauimodis,            &
           Ltautlogmodis, Ltauwlogmodis, Ltauilogmodis, Lreffclwmodis,          &
           Lreffclimodis, Lpctmodis, Llwpmodis, Liwpmodis, Lclmodis, Lcrimodis, &
           Lcrlmodis, Llcdncmodis, Lsimplereffmodis, Ltwotaumodis, Lceres_sim,  &
           use_vgrid, Lswath, Linstant, ISCOSP

  !> Parameters for planetary boundary layer diagnostics
  !> Switches for branching within boundary layer diagnostics
  logical :: switch_pbl_state_heights   = .false.    ! Switch to calculate state variables on predefined heights (z_pbl_thermo_state, z_pbl_momentum_state)
  logical :: switch_pbl_wind_gust       = .false.    ! Switch to estimate wind gusts on predefined heights (z_pbl_momentum_state)

  ! Set maximum number of diagnostic boundary layer heights
  integer, parameter :: iz_pbl_max = 20
  !> Diagnostic Planetary boundary layer height levels
  real, dimension(iz_pbl_max) :: z_pbl_thermo_state
  real, dimension(iz_pbl_max) :: z_pbl_momentum_state

  !> Namelist for planatary boundary layer switches
  namelist / pbl_diag_options / &
           switch_pbl_state_heights, switch_pbl_wind_gust, &
           z_pbl_thermo_state, z_pbl_momentum_state

  z_pbl_momentum_state = -1.
  z_pbl_thermo_state = -1.

  ! Read in namelists from >>canam_settings<< file and overwrite defaults
  rewind(nml_unit)
  read(nml_unit, nml=diag_switches)

  if (mynode == 0) then
     write(6, *) "Diagnostics to be executed in physics"
     write(6, nml = diag_switches)
  end if

  if (switch_cosp_diag) then
      rewind(nml_unit)
      read(nml_unit, nml=cosp_diag_param, iostat = istat)

      if (istat < 0) then
         if (mynode == 0) write(6, *) 'cosp_diag_param namelist section is missing '
         call xit('AGCM_TYPES_MOD', -5)
      end if
      if (mynode == 0) then
         write(6, *) "Parameters for COSP diagnostics"
         write(6, nml = cosp_diag_param)
      end if
  end if

  if (switch_pbl_diag) then
      rewind(nml_unit)
      read(nml_unit, nml=pbl_diag_options, iostat = istat)
      if (istat < 0) then
         if (mynode == 0) write(6, *) 'pbl_diag_options namelist section is missing '
         call xit('AGCM_TYPES_MOD', -6)
      end if

      izpblt = count(z_pbl_thermo_state > 0.0)
      izpblm = count(z_pbl_momentum_state > 0.0)
      if (mynode == 0) then
         write(6, *) "Diagnostic PBL momentum and thermodyamic state parameters"
         write(6, nml = pbl_diag_options)
         write(6, *) 'izpblm and izpblt are: ', izpblm, izpblt
      end if

      if (izpblm == 0 .and. izpblt == 0) then
         if (mynode == 0) write(6, *) 'The switch_pbl_diag switch is set, but no thermodynamic '
         if (mynode == 0) write(6, *) 'state height levels (z_pbl_thermo_state array), nor the '
         if (mynode == 0) write(6, *) 'momentum state height levels (z_pbl_momentum_state array) are set '
         call xit('AGCM_TYPES_MOD', -8)
      end if

      !Allocatable diagnostic state heights arrays
      allocate(self%z_pbl_thermo_state (izpblt))
      allocate(self%z_pbl_momentum_state (izpblm))

  end if

  ! Add switches
  self%switch_cosp_diag = switch_cosp_diag
  self%switch_pbl_diag  = switch_pbl_diag

  self%radforce = radforce
  self%radforce_scenario = radforce_scenario
  self%radforce_trop_idx = radforce_trop_idx
  self%tprhs = tprhs
  self%qprhs = qprhs
  self%uprhs = uprhs
  self%vprhs = vprhs
  self%qconsav = qconsav
  self%tprhsc = tprhsc
  self%qprhsc = qprhsc
  self%uprhsc = uprhsc
  self%vprhsc = vprhsc
  self%xconsav = xconsav
  self%xprhs = xprhs
  self%xprhsc = xprhsc
  self%xtraconv = xtraconv
  self%xtrachem = xtrachem
  self%xtrapla1 = xtrapla1
  self%xtrapla2 = xtrapla2
  self%xtrals = xtrals
  self%aodpth = aodpth
  self%aodpth_masked = aodpth_masked
  self%xtradust = xtradust
  self%x01 = x01
  self%x02 = x02
  self%x03 = x03
  self%x04 = x04
  self%x05 = x05
  self%x06 = x06
  self%x07 = x07
  self%x08 = x08
  self%x09 = x09
  self%pfrc = pfrc
  self%cdncfrc = cdncfrc
  self%amradfrc = amradfrc
  self%ssradfrc = ssradfrc
  self%dsradfrc = dsradfrc
  self%bcradfrc = bcradfrc
  self%ocradfrc = ocradfrc
  self%rad_flux_profs = rad_flux_profs
  self%canam_mach = canam_mach

  ! COSP parameters
  if (switch_cosp_diag) then
    self%Lisccp_sim        = Lisccp_sim
    self%Lalbisccp         = Lalbisccp
    self%Lcltisccp         = Lcltisccp
    self%Lpctisccp         = Lpctisccp
    self%Ltauisccp         = Ltauisccp
    self%Lclisccp          = Lclisccp
    self%Llidar_sim        = Llidar_sim
    self%Lclcalipso        = Lclcalipso
    self%Lclcalipsoliq     = Lclcalipsoliq
    self%Lclcalipsoice     = Lclcalipsoice
    self%Lclcalipsoun      = Lclcalipsoun
    self%Lclcalipsotmp     = Lclcalipsotmp
    self%Lclcalipsotmpliq  = Lclcalipsotmpliq
    self%Lclcalipsotmpice  = Lclcalipsotmpice
    self%Lclcalipsotmpun   = Lclcalipsotmpun
    self%Lclhcalipso       = Lclhcalipso
    self%Lclmcalipso       = Lclmcalipso
    self%Lcllcalipso       = Lcllcalipso
    self%Lcltcalipso       = Lcltcalipso
    self%Lclhcalipsoliq    = Lclhcalipsoliq
    self%Lcllcalipsoliq    = Lcllcalipsoliq
    self%Lclmcalipsoliq    = Lclmcalipsoliq
    self%Lcltcalipsoliq    = Lcltcalipsoliq
    self%Lclhcalipsoice    = Lclhcalipsoice
    self%Lcllcalipsoice    = Lcllcalipsoice
    self%Lclmcalipsoice    = Lclmcalipsoice
    self%Lcltcalipsoice    = Lcltcalipsoice
    self%Lclhcalipsoun     = Lclhcalipsoun
    self%Lcllcalipsoun     = Lcllcalipsoun
    self%Lclmcalipsoun     = Lclmcalipsoun
    self%Lcltcalipsoun     = Lcltcalipsoun
    self%LparasolRefl      = LparasolRefl
    self%LcfadLidarsr532   = LcfadLidarsr532
    self%Lradar_sim        = Lradar_sim
    self%Lclcalipso2       = Lclcalipso2
    self%Lcltlidarradar    = Lcltlidarradar
    self%LcfadDbze94       = LcfadDbze94
    self%Lmisr_sim         = Lmisr_sim
    self%LclMISR           = LclMISR
    self%Lmodis_sim        = Lmodis_sim
    self%Lcltmodis         = Lcltmodis
    self%Lclwmodis         = Lclwmodis
    self%Lclimodis         = Lclimodis
    self%Lclhmodis         = Lclhmodis
    self%Lclmmodis         = Lclmmodis
    self%Lcllmodis         = Lcllmodis
    self%Ltautmodis        = Ltautmodis
    self%Ltauwmodis        = Ltauwmodis
    self%Ltauimodis        = Ltauimodis
    self%Ltautlogmodis     = Ltautlogmodis
    self%Ltauwlogmodis     = Ltauwlogmodis
    self%Ltauilogmodis     = Ltauilogmodis
    self%Lreffclwmodis     = Lreffclwmodis
    self%Lreffclimodis     = Lreffclimodis
    self%Lpctmodis         = Lpctmodis
    self%Llwpmodis         = Llwpmodis
    self%Liwpmodis         = Liwpmodis
    self%Lclmodis          = Lclmodis
    self%Lcrimodis         = Lcrimodis
    self%Lcrlmodis         = Lcrlmodis
    self%Llcdncmodis       = Llcdncmodis
    self%Lsimplereffmodis  = Lsimplereffmodis
    self%Ltwotaumodis      = Ltwotaumodis
    self%Lceres_sim        = Lceres_sim
    self%use_vgrid         = use_vgrid
    self%Lswath            = Lswath
    self%Linstant          = Linstant
    self%ISCOSP            = ISCOSP
  end if

  ! Planetary boundary layer diagnostics
  if (switch_pbl_diag) then
    if (switch_pbl_state_heights) then
      self%switch_pbl_state_heights = switch_pbl_state_heights
      self%z_pbl_thermo_state       = pack(z_pbl_thermo_state, z_pbl_thermo_state > 0.0)
      self%iz_pbl_thermo_state      = izpblt
      self%z_pbl_momentum_state     = pack(z_pbl_momentum_state, z_pbl_momentum_state > 0.0)
      self%iz_pbl_momentum_state    = izpblm
    end if

    if (switch_pbl_wind_gust) then
      self%switch_pbl_wind_gust     = switch_pbl_wind_gust
    end if
  end if

end subroutine initialize_phys_diag

subroutine initialize_config( self, nml_unit_phys, mynode )
  class(phys_config_type), intent(inout) :: self
  integer,                 intent(in)    :: nml_unit_phys         !< Handle to the opened namelist for physics
  integer,                 intent(in)    :: mynode                !< MPI CPU tile

  self%options => phys_options
  call self%options%initialize(nml_unit_phys, mynode)
  self%diag => phys_diag
  call self%diag%initialize(nml_unit_phys, mynode)
  self%dims => phys_dims
  call self%dims%initialize(nml_unit_phys, mynode)

end subroutine initialize_config

end module agcm_types_mod
