!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getrac4 (nf,trac,la,lm,ilev,lh,itrac,ntrac,ntraca, &
                    indxa,latotal,lmtotal,lsrtotal)
  !
  !     * nov 03/03 - m.lazare.   new version based on getrac3, which
  !     *                         calls new routine "GETSPEN" using
  !     *                         work array "GLL", to get load-balanced
  !     *                         spectral data applicable to each node.
  !     * oct 31/02 - j.scinocca. previous version getrac3.
  !
  !     * selectively gets the input spectral tracer fields, under control
  !     * of "IC" attribute of tracers' properties.
  !
  use tracers_info_mod, only: modl_tracers
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(in) :: la
  integer, intent(in) :: latotal
  integer, intent(in) :: lm
  integer, intent(in) :: lmtotal
  integer, intent(in) :: nf
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(in) :: ntraca !< Variable description\f$[units]\f$

  complex, intent(inout) :: trac(la,ilev,ntraca) !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(ntrac) :: indxa !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  integer, intent(in) :: lsrtotal(2,lmtotal + 1) !< Variable description\f$[units]\f$
  !
  integer :: l
  integer :: n
  integer :: na
  integer :: nam
  integer, external :: nc4to8
  logical :: ok
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !--------------------------------------------------------------------------
  if (itrac == 0) return
  !
  do na = 1,ntraca
    n = indxa(na)
    if (modl_tracers(n)%ic == 1) then
      nam = nc4to8(modl_tracers(n)%name)
      do l = 1,ilev
        call getspn (nf,trac(1,l,na),latotal,lmtotal,la,lm,lsrtotal, &
                     nc4to8("SPEC"),0,nam,lh(l),ok)
      end do ! loop 200
    end if
  end do ! loop 300

  return
  !--------------------------------------------------------------------------
end subroutine getrac4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

