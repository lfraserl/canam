!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine getfrc  (amldfpak,reamfpak,veamfpak,fr1fpak,fr2fpak, &
                    ssldfpak,ressfpak,vessfpak,dsldfpak,redsfpak, &
                    vedsfpak,bcldfpak,rebcfpak,vebcfpak,ocldfpak, &
                    reocfpak,veocfpak,zcdnfpak,bcicfpak,bcdpfpak, &
                    amldfpal,reamfpal,veamfpal,fr1fpal,fr2fpal, &
                    ssldfpal,ressfpal,vessfpal,dsldfpal,redsfpal, &
                    vedsfpal,bcldfpal,rebcfpal,vebcfpal,ocldfpal, &
                    reocfpal,veocfpal,zcdnfpal,bcicfpal,bcdpfpal, &
                    nlon,nlat,ilev,incd,iday,mday,mday1, &
                    kount,ijpak,nupf,lh,gg)
  !
  !     * mar 26/13 - k.von salzen. new.
  !
  !     * read in aerosol forcing fields based on pla calculations.
  !
  !     * time label is the julian date of the first of the month
  !     * (nfdm) for which the sst are the monthly average.
  !
  !     * when the model is moving through the year (incd/=0),
  !     * then new fields are read on every mid month day (mmd),
  !     * and interpolation will be done between the two adjacent
  !     * mid month days. "PAK" is the precedent, "PAL" is the target.
  !
  implicit none
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: incd
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  integer, intent(in) :: mday1 !< Variable description\f$[units]\f$
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nupf
  !
  !     * single-level fields.
  !
  real, intent(inout), dimension(ijpak) :: bcdpfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bcdpfpal !< Variable description\f$[units]\f$
  !
  !     * multi-level fields.
  !
  real, intent(inout), dimension(ijpak,ilev) :: amldfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: amldfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: reamfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: reamfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: veamfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: veamfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: fr1fpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: fr1fpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: fr2fpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: fr2fpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: ssldfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: ssldfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: ressfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: ressfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: vessfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: vessfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: dsldfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: dsldfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: redsfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: redsfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: vedsfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: vedsfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: bcldfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: bcldfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: rebcfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: rebcfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: vebcfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: vebcfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: ocldfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: ocldfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: reocfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: reocfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: veocfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: veocfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: zcdnfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: zcdnfpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: bcicfpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak,ilev) :: bcicfpal !< Variable description\f$[units]\f$
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !
  integer, intent(in), dimension(ilev) :: lh !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: jour
  integer :: jour1
  integer :: l
  integer :: lon
  integer, external :: nc4to8
  real :: w1
  real :: w2

  integer*4 :: mynode
  !
  common /mpinfo/ mynode
  !----------------------------------------------------------------------
  jour = mday
  jour1 = mday1
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      rewind nupf
      do l = 1,ilev
        call getggbx(amldfpak(1,l),nc4to8("AMLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reamfpak(1,l),nc4to8("REAM"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veamfpak(1,l),nc4to8("VEAM"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr1fpak(1,l),nc4to8(" FR1"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr2fpak(1,l),nc4to8(" FR2"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ssldfpak(1,l),nc4to8("SSLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ressfpak(1,l),nc4to8("RESS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vessfpak(1,l),nc4to8("VESS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(dsldfpak(1,l),nc4to8("DSLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(redsfpak(1,l),nc4to8("REDS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vedsfpak(1,l),nc4to8("VEDS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcldfpak(1,l),nc4to8("BCLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(rebcfpak(1,l),nc4to8("REBC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vebcfpak(1,l),nc4to8("VEBC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ocldfpak(1,l),nc4to8("OCLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reocfpak(1,l),nc4to8("REOC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veocfpak(1,l),nc4to8("VEOC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(zcdnfpak(1,l),nc4to8("ZCDN"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcicfpak(1,l),nc4to8("BCIC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        do i = 1,ijpak
          amldfpal(i,l) = amldfpak(i,l)
          reamfpal(i,l) = reamfpak(i,l)
          veamfpal(i,l) = veamfpak(i,l)
          fr1fpal (i,l) = fr1fpak (i,l)
          fr2fpal (i,l) = fr2fpak (i,l)
          ssldfpal(i,l) = ssldfpak(i,l)
          ressfpal(i,l) = ressfpak(i,l)
          vessfpal(i,l) = vessfpak(i,l)
          dsldfpal(i,l) = dsldfpak(i,l)
          redsfpal(i,l) = redsfpak(i,l)
          vedsfpal(i,l) = vedsfpak(i,l)
          bcldfpal(i,l) = bcldfpak(i,l)
          rebcfpal(i,l) = rebcfpak(i,l)
          vebcfpal(i,l) = vebcfpak(i,l)
          ocldfpal(i,l) = ocldfpak(i,l)
          reocfpal(i,l) = reocfpak(i,l)
          veocfpal(i,l) = veocfpak(i,l)
          zcdnfpal(i,l) = zcdnfpak(i,l)
          bcicfpal(i,l) = bcicfpak(i,l)
        end do
      end do
      !
      call getagbx(bcdpfpak,nc4to8("DEPB"),nupf,nlon,nlat,jour,gg)
      do i = 1,ijpak
        bcdpfpal(i) = bcdpfpak(i)
      end do

    else
      !
      !         * not a new integration. no new fields required.
      !
    end if

  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !        * start-up time. get fields for previous and target mid-month days.
      !
      rewind nupf
      do l = 1,ilev
        call getggbx(amldfpak(1,l),nc4to8("AMLD"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reamfpak(1,l),nc4to8("REAM"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veamfpak(1,l),nc4to8("VEAM"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr1fpak (1,l),nc4to8(" FR1"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr2fpak (1,l),nc4to8(" FR2"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ssldfpak(1,l),nc4to8("SSLD"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ressfpak(1,l),nc4to8("RESS"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vessfpak(1,l),nc4to8("VESS"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(dsldfpak(1,l),nc4to8("DSLD"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(redsfpak(1,l),nc4to8("REDS"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vedsfpak(1,l),nc4to8("VEDS"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcldfpak(1,l),nc4to8("BCLD"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(rebcfpak(1,l),nc4to8("REBC"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vebcfpak(1,l),nc4to8("VEBC"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ocldfpak(1,l),nc4to8("OCLD"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reocfpak(1,l),nc4to8("REOC"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veocfpak(1,l),nc4to8("VEOC"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(zcdnfpak(1,l),nc4to8("ZCDN"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcicfpak(1,l),nc4to8("BCIC"),nupf,nlon,nlat, &
                     jour1,lh(l),gg)
      end do
      !
      call getggbx(bcdpfpak,nc4to8("DEPB"),nupf,nlon,nlat,jour1,1, &
                   gg)
      !
      rewind nupf
      !
      do l = 1,ilev
        call getggbx(amldfpal(1,l),nc4to8("AMLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reamfpal(1,l),nc4to8("REAM"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veamfpal(1,l),nc4to8("VEAM"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr1fpal (1,l),nc4to8(" FR1"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr2fpal (1,l),nc4to8(" FR2"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ssldfpal(1,l),nc4to8("SSLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ressfpal(1,l),nc4to8("RESS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vessfpal(1,l),nc4to8("VESS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(dsldfpal(1,l),nc4to8("DSLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(redsfpal(1,l),nc4to8("REDS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vedsfpal(1,l),nc4to8("VEDS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcldfpal(1,l),nc4to8("BCLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(rebcfpal(1,l),nc4to8("REBC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vebcfpal(1,l),nc4to8("VEBC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ocldfpal(1,l),nc4to8("OCLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reocfpal(1,l),nc4to8("REOC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veocfpal(1,l),nc4to8("VEOC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(zcdnfpal(1,l),nc4to8("ZCDN"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcicfpal(1,l),nc4to8("BCIC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      call getggbx(bcdpfpal,nc4to8("DEPB"),nupf,nlon,nlat,jour,1, &
                   gg)
      !
      lon = nlon - 1
      day1 = real(mday1)
      day2 = real(iday)
      day3 = real(mday)
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) write(6,6000) iday,mday1,mday,w1,w2
      !
      do i = 1,ijpak
        bcdpfpak(i) = w1 * bcdpfpal(i) + w2 * bcdpfpak(i)
      end do
      !
      do l = 1,ilev
        do i = 1,ijpak
          amldfpak(i,l) = w1 * amldfpal(i,l) + w2 * amldfpak(i,l)
          reamfpak(i,l) = w1 * reamfpal(i,l) + w2 * reamfpak(i,l)
          veamfpak(i,l) = w1 * veamfpal(i,l) + w2 * veamfpak(i,l)
          fr1fpak (i,l) = w1 * fr1fpal (i,l) + w2 * fr1fpak (i,l)
          fr2fpak (i,l) = w1 * fr2fpal (i,l) + w2 * fr2fpak (i,l)
          ssldfpak(i,l) = w1 * ssldfpal(i,l) + w2 * ssldfpak(i,l)
          ressfpak(i,l) = w1 * ressfpal(i,l) + w2 * ressfpak(i,l)
          vessfpak(i,l) = w1 * vessfpal(i,l) + w2 * vessfpak(i,l)
          dsldfpak(i,l) = w1 * dsldfpal(i,l) + w2 * dsldfpak(i,l)
          redsfpak(i,l) = w1 * redsfpal(i,l) + w2 * redsfpak(i,l)
          vedsfpak(i,l) = w1 * vedsfpal(i,l) + w2 * vedsfpak(i,l)
          bcldfpak(i,l) = w1 * bcldfpal(i,l) + w2 * bcldfpak(i,l)
          rebcfpak(i,l) = w1 * rebcfpal(i,l) + w2 * rebcfpak(i,l)
          vebcfpak(i,l) = w1 * vebcfpal(i,l) + w2 * vebcfpak(i,l)
          ocldfpak(i,l) = w1 * ocldfpal(i,l) + w2 * ocldfpak(i,l)
          reocfpak(i,l) = w1 * reocfpal(i,l) + w2 * reocfpak(i,l)
          veocfpak(i,l) = w1 * veocfpal(i,l) + w2 * veocfpak(i,l)
          zcdnfpak(i,l) = w1 * zcdnfpal(i,l) + w2 * zcdnfpak(i,l)
          bcicfpak(i,l) = w1 * bcicfpal(i,l) + w2 * bcicfpak(i,l)
        end do
      end do

    else
      !
      !        * this is in the middle of a run.
      !
      rewind nupf
      do l = 1,ilev
        call getggbx(amldfpal(1,l),nc4to8("AMLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reamfpal(1,l),nc4to8("REAM"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veamfpal(1,l),nc4to8("VEAM"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr1fpal (1,l),nc4to8(" FR1"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(fr2fpal (1,l),nc4to8(" FR2"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ssldfpal(1,l),nc4to8("SSLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ressfpal(1,l),nc4to8("RESS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vessfpal(1,l),nc4to8("VESS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(dsldfpal(1,l),nc4to8("DSLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(redsfpal(1,l),nc4to8("REDS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vedsfpal(1,l),nc4to8("VEDS"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcldfpal(1,l),nc4to8("BCLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(rebcfpal(1,l),nc4to8("REBC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(vebcfpal(1,l),nc4to8("VEBC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(ocldfpal(1,l),nc4to8("OCLD"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(reocfpal(1,l),nc4to8("REOC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(veocfpal(1,l),nc4to8("VEOC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(zcdnfpal(1,l),nc4to8("ZCDN"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      do l = 1,ilev
        call getggbx(bcicfpal(1,l),nc4to8("BCIC"),nupf,nlon,nlat, &
                     jour,lh(l),gg)
      end do
      !
      call getggbx(bcdpfpal,nc4to8("DEPB"),nupf,nlon,nlat,jour,1, &
                   gg)
      !
    end if

  end if
  return
  !---------------------------------------------------------------------
6000 format(' INTERPOLATING FOR', i5, ' BETWEEN', i5, ' AND', &
        i5, ' WITH WEIGHTS = ', 2f7.3)
end subroutine getfrc
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

