!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine gettemi2 (sairpak,ssfcpak,sbiopak,sshipak, &
                     sstkpak,sfirpak,sairpal,ssfcpal, &
                     sbiopal,sshipal,sstkpal,sfirpal, &
                     oairpak,osfcpak,obiopak,oshipak, &
                     ostkpak,ofirpak,oairpal,osfcpal, &
                     obiopal,oshipal,ostkpal,ofirpal, &
                     bairpak,bsfcpak,bbiopak,bshipak, &
                     bstkpak,bfirpak,bairpal,bsfcpal, &
                     bbiopal,bshipal,bstkpal,bfirpal, &
                     nlon,nlat,incd,irefyr,iday,kount, &
                     ijpak,nuan,num_em_aero,ind_bc,    &
                     ind_oc,ind_sulf,gg)
  !
  !     * april 17, 2015 - m.lazare.     cosmetic revision (same name kept)
  !     *                                to have "keeptim" common block
  !     *                                definition line in upper case,
  !     *                                so that "myrssti" does not conflict
  !     *                                with the cpp directive (compiler
  !     *                                warnings generated).
  !     * jun 18/2013 - k.vonsalzen. new version for gcm17+:
  !     *                            - add {sbio,sshi,obio,oshi,bbio,bshi}.
  !     * apr 20/2010 - k.vonsalzen. previous version gettem for gcm15i+.
  !
  !     * read-in transient emission data.
  !
  use msizes, only: ilat
  implicit none
  integer, intent(in) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: ijpak
  integer, intent(in) :: incd
  integer, intent(in) :: irefyr(num_em_aero)
  integer, intent(in) :: kount  !< Current model timestep \f$[unitless]\f$
  integer, intent(in) :: nlat
  integer, intent(in) :: nlon
  integer, intent(in) :: nuan
  integer, intent(in) :: num_em_aero !< Number of aerosol emission scenarios \f$[]\f$
  integer, intent(in) :: ind_bc      !< Index of BC aerosol emission scenario \f$[]\f$
  integer, intent(in) :: ind_oc      !< Index of OC aerosol emission scenario \f$[]\f$
  integer, intent(in) :: ind_sulf    !< Index of sulfate aerosol emission scenario \f$[]\f$

  !
  !     * 2d emission fields.
  !
  real, intent(inout), dimension(ijpak) :: sairpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sairpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ssfcpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ssfcpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sbiopak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sbiopal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sshipak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sshipal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sstkpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sstkpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sfirpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: sfirpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: oairpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: oairpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: osfcpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: osfcpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: obiopak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: obiopal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: oshipak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: oshipal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ostkpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ostkpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ofirpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: ofirpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bairpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bairpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bsfcpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bsfcpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bbiopak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bbiopal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bshipak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bshipal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bstkpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bstkpal !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bfirpak !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ijpak) :: bfirpal !< Variable description\f$[units]\f$
  !
  !     * work space
  !
  real, intent(in) :: gg( * ) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  real :: day1
  real :: day2
  real :: day3
  integer :: i
  integer :: ijpakx
  integer :: imdh
  integer :: isavdts
  integer :: iyear
  integer :: myrssti
  integer, external :: nc4to8
  real :: w1
  real :: w2
  !--- mynode used to control i/o to stdout
  integer*4 :: mynode
  common /mpinfo/ mynode

  !--- keeptime is required for iyear
  common /keeptim/ iyear,imdh,myrssti,isavdts

  !--- local
  integer :: year_prev(num_em_aero)
  integer :: year_next(num_em_aero)
  integer :: mon_prev
  integer :: mon_next
  integer :: ib2p_all,ib2p_bc,ib2p_oc,ib2p_sulf
  integer :: ib2n_all,ib2n_bc,ib2n_oc,ib2n_sulf
  integer :: curr_year(num_em_aero)

  !--- a list containing mid month days of the year
  integer, parameter, dimension(12) :: mm_doy = (/ 16,46,75,106,136,167,197,228,259,289,320,350 /)

  !--- a list containing mid month days for each month
  integer, parameter, dimension(12) :: mm_dom = (/ 16,14,16,15,16,15,16,16,15,16,15,16 /)
  !----------------------------------------------------------------------
  !
  if (mynode == 0) then
    write(6, * )'    GETTEM: NEW CHEMISTRY FORCING REQUIRED'
  end if

  !.....determine the year for which data is required
  where (irefyr > 0)
    !--- when irefyr > 0 read emissions from the data file for
    !--- a repeated annual cycle of year irefyr
    curr_year = irefyr
  else where
    !--- otherwise read time dependent emissions from the data
    !--- file using iyear to determine the current year
    curr_year = iyear
  end where

  !.....check on iday
  if (iday < 1 .or. iday > 365) then
    if (mynode == 0) then
      write(6, * )'GETTEM: IDAY is out of range. IDAY = ',iday
    end if
    call xit("GETTEM", - 1)
  end if

  !.....determine previous and next year/month, relative to iday
  mon_prev = 0
  if (iday >= 1 .and. iday < mm_doy(1)) then
    year_prev(:) = curr_year(:) - 1
    year_next(:) = curr_year(:)
    mon_prev = 12
    mon_next = 1
  else if (iday >= mm_doy(12) .and. iday <= 365) then
    year_prev(:) = curr_year(:)
    year_next(:) = curr_year(:) + 1
    mon_prev = 12
    mon_next = 1
  else
    do i = 2,12
      if (iday >= mm_doy(i - 1) .and. iday < mm_doy(i)) then
        year_prev(:) = curr_year(:)
        year_next(:) = curr_year(:)
        mon_prev = i - 1
        mon_next = i
        exit
      end if
    end do
  end if

  if (mon_prev == 0) then
    if (mynode == 0) then
      write(6, * )'GETTEM: Problem setting year/month on IDAY = ',iday
    end if
    call xit("GETTEM", - 2)
  end if

  ! xxx
  if (any(year_prev == 1849) .and. mon_prev == 12) then
    where(year_prev == 1849)
        year_prev = 1850
    end where
    mon_prev = 12
  end if
  if (any(year_next == 2001) .and. mon_next == 1) then
    where(year_next == 2001)
       year_next = 2000
    end where
    mon_next = 1
  end if
  ! xxx

  !--- determine time stamps (ibuf2 values) to be used to read
  !--- appropriate data from the file
  ! ib2p=1000000*year_prev+10000*mon_prev+100*mm_dom(mon_prev)
  ! ib2n=1000000*year_next+10000*mon_next+100*mm_dom(mon_next)
  ib2p_bc = year_prev(ind_bc) * 100 + mon_prev
  ib2n_bc = year_next(ind_bc) * 100 + mon_next
  ib2p_oc = year_prev(ind_oc) * 100 + mon_prev
  ib2n_oc = year_next(ind_oc) * 100 + mon_next
  ib2p_sulf = year_prev(ind_sulf) * 100 + mon_prev
  ib2n_sulf = year_next(ind_sulf) * 100 + mon_next

  if (mynode == 0) then
    write(6, * )'GETTEM: year_prev,mon_prev: ', &
                     year_prev,mon_prev
    write(6, * )'GETTEM: year_next,mon_next: ', &
                     year_next,mon_next
    write(6, * )'GETTEM: ib2p_bc,ib2n_bc,ib2p_oc,ib2n_oc,ib2p_sulf,ib2n_sulf: ',&
                     ib2p_bc,ib2n_bc,ib2p_oc,ib2n_oc,ib2p_sulf,ib2n_sulf
  end if
  !
  !     * get new boundary fields for next mid-month day.
  !
  if (incd == 0) then
    !
    !       * model is stationary.
    !
    if (kount == 0) then
      !
      !         * start-up time. read-in fields for average of month.
      !         * initialize target fields as well, although not used.
      !
      call getagbx(sairpak,nc4to8("SAIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(ssfcpak,nc4to8("SSFC"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sbiopak,nc4to8("SBIO"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sshipak,nc4to8("SSHP"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sstkpak,nc4to8("SSTK"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sfirpak,nc4to8("SFIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(bairpak,nc4to8("BAIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bsfcpak,nc4to8("BSFC"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bbiopak,nc4to8("BBIO"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bshipak,nc4to8("BSHP"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bstkpak,nc4to8("BSTK"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bfirpak,nc4to8("BFIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(oairpak,nc4to8("OAIR"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(osfcpak,nc4to8("OSFC"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(obiopak,nc4to8("OBIO"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(oshipak,nc4to8("OSHP"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ostkpak,nc4to8("OSTK"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ofirpak,nc4to8("OFIR"),nuan,nlon,nlat,ib2n_oc,gg)
      !
      call getagbx(sairpal,nc4to8("SAIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(ssfcpal,nc4to8("SSFC"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sbiopal,nc4to8("SBIO"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sshipal,nc4to8("SSHP"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sstkpal,nc4to8("SSTK"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sfirpal,nc4to8("SFIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(bairpal,nc4to8("BAIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bsfcpal,nc4to8("BSFC"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bbiopal,nc4to8("BBIO"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bshipal,nc4to8("BSHP"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bstkpal,nc4to8("BSTK"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bfirpal,nc4to8("BFIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(oairpal,nc4to8("OAIR"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(osfcpal,nc4to8("OSFC"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(obiopal,nc4to8("OBIO"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(oshipal,nc4to8("OSHP"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ostkpal,nc4to8("OSTK"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ofirpal,nc4to8("OFIR"),nuan,nlon,nlat,ib2n_oc,gg)
    end if
    !
  else
    !
    !       * the model is moving.
    !
    if (kount == 0) then
      !
      !         * start-up time. get fields for previous and target mid-month days.
      !
      rewind nuan
      call getagbx(sairpak,nc4to8("SAIR"),nuan,nlon,nlat,ib2p_sulf,gg)
      call getagbx(ssfcpak,nc4to8("SSFC"),nuan,nlon,nlat,ib2p_sulf,gg)
      call getagbx(sbiopak,nc4to8("SBIO"),nuan,nlon,nlat,ib2p_sulf,gg)
      call getagbx(sshipak,nc4to8("SSHP"),nuan,nlon,nlat,ib2p_sulf,gg)
      call getagbx(sstkpak,nc4to8("SSTK"),nuan,nlon,nlat,ib2p_sulf,gg)
      call getagbx(sfirpak,nc4to8("SFIR"),nuan,nlon,nlat,ib2p_sulf,gg)
      call getagbx(bairpak,nc4to8("BAIR"),nuan,nlon,nlat,ib2p_bc,gg)
      call getagbx(bsfcpak,nc4to8("BSFC"),nuan,nlon,nlat,ib2p_bc,gg)
      call getagbx(bbiopak,nc4to8("BBIO"),nuan,nlon,nlat,ib2p_bc,gg)
      call getagbx(bshipak,nc4to8("BSHP"),nuan,nlon,nlat,ib2p_bc,gg)
      call getagbx(bstkpak,nc4to8("BSTK"),nuan,nlon,nlat,ib2p_bc,gg)
      call getagbx(bfirpak,nc4to8("BFIR"),nuan,nlon,nlat,ib2p_bc,gg)
      call getagbx(oairpak,nc4to8("OAIR"),nuan,nlon,nlat,ib2p_oc,gg)
      call getagbx(osfcpak,nc4to8("OSFC"),nuan,nlon,nlat,ib2p_oc,gg)
      call getagbx(obiopak,nc4to8("OBIO"),nuan,nlon,nlat,ib2p_oc,gg)
      call getagbx(oshipak,nc4to8("OSHP"),nuan,nlon,nlat,ib2p_oc,gg)
      call getagbx(ostkpak,nc4to8("OSTK"),nuan,nlon,nlat,ib2p_oc,gg)
      call getagbx(ofirpak,nc4to8("OFIR"),nuan,nlon,nlat,ib2p_oc,gg)
      !
      rewind nuan
      call getagbx(sairpal,nc4to8("SAIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(ssfcpal,nc4to8("SSFC"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sbiopal,nc4to8("SBIO"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sshipal,nc4to8("SSHP"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sstkpal,nc4to8("SSTK"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sfirpal,nc4to8("SFIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(bairpal,nc4to8("BAIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bsfcpal,nc4to8("BSFC"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bbiopal,nc4to8("BBIO"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bshipal,nc4to8("BSHP"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bstkpal,nc4to8("BSTK"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bfirpal,nc4to8("BFIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(oairpal,nc4to8("OAIR"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(osfcpal,nc4to8("OSFC"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(obiopal,nc4to8("OBIO"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(oshipal,nc4to8("OSHP"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ostkpal,nc4to8("OSTK"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ofirpal,nc4to8("OFIR"),nuan,nlon,nlat,ib2n_oc,gg)
      !
      !--- interpolate to current day
      day1 = real(mm_doy(mon_prev))
      day2 = real(iday)
      day3 = real(mm_doy(mon_next))
      if (day2 < day1) day2 = day2 + 365.
      if (day3 < day2) day3 = day3 + 365.
      w1 = (day2 - day1)/(day3 - day1)
      w2 = (day3 - day2)/(day3 - day1)
      if (mynode == 0) then
        write(6, * ) &
         'GETTEM: Interpolating at ',curr_year,' day',iday, &
         ' between ',year_prev,' day',mm_doy(mon_prev), &
         ' and ',year_next,' day',mm_doy(mon_next), &
         ' using weights ',w1,w2
      end if
      !
      ijpakx = (nlon - 1) * ilat
      do i = 1,ijpakx
        sairpak(i) = w1 * sairpal(i) + w2 * sairpak(i)
        ssfcpak(i) = w1 * ssfcpal(i) + w2 * ssfcpak(i)
        sbiopak(i) = w1 * sbiopal(i) + w2 * sbiopak(i)
        sshipak(i) = w1 * sshipal(i) + w2 * sshipak(i)
        sstkpak(i) = w1 * sstkpal(i) + w2 * sstkpak(i)
        sfirpak(i) = w1 * sfirpal(i) + w2 * sfirpak(i)
        oairpak(i) = w1 * oairpal(i) + w2 * oairpak(i)
        osfcpak(i) = w1 * osfcpal(i) + w2 * osfcpak(i)
        obiopak(i) = w1 * obiopal(i) + w2 * obiopak(i)
        oshipak(i) = w1 * oshipal(i) + w2 * oshipak(i)
        ostkpak(i) = w1 * ostkpal(i) + w2 * ostkpak(i)
        ofirpak(i) = w1 * ofirpal(i) + w2 * ofirpak(i)
        bairpak(i) = w1 * bairpal(i) + w2 * bairpak(i)
        bsfcpak(i) = w1 * bsfcpal(i) + w2 * bsfcpak(i)
        bbiopak(i) = w1 * bbiopal(i) + w2 * bbiopak(i)
        bshipak(i) = w1 * bshipal(i) + w2 * bshipak(i)
        bstkpak(i) = w1 * bstkpal(i) + w2 * bstkpak(i)
        bfirpak(i) = w1 * bfirpal(i) + w2 * bfirpak(i)
      end do
    else
      !
      !         * this is in the middle of a run.
      !
      rewind nuan
      !
      call getagbx(sairpal,nc4to8("SAIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(ssfcpal,nc4to8("SSFC"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sbiopal,nc4to8("SBIO"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sshipal,nc4to8("SSHP"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sstkpal,nc4to8("SSTK"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(sfirpal,nc4to8("SFIR"),nuan,nlon,nlat,ib2n_sulf,gg)
      call getagbx(bairpal,nc4to8("BAIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bsfcpal,nc4to8("BSFC"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bbiopal,nc4to8("BBIO"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bshipal,nc4to8("BSHP"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bstkpal,nc4to8("BSTK"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(bfirpal,nc4to8("BFIR"),nuan,nlon,nlat,ib2n_bc,gg)
      call getagbx(oairpal,nc4to8("OAIR"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(osfcpal,nc4to8("OSFC"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(obiopal,nc4to8("OBIO"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(oshipal,nc4to8("OSHP"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ostkpal,nc4to8("OSTK"),nuan,nlon,nlat,ib2n_oc,gg)
      call getagbx(ofirpal,nc4to8("OFIR"),nuan,nlon,nlat,ib2n_oc,gg)
    end if
  end if

  return
end subroutine gettemi2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
