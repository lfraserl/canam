!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine fourf(pressf,four1j,nlev, &
                 ilh,lon,lm,ifax,trigs,nlat,ilat,joff)
  !
  !     * nov 16/03 - m. lazare.   revised to support revised swapf1x.
  !     * oct 29/03 - m. lazare.   new routine based on mhexp_.
  !     *                          legendre moved to new legf routine.
  !     * aug 01/03 - m. lazare. - calls new ffgwf4,swapf1x,swapf2x.
  !     * may 20/03 - m. lazare. - move call to swaps2 to main driver outside
  !     *                          of parallel region (only needs to be
  !     *                          called once since operating on spectral
  !     *                          fields. thus "WRKT" removed.
  !     *                        - "P2" removed.
  !     *                        - dimension->real.
  !     *                        - old commented-out code removed.
  !     * nov 22/98 - m. lazare. previous version mhexp7.
  !     *
  !     * converts fourier arrays of streamfunction (p) and
  !     * velocity potential (c) to grid point slices of vorticity (qf),
  !     * divergence (df), and winds (uf,vf).
  !     * also converts ln(surface pressure) and its derivatives.
  !     * also converts temperature (t) and dew point depression (es).
  !     * tracer variable transformed only if itrac/=0.
  !     *
  !     * input fields
  !     * ------------
  !
  !     * fourier array:  the four1j array is big enough to contain all
  !     * the data for the ffts.
  !
  !     * output fields
  !     * -------------
  !     *
  !     * the transformed real :: grid fields are aligned as in the main
  !     * gcm driver, referenced at the starting address of "PRESSF".
  !     *
  !     * work space
  !     * ----------
  !     *
  !     * the array "WRKS" must be dimensioned at least as large as the sum
  !     * of the sizes of all the input grid data.
  !     *
  !     * additional parameters
  !     * ---------------------
  !     *
  !     *  ilh  = first dimension of complex :: fourier arrays.
  !     *  lon  = number of distinct longitudes around a latitude circle.
  !     *  nlat = number of latitudes processed in single pass. [ajs 29/sep/92]
  !     *  ilat = number of latitudes on a single node.
  !     *  joff = offset into total latitude set for given task.
  !     *  nlev = number of total vertical levels (across all variables)
  !     *         being transformed.
  !     *
  !     * routines called
  !     * ---------------
  !     *
  !     * ffgfw4 - fourier to grid transforms.
  !     * swapf1x- internal data re-ordering routines.
  !     * swapf2x-
  !
  implicit none
  integer, intent(in) :: ilat
  integer, intent(in) :: ilh
  integer, intent(in) :: joff
  integer, intent(in) :: lm
  integer, intent(in) :: lon
  integer, intent(in) :: nlat
  integer, intent(in) :: nlev
  !
  !     * output array (contiguous at this starting address).
  !
  real, intent(out) :: pressf(*) !< Variable description\f$[units]\f$
  !
  !     * input array.
  !
  real, intent(in), dimension(2, nlev, ilat, ilh) :: four1j !< Variable description\f$[units]\f$
  !
  !     * other arrays.
  !
  real, intent(in), dimension(lon) :: trigs !< Variable description\f$[units]\f$
  integer, intent(in) :: ifax(*) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: ilg
  integer :: ir
  integer :: nlevn
  !
  !     * work arrays.
  !
  real, dimension(2,  ilh, nlat, nlev), target :: four1 !< Variable description\f$[units]\f$
  real, dimension(:, :), pointer :: four1_ptr
  !--------------------------------------------------------------------
  ir       = lm - 1
  ilg      = 2*ilh
  !
  !     * first transpose the input data from
  !     * four1j(2,nlev,ilat,ilh) --> four1(2,ilh,nlat,nlev)
  !
  call swapf1x(four1,four1j,nlev,nlat,ilat,joff,ilh)
  !
  !     * now call the fft.
  !
  nlevn=nlev*nlat
  four1_ptr(1:ilg,1:nlevn) => four1
  call ffgfw4(ilg,four1_ptr,ilh,ir,lon,nlevn,ifax,trigs)

  !     * finally, store the data into the format used by the physics routines.
  !     * four1(2*ilh,nlat,nlev) --> g(2*ilh*nlat+1,nlev)
  !     * where "g" points at the starting address of "pressg" in
  !     * the "GR" common block (i.e. pressg-psdlg, inclusive).
  !
  call swapf2x(four1,nlev,nlat,ilh,lon,pressf)
  return
end subroutine fourf
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
