!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine sumsptp (s, sptp,nel)
  !
  !     * apr 02/98 - b.denis  - sumsptp version. to be used in conjonction
  !     *                        of mhanl7/mhalp3 spectral forward
  !     *                        routines.
  !
  !     * this routine finalizes forward legendre transform.
  !     * this is the last step of the transform. it adds spectral tendencies
  !     * contribution (vector sptp) form each latitude set to the global spectral
  !     * spectral tendencies (vector s). all the spectral tendencies variables
  !     * through all levels are lined-up in s and sptp.
  !
  implicit none
  integer :: i
  integer, intent(in) :: nel

  real, intent(inout), dimension(nel) :: s !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nel) :: sptp !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  ! vdir nodep
  do i=1,nel
    s(i)= s(i) + sptp(i)
  end do ! loop 100

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
