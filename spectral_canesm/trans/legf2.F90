!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine legf2(four1, spec1, spec2, ilev,levs, &
                 alp, dalp, delalp, nlat, itrac, ntrac, &
                 m, ntot)
  !
  !     * sep 11/06 - m. lazare. new version for gcm15f:
  !     *                        - calls new vstafx2 to support 32-bit driver.
  !     * oct 29/03 - m. lazare. previous version from ibm conversion:
  !     *                        new routine to do exclusively the forward
  !     *                        legendre transform, based on old code in
  !     *                        mhexp_ and using dgemm.
  !     *
  !     * converts global spectral arrays to fourier coefficients.
  !     * tracer variable transformed only if itrac/=0.
  !     * this is called for a given zonal wavenumber index "M", ie inside
  !     * a loop over m.
  !     *
  !     * input fields
  !     * ------------
  !     *
  !     *   main spectral set:
  !     *
  !     *   spec1( 2, 1+3*ilev+levs+ntrac*ilev, ntot)
  !     *
  !     *   p and c components:
  !     *
  !     *   spec2( 2, 2*ilev, ntot)
  !     *
  !     * this storage convention allows unit-stride references for better
  !     * performance.
  !     *
  !     * other input fields used in the legendre and fast fourier transforms:
  !     *
  !     *    alp(nlat,ntot+1)   = legendre polynomials at given latitude.
  !     *   dalp(nlat,ntot+1)   = cos(lat) times n-s derivative of alp.
  !     * delalp(nlat,ntot+1)   = -n(n+1)*alp
  !
  !     *   nlat   = number of latitudes processed in single pass. [ajs 29/sep/92]
  !     *
  !     * output fields
  !     * -------------
  !     *
  !     * this routine is called within a loop over zonal index "M" and
  !     * thus writes the results for that particular zonal index into
  !     * the calling array.
  !     * the transformed complex :: fourier fields are aligned as follows:
  !     *
  !     * pressf (nlat)
  !     *     qf (ilev,nlat)           - vorticity
  !     *     df (ilev,nlat)           - divergence
  !     *     tf (ilev,nlat)           - temperature
  !     *    esf (levs,nlat)           - dew point depression
  !     *  tracf (ilev,nlat,ntrac)     - tracer ( if itrac /= 0)
  !     *
  !     * these fields are equivalent to a single real :: array dimensioned as:
  !     *
  !     *   four1( 2, (1+3*ilev+levs+ntrac*ilev)*nlat)
  !     *
  !     * as described above, the "FOUR1" array (and similarly for "FOUR2"
  !     * and "FOUR3" work arrays) is actually declared and referenced as
  !     *
  !     *   four1( 2, nlev, nlat)
  !     *
  !     * work fields
  !     * -----------
  !     *
  !     * additional work fields calculated are the derivative fields:
  !     *
  !     *  psdpf (nlat)
  !     *     uf (ilev,nlat) - wind velocity field (u-component)
  !     *     vf (ilev,nlat) - wind velocity field (v-component)
  !     *
  !     * which are equivalent to a single array dimensioned as
  !     *
  !     *   four2( 2, (1 + 2*ilev), nlat)
  !     *
  !     * and the laplacian term:
  !     *
  !     *   four3( 2, 2*ilev,       nlat)
  !     *
  !     * these two work fields are merged into "FOUR1" before exiting this
  !     * routine.
  !
  implicit none
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: itrac  !< Switch to indicate use of tracers in CanAM (0 = no, 1 = yes) \f$[unitless]\f$
  integer, intent(in) :: levs  !< Number of moisture levels in the vertical \f$[unitless]\f$
  integer, intent(in) :: m
  integer, intent(in) :: nlat
  integer, intent(in) :: ntot
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  !
  !     * input spectral arrays.
  !
  complex, intent(in), target :: spec1(1+3*ilev+levs+ntrac*ilev, ntot) !< Variable description\f$[units]\f$
  complex, intent(in), target :: spec2(2*ilev, ntot) !< Variable description\f$[units]\f$
  !
  !     * output fourier array.  the four1 array is big enough to contain all
  !     * the data for the ffts.
  !
  complex, intent(inout), target :: four1 (3+5*ilev+levs+ntrac*ilev,nlat) !< Variable description\f$[units]\f$
  !
  !     * other arrays.
  !
  real*8, intent(in)  :: alp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in)  :: dalp(nlat,ntot+1) !< Variable description\f$[units]\f$
  real*8, intent(in)  :: delalp(nlat,ntot+1) !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * work arrays.
  !
  !     * the four2 and four3 are specialized arrays for specific
  !     * legendre transforms of derivative fields.  the contents of
  !     * four2 and four3 are merged into four1 prior to exiting the routine.
  !
  complex, allocatable, dimension(:, :) :: four0 !<
  complex, allocatable, dimension(:, :) :: four2 !<
  complex, allocatable, dimension(:, :) :: four3 !<
  complex, allocatable, dimension(:, :) :: spec0 !<
  !
 !
  real, dimension(ilev, nlat) :: f1tmp
  real :: fms
  integer :: idf
  integer :: iesf
  integer :: ipressf
  integer :: ipsdlf
  integer :: ipsdpf
  integer :: iqf
  integer :: itf
  integer :: itracf
  integer :: iuf
  integer :: iuf2
  integer :: ivf
  integer :: ivf2
  integer :: nilev
  integer :: nlev
  integer :: nlev1
  integer :: nlev2
  integer :: nlev2p1
  !
  !--------------------------------------------------------------------
  fms = real(m-1)
  !
  !     * calculate number of parallel transforms.
  !
  nlev1                   = 1 + 3*ilev + levs
  nilev                   = ilev*ntrac
  if (itrac /= 0) nlev1 = nlev1 + nilev
  nlev2                   = 2*ilev
  nlev2p1                 = nlev2 + 1
  nlev                    = nlev1 + nlev2p1 + 1
  !
  !
  !     * initialize pointers to variables in "FOUR" arrays.
  !     * these constants are equal to the number of levels that
  !     * must be skipped to access the data for a particular variable.
  !
  !     * pointers into four1.
  !
  ipressf  = 0
  iqf      = ipressf + 1
  idf      = iqf     + ilev
  itf      = idf     + ilev
  iesf     = itf     + ilev
  itracf   = iesf    + levs
  ipsdpf   = itracf
  if (itrac /= 0) ipsdpf   = itracf  + ntrac*ilev
  iuf      = ipsdpf  + 1
  ivf      = iuf     + ilev
  ipsdlf   = ivf     + ilev
  !
  !     * pointers into four2.
  !
  iuf2     = 1
  ivf2     = iuf2 + ilev
  !
  !     * direct legendre transforms first.
  !     * this presumes that grid slices and spectral fields are
  !     * lined up in memory, with ps as the first of the series.
  !     *     nlev1 =       1  * 1 (for ps).
  !     *               + ilev * 3 (for p,c,t)
  !     *               + levs * 1 (for es)
  !     *               + ilev * ntrac (for trac, if itrac/=0)
  !
  allocate(four0(nlev1, nlat))

  call vstafx2(four0, spec1, alp, nlev1, nlat, ntot)

  four1(1:nlev1, 1:nlat) = four0(1:nlev1, 1:nlat)
  deallocate(four0)
  !
  !     * e-w derivatives of ps and p,c (in qf,df).
  !
  four1(ipsdlf+1, :) = cmplx(-fms * aimag(four1(ipressf+1, :)), &
                             fms * real(four1(ipressf+1, :)))
  !
  f1tmp = real(four1(iqf+1:idf, :))
  four1(iqf+1:idf, :) = cmplx(-fms * aimag(four1(iqf+1:idf, :)), &
                                    fms * f1tmp)

  f1tmp = real(four1(idf+1:itf, :))
  four1(idf+1:itf, :) = cmplx(-fms * aimag(four1(idf+1:itf, :)), &
                                    fms * f1tmp)
  !
  !     * n-s derivatives of ps and p,c (in uf,vf).
  !     * this presumes that all arrays are lined up in memory
  !     * with ps as the first of the series.
  !     * number = ilev *2 (for p,c)
  !     *         +  1  *1 (for ps).
  !
  allocate(four2(nlev2p1, nlat))
  allocate(spec0(nlev2p1, ntot))
  spec0(1:nlev2p1, 1:ntot) = spec1(1:nlev2p1, 1:ntot)
  !
  call vstafx2(four2, spec0 , dalp, nlev2p1, nlat, ntot)
  deallocate(spec0)
  !
  !     * u,v in uf,vf.
  !     * the uf,vf fields are gathered from their four2 holding array and
  !     * passed into the four1 holding array.
  !
  four1(ipsdpf+1, :)     = four2(1, :)
  four1(iuf+1:ivf, :)    = four1(idf+1:itf, :) - four2(iuf2+1:ivf2, :)
  four1(ivf+1:ipsdlf, :) = four1(iqf+1:idf, :) + four2(ivf2+1:nlev2p1, :)
  !
  deallocate(four2)
  allocate(four3(nlev2, nlat))
  !
  !     * laplacian terms.
  !
  call vstafx2(four3, spec2, delalp, nlev2, nlat, ntot)
  !
  !     * transfer results into primary array of fourier data.
  !
  four1(iqf+1:idf, :) = four3(1:ilev, :)
  four1(idf+1:itf, :) = four3(ilev+1:nlev2, :)

  deallocate(four3)

  return
end subroutine legf2
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}

