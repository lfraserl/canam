!> \file convert_dayofyear2monthday
!> \brief Simple utility routine to convert day of year to month, day
!!
!! @author Clint Seinen

subroutine convert_dayofyear2monthday(day_of_year, month, day)
    use iso_fortran_env, only : STDERR => error_unit, STDOUT => output_unit
    implicit none
    !========
    ! Inputs
    !========
    integer, intent(in) :: day_of_year  !< Day of year (1-365) \f$[days]\f$

    !=========
    ! Outputs
    !=========
    integer, intent(out) :: month       !< Integer months (1-12) \f$[months]\f$
    integer, intent(out) :: day         !< Day of month \f$[days]\f$

    !=================
    ! Local variables
    !=================
    integer :: i,j
    integer, dimension(12) :: month_end_days = [31,59,90,120,151,181,212,243,273,304,334,365] ! last Julian day of each month

    if ( day_of_year > 365 ) then
        write(STDERR,*) "convert_dayofyear2monthday: input 'day_of_year' can't be greater than 365"
        call xit("convert_dayofyear2monthday", -1)
    end if

    do i = 1,12
       if ( day_of_year <= month_end_days(i) ) then
            month = i
            exit
       end if
    end do
    if ( month == 1 ) then
        ! avoid referencing out side of month_end_days bounds
        day = day_of_year
    else
        day = day_of_year - month_end_days(month-1)
    end if

   return
end subroutine convert_dayofyear2monthday
