!> \file agcm_phys_init.F90
!> \brief Define all the time-invariant constants for the physics/chemistry.
!!
!! @author Routine author name(s)
!
 subroutine agcm_phys_init(mynode, shb, sg, ptoit, icosp)
  use psizes_19,         only : ntlk, nbs, nbl, ican, ilev, ip0j, nxloc, ioztyp, levsa, &
                                ioxtyp, levox
  use agcm_types_mod,    only : phys_options
  use agcm_types_mod,    only : phys_diag
  use comopen,           only : nurad1, nucoa, nuact, nurad1, nughg, nujtab1, nujtab2
  use io_labels,         only : lh
  use intervals,         only : icsw
  use compak,            only : kthpal
  use em_scenarios_defs, only : interp_ghgts
  use rad_force2,        only : set_radforce_trop_idx
  use radcons_mod,       only : set_mmr, co2_ppm
  use xcw_data,          only : calc_xcw_data
  use chem_params_mod,   only : jpnjt1, jpnjt2, jpnalb, jpntpl, jpntsza, jpntoz, &
                                jpidx1, jpidx2, qralb, qo3srat, qo3c, qpref, &
                                qzenang, rjvtab1, rjvtab2

  implicit none

  integer,                  intent(in)  :: mynode
  integer,                  intent(out) :: icosp
  real,                     intent(in)  :: ptoit
  real,    dimension(ilev), intent(in)  :: sg
  real,    dimension(ilev), intent(in)  :: shb

  integer :: ksalb

  integer :: ii
  integer :: jj
  integer :: kk
  integer :: ll
  integer :: mm

  ! Additional variables related to gas-phase chemistry.
  integer :: itjtab
  integer, parameter :: dp = kind(1.0d0)
  integer (kind=dp) :: jp1, jp2, jp3, jp4, jp5   !< reference J-table dimensions
  !
  ! * Consistency checks for lakes.
  !
  ! 1) There are no lake tiles but one of lake cpp definitions is turned on !
  !
  if (phys_options%cslm) then
     if (ntlk == 0) then
        call xit('GCM_PHYS_INIT', -1)
     end if
  end if
  !
  ! 2) There are lake tiles but the lake cpp definition
  !    is not activated !
  !
  if (.not. phys_options%cslm) then
     if (ntlk /= 0) then
        call xit('GCM_PHYS_INIT', -2)
     end if
  end if
  !
  ! * Consistency checks for oxidants levels.
  !  * if oxidants are cmip6 ones on model thermodynamic levels, set levox to ilev
  if (ioxtyp == 0) levox = ilev
  !
  if (phys_diag%radforce) then

     call set_radforce_trop_idx(mynode, lh)
     !--- initialize the tropopause height index array kthpal
     !--- this should be set to the index of the model level that is
     !--- closest to the tropopause. setting kthpal(:)==0 will result
     !--- in instantaneous radiative forcing while any kthpal(:)/=0
     !--- implies adjusted radiative forcing.
     kthpal(:) = phys_diag%radforce_trop_idx

  end if
  !
  ! * Abort if this is not the correct number of bands for this model version.
  !
  if (nbs /= 4 .or. nbl /= 9) call xit('GCM_PHYS_INIT', -3)
  !
  ! * Abort if nbs>ican (lc is used for level index for i/o on salb in model).
  !
  if (nbs > ican)  call xit('GCM_PHYS_INIT', -4)
  !     * define level which serves as interface between full l/w radiation
  !     * and more simplified code above ("LEV1").
  !
  call toplw(shb, ptoit, ilev)
  !
  !     * initialize aerosol properties and set up pla calculations.
  !
  if (phys_options%pla) then
      call init_pla_pam(nucoa, nuact, nurad1, './', mynode)
  end if
  !
  if (phys_options%gas_chem) then

    ! * Read in the first photolysis rate look-up table.
    write(6,*) "agcm_phys_init: reading albedo-independent J-table from unit", nujtab1
    read(nujtab1) jp1, jp2, jp3, jp4
    itjtab = abs(jp1-jpnjt1) + abs(jp2-jpntpl) + abs(jp3-jpntsza) &
           + abs(jp4-jpntoz)
    write(6,*) "agcm_phys_init: incoming jtable dimensions:"
    write(6,*) jp1, jp2, jp3, jp4
    write(6,*) "agcm_phys_init: compare to jpnjt1, jpntpl, jpntsza, jpntoz:"
    write(6,*) jpnjt1, jpntpl, jpntsza, jpntoz
    if (itjtab /= 0) then
      stop "agcm_phys_init gas-chem: A) mis-match between J-table dimensions coming from the file and those coming from the model. Stopping."
    endif

    ! Read in the ancillary information from the table
    read(nujtab1) (jpidx1(ii),ii=1,jpnjt1)
    read(nujtab1) (qzenang(ii),ii=1,jpntsza)
    read(nujtab1) (qo3srat(ii),ii=1,jpntoz)
    read(nujtab1) (qo3c(ii),ii=1,jpntpl)
    read(nujtab1) (qpref(ii),ii=1,jpntpl)

    if (mynode == 0) then
      write(6,*) "agcm_phys_init gas-chem: A) Read from look-up table 1:"
      write(6,*) "                   jpidx1 indices"
      write(6,'(19i8)') (jpidx1(ii),ii=1,jpnjt1)
      write(6,*) "                   SZA (qzenang)"
      write(6,'(33f8.2)') (qzenang(ii),ii=1,jpntsza)
      write(6,*) "                   Fractions of standard O3 column (qo3srat)"
      write(6,'(8f8.2)') (qo3srat(ii),ii=1,jpntoz)
      write(6,*) "      qo3c, index 1, jpntl", qo3c(1), qo3c(jpntpl)
      write(6,*) "     qpref, index 1, jpntl", qpref(1), qpref(jpntpl)
    endif

    ! Check that vertical levels run from the ground (k=1) to the top (k=jpntpl)
    if (qo3c(1) < qo3c(jpntpl) .or. qpref(1) < qpref(jpntpl)) then
      write(6,*) "agcm_phys_init gas-chem: bad ordering of vertical levels in J-tables"
      write(6,*) "pressure:",  qpref(1),qpref(jpntpl)
      write(6,*) "ozone column", qo3c(1),qo3c(jpntpl)
      stop "agcm_phys_init: stopping"
    endif

    do ll = 1, jpntoz
      do kk = 1, jpntsza
        read(nujtab1) ( (rjvtab1(ii,jj,kk,ll),ii=1,jpnjt1), jj=1,jpntpl )
      enddo
    enddo

    ! Read in the rates dependent on surface albedo (second table)
    write(6,*) "agcm_phys_init: reading albedo-dependent J-table from unit", nujtab2
    read(nujtab2) jp1, jp2, jp3, jp4, jp5
    itjtab = abs(jp1-jpnjt2) + abs(jp2-jpntpl)        &
           + abs(jp3-jpntsza) + abs(jp4-jpntoz) + abs(jp5-jpnalb)
    write(6,*) "agcm_phys_init gas-chem: incoming jtable dimensions:"
    write(6,*) jp1, jp2, jp3, jp4, jp5
    write(6,*) "agcm_phys_init: compare to jpntjv, jpntpl, jpntsza, jpntoz, jpnalb:"
    write(6,*) jpnjt2, jpntpl, jpntsza, jpntoz, jpnalb

    if (itjtab /= 0) then
      stop "agcm_phys_init gas-chem: B) mis-match between J-table dimensions coming from the file and those coming from the model. Stopping."
    endif

    ! Read in the ancillary information from the table
    read(nujtab2) (jpidx2(ii),ii=1,jpnjt2)
    read(nujtab2) (qzenang(ii),ii=1,jpntsza)
    read(nujtab2) (qo3srat(ii),ii=1,jpntoz)
    read(nujtab2) (qralb(ii),ii=1,jpnalb)
    read(nujtab2) (qo3c(ii),ii=1,jpntpl)
    read(nujtab2) (qpref(ii),ii=1,jpntpl)

    if (mynode == 0) then
      write(6,*) "agcm_phys_init gas-chem: B) Read in from look-up table 2:"
      write(6,*) "                   jpidx2 indices"
      write(6,'(22i8)') (jpidx2(ii),ii=1,jpnjt2)
      write(6,*) "                   SZA (qzenang)"
      write(6,'(33f8.2)') (qzenang(ii),ii=1,jpntsza)
      write(6,*) "                   Fractions of standard O3 column (qo3srat)"
      write(6,'(8f8.2)') (qo3srat(ii),ii=1,jpntoz)
      write(6,*) "                   Albedo (qralb)"
      write(6,'(4f8.2)') (qralb(ii),ii=1,jpnalb)
      write(6,*) "      qo3c at k=1, jpntl", qo3c(1), qo3c(jpntpl)
      write(6,*) "     qpref at k=1, jpntl", qpref(1), qpref(jpntpl)
    endif

    ! Check that vertical levels run from the ground (k=1) to the top (k=jpntpl)
    if (qo3c(1) < qo3c(jpntpl) .or. qpref(1) < qpref(jpntpl)) then
      write(6,*) "agcm_phys_init gas-chem: bad ordering of vertical levels in J-tables"
      write(6,*) "pressure:",  qpref(1),qpref(jpntpl)
      write(6,*) "ozone column", qo3c(1),qo3c(jpntpl)
      stop "agcm_phys_init: stopping"
    endif

    do mm = 1, jpnalb
      do ll = 1, jpntoz
        do kk = 1, jpntsza
          read(nujtab2) ( (rjvtab2(ii,jj,kk,ll,mm),ii=1,jpnjt2), jj=1,jpntpl )
        enddo
      enddo
    enddo
    !
  endif  ! end conditional on phys_options%gas_chem
  !
  !     * call subroutines that fills look-up table for new gwd.
  !
  call gwlooku3
  if (phys_options%use_mcica .or. phys_diag%switch_cosp_diag) then
! jnsc add call to read xcw
      call calc_xcw_data()
  end if

  icosp=0
  if (phys_diag%switch_cosp_diag) then
     icosp=icsw
     call init_cosp(ip0j, ilev, nxloc)
  end if
  if (mod(icosp, icsw) /= 0)  call xit ('GCM_PHYS_INIT', -6)
!
! * Initialize surface albedo parameters
! * ksalb = 0...surface albedo is calculated in class_
! * ksalb = 1...surface albedo is calculated from a lookup table
! * ksalb = 2...surface albedo is calculated from a formula
!
  if (phys_options%salbtable) then
     ksalb=1
  else if (phys_options%salbform1) then
     ksalb=2
  else
     ksalb=0
  end if

  call init_surface_albedo2 (ksalb)
  call init_snow_rt_lut()
  !
  if (interp_ghgts) then
    call read_ghg_timeseries(nughg, mynode)
    call init_ghg_scenario2(mynode)
  else
    !-- Set GHG trace gas mass mixing ratios based on default ppmv values
    call set_mmr()
  end if
  !
  !     * error inconsistency checks.
  !
  if (phys_options%transient_ozone_concentrations) then
     !   * only randel and cmip6 ozone have transient data !
     if (ioztyp /= 4 .and. ioztyp /= 6) call xit("GCM_PHYS_INIT", -7)
  end if

  if (phys_options%mam) then
  ! Character string "scheme_nogwd" gives the choice of non-orographic
  ! gravity-wave drag scheme. It is set on canam-settings. The default scheme
  ! is SCINOCCA. Other options are HINES and MEDKLA (for Medvedev-Klaassen).
      write(6,*) "agcm_phys_init: choice of non-oro GWD scheme is ",trim(phys_options%scheme_nogwd)
      call mamrad_init3(sg, ptoit, co2_ppm)
  end if

  if (phys_options%with_solvar) then
      call read_solar_variability()
  endif

  return
 end subroutine agcm_phys_init
 !> \file
