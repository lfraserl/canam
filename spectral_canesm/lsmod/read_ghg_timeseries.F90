!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine read_ghg_timeseries(nughg, mynode)

  ! purpose:
  !
  !-----------------------------------------------------------
  ! read in the greenhouse gas time-series file selected.
  !-----------------------------------------------------------

  ! method:
  !
  !-----------------------------------------------------------
  ! read in all data from the specified file.  the expectation
  ! is that the following data will be in the file in the
  ! following order with the following variable names.
  !
  ! it is also assumed that the time-series are "padded" with an
  ! extra points at the beginning and end so the values can be
  ! interpolated.
  !
  ! yrfr - years associated with each time-series as year fractions
  !        i.e., 1990.5
  !  co2 - carbon dioxide
  !  ch4 - methane
  !  n2o - nitrous oxide
  ! ce11 - effective cfc-11
  ! cf11 - cfc-11
  ! cf12 - cfc-12
  ! f113 - cfc-113
  ! f114 - cfc-114
  !-----------------------------------------------------------

  use ghgts_defs

  implicit none

  ! input
  integer, intent(in) :: nughg !< GHG input file unit\f$[unit-less]\f$
  integer, intent(in) :: mynode

  ! output

  ! local

  ! array

  real, dimension(max_ghg_scn) :: wrk !<
  integer :: ibuf(8) !<
  integer :: idat(2*max_ghg_scn) !<
  common /icom/ ibuf,idat

  ! scalar

  integer :: nc4to8 !<

  logical :: ok !<

  !--- read ghg values from a file
  if (mynode==0) then
     write(6,*)' '
     write(6,*) &
       'READ_GHG_TIMESERIES: Initializing GHG time series from file.'
     write(6,*)' '
  end if

  ! read in the time

  ghgts_time(:) = 0.0
  call getfld2(nughg,ghgts_time,nc4to8("TIME"),-1,nc4to8("YRFR"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not. ok) call xit('GCM:GHGTS',-2)

  if (ibuf(5) > max_ghg_scn) then
    write(6,*) &
        'INCREASE THE VALUE OF MAX_GHG_SCN IN MODULE GHGTS2_DEFS', &
        ' TO AT LEAST ',ibuf(5)
    call xit('GCM:GHGTS',-3)
  end if

  ! set the number of timesteps to ibuf(5) for all gases
  ! note that "f11" will cover cfc-11 and effective cfc-11
  n_co2_scn  = ibuf(5)
  n_ch4_scn  = ibuf(5)
  n_n2o_scn  = ibuf(5)
  n_f11_scn  = ibuf(5)
  n_f12_scn  = ibuf(5)
  n_f113_scn = ibuf(5)
  n_f114_scn = ibuf(5)

  ! now read in the gases from the file

  ! co2
  ghgts_co2(:) = 0.0
  call getfld2(nughg,ghgts_co2,nc4to8("TIME"),-1,nc4to8(" CO2"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  ! ch4
  ghgts_ch4(:) = 0.0
  call getfld2(nughg,ghgts_ch4,nc4to8("TIME"),-1,nc4to8(" CH4"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-6)

  ! n2o
  ghgts_n2o(:) = 0.0
  call getfld2(nughg,ghgts_n2o,nc4to8("TIME"),-1,nc4to8(" N2O"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  ! ce11
  ghgts_e11(:) = 0.0
  call getfld2(nughg,ghgts_e11,nc4to8("TIME"),-1,nc4to8("CE11"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  ! cf11
  ghgts_f11(:) = 0.0
  call getfld2(nughg,ghgts_f11,nc4to8("TIME"),-1,nc4to8("CF11"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  ! cf12
  ghgts_f12(:) = 0.0
  call getfld2(nughg,ghgts_f12,nc4to8("TIME"),-1,nc4to8("CF12"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  ! f113
  ghgts_f113(:) = 0.0
  call getfld2(nughg,ghgts_f113,nc4to8("TIME"),-1,nc4to8("F113"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  ! f114
  ghgts_f114(:) = 0.0
  call getfld2(nughg,ghgts_f114,nc4to8("TIME"),-1,nc4to8("F114"), &
                   -1,ibuf,2*max_ghg_scn,ok)

  if (.not.ok) call xit('GCM:GHGTS',-5)

  return

end subroutine read_ghg_timeseries
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
