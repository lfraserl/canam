!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

subroutine init_relax_parm(phys_parm_iu, mynode)
  !***********************************************************************
  ! read relaxation parameter values from a file named phys_parm
  !
  ! input
  !   integer*4 :: mynode  ...mpi node (used to restrict i/o to node 0)
  !
  ! slava kharin ...20 sep 2018
  !***********************************************************************

  use relax_parm_defs

  implicit none

  !--- input
  integer*4, intent(in) :: mynode !< Variable description\f$[units]\f$
  integer,   intent(in) :: phys_parm_iu !<PHYS_PARM file unit number\f$[unit-less]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !-----------------------------------------------------------------------
  !--- set the relaxation parameter defaults
  relax_spc = 1             ! spectral nudging
  relax_spc_bc = 0          ! spectral climatological :: nudging

  relax_tg1 = 0             ! ground temperature level 1 nudging
  relax_tg1_bc = 0          ! ground temperature level 1 climatological :: nudging

  relax_wg1 = 0             ! ground moisture level 1 nudging
  relax_wg1_bc = 0          ! ground moisture level 1 climatological :: nudging

  relax_ocn = 0             ! upper ocean (sst/sic/sicn) nudging
  relax_ocn_bc = 0          ! upper ocean climatological :: nudging

  iperiod_spc=6             ! spectral nudging cycle in hours.
  iperiod_lnd=24            ! land nudging cycle in hours.
  iperiod_lnd_clm=6         ! land climatological :: nudging cycle in hours.
  iperiod_ocn=24            ! ocean nudging cycle in hours.
  iperiod_ocn_clm=24        ! ocean climatological :: nudging cycle in hours.

  relax_p=1.                ! vort vorticity relaxation switch.
  relax_c=1.                ! div  divergence relaxation switch.
  relax_t=1.                ! temp temperature relaxation switch.
  relax_es=1.               ! es   humidity relaxation switch.
  relax_ps=0.               ! ps   ln(surface pressure) relaxation switch.

  tauh_p=24.                ! vort relaxation time scale in hours
  tauh_c=24.                ! div  relaxation time scale in hours
  tauh_t=24.                ! temp relaxation time scale in hours
  tauh_es=24.               ! hum  relaxation time scale in hours
  tauh_ps=24.               ! ps   relaxation time scale in hours

  tauh_tg1=72.              ! tg1 relaxation time scale in hours
  tauh_wg1=72.              ! wg1 relaxation time scale in hours

  ntrunc_p=21               ! truncation of vort tendencies
  ntrunc_c=21               ! truncation of div  tendencies
  ntrunc_t=21               ! truncation of temp tendencies
  ntrunc_es=21              ! truncation of hum  tendencies
  ntrunc_ps=21              ! truncation of ps   tendencies

  itrunc_tnd = 1            ! itrunc_tnd - (0/1) truncate tendency with truncf
  itrunc_anl = 0            ! trunc_anl - (0/1) truncate analysis data towards which model is nudged

  ivfp=0                    ! =0, no vertical profile to nudge everywhere with equal strength
  ! =1, use vertical profile at the top to reduce nudging near the top
  ! =2 vertical profile at the bottom to reduce nudging near the bottom
  ! =3 vertical profile both at the top and bottom to reduce nudging at both ends

  sgtop=0.001               ! top full levels (0.001~1hpa)
  shtop=0.001               ! top half levels (0.001~1hpa)

  sgfullt_p=0.010           ! where to start reducing forcing (0.010~10hpa)
  sgfullt_c=0.010           ! where to start reducing forcing (0.010~10hpa)
  shfullt_t=0.010           ! where to start reducing forcing (0.010~10hpa)
  shfullt_e=0.010           ! where to start reducing forcing (0.010~10hpa)

  sgdampt_p=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
  sgdampt_c=0.010           ! reduction factor from sgfull to sgtop (0.01=1%)
  shdampt_t=0.010           ! reduction factor from shfull to shtop (0.01=1%)
  shdampt_e=0.010           ! reduction factor from shfull to shtop (0.01=1%)

  sgbot=0.995               ! bottom full levels (0.995~995hpa)
  shbot=0.995               ! bottom half levels (0.995~995hpa)
  sgfullb_p=0.850           ! where to start reducing forcing (0.850~850hpa)
  sgfullb_c=0.850           ! where to start reducing forcing (0.850~850hpa)
  shfullb_t=0.850           ! where to start reducing forcing (0.850~850hpa)
  shfullb_e=0.850           ! where to start reducing forcing (0.850~850hpa)

  sgdampb_p=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
  sgdampb_c=0.010           ! reduction factor from sgfulb to sgbot (0.01=1%)
  shdampb_t=0.010           ! reduction factor from shfulb to shbot (0.01=1%)
  shdampb_e=0.010           ! reduction factor from shfulb to shbot (0.01=1%)

  tauh_sst=72.              ! in hours - nudging time scale for ssts
  tauh_edge=72.             ! in hours - nudging time scale for sea-ice mass (sic) near edge
  ! may use shorter time scale since it is better observed
  tauh_sic=168.             ! in hours - nudging time scale for sea-ice mass (sic)

  rlx_sst=1.                ! convert delta sst to heat tendency
  rlx_sic=1.                ! convert delta sic to heat tendency

  rlx_tbeg=1.               ! turn on/off (1/0) tbeg heat tendency
  rlx_tbei=1.               ! turn on/off (1/0) tbei heat tendency
  rlx_tbes=1.               ! turn on/off (1/0) tbes heat tendency

  rlc_tbeg=0.               ! turn on/off (1/0) tbeg heat clim. tendency
  rlc_tbei=0.               ! turn on/off (1/0) tbei heat clim. tendency
  rlc_tbes=0.               ! turn on/off (1/0) tbes heat clim. tendency

  rlx_sbeg=1.               ! turn on/off (1/0) sbeg heat tendency
  rlx_sbei=1.               ! turn on/off (1/0) sbei heat tendency
  rlx_sbes=1.               ! turn on/off (1/0) sbes heat tendency

  rlc_sbeg=0.               ! turn on/off (1/0) sbeg heat clim. tendency
  rlc_sbei=0.               ! turn on/off (1/0) sbei heat clim. tendency
  rlc_sbes=0.               ! turn on/off (1/0) sbes heat clim. tendency

  !--- read relaxation parameters from a namelist

  rewind(phys_parm_iu)
  read(phys_parm_iu,nml=relax_parm)
  !        close(iu)
  if (mynode==0) then
    write(6,*)' '
    write(6,relax_parm)
    write(6,*)' '
    call flush(6)
  end if

end subroutine init_relax_parm
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
