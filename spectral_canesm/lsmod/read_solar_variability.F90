!>\file
!>\brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!

      subroutine read_solar_variability()

! Purpose:
!
!-----------------------------------------------------------
! Read in the greenhouse gas time-series file selected.
!-----------------------------------------------------------

! Method:
!
!-----------------------------------------------------------
! Read in all data from the specified file.  The expectation
! is that the following data will be in the file in the
! following order with the following variable names.
!
! It is also assumed that the time-series are "padded" with an
! extra points at the beginning and end so the values can be
! interpolated.
!
! YRFR - Years associated with each time-series as year fractions
!        i.e., 1990.5
! B101 - Solar irradiance anomaly for band 1, quadrature point 1
! B102 - Solar irradiance anomaly for band 1, quadrature point 2
! B103 - Solar irradiance anomaly for band 1, quadrature point 3
! B104 - Solar irradiance anomaly for band 1, quadrature point 4
! B105 - Solar irradiance anomaly for band 1, quadrature point 5
! B106 - Solar irradiance anomaly for band 1, quadrature point 6
! B107 - Solar irradiance anomaly for band 1, quadrature point 7
! B108 - Solar irradiance anomaly for band 1, quadrature point 8
! B109 - Solar irradiance anomaly for band 1, quadrature point 9
! BND2 - Solar irradiance anomaly for band 2
! BND3 - Solar irradiance anomaly for band 3
! BND4 - Solar irradiance anomaly for band 4

use solvar, only : sv_nts_max, sv_nts, solvts, sv_bndnames, &
                   sv_subands, solvtimes

implicit none

! input

! output

! local

! array

integer :: ibuf(8), idat(1)
common /icom/ ibuf,idat

real, dimension(sv_nts_max) :: sv_wrk

! scalar

integer :: &
  nusv, &
  newunit, &
  nc4to8, &
  ibnd

logical :: &
  found, &
  ok

character*4 :: sv_ib3

! open the solar variability file.
inquire(file='SOLVAR',exist=found)
if (found) then
  nusv=newunit(70)
  open(nusv,file='SOLVAR',form='unformatted')
else
  write(6,*)'file SOLVAR is missing.'
  call xit('gcm:solv',-1)
endif

! loop over each of the bands represented in the solar variability dataset.

do ibnd = 1, sv_subands
  ! get the current solar variability band name
  sv_ib3 = sv_bndnames(ibnd)

  ! read the data from file.
  sv_wrk(:) = 0.0
  call getfld2(nusv,sv_wrk,nc4to8("TIME"),-1, &
                    nc4to8(sv_ib3(1:4)),-1,ibuf,2*sv_nts_max,ok)
    if (.not.ok) call xit('gcm:solv',-2)
    if (ibuf(5).gt.sv_nts_max) then
    write(6,*) &
    'increase the value of sv_nts_max in module solvar', &
    ' to at least ',ibuf(5)
      call xit('gcm:solv',-3)
    endif

    ! put the data for this particular band into an array holding the data
    ! for all of the solar variability.
    solvts(ibnd,1:ibuf(5)) = sv_wrk(1:ibuf(5))

enddo

rewind(nusv)
sv_wrk(:) = 0.0
call getfld2(nusv,sv_wrk,nc4to8("TIME"),-1,nc4to8("YRFR"), &
            -1,ibuf,2*sv_nts_max,ok)
if (.not.ok) call xit('gcm:solv',-4)

! the times from the file are in fraction of year.
solvtimes(1:ibuf(5)) = sv_wrk(1:ibuf(5))

! the number of points in the time-series.
sv_nts = ibuf(5)

! close up the solar variability file.
close(nusv)

return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
