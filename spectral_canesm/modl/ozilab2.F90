!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine ozilab2(lo,levoz,ioztyp)

  !     * jan 09/2017 - d.plummer modified to include labels for
  !                                cmip6 ozone (ioztyp=6)
  !     * jun 28/2009 - m.lazare. new version for gcm15i:
  !     *                         - for randel ozone data (ioztyp=4),
  !     *                           use actual pressure levels rather
  !     *                           than {1,levoz}.
  !     *                           **note**! extra field ioztyp passed in !
  !     * apr 30/2003 - m.lazare. previous version ozilab up to gcm15h.
  !
  !     * defines level index values for input ozone fields.
  !
  implicit none
  !
  integer, intent(in) :: levoz   !< Number of vertical layers for ozone input data (other than chemistry) \f$[unitless]\f$
  integer, intent(in) :: ioztyp   !< Switch to control the type of ozone input data \f$[unitless]\f$
  integer, intent(out) , dimension(levoz) :: lo !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !     * pressure-level data for ioztyp=4 (randel).
  !
  integer, parameter, dimension(24) :: p4 = (/ &
                                      -100, -150, -200, -300, -500, &
                                      -700,  10,   15,   20,   30,  &
                                       50,   70,   80,   100,  150, &
                                       200,  250,  300,  400,  500, &
                                       600,  700,  850,  1000 /) !<
  !
  !     * pressure-level data for ioztyp=6 (cmip6)
  !
  integer, parameter, dimension(49) :: p6 = (/ &
                                      -4500, -3100, -3200, -3400, -3700, &
                                      -2100, -2200, -2400, -2700, -1100, &
                                      -1200, -1400, -1700, -100,  -150,  &
                                      -200,  -300,  -400,  -500,  -700,  &
                                       10,    15,    20,    25,    30,   &
                                       35,    40,    50,    60,    70,   &
                                       80,    90,   100,   115,   130,   &
                                       150,   170,   200,   250,   300,  &
                                       350,   400,   500,   600,   700,  &
                                       800,   850,   925,  1000 /) !<
  !
  integer :: l !<
  !
  !-----------------------------------------------------------------------
  if (ioztyp==4) then
    do l=1,levoz
      lo(l)=p4(l)
    end do
  else if (ioztyp==6) then
    do l=1,levoz
      lo(l)=p6(l)
    end do
  else
    do l=1,levoz
      lo(l)=l
    end do
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
