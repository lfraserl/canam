!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine levcal3(sgbj,shtj, sgj,shj, dsgj,dshj, &
                         ag,bg,ah,bh, acg,bcg,ach,bch, psj, &
                         nk,nk1,ilg,il1,il2,jlat,ptoit)

  !     * jun 10/03 - m.lazare. use full shtj instead of splitting call
  !     *                       to isolate moon layer, so that "SHTJ"
  !     *                       can be declared as 1-d in com13pa.
  !     * apr 29/03 - m.lazare. 1,lon -> il1,il2.
  !     * dec 15/92 - m.lazare. - previous version levcal2.
  !
  !     * calcul la position des bases et centres de couches pour
  !     * la physique de la version hybride a elements finis constants
  !     * du gcm, a variables intercalees ou non.
  !     *
  !     * parametres de sortie:
  !     *
  !     * sgj (i,k)    : milieu des couches du vent.
  !     * shj (i,k)    : milieu des couches de temperature.
  !     * sgbj(i,k)    : bases des couches du vent.
  !     * shtj(i,k)    : bases des couches de temperature.
  !     * dsgj(i,k)    : epaisseurs sigma des couches du vent.
  !     * dshj(i,k)    : epaisseurs sigma des couches de temperature.
  !     *
  !     * PARAMETRES D'ENTREE:
  !     *
  !     * ag ,bg ,ah ,bh (k): info. sur position des bases de couches.
  !     * acg,bcg,ach,bch(k): info. sur position des milieux de couches.
  !     * nk         : nombre de couches.
  !     * nk1        : simplement  nk + 1.
  !     * ilg        : dimension en longitude.
  !     * lon        : nombre de longitudes distinctes.
  !     * alfmod     : poids au sommet.
  !     * jlat       : numero de cette latitude (utilise pour debug)
  !     * ptoit      : pressure where lid of model applied.

  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: jlat
  integer, intent(in) :: nk
  integer, intent(in) :: nk1
  real, intent(in) :: ptoit

  real, intent(in), dimension(ilg) :: psj !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,nk) :: sgbj   !< Eta-level for base of momentum layers \f$[unitless]\f$
  real, intent(inout), dimension(ilg,nk1) :: shtj   !< Eta-level for top of thermodynamic layer, plus eta-level for surface (base of lowest layer) \f$[unitless]\f$
  real, intent(inout), dimension(ilg,nk) :: sgj   !< Eta-level for mid-point of the momentum layer \f$[unitless]\f$
  real, intent(inout), dimension(ilg,nk) :: shj   !< Eta-level for mid-point of thermodynamic layer \f$[unitless]\f$
  real, intent(inout), dimension(ilg,nk) :: dsgj   !< Thickness of momentum layers in eta coordinates \f$[unitless]\f$
  real, intent(inout), dimension(ilg,nk) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(nk) :: ag !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: bg !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: ah !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: bh !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: acg !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: bcg !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: ach !< Variable description\f$[units]\f$
  real, intent(in), dimension(nk) :: bch !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: i
  integer :: k
  real :: stoit
  !--------------------------------------------------------------------
  !     * definition des milieux et bases de couches.
  !
  ! vdir novector
  do k=1,nk
    do i=il1,il2
      sgj (i  ,k)   = acg(k)/psj(i)+bcg(k)
      shj (i  ,k)   = ach(k)/psj(i)+bch(k)
      sgbj(i  ,k)   = ag (k)/psj(i)+bg (k)
      shtj(i  ,k+1) = ah (k)/psj(i)+bh (k)
    end do
  end do ! loop 100

  !     * calcul des epaisseurs des couches.
  !     * stoit is the lid of the model.

  do i=il1,il2
    stoit     = ptoit/psj(i)
    dsgj(i,1) = sgbj(i,1) - stoit
    dshj(i,1) = shtj(i,2) - stoit
    shtj(i,1) = stoit
  end do ! loop 250

  do k=2,nk
    do i=il1,il2
      dsgj(i,k) = sgbj(i,k)  - sgbj(i,k-1)
      dshj(i,k) = shtj(i,k+1)- shtj(i,k)
    end do
  end do ! loop 300

  return
  !--------------------------------------------------------------
end subroutine levcal3
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
