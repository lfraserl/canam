!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine emonsav(lsemon,iday,nsecs,delt)
  !
  !     * dec 12/2018 - s.kharin. new routine to calculate logical
  !     *                         switch for end of month.
  !
  !     * lsemon = logical :: switch for end of a month.
  !     * iday   = day of the year.
  !     * nsecs  = number of seconds elapsed in the current day.
  !     * delt   = model timestep length (seconds).
  !
  implicit none

  logical, intent(inout) :: lsemon !< Variable description\f$[units]\f$
  integer, intent(in) :: iday   !< Julian calendar day of year \f$[day]\f$
  integer, intent(in) :: nsecs !< Variable description\f$[units]\f$
  real, intent(in) :: delt   !< Timestep for atmospheric model \f$[seconds]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  integer :: monthend(12) !<
  integer :: idt !<
  integer :: i !<
  data monthend/31,59,90,120,151,181,212,243,273,304,334,365/
  !--------------------------------------------------------------------
  lsemon=.false.
  idt=nint(delt)
  do i=1,12
    if ((iday-1)*86400+nsecs==monthend(i)*86400-idt) then
      lsemon=.true.
      exit
    end if
  end do
  return
  !--------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
