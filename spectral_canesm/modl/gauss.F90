!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine gauss(nracp,racp,pg,sia,rad,pgssin2)
  !
  !     * revision:      nov 3/98 - ml. add implicit real*i declaration
  !     *                               so that accuracy will be maintained
  !     *                               even when running in 32-bit mode.
  !     ****************************************************************
  !     author: evhen yakimiw,rpn,dorval
  !             september 1995
  !     ****************************************************************
  !
  !     comments: this subroutine uses a highly efficient and accurate
  !       method developed by the author for evaluating the gaussian
  !       latitudes and the gaussian weights required in the gauss-legendre
  !       quadrature rule. it is especially well suited for extremely high
  !       order quadrature rules.
  !
  !     ****************************************************************
  !
  !     *****************************************************************
  !     nracp        : number of positive roots for the legendre polynomials
  !                    of degree 2*nracp.
  !     racp(i)      : positive roots for the legendre polynomials.
  !                    these are xo, the sines of the gaussian latitudes.
  !     pg(i)        : gaussian weights at the gaussian latitudes.
  !     sia(i)       : sin(colatitude) = cos(latitude).
  !     rad(i)       : colatitude in radians.
  !     pgssin2(i)   : pg(i)/(sia(i)**2
  !     sinm1(i)     : 1./sia(i)      **** removed for ccrd use ****
  !     sinm2(i)     : 1./sia(i)**2   **** removed for ccrd use ****
  !     sin2(i)      : sia(i)**2      **** removed for ccrd use ****
  !     *****************************************************************
  !
  implicit none
  real*8 :: a
  real*8 :: ac
  real*8 :: ac0
  real*8 :: ac1
  real*8 :: ang
  real*8 :: c1
  real*8 :: c2
  real*8 :: cm1
  real*8 :: cm2
  real*8 :: dp
  real*8 :: dp1
  real*8 :: dpn
  real*8 :: dx
  real*8 :: f1
  real*8 :: f2
  real*8 :: f22
  real*8 :: f2k
  real*8 :: f2ksq
  real*8 :: f2n
  real*8 :: f3
  real*8 :: f4
  real*8 :: f5
  real*8 :: fk
  real*8 :: fkm
  real*8 :: fkp
  real*8 :: fm
  real*8 :: fn
  real*8 :: fn4
  real*8 :: fnn
  real*8 :: fnn2
  real*8 :: fnn3
  real*8 :: fnn4
  real*8 :: fp
  real*8 :: gp
  integer :: i
  integer :: k
  integer :: m
  integer :: n
  integer, intent(inout) :: nracp
  real*8 :: p
  real*8 :: pgs
  real*8 :: pm
  real*8 :: pn
  real*8 :: pp
  real*8 :: s1
  real*8 :: s2
  real*8 :: sn2
  real*8 :: t
  real*8 :: t1
  real*8 :: theta
  real*8 :: x1
  real*8 :: x2
  real*8 :: xo
  real*8, intent(inout) , dimension(nracp) :: racp !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(nracp) :: pg !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(nracp) :: sia !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(nracp) :: rad !< Variable description\f$[units]\f$
  real*8, intent(inout) , dimension(nracp) :: pgssin2 !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  integer :: lg !<
  !======================================================================
  !
  !     defining the constants needed
  !
  n=2*nracp
  !---------------------------------
  !     the following code may be used for even or odd values of n
  !     ranging from 2 and up.
  !
  m=(n+1)/2
  fn=float(n)
  f2n=fn+fn
  fn4=1./(384.*fn**4)
  fnn=float(n*(n+1))
  fnn2=fnn-2.
  fnn3=float(n*(n+1)-6)
  fnn4=fnn-12.
  fm=float(n-1)
  fp=float(n+1)
  lg=3.3*log(fn)
  !
  ac1=sqrt(2.d0)
  do k=1,n
    fk=float(k)
    f2k=2.*fk
    f2ksq=f2k*f2k
    ac1=ac1*sqrt(1.-1./f2ksq)
  end do ! loop 1
  ac1=ac1/sqrt(fn+.5)
  !
  !---------------------------------
  x1=acos(-1.d0)/float(4*n+2)
  x2=1.-.125*fm/fn**3
  a=-1.
  !**********************************
  do i=1,m
    a=a+4.
    t=a*x1
    !---------------------------------
    !     a high order accurate initial guess of the gaussian latitudes is used.
    !     ref: f.g.lether in j.comput.app.math.4,pp.47-52(1978). precision o(n-5).
    !---------------------------------
    sn2=1.-cos(t)*cos(t)
    xo=(x2-fn4*(39.-28./sn2))*cos(t)
    !---------------------------------
    !     to accurately determine the gaussian latitudes, two methods are combined.
    !     the corrections dx to the gaussian sin(latitude)=xo are first evaluated
    !     by reversing the taylor expansion of pn, the legendre polynomials, near
    !     its zeros,followed by a Newton's rule. The procedure can be repeated.
    !     in this code, a 5th order scheme is implemented, leading to gaussian
    !     latitudes accurate to machine precision for all values of n.
    !     no need to iterate.
    !---------------------------------
    pm=1.
    pn=xo
    do k=2,n
      t1=xo*pn
      pp=t1-pm
      pp=pp-pp/float(k)+t1
      pm=pn
      pn=pp
    end do ! loop 3
    c2=(1.+xo)*(1.-xo)
    cm2=1./c2
    dp1=fn*(pm-xo*pn)
    dpn=dp1*cm2
    f1=pn/dpn
    f2=(2.*xo-fnn*f1)*cm2
    f3=(4.*xo*f2-fnn2)*cm2
    f4=(6.*xo*f3-fnn3*f2)*cm2
    f5=(8.*xo*f4-fnn4*f3)*cm2
    f22=f2*f2
    !---------------------------------
    !     reversion of taylor series (handbook of mathematical
    !     functions, abramowitz and stegun, p.16,eq.3.6.25)
    !
    dx=-f1*(1.+.5*f1*(f2+f1*(f22-f3/3.-f1*(f2*(10.*f3-15.*f22)-f4 &
      -f1*(3.*f2*f4+2.*f3*f3+21.*f22*(f22-f3)-.2*f5))/12.)))
    !
    !---------------------------------
    !     Newton's rule (5th order)(Abramowitz and Stegun, p.18,Eq.3.9.5)
    !
    p=f1+dx*(1.+.5*dx*(f2+dx*(f3+.25*dx*(f4+.2*dx*f5))/3.))
    dp=1.+dx*(f2+.5*dx*(f3+dx*(f4+.25*dx*f5)/3.))
    !
    !---------------------------------
    dx=dx-p/dp
    xo=xo+dx
    !---------------------------------
    !     this completes the evaluation of the gaussian latitudes.
    !---------------------------------
    !
    !     the next step calculates the gaussian weights. to proceed, one needs
    !     the values of the legendre polynomials and its first derivative at
    !     the gaussian latitudes. two algorithms are used. the first algorithm
    !     gives the best accuracy at very high latitudes. the second algorithm
    !     is more accurate at lower latitudes as it is also much more effficient.
    !
    theta=acos(xo)
    c2=(1.+xo)*(1.-xo)
    c1=sin(theta)
    cm1=1./c1
    cm2=1./c2
    if (i<=lg) then
      !
      fkm=0.
      s1=0.
      s2=0.
      ac=1.
      !
      do k=1,n-1,2
        fk=float(k)
        fkp=fk+1.
        ac0=fn-fkm
        ang=ac0*theta
        s1=s1+ac*cos(ang)
        s2=s2-ac*ac0*sin(ang)
        ac=(fk*(f2n-fkm))/(fkp*(f2n-fk))*ac
        fkm=fkp
      end do ! loop 4
      if ((k-1)==n) ac=0.5*ac
      ac0=fn-fkm
      ang=ac0*theta
      !
      s1=s1+ac*cos(ang)
      s2=s2-ac*ac0*sin(ang)
      pn=ac1*s1
      dp1=ac1*s2
      !
      dpn=-cm1*dp1
    else
      pm=1.
      pn=xo
      do k=2,n
        t1=xo*pn
        pp=t1-pm
        pp=pp-pp/float(k)+t1
        pm=pn
        pn=pp
      end do ! loop 5
      dp1=fn*(pm-xo*pn)
      dpn=dp1*cm2
      !-----------------------------------
    end if
    f1=pn/dpn
    !---------------------------------
    gp=dpn*(c2-xo*f1+.5*(fnn+cm2)*f1**2)
    pgs=2./(gp*gp)
    pg(i)=c2*pgs
    racp(i)=xo
    !---------------------------------
    !       calculate other quantities
    !
    rad(i)=theta
    sia(i)=c1
    pgssin2(i)=pgs
  end do ! loop 2
  ! ***********************************
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
