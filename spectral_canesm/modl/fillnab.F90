!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine fillnab(tracna, fslice, wfs, fac, ioff, &
                   ntrac, ntracn, ntrspec, itradv, &
                   ilev, ijpak, ilg, ilgp, il1, il2)

  !     * dec 10/03 - m.lazare.   consistent with new methodology for
  !     *                         ibm port.
  !     * oct 29/02 - j.scinocca. previous version filnab.

  !     * updates tracna with physics tendencies (fslice) for non-advected
  !     * tracers. otherwise, tendencies are stored into "WFS".
  !     * "IOFF" is offset into tracna for the particular task.

  implicit none
  real, intent(in) :: fac
  integer, intent(in) :: ijpak
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ilgp !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioff
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  integer, intent(in) :: ntracn !< Variable description\f$[units]\f$
  integer, intent(in) :: ntrspec
  !
  real, intent(inout), dimension(ijpak,ilev,ntracn) :: tracna !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,ilev,ntrac)    :: fslice !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilgp,ilev,ntrspec) :: wfs !< Variable description\f$[units]\f$
  integer, intent(in), dimension(ntrac) :: itradv !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: i
  integer :: l
  integer :: n
  integer :: na
  integer :: nna
  real :: tracnao
  !--------------------------------------------------------------------
  na=0
  nna=0
  do n=1,ntrac
    if (itradv(n)==1) then
      na=na+1
      do l=1,ilev
        do i=il1,il2
          wfs(i,l,na)=fslice(i,l,n)
        end do
      end do
    else
      nna=nna+1
      do l=1,ilev
        do i=il1,il2
          tracnao=tracna(ioff+i,l,nna)
          tracna(ioff+i,l,nna)=max(tracnao+fac*fslice(i,l,n),0.)
        end do
      end do
    end if
  end do ! loop 300

  return
end subroutine fillnab
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
