!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine write_station_info(ggpak,stn_loc,name,kount, & ! input
                                    ilg1,ilat,l,nstations, &
                                    nusite)

  ! subroutine based on putggb3 to gather up the variable stnidpak
  ! and save it in stnpak
  ! will be used to extract stations from the gathered fields

  implicit none

  !
  ! input data
  !

  real, intent(in) :: ggpak(ilat*(ilg1-1)) !<

  integer, intent(in) :: ilg1 !< Variable description\f$[units]\f$
  integer, intent(in) :: ilat !< Variable description\f$[units]\f$
  integer, intent(in) :: l !< Variable description\f$[units]\f$
  integer, intent(in) :: kount   !< Current model timestep \f$[unitless]\f$
  integer, intent(in) , dimension(nstations) :: stn_loc !< Variable description\f$[units]\f$
  integer, intent(in) :: nstations !< Variable description\f$[units]\f$
  integer, intent(in) :: nusite !< Variable description\f$[units]\f$
  integer, intent(in) :: name !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! local data
  !

  integer :: i !<
  integer :: ioff !<
  integer :: ilg !<
  integer :: j !<
  integer :: ij !<
  integer :: maxx !<
  integer :: istat !<

  real :: ptoit !<
  integer :: lonsl !<
  integer :: nlat !<
  integer :: ilev !<
  integer :: levs !<
  integer :: lrlmt !<
  integer :: icoord !<
  integer :: lay !<
  integer :: ipio_loc !<

  common /sizes/  lonsl,nlat,ilev,levs,lrlmt,icoord,lay,ptoit

  integer*4 :: mynode !<
  common /mpinfo/ mynode

  !     * common block to hold the number of nodes and communicator.

  integer*4 :: nnode !<
  integer*4 :: agcm_commwrld !<
  integer*4 :: my_cmplx_type !<
  integer*4 :: my_real_type !<
  integer*4 :: my_int_type !<
  common /mpicomm/ nnode, agcm_commwrld, my_cmplx_type,my_real_type, &
                  my_int_type

  real, target  :: gll(ilg1*ilat) !<

  !     * hard code "XPAK" dimension due to problem with "NANS" initialization
  !     * UNDER AIX. IT'S SET TO HANDLE UP TO 641X320 (T213 GRID).

  real, target  , dimension(205120) :: xpak !<
  real, pointer , dimension(:) :: gbuf !<

  integer*4 :: sz !<
  integer*4 :: master !<
  integer*4 :: ierr !<
  !
  !     * common block to hold switch for doing parallel i/o or not.
  !
  integer :: ipio !<
  common /ipario/ ipio
  !
  !     * common block to hold logical :: switch for rcm.
  !
  logical :: lrcm !<
  common /model/  lrcm

  !     * icom is a shared i/o area. it must be large enough
  !     * for an 8 word label followed by a packed gaussian grid.
  !
  integer :: ibuf !<
  integer :: idat !<
  common /icom/ ibuf(8),idat(1)

  integer :: iyear !<
  integer :: iymdh !<
  integer :: myrssti !<
  integer :: isavdts !<
  common /keeptim/ iyear,iymdh,myrssti,isavdts

  real :: stn_buff(nstations) !<

  integer :: ip !<
  integer :: i_stat !<
  integer :: ibuf2 !<
  integer :: igrid !<
  integer :: npack !<
  integer :: nrow !<
  integer :: khem !<
  integer :: nc4to8 !<

  !---------------------------------------------------------------------
  if (ilg1*nlat>205120)            call xit('WRITE_STN',-1)

  igrid=nc4to8("GRID")
  npack=2
  nrow =1
  khem =0

  !     * determine proper ibuf(2) to use for saved fields, based on
  !     * value of option switch "ISAVDTS".
  !     * note that the value of "NF" is checked to see if we are
  !     * writing to the interactive file; if so kount is used in
  !     * ibuf(2) regardless of whether we are running with the
  !     * date-time stamp or not.
  !     * this enables us to just use putggb3 and not require a separate
  !     * subroutine putggbx to write to the interactive file.

  if (isavdts/=0 .and. nusite/=18) then
    ibuf2=iymdh
  else
    ibuf2=kount
  end if


  do ip = 1, nstations
    stn_buff(ip) = -999.0 ! set to missing flag
  end do


  !     * store values of ggpak into gll, adding cyclic longitude.
  !     * note that this is not done for the rcm !
  !
  ilg=ilg1-1
  if (.not.lrcm) then
    do j=1,ilat
      ij   = (j-1)*ilg1
      ioff = (j-1)*ilg
      do i=1,ilg
        gll(ij+i)=ggpak(ioff+i)
      end do ! loop 200
      gll(ij+ilg1)=gll(ij+1)
    end do ! loop 210
  end if

  !     * this routine always needs the parallel i/o turned off.

  ipio_loc = 0

  if (nnode>1 .and. ipio_loc==0) then
    !
    !       * abort if trying to run the rcm with mpi - not supported.
    !
    if (lrcm)      call xit('WRITE_STN',-2)
    !
    !       * gather gll into gbuf (=>xbuf) from all nbr nodes.
    !
    master = 0
    sz     = ilat*ilg1
    gbuf => xpak(1:ilg1*nlat)
    call wrap_gather(gll   , sz,            my_real_type, &
                         gbuf  , sz,            my_real_type, &
                         master, agcm_commwrld, ierr)

    if (mynode == 0) then
      ! extract the fields to output
      do i_stat = 1, nstations
        ip = stn_loc(i_stat)
        stn_buff(i_stat) = gbuf(ip)
      end do

      ! set the labels
      call setlab(ibuf,igrid,ibuf2,name,l,nstations,nrow,khem,npack)
      maxx=2*nstations
      ! write to file
      call putfld2(nusite,stn_buff,ibuf,maxx)
    end if
  else

    if (lrcm) then
      do i_stat = 1, nstations
        ip = stn_loc(i_stat)
        stn_buff(i_stat) = gbuf(ip)
      end do
      ! set the labels
      call setlab(ibuf,igrid,ibuf2,name,l,nstations,nrow,khem, &
                        npack)
      maxx=2*nstations
      ! write to file
      call putfld2(nusite,stn_buff,ibuf,maxx)
    else
      do i_stat = 1, nstations
        ip = stn_loc(i_stat)
        stn_buff(i_stat) = gll(ip)
      end do
      ! set the labels
      call setlab(ibuf,igrid,ibuf2,name,l,nstations,nrow,khem, &
                        npack)
      maxx=2*nstations
      ! write to file
      call putfld2(nusite,stn_buff,ibuf,maxx)
    end if

  end if

  return

  4011 format(a12,i12,i12,150(e12.3))

end subroutine write_station_info
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
