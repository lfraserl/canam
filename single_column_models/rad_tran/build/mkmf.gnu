# NOTE:
#   - note tested
FC = gfortran
LD = gfortran

# TODO: add netcdf consideration if it can't be handled by the module system
INCLUDE_FLAGS = -I/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc7.3/netcdf-fortran/4.4.4/include
LDFLAGS = -L/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc7.3/netcdf-fortran/4.4.4/lib -lnetcdff -L/cvmfs/soft.computecanada.ca/nix/var/nix/profiles/gcc-7.3.0/lib64 -L/cvmfs/soft.computecanada.ca/nix/var/nix/profiles/gcc-7.3.0/lib -L/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/gcc7.3/netcdf/4.6.1/lib64 -lnetcdf -lnetcdf -lm -lpthread

# Jason's laptop
#INCLUDE_FLAGS = -I/home/jcl/opt_local/include
#LDFLAGS = -Wl,-rpath=/home/jcl/opt_local/lib -L/home/jcl/opt_local/lib -lnetcdff 

# create FFLAGS depending on debug/optimization settings
FFLAGS_DEBUG = -g -O0 -fbacktrace -ffpe-trap=invalid,zero,overflow -fcheck=all -fno-automatic -Wall -Wextra
FFLAGS_OPT = -O2 -march=native
FFLAGS = -cpp
ifeq ($(OPTIMIZATION_LVL),0)
FFLAGS += $(FFLAGS_DEBUG)
else 
ifeq ($(OPTIMIZATION_LVL),1)
FFLAGS += $(FFLAGS_OPT)
endif
endif

# add include flags to FFLAGS
FFLAGS += $(INCLUDE_FLAGS)
