!>\file
!>\brief Exit routine for single column model
!!
!! @author Jason Cole
!

  subroutine xit(name,n)

    implicit none
    
    ! input
    integer, intent(in) :: n             !< Exit value [1]
    character(len=*), intent(in) :: name !< Exit message [1]
    
    write(*,*) trim(name), n
    stop 
    
    return
  end subroutine xit
