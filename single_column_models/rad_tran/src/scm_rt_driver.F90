!>\file
!>\brief A driver to run the CCCma radiative transfer model in an "offline" single
!! column model.
!! @author Jason Cole
!

program scm_rt_driver

!===============================================================================
! This code is the driver of calls to run the CCCma radiative transfer model in
! a single column mode.
! It has 3 main parts.
!
! 1. Read in the required profile information and set variables needed for the
!    radiative transfer calculations.
!
! 2. Perform the radiative transfer calculations.  This will use calls to the
!    radiative transfer routines in CanAM to the fullest extent possible.
!
! 3. Write out the results of the radiative transfer calculations.
!
! The input will be a combination of netCDF, namelists and perhaps text files.
! The output will be a netCDF file.
!===============================================================================

  use read_command_line_mod, only : read_command_line

  use read_runinfo_file_mod, only : read_runinfo_file

  use read_config_file_mod, only : cfg_values, &
                                   read_config_file

  use arrays_mod

  use read_input_mod, only : read_input_data

  use write_output_mod, only : write_output

  use manipulate_arrays_mod, only : alloc_dealloc_arrays, &
                                    zero_arrays

  use initialization_rt_mod, only : init_rng

  use parameters_mod, only : maxlen_char

  use subgrid_clouds_mod, only : subgrid_clouds

  use rad_tran_calc_mod, only : rad_tran_calc

  use radcons_mod, only : set_mmr

  implicit none

! Local variables

  type(cfg_values) :: &
  cfg_settings

  character(len=maxlen_char) :: &
  runinfo_filename,             &
  cfg_filename,                 &
  input_filename,               &
  output_dir,                   &
  output_filename

  logical :: &
  allocate_arrays ! Indicate if arrays should allocated (true) or deallocated (false)

!    integer, intent(out) :: lcsw      !< Perform solar calculation, no=0, yes=1 [1] ! set in driver
!    integer, intent(out) :: lclw      !< Perform thermal calculation, no=0, yes=1 [1] ! set in driver
!    integer, intent(out) :: jlat      !< Point for printing diagnostic output [1]
!    integer, intent(out) :: kount     !< CanAM timestep count [1]

!===============================================================================
! Read in the command line information.
!===============================================================================

  call read_command_line(runinfo_filename)

!===============================================================================
! Read the file with the run information.
!===============================================================================

  call read_runinfo_file(runinfo_filename, & ! Input
                         cfg_filename,     & ! Output
                         input_filename,   &
                         output_dir,       &
                         output_filename   )

!===============================================================================
! Read the configuration file.
!===============================================================================

  call read_config_file(cfg_filename,    & ! Input
                        cfg_settings     ) ! Output

!===============================================================================
! Allocate arrays as needed and initialize all to zero.
!===============================================================================
  allocate_arrays = .true.

  call alloc_dealloc_arrays(allocate_arrays, & ! Input
                            cfg_settings     )
  call zero_arrays()

!===============================================================================
! Initialize variables as needed.
!===============================================================================

  call init_rng(cfg_settings)

  ! Set absorber mixing ratios using default values.
  call set_mmr()

!===============================================================================
! Read in the input.
!===============================================================================

  call read_input_data(input_filename, &
                       cfg_settings    )

!===============================================================================
! Set up for the radiative transfer calculations.
!===============================================================================
  if (cfg_settings%compute_subgrid_clouds) then

    call subgrid_clouds(cfg_settings)

  end if

!===============================================================================
! Perform the radiative transfer calculations.
!===============================================================================

  call rad_tran_calc(cfg_settings)

!===============================================================================
! Save the results to file(s).
!===============================================================================

 call write_output(output_dir,      &
                   output_filename, &
                   cfg_settings     )

!===============================================================================
! Clean up things so the code exits in a nice clean state.
!===============================================================================
  allocate_arrays = .false.

  call alloc_dealloc_arrays(allocate_arrays, & ! INPUT
                            cfg_settings     )

end program scm_rt_driver

!> \file
!> This routine drives the CCCma atmiospheric radiative transfer code in an
!! offline single column mode.
