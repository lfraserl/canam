!!>\file
!!>\brief Brief description of the routine purpose.
!!!
!!! @author Routine author name(s)
!!
!! For the units specification, the following conventions have been
!! adopted for dimensionless fields:
!! - Explict fields like albedo and cloud amount, etc., which could be either fractions or %, will be indicated as such.
!! - Where it is convention and sensible write ratios explicitly, e.g., mixing ratios would be written as [Kg/Kg].
!! - All other dimensionless fields would be [dimensionless],
!!
!! All tiled fields are instantaneous unless otherwise noted.
!C===================================================================
!C     * GENERAL PHYSICS ARRAYS.
!C
!
 subroutine phybusinit(ni, nk)
  use phybus
  use agcm_types_mod, only : options => phys_options
  use agcm_types_mod, only : diag_options => phys_diag
  use compar,     only : nrmfld, kextt, isdust, isdiag, isdnum
  use msizes,     only : ntraca, ntracn, ntld, ilev, ilevp2, ilg, ntrac, im
  use psizes_19,  only : ictem, ican, ignd, levozc, levwf, levair, nlklm, nrfp
  use psizes_19,  only : nbs, nbl, levsa, levox, levoz
  use psizes_19,  only : izpblm, izpblt
  use tracers_info_mod, only: modl_tracers, num_agcm_tracers, indxa, indxna
  use chem_params_mod,  only: levssad, nlvarc, nchdg, nlso4, nsfcem, nvdep, nwflx
  use tracers_defs_mod, only : tr_hzd, tr_massc, tr_min, tr_monot

  implicit none
  integer, intent(in) :: ni, nk

  character(len=4) :: c_im_nbs,c_nbs,c_im,c_ignd,c_levoz,c_levozc,c_ntrac,c_levwf,  &
                      c_levair,c_ntld,c_ntld_ican,c_ntld_icanp1,c_ntld_ignd,c_ntld_4,c_2_ntrac,  &
                      c_ntld_ican_ignd,c_ntld_ictem_ignd,c_ntld_ictem,n_nk, &
                      c_izpblm, c_izpblt

  character(len=4) :: c_4, c_2, c_nlklm, c_ntld_2, c_ntld_ictemp1,  &
                      c_ilevp2, c_ilevp2_nrfp, c_nrfp, c_im_nrfp, c_kextt, &
                      c_kextt_nrmfld, c_nrmfld, c_isdnum,           &
                      c_isdiag, c_isdust, c_ictem, c_levox
  character(len=4) :: c_levsa_nbs, c_levsa_nbl, c_levsa

  character(len=4) :: c_levssad, c_nlvarc, c_nchdg, c_nlso4, c_nsfcem, c_nvdep, c_nwflx

  integer :: nm, nn
  integer :: ioption
  character(len=1)   :: imono, imassc, ihzd
  character(len=4)   :: trname
  character(len=12)  :: tmin_s
  character(len=80)  :: vdname      ! Tracer descriptive name
  character(len=28)  :: dyn_attr
  character(len=128) :: bus_string

  write(c_ignd,               '(i4)') ignd
  write(c_nbs,                '(i4)') nbs
  write(c_ntld,               '(i4)') ntld
  write(c_ntld_4,             '(i4)') ntld * 4
  write(c_ntld_2,             '(i4)') ntld * 2
  write(c_im,                 '(i4)') im
  write(c_im_nbs,             '(i4)') im * nbs
  write(c_ntld_ican,          '(i4)') ntld * ican
  write(c_ntld_icanp1,        '(i4)') ntld * (ican+1)
  write(c_ntld_ignd,          '(i4)') ntld * ignd
  write(c_levoz,              '(i4)') levoz
  write(c_levozc,             '(i4)') levozc
  write(c_ntrac,              '(i4)') ntrac
  write(c_2_ntrac,            '(i4)') ntrac * 2
  write(c_levwf,              '(i4)') levwf
  write(c_levair,             '(i4)') levair
  write(c_ntld_ican_ignd,     '(i4)') ntld * ican * ignd
  write(c_ntld_ictem_ignd,    '(i4)') ntld * ictem * ignd
  write(c_ntld_ictem,         '(i4)') ntld * ictem
  write(c_ntld_ictemp1,       '(i4)') ntld * (ictem + 1)
  write(c_ictem,              '(i4)') ictem
  write(n_nk,                 '(i4)') nk
  write(c_nlklm,              '(i4)') nlklm
  write(c_ilevp2,             '(i4)') ilev + 2
  c_4="   4"
  c_2="   2"
  write(c_levox,              '(i4)') levox

  if (options%explvol) then
     write(c_levsa_nbs,          '(i4)') levsa * nbs
     write(c_levsa_nbl,          '(i4)') levsa * nbl
     write(c_levsa,              '(i4)') levsa
  end if

  if (diag_options%radforce) then
     write(c_ilevp2_nrfp,        '(i4)') (ilev + 2) * nrfp
     write(c_im_nrfp,            '(i4)') im * nrfp
     write(c_nrfp,               '(i4)') nrfp
  end if

  if (options%pla .and. options%pam) then
     write(c_kextt,                '(i4)') kextt
     write(c_kextt_nrmfld,         '(i4)') kextt * nrmfld
     write(c_nrmfld,               '(i4)') nrmfld
     if (diag_options%xtrapla2) then
        write(c_isdiag,             '(i4)') isdiag
        write(c_isdnum,             '(i4)') isdnum
        write(c_isdust,             '(i4)') isdust
     end if
  end if

  if (options%gas_chem) then
     write(c_levssad, '(i4)') levssad
     write(c_nlvarc,  '(i4)') nlvarc
     write(c_nchdg,   '(i4)') nchdg
     write(c_nlso4,   '(i4)') nlso4
     write(c_nsfcem,  '(i4)') nsfcem
     write(c_nvdep,   '(i4)') nvdep
     write(c_nwflx,   '(i4)') nwflx
  end if

  if (diag_options%switch_pbl_state_heights) then
     write(c_izpblt,  '(i4)') izpblt
     write(c_izpblm,  '(i4)') izpblm
  end if
  if (diag_options%switch_pbl_wind_gust) then
     write(c_izpblm,  '(i4)') izpblm
  end if

#include "phyvars_macros.inc"

#include "phys_arrays.inc"
#include "phyvars_gem_extras.inc"
  !
  !  * Add the tracers onto the bus
  !
  allocate(tr_bus_offset(ntrac))

  if (trim(tr_monot) == 'ILMC') then
     ioption = 2
  else if (trim(tr_monot) == 'CLIP') then
     ioption = 1
  else ! assumes trim(tr_monot) == 'NIL'
     ioption = 0
  end if
  write(imono, '(i1)') ioption

  if (trim(tr_massc) == 'BC') then
     ioption = 1
  else ! assumes trim(tr_massc) == 'NIL'
     ioption = 0
  end if
  write(imassc, '(i1)') ioption

  if (tr_hzd) then
     ioption = 1
  else
     ioption = 0
  end if
  write(ihzd, '(i1)') ioption

  write(tmin_s, '(e12.3)') tr_min
   !
   ! First setup advected tracers on the dynamic bus
   ! Convert all dynamic options to strings
  dyn_attr = ';min=' // trim(tmin_s) // ';monot=' // imono  // ';massc=' // imassc // ';hzd=' // ihzd
  do nm = 1, ntraca
     nn = indxa(nm)
     trname = adjustl(modl_tracers(nn) % name)
     vdname = modl_tracers(nn) % lng_name
     bus_string = 'VN=TR/' // trim(trname) // ':P' // trim(dyn_attr) // &
                   & '; ON=' // trim(trname) // ';VD=' // trim(vdname) // ';VS=T;VB=D1'
     call gesdict(ni, nk, tr_bus_offset(nn), trim(bus_string))
  end do
   ! Then setup the non-advected tracers on the permanent bus
  do nm = 1, ntracn
     nn = indxna(nm)
     trname = adjustl(modl_tracers(nn) % name)
     vdname = modl_tracers(nn) % lng_name
     bus_string = 'VN=' // trim(trname) // '; ON=' // trim(trname) // &
                   & ';VD=' // trim(vdname) // ';VS=T;VB=P1'
     call gesdict(ni, nk, tr_bus_offset(nn), trim(bus_string))
  end do

  return
 end subroutine phybusinit

!!$OMP THREADPRIVATE (/ROW/)
!!> \file
!!> This is an example of adding text at the end of the routine.
!!! Your detailed description of the routine can be put here, including scientific
!!! numerics, and another other important information.
!!! Doxygen should be able to translate LaTeX and it is possible to include
!!! references using "\cite", for example, \cite vonSalzen2013.
!!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!!! or in the equation environment \n
!!! (NOTE that HTML will not number the equation but it will in LaTeX),
!!! \f{equation}{
!!!  F_1=ma_1
!!! \f}
