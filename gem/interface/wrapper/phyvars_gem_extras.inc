#ifdef PHYBUSINDDCL

#define PHYVAR2D(NAME, DESC) integer :: NAME = 0
#define PHYVAR3D(NAME, DESC) integer :: NAME = 0

#else

#define PHYVAR2D(NAME, DESC) call gesdict(ni, nk, NAME, DESC)
#define PHYVAR3D(NAME, DESC) call gesdict(ni, nk, NAME, DESC)

#endif


! BUS DYN
  PHYVAR3D(gz_moins,          'VN=PW_GZ:M     ;ON=GZM  ;VD=Geopotential at t-dt on momentum levels        ;VS=M  ;VB=d1' )

  PHYVAR3D(huphytd ,          'VN=phytd_hu    ;ON=QPHY ;VD=Total Q tendency due to physics                ;VS=T  ;VB=v0' )
  PHYVAR3D(huplus  ,          'VN=TR/HU:P     ;ON=UH   ;VD=Specific humidity at t+dt                      ;VS=T  ;VB=d1;IN=HU')

  PHYVAR3D(sigm    ,          'VN=PW_PM:P     ;ON=SL   ;VD=Sigma momentum values at t+dt                  ;VS=M  ;VB=d1' )
  PHYVAR3D(sigt    ,          'VN=PW_PT:P     ;ON=SIGT ;VD=Sigma thermo values at t+dt                    ;VS=T  ;VB=d1' )

  PHYVAR3D(tphytd  ,          'VN=phytd_tt    ;ON=TPHY ;VD=Total T tendency due to physics                ;VS=T  ;VB=v0' )
  PHYVAR3D(tplus   ,          'VN=PW_TT:P     ;ON=2T   ;VD=Temperature at t+dt                            ;VS=T  ;VB=d1;IN=TT')

  PHYVAR3D(uphytd  ,          'VN=phytd_uu    ;ON=UPHY ;VD=Total U tendency due to physics                ;VS=M  ;VB=v0' )
  PHYVAR3D(uplus   ,          'VN=PW_UU:P     ;ON=UP   ;VD=Wind speed along-x at t+dt                     ;VS=M  ;VB=d1;IN=UU')

  PHYVAR3D(vphytd  ,          'VN=phytd_vv    ;ON=VPHY ;VD=Total V tendency due to physics                ;VS=M  ;VB=v0' )
  PHYVAR3D(vplus   ,          'VN=PW_VV:P     ;ON=VP   ;VD=Wind speed along-y at t+dt                     ;VS=M  ;VB=d1;IN=VV')
  PHYVAR3D(wplus   ,          'VN=PW_WZ:P     ;ON=WP   ;VD=Wind speed along-z at t+dt                     ;VS=T  ;VB=d1;IN=WW')

  PHYVAR2D(me_moins,          'VN=PW_ME:M     ;ON=MEM  ;VD=Driver topography at t-dt                      ;VS=A  ;VB=d1')
  PHYVAR2D(p0_plus ,          'VN=PW_P0:P     ;ON=P0P  ;VD=Driver surface pressure at t+dt                ;VS=A  ;VB=d1; MIN=0')

  PHYVAR2D(tdiag    ,         'VN=tdiag       ;ON=TJ   ;VD=Diagnostic screen level temperature                                       ;VS=A  ;VB=p0' )
  PHYVAR2D(qdiag    ,         'VN=qdiag       ;ON=DQ   ;VD=Diagnostic screen level humidity                                      ;VS=A  ;VB=p0' )
  PHYVAR2D(udiag    ,         'VN=udiag       ;ON=UD   ;VD=Diagnostic anemometer-level zonal wind ;VS=A  ;VB=p0' )
  PHYVAR2D(vdiag    ,         'VN=vdiag       ;ON=VD   ;VD=Diagnostic anemometer-level meridional wind ;VS=A  ;VB=p0' )
  PHYVAR2D(dlat    ,          'VN=dlat        ;ON=LA   ;VD=Latitude                                       ;VS=A  ;VB=p0' )
  PHYVAR2D(dlon    ,          'VN=dlon        ;ON=LO   ;VD=Longitude                                      ;VS=A  ;VB=p0' )
  PHYVAR2D(dxdy    ,          'VN=dxdy        ;ON=DX   ;VD=Area of each model grid tile                   ;VS=A  ;VB=p0' )
  PHYVAR2D(tdmask  ,          'VN=tdmask      ;ON=MPTD ;VD=Horizontal weight for tendency application     ;VS=A  ;VB=p0' )

  PHYVAR2D(wrk_sst ,          'VN=WRK_SST     ;ON=SSTW ;VD=Working bus for reading sst                    ;VS=A  ;VB=p1 ;IN=GT' )
!BUS PHY
  PHYVAR3D(spnutend,          'VN=SPNUTEND     ;ON=USPN  ;VD=spn tendency for u   ;VS=M        ;VB=p1; IN=SPNU' )
  PHYVAR3D(spnvtend,          'VN=SPNVTEND     ;ON=VSPN  ;VD=spn tendency for v   ;VS=M        ;VB=p1; IN=SPNV' )
  PHYVAR3D(spnttend,          'VN=SPNTTEND     ;ON=TSPN  ;VD=spn tendency for t   ;VS=T        ;VB=p1; IN=SPNT' )
  PHYVAR3D(spnqtend,          'VN=SPNQTEND     ;ON=QSPN  ;VD=spn tendency for q   ;VS=T        ;VB=p1; IN=SPNQ' )
! BUS ENT

  PHYVAR2D(sic_ent ,          'VN=SIC_ENT     ;ON=SICW ;VD=Entry bus for reading sic                      ;VS=A  ;VB=e1 ;IN=SIC0' )

  PHYVAR2D(sicn_ent,          'VN=SICN_ENT    ;ON=SCNW ;VD=Entry bus for reading sicn                     ;VS=A  ;VB=e1 ;IN=SCN0' )
!
IF_BLOCK(options%explvol)
! Work-around for issues in reading multi-level explosive volcano optics input, when 'LEVSA'
! is greater than number of model levels
  PHYVAR3D(n_sws1_row, 'VN= SWS1_ROW ;ON=SWS1 ;VD=  SWS1_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWS1 ')
  PHYVAR3D(n_sws2_row, 'VN= SWS2_ROW ;ON=SWS2 ;VD=  SWS2_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWS2 ')
  PHYVAR3D(n_sws3_row, 'VN= SWS3_ROW ;ON=SWS3 ;VD=  SWS3_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWS3 ')
  PHYVAR3D(n_sws4_row, 'VN= SWS4_ROW ;ON=SWS4 ;VD=  SWS4_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWS4 ')

  PHYVAR3D(n_swe1_row, 'VN= SWE1_ROW ;ON=SWE1 ;VD=  SWE1_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWE1 ')
  PHYVAR3D(n_swe2_row, 'VN= SWE2_ROW ;ON=SWE2 ;VD=  SWE2_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWE2 ')
  PHYVAR3D(n_swe3_row, 'VN= SWE3_ROW ;ON=SWE3 ;VD=  SWE3_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWE3 ')
  PHYVAR3D(n_swe4_row, 'VN= SWE4_ROW ;ON=SWE4 ;VD=  SWE4_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWE4 ')

  PHYVAR3D(n_swg1_row, 'VN= SWG1_ROW ;ON=SWG1 ;VD=  SWG1_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWG1 ')
  PHYVAR3D(n_swg2_row, 'VN= SWG2_ROW ;ON=SWG2 ;VD=  SWG2_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWG2 ')
  PHYVAR3D(n_swg3_row, 'VN= SWG3_ROW ;ON=SWG3 ;VD=  SWG3_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWG3 ')
  PHYVAR3D(n_swg4_row, 'VN= SWG4_ROW ;ON=SWG4 ;VD=  SWG4_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= SWG4 ')

  PHYVAR3D(n_lws1_row, 'VN= LWS1_ROW ;ON=LWS1 ;VD=  LWS1_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS1 ')
  PHYVAR3D(n_lws2_row, 'VN= LWS2_ROW ;ON=LWS2 ;VD=  LWS2_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS2 ')
  PHYVAR3D(n_lws3_row, 'VN= LWS3_ROW ;ON=LWS3 ;VD=  LWS3_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS3 ')
  PHYVAR3D(n_lws4_row, 'VN= LWS4_ROW ;ON=LWS4 ;VD=  LWS4_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS4 ')
  PHYVAR3D(n_lws5_row, 'VN= LWS5_ROW ;ON=LWS5 ;VD=  LWS5_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS5 ')
  PHYVAR3D(n_lws6_row, 'VN= LWS6_ROW ;ON=LWS6 ;VD=  LWS6_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS6 ')
  PHYVAR3D(n_lws7_row, 'VN= LWS7_ROW ;ON=LWS7 ;VD=  LWS7_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS7 ')
  PHYVAR3D(n_lws8_row, 'VN= LWS8_ROW ;ON=LWS8 ;VD=  LWS8_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS8 ')
  PHYVAR3D(n_lws9_row, 'VN= LWS9_ROW ;ON=LWS9 ;VD=  LWS9_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWS9 ')

  PHYVAR3D(n_lwe1_row, 'VN= LWE1_ROW ;ON=LWE1 ;VD=  LWE1_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE1 ')
  PHYVAR3D(n_lwe2_row, 'VN= LWE2_ROW ;ON=LWE2 ;VD=  LWE2_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE2 ')
  PHYVAR3D(n_lwe3_row, 'VN= LWE3_ROW ;ON=LWE3 ;VD=  LWE3_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE3 ')
  PHYVAR3D(n_lwe4_row, 'VN= LWE4_ROW ;ON=LWE4 ;VD=  LWE4_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE4 ')
  PHYVAR3D(n_lwe5_row, 'VN= LWE5_ROW ;ON=LWE5 ;VD=  LWE5_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE5 ')
  PHYVAR3D(n_lwe6_row, 'VN= LWE6_ROW ;ON=LWE6 ;VD=  LWE6_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE6 ')
  PHYVAR3D(n_lwe7_row, 'VN= LWE7_ROW ;ON=LWE7 ;VD=  LWE7_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE7 ')
  PHYVAR3D(n_lwe8_row, 'VN= LWE8_ROW ;ON=LWE8 ;VD=  LWE8_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE8 ')
  PHYVAR3D(n_lwe9_row, 'VN= LWE9_ROW ;ON=LWE9 ;VD=  LWE9_ROW explvol variable ;VS=A*'//C_LEVSA//' ;VB=e1 ;IN= LWE9 ')
!
IF_BLOCK_END
!
!!> \file
!!> This is an example of adding text at the end of the routine.
!!! Your detailed description of the routine can be put here, including scientific
!!! numerics, and another other important information.
!!! Doxygen should be able to translate LaTeX and it is possible to include
!!! references using "\cite", for example, \cite vonSalzen2013.
!!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!!! or in the equation environment \n
!!! (NOTE that HTML will not number the equation but it will in LaTeX),
!!! \f{equation}{
!!!  F_1=ma_1
!!! \f}
