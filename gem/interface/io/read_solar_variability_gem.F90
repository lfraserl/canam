!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
  subroutine read_solar_variability_gem(F_iu, mynode)

! Purpose:
!
!-----------------------------------------------------------
! Read in the solar variability file for the GEM wrapper.
!-----------------------------------------------------------

! Method:
!
!-----------------------------------------------------------
! Read in all data from the specified file.  The expectation
! is that the following data will be in the file in the
! following order with the following variable names.
!
! It is also assumed that the time-series are "padded" with an
! extra points at the beginning and end so the values can be
! interpolated.
!
! YRFR - Years associated with each time-series as year fractions
!        i.e., 1990.5
! B101 - Solar irradiance anomaly for band 1, quadrature point 1
! B102 - Solar irradiance anomaly for band 1, quadrature point 2
! B103 - Solar irradiance anomaly for band 1, quadrature point 3
! B104 - Solar irradiance anomaly for band 1, quadrature point 4
! B105 - Solar irradiance anomaly for band 1, quadrature point 5
! B106 - Solar irradiance anomaly for band 1, quadrature point 6
! B107 - Solar irradiance anomaly for band 1, quadrature point 7
! B108 - Solar irradiance anomaly for band 1, quadrature point 8
! B109 - Solar irradiance anomaly for band 1, quadrature point 9
! BND2 - Solar irradiance anomaly for band 2
! BND3 - Solar irradiance anomaly for band 3
! BND4 - Solar irradiance anomaly for band 4

  use solvar, only : sv_nts_max, sv_nts, solvts, sv_bndnames, &
                     sv_subands, solvtimes

  implicit none

  integer, intent(in) :: F_iu     !> Input FST file unit
  integer, intent(in) :: mynode

  real, dimension(sv_nts_max) :: sv_wrk
  character(len=4) :: sv_ib3

  integer :: ier, ni, nj, nk, ibnd
  integer, external :: fstlir, fstopc

  if (mynode==0) then
     write(6,*)'GEM:SOLVAR: Initializing solar irradiance anomalies from file '
  endif

  ier = fstopc('MSGLVL', 'WARNIN', 0)

  !.... Read in the time
  sv_wrk(:) = 0.0
  ier = fstlir(sv_wrk, F_iu, ni, nj, nk, -1, 'SOLAR', -1, -1, -1, ' ', 'YRFR')
  if (ier < 0) call xit('GEM:SOLVAR', -1)
  if (ni > sv_nts_max) then
     write(6,*) 'INCREASE THE VALUE OF sv_nts_max IN MODULE SOLVAR', &
                ' TO AT LEAST ', ni
     call xit('GEM:SOLVAR', -2)
  end if

   ! the times from the file are in fraction of year.
  solvtimes(1:ni) = sv_wrk(1:ni)

   ! the number of points in the time-series.
  sv_nts = ni

  ! loop over each of the bands represented in the solar variability dataset.
  do ibnd = 1, sv_subands
     ! get the current solar variability band name
     sv_ib3 = sv_bndnames(ibnd)

     ! read the data from file.
     sv_wrk(:) = 0.0
     ier = fstlir(sv_wrk, F_iu, ni, nj, nk, -1, 'SOLAR', -1, -1, -1, ' ', sv_ib3)
     if (ier < 0) call xit('GEM:SOLVAR', -2)

     ! put the data for this particular band into an array holding the data
     ! for all of the solar variability.
     solvts(ibnd, 1:ni) = sv_wrk(1:ni)
  end do

  return

 end subroutine read_solar_variability_gem
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
