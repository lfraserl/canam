#!/bin/bash
set -ex

function monthly_convert {
  set -x

  mnt=$1

  gsfile=${uxxx}_${runid}_${RUNCYCLE_year}_m${mnt}_gs
  tempdir=${TASK_WORK}/monthly_convert_${mnt}
  mnt=10#${mnt}
  mkdir -p ${tempdir}
  cd ${tempdir}
  rm -f $gsfile

  #number of days before the begining of the month
  nmd=(0 31 59 90 120 151 181 212 243 273 304 334 365)

  #Obtain and process each model output file within the month
  # Note: This assumes the output are requested from outcfg.out in hourly steps
  if [[ $mnt -gt 0 ]] ; then
     prev_mnt=$(( mnt - 1 ))
     file_out_start=$(expr ${nmd[${prev_mnt}]} * 24 + $outfreq)
#     file_out_start=$(( file_out_start + outfreq ))
     file_out_stop=$(expr ${nmd[$mnt]} * 24)
  else
     #special case, only for the initial hour to obtain grid description fields
     file_out_start=${outfreq}
     file_out_stop=${file_out_start}
  fi

  for file_out_suffix in $(seq ${file_out_start} ${outfreq} ${file_out_stop}) ; do
     modfile=${CANAM_GEM_DATE}_$(printf "%08d" ${file_out_suffix})
     rm -f ccctmp* griddesc.tmp

     if [[ $modgrid == U || ${file_out_suffix} == ${runlength} ]]; then
        # for GYY output on U grid, interpolate data to target GU/GG grid
        if [[ $modgrid == U ]] ; then
          # Create a copy of the tictac on target grid here
          cp ${TASK_INPUT}/u_grid_tictac ${modfile}
          read -r ip1 ip2 ip3 <<<$(${TASK_BIN}/r.fstliste -izfst ${modfile} -nomvar ">>" -col 3-5)
          # else; in case of last step in a RCM run, the dynamical fields output
          #       are expected to be on the larger model grid (as opposed to the 'core' physics grid).
          #       Therefore, interpolate all the model output fields at this step to the physics grid
        else
          read -r ip1 ip2 ip3<<<$(${TASK_BIN}/r.fstliste -izfst ${TASK_INPUT}/model_out_dir/${modfile} -nomvar "SICN" -col 15-17)
          ${TASK_BIN}/editfst -s ${TASK_INPUT}/model_out_dir/${modfile} -d ${modfile} <<EOF
   desire(-1,[">>","^^","!!"],-1,-1,$ip1,$ip2)
   end
EOF
        fi

        echo "
      SORTIE(STD,4000,R)
      SETINTX($setintx)
      GRILLE(TAPE2,$ip1,$ip2,$ip3)
      HEURE(TOUT)
      CHAMP(TOUT)
      CHAMP('UV',TOUT)  " > p1.dir

        ${TASK_BIN}/pgsm -iment ${TASK_INPUT}/model_out_dir/${modfile} -ozsrt ${modfile} -i p1.dir
        rm -f liste p1.dir

     else
        cp ${TASK_INPUT}/model_out_dir/${modfile} ${modfile}
     fi

     if [[ ${file_out_suffix} -eq ${file_out_start} ]]; then
        read -r lonsl nlat <<<$(${TASK_BIN}/r.fstliste -izfst ${modfile} -nomvar "SICN" -col 6-7)
        read -r ig1 ig2 <<<$(${TASK_BIN}/r.fstliste -izfst ${modfile} -nomvar "SICN" -col 15-16)
        # Obtain the tictac info for the grid description super-label
        if [[ $mnt -eq 0 ]] ; then
           rm -f zticztac.fst
           ${TASK_BIN}/editfst -s ${modfile} -d zticztac.fst <<EOF
   desire(-1,">>",-1,-1,$ig1,$ig2)
   zap('A',"ZTIC")
   stdcopi(-1)
   desire(-1,"^^",-1,-1,$ig1,$ig2)
   zap('A',"ZTAC")
   stdcopi(-1)
   end
EOF
           #Disable the next line for now, it crashes r.diag's convert...
           #${TASK_BIN}/editfst -s zticztac.fst -d ${modfile} -i 0
        fi
     fi

    ### Hack to temporarily include 'RIVO' for use in the gsstats7.sh diagnostic script
     rm -f rivo_temp.fst
     ${TASK_BIN}/editfst -s ${modfile} -d rivo_temp.fst <<EOF
   desire(-1,"ROVG")
   zap(-1,"RIVO")
   end
EOF
      ${TASK_BIN}/editfst -s rivo_temp.fst -d ${modfile} -i 0
    ###

     ### convert data from rpn format into ccc format
     export BIG_TMPDIR=$(pwd)

     # generate px (in hPa) on-the-fly
     if [[ -z $(${TASK_BIN}/r.fstliste -izfst $modfile -nomvar "PX") && $(${TASK_BIN}/r.fstliste -izfst $modfile -nomvar "TT") ]]; then
       ${TASK_BIN}/compute_pressure -s $modfile -d $modfile -var all_levels
     fi

     # remove ES from rpn file to avoid conflict with SHUM(HU), which will be converted into ES in ccc format
     if [[ -z $(${TASK_BIN}/r.fstliste -izfst $modfile -nomvar "ES") ]]; then
       ${TASK_BIN}/r.diag enleve $modfile ccctmp1 -name es
     else
       cp $modfile ccctmp1 
     fi

     ### rotate uu vv; su sv; ufs vfs and oufs ovfs from tic/tac wind to lon/lat wind, if they exist
     wind=""
     for var in uu vv su sv ufs vfs oufs ovfs; do
       [[ $(${TASK_BIN}/r.fstliste -izfst ccctmp1 |grep ${var^^}) ]] && wind="$wind $var"
     done

     rm -f $wind *.rot
     ${TASK_BIN}/r.diag select ccctmp1 $wind -name $wind
     ${TASK_BIN}/r.diag enleve ccctmp1 ccctmp2 -name $wind

     if [[ -e uu ]]; then
       ### sort uu and vv 
       rm -f u1 v1
       ${TASK_BIN}/r.diag fsttri -s uu -d u1 -ip1 1
       ${TASK_BIN}/r.diag fsttri -s vv -d v1 -ip1 1
       ### rotate sorted uu and vv
       ${TASK_BIN}/r.diag ggvecz u1 v1 uu.rot vv.rot
       rm -f u1 v1
     fi

     ### rotate surface vectors
     [[ -e su   ]] && ${TASK_BIN}/r.diag ggvecz   su   sv   su.rot   sv.rot
     [[ -e ufs  ]] && ${TASK_BIN}/r.diag ggvecz  ufs  vfs  ufs.rot  vfs.rot
     [[ -e oufs ]] && ${TASK_BIN}/r.diag ggvecz oufs ovfs oufs.rot ovfs.rot

     ${TASK_BIN}/r.diag joinup ccctmp3 ccctmp2 *.rot 
     rm -f $wind *.rot

     # convert SHUM into ES. Don't forget to set moist=Q in ccc diag package
     ${TASK_BIN}/r.diag convert -opktyp PK92 -es SHUM -pmslmb -rpn ccctmp3 -ccrn ccctmp4

     # remove tictactoe
     ${TASK_BIN}/sortlab ccctmp4 ccctmp5

     # split all variables by name
     ${TASK_BIN}/spltall ccctmp5

      ### Create mask/soil parameters to go into data description
     if [[ ${mnt} -eq 0 ]]; then
        if [[ -f LO ]]; then
          echo "C*  NEWNAM ZLON"|${TASK_BIN}/ccc newnam LO PLON
          sed -i '0,/  LO /s//ZLON /' .VARTABLE
          rm -f LO
        fi

        if [[ -f LA ]]; then
          echo "C*  NEWNAM ZLAT"|${TASK_BIN}/ccc newnam LA PLAT
          sed -i '0,/  LA /s//ZLAT /' .VARTABLE
          rm -f LA
        fi

        for var in MASK DZG PORG FLND FLKU FLKR PLAT PLON CLON CLAT ZTIC ZTAC ; do
           if [[ -f $var ]] ; then
              cat $var >> griddesc.tmp
           fi
        done
        return 0
     fi

     if [[ $(basename $(${TASK_BIN}/readlink ${TASK_INPUT}/model_out_dir)) == "pres" ]]; then
      # sort selected dynamics variables into hourly files based on the timestamps inside the file
        for var in $(cat .VARTABLE |awk '{print $1}'); do
           if [[ -s $var ]]; then
              if [[ $modfile != *d && $modhour == $nexthour ]]; then
                 :
              else
                 rm -f sortout*
                 ${TASK_BIN}/sortime $var
                 for sortout in $(ls sortout*); do
                     [[ -s $sortout ]] && ${TASK_BIN}/sortlev $sortout ${sortout}lev
                 done
                 cat sortout*lev >> gpres_$var
              fi
           fi
        done
     else
      # remove level 1 from the output of GEM dynamics variables
        for var in U V TEMP ES PHI RHUM ; do
           if [[ -s $var ]]; then
              nvar=$(printf "%4s\n" ${var})
              echo "C*ENLEVE STEP           0  99999999    6 LEVS    1    1 name $nvar"|${TASK_BIN}/ccc enleve $var tempo
              mv tempo $var
           fi
        done

      # in gem output, phi has been output on both the momentum levels and the thermal levels
      # here we select and keep phi on the thermal levels only
        if [[ -f PHI ]]; then
          ${TASK_BIN}/sortvar PHI TEMP tempo; mv tempo PHI
        fi

      # Rename PMSL (converted from the 'PN' field in the original RPN output format) to PNSL,
      # to avoid conflict with GCM diag PMSL
        if [[ -f PMSL ]]; then
          echo "C*  NEWNAM PNSL"|${TASK_BIN}/ccc newnam PMSL PNSL
          sed -i '0,/PMSL /s//PNSL /' .VARTABLE
          rm -f PMSL
        fi

        # split px into thermal levels (APT) and momentum levels (APM)
        if [[ -f PX ]]; then
          ${TASK_BIN}/sortvar PX TEMP tempo

          echo "C*  NEWNAM  APT"|${TASK_BIN}/ccc newnam tempo APT
          sed -i -e '/ PX /p' -e '0,/  PX /s// APT /' .VARTABLE

          ${TASK_BIN}/sortvar PX U tempo
          echo "C*  NEWNAM  APM"|${TASK_BIN}/ccc newnam tempo APM
          sed -i '0,/  PX /s// APM /' .VARTABLE
          rm -f PX
        fi

        # keep/rename omeg on level 1 into 1000
        if [[ -f OMEG ]]; then
          printf "C*RELABL   GRID           OMEG    1\nC*RELABL   GRID           OMEG 1000" | ${TASK_BIN}/ccc relabl OMEG tempo
          mv tempo OMEG
        fi

        if [[ -f GTO ]]; then
          echo "C*  NEWNAM   GT"|${TASK_BIN}/ccc newnam GTO GT
          sed -i '0,/ GTO /s//  GT /' .VARTABLE
          rm -f GTO
        fi

        # for all other steps of the month, all variables go to gs file
        while read line; do
          var=$(echo $line | cut -d" " -f1)
          cat $var >> ccctmp6
          rm -f $var
        done < .VARTABLE

        # sort vertical levels monotonically
        ${TASK_BIN}/sortlev ccctmp6 ccctmp7

      # put data description at the begining of each gs file
        if [[ ${file_out_suffix} -eq ${file_out_start} ]]; then

          rm -f dummy parm.*
          echo " +PARM                                         "   > parm.txt
          echo "    gem_settings.nml/outcfg.out                "  >> parm.txt
          cat ${TASK_INPUT}/gem_settings                          >> parm.txt
          cat ${SEQ_EXP_HOME}/config/${SEQ_CURRENT_CONTAINER}.cfg >> parm.txt
          cat ${TASK_INPUT}/outcfg                                >> parm.txt
          echo "    nlat=${nlat}                               "  >> parm.txt
          echo "    lonsl=${lonsl}                             "  >> parm.txt
          echo "    ilev=${ilev}                               "  >> parm.txt
          echo "    levs=$(( ilev + 1 ))                       "  >> parm.txt
          echo "    delt=${CanAM_delt}                         "  >> parm.txt
          echo "    isgg=${isgg}                               "  >> parm.txt
          echo "    isbeg=${isbeg}                             "  >> parm.txt
          echo "    israd=${israd}                             "  >> parm.txt
          ${TASK_BIN}/txt2bin parm.txt parm.ccc

          # access tracers_info and convert to binary
          echo " +TRIN" > txt2bin_tr.in
          cat ${CANAM_GEM_xfer}/TRACERS_INFO >> txt2bin_tr.in
          ${TASK_BIN}/txt2bin txt2bin_tr.in TRIN output=TRINFO_out
          rm -f TRINFO_out txt2bin_tr.in

          if [[ -e ${TASK_WORK}/monthly_convert_00/griddesc.tmp ]]; then
            ${TASK_BIN}/sortlev ${TASK_WORK}/monthly_convert_00/griddesc.tmp griddesc.ccc
            cat griddesc.ccc >> parm.ccc
          fi

          # merge TRIN and PAR records
          ${TASK_BIN}/joinup datadesc TRIN parm.ccc

          printf "C*XSAVE       DATA DESCRIPTION  \n\n"  > ic.xsave
          printf "C*XSAVE       MODEL OUTPUT      \n\n" >> ic.xsave
          ${TASK_BIN}/xsave dummy datadesc ccctmp7 $gsfile input=ic.xsave
          rm -f ic.xsave dummy parm.* TRIN datadesc griddesc.ccc
        else
          # sort by time in case of GEM output are saved in daily files
          ${TASK_BIN}/sortime ccctmp7
          cat sortout* >> $gsfile
          rm -f sortout*
        fi

     fi
  done

  mv ${gsfile} ${DATAPATH}/${gsfile}.001
  ${TASK_BIN}/fdb delete ${gsfile} 001 || :
  ${TASK_BIN}/fdb save   ${DATAPATH}/${gsfile}.001
  cd ${workdir} ; rm -rf ${tempdir}
}

#--------------------------------------------------------------------
# Script start
#--------------------------------------------------------------------

workdir=$(pwd)

uxxx=ma

grdtyp=$(${TASK_BIN}/rpy.nml_get -f ${TASK_INPUT}/gem_settings -n grid Grd_typ_s)

# get the length of model run in hours
eval $(${TASK_BIN}/rpy.nml_get Fcst_end_S -n step -f ${TASK_INPUT}/gem_settings -k)
runlength=$(echo ${Fcst_end_S} | tr '[:lower:]' '[:upper:]')
if [[ $runlength == *D ]] ; then
  runlength=$((${runlength%D}*24))
else
  runlength=${runlength%H}
fi
firststep=${CANAM_GEM_DATE}_$(printf "%08d" 0)
laststep=${CANAM_GEM_DATE}_$(printf "%08d" ${runlength})

# Determine the model grid from the first file
modgrid=$(r.fstliste -izfst ${TASK_INPUT}/model_out_dir/${firststep} -nomvar TT -col 14)
if [[ ${modgrid} == U ]] ; then
  # todo: parameter to control interpolation form GY to GU/GG grid
  setintx=lineair    #lineair or voisin or cubique
else
  setintx=cubique
fi

outfreq=${__CANAM_GEM_outfreq}

mkdir -p ${DATAPATH}
# Create an empty database (at $DATAPATH_DB), if one does not yet exist
if [ ! -e ${DATAPATH_DB} ] ; then fdb empty ; fi

#First obtain the grid descrption/masksoil variables
thismnt=00
monthly_convert $thismnt > ${TASK_WORK}/monthly_convert_${thismnt}_job.log 2>&1

# Run the file format conversion script in parallel
for mm in $(seq ${RUNCYCLE_start_month} ${RUNCYCLE_stop_month}) ; do
  thismnt=$(printf "%02d" ${mm})
  (monthly_convert $thismnt > ${TASK_WORK}/monthly_convert_${thismnt}_job.log 2>&1) &
  sleep 3
done

wait

echo "model history files in CCCma format created"
