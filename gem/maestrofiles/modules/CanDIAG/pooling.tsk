#!/bin/bash
###############################################################
#Objective: multi-year pooling for plotmix
#
#
#set -e

RUNCYCLE_current_date=$(echo ${SEQ_SHORT_DATE} | cut -c1-6)
RUNCYCLE_end_date=${run_stop_time}${RUNCYCLE_start_month}

if [[ ${RUNCYCLE_end_date} = ${RUNCYCLE_current_date} && "${CANESM_mode}" = "GCM" ]] ; then

 # pool_var=on means pool variances as well as means
 pool_sea_pool_var=off
 pool_var=$pool_sea_pool_var
 # poolabort=off means do not abort when variables are missing
 poolabort=off

 uxxx=ma
 pool_uxxx=pa; pool_sea_flabel_uxxx=$pool_uxxx
 diag_uxxx=da; pool_sea_model_uxxx=$diag_uxxx
 pool_sea_flabel_prefix=${pool_sea_flabel_uxxx}_${runid}
 pool_sea_model_prefix=${pool_sea_model_uxxx}_${runid}
 pool_ann_flabel_prefix=${pool_ann_flabel_uxxx}_${runid}
 pool_ann_model_prefix=${pool_ann_model_uxxx}_${runid}

 # pool_suffix_list is used by poolseas4
 pool_sea_suffix_list=''
 pool_suffix_list="gp xp"
 # pool_var=on means pool variances as well as means
 pool_ann_pool_var=off
 pool_var=$pool_ann_pool_var

 flabel_mul=()
 flabel_djf=()
 flabel_mam=()
 flabel_jja=()
 flabel_son=()
 flabel_ann=()

 year=${pool_start_year}
 while [[ ${year} -le ${RUNCYCLE_year} ]] ; do
    for days in MAM JJA SON DJF ; do
       season=$(echo "$days" | tr '[:upper:]' '[:lower:]')
       flabel="${pool_sea_flabel_prefix}_${year}_${season}_"
       use_month_names=0
       if [[ "$season" == "djf" ]] ; then
          if [[ ${year} -eq ${pool_start_year} ]] ; then
             continue
          fi
          flabel_djf+=("${flabel}")
          mm01=m12; mm02=m01; mm03=m02
          mn01=dec; mn02=jan; mn03=feb
          # number of days in each month used for seasonal pooling
          ml01="   31"; ml02="   31"; ml03="   28";
       elif [[ "$season" == "mam" ]] ; then
          flabel_mam+=("${flabel}")
          mm01=m03; mm02=m04; mm03=m05
          mn01=mar; mn02=apr; mn03=may
          # number of days in each month used for seasonal pooling
          ml01="   31"; ml02="   30"; ml03="   31";
       elif [[ "$season" == "jja" ]] ; then
          flabel_jja+=("${flabel}")
          mm01=m06; mm02=m07; mm03=m08
          mn01=jun; mn02=jul; mn03=aug
          # number of days in each month used for seasonal pooling
          ml01="   30"; ml02="   31"; ml03="   31";
       elif [[ "$season" == "son" ]] ; then
          flabel_son+=("${flabel}")
          mm01=m09; mm02=m10; mm03=m11
          mn01=sep; mn02=oct; mn03=nov
          # number of days in each month used for seasonal pooling
          ml01="   30"; ml02="   31"; ml03="   30";
       fi

       if [ x"$mm01" = x"m12" -o x"$mn01" = x"dec" ]; then
          year1=`echo $year|awk '{printf "%04d",$1-1}' -`
       else
          year1=$year
       fi

       if [ $use_month_names -eq 1 ]; then
          model1="${pool_sea_model_prefix}_${year1}_${mn01}_";
          model2="${pool_sea_model_prefix}_${year}_${mn02}_";
          model3="${pool_sea_model_prefix}_${year}_${mn03}_";
       else
          model1="${pool_sea_model_prefix}_${year1}_${mm01}_";
          model2="${pool_sea_model_prefix}_${year}_${mm02}_";
          model3="${pool_sea_model_prefix}_${year}_${mm03}_";
       fi

       join=3
       source poolseas4.sh

    done

    flabel="${pool_sea_flabel_prefix}_${year}_ann_"
    model1="${pool_sea_model_prefix}_${year}_m01_";
    model2="${pool_sea_model_prefix}_${year}_m02_";
    model3="${pool_sea_model_prefix}_${year}_m03_";
    model4="${pool_sea_model_prefix}_${year}_m04_";
    model5="${pool_sea_model_prefix}_${year}_m05_";
    model6="${pool_sea_model_prefix}_${year}_m06_";
    model7="${pool_sea_model_prefix}_${year}_m07_";
    model8="${pool_sea_model_prefix}_${year}_m08_";
    model9="${pool_sea_model_prefix}_${year}_m09_";
    model10="${pool_sea_model_prefix}_${year}_m10_";
    model11="${pool_sea_model_prefix}_${year}_m11_";
    model12="${pool_sea_model_prefix}_${year}_m12_";

    ml01="31"; ml02="28"; ml03="31"; ml04="30"; ml05="31"; ml06="30";
    ml07="31"; ml08="31"; ml09="30"; ml10="31"; ml11="30"; ml12="31";

    join=12
    source poolseas4.sh

    flabel_ann+=("${flabel}")
    year=$(( year + 1 ))
 done

#=======================================================================
# multi-year pool of diagnostic and/or pooled files     --- pool_mul_ ---
 range=${pool_start_year}"m"${RUNCYCLE_start_month}"_"${RUNCYCLE_year}"m"${RUNCYCLE_stop_month}
 for season in djf mam jja son ann ; do
   if [[ "$season" == "djf" ]] ; then
      flabel_sea=${flabel_djf[@]}
   elif [[ "$season" == "mam" ]] ; then
      flabel_sea=${flabel_mam[@]}
   elif [[ "$season" == "jja" ]] ; then
      flabel_sea=${flabel_jja[@]}
   elif [[ "$season" == "son" ]] ; then
      flabel_sea=${flabel_son[@]}
   elif [[ "$season" == "ann" ]] ; then
      flabel_sea=${flabel_ann[@]}
   fi
   nyr=0
   for flabel_mul in ${flabel_sea} ; do
      nyr=$(( nyr + 1 ))
      eval model${nyr}=${flabel_mul}
   done
   flabel=${pool_sea_flabel_prefix}_${range}_${season}_
   join=${nyr}
   source pool4.sh
 done

 for mm in $(seq ${RUNCYCLE_start_month} ${RUNCYCLE_stop_month}) ; do
   thismnt="m"$(printf "%02d" ${mm})
   year=${pool_start_year}
   nyr=0
   while [[ ${year} -le ${RUNCYCLE_year} ]] ; do
      nyr=$(( nyr + 1 ))
      eval model${nyr}=${pool_sea_model_prefix}_${year}_${thismnt}_
      year=$(( year + 1 ))
   done
   flabel=${pool_sea_flabel_prefix}_${range}_${thismnt}_
   join=${nyr}
   source pool4.sh
 done

 printf "\n=====>  CanDIAG Pooling job ends: $(date) ###########\n\n"

else
  echo "No pooling done for the current run cycle ${RUNCYCLE_current_date}"
fi
