#!/bin/bash
#
# Launch a CanESM maestro experiment
#   - First arg:    Path to the maestro suite experiment

if [ -n "$1" ] ; then
   export SEQ_EXP_HOME=$1
else
   if [ -z "${SEQ_EXP_HOME}" ] ; then
      echo "ERROR: You provide the path to the maestro experiment as the first argument"
      echo "OR set the SEQ_EXP_HOME env. var."
      exit 1
   fi
fi

source ${SEQ_EXP_HOME}/experiment.cfg
if [ -z "${run_start_time}" ]; then
   echo "ERROR: run_start_time is not set in ${SEQ_EXP_HOME}/experiment.cfg."
   exit 1
fi

#expbegin -d ${run_start_time}0101 -e ${SEQ_EXP_HOME}
#flow -exp ${SEQ_EXP_HOME} &

cd ${SEQ_EXP_HOME}
maestro -n /canesm/BuildDates -s submit -i -d ${run_start_time}0101
xflow &
