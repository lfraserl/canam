#!/bin/bash

# Get keys needed for timestep loop from nml file
eval $(${TASK_BIN}/rpy.nml_get -u -k relax_cslm_param/iperiod_lake_cor | cut -f1 -d"!") #in hours
eval $(${TASK_BIN}/rpy.nml_get -u -k relax_cslm_param/lake_ref_year | cut -f1 -d"!")

current_datetime=$(echo ${GEMSET_STRT_DT} | cut -c1-8)
# Start from the previous 12Z to the run start date
current_hr=-12
current_datetime=$(${TASK_BIN}/r.date -V ${current_datetime} +${current_hr}H)

# Directive for positional parameters
cat << EOF > ./dir_coord
 desire(-1,["^^",">>",'^>'])
 end
EOF

lkn_iref_src=${TASK_INPUT}/cslmireffile
lkn_tref_src=${TASK_INPUT}/cslmtreffile

rm -f lkn_data_file
#===============================================================================
#  Prep the lake nudging data for the model run
#===============================================================================
end_hr=$(( runlength + iperiod_lake_cor ))
while [[ ${current_hr} -le ${end_hr} ]] ; do

  lkn_ref_datetime=${lake_ref_year}$(echo ${current_datetime} | cut -c5-16)
  cmcdate=$(${TASK_BIN}/r.date ${current_datetime})
  lkn_ref_cmcdate=$(${TASK_BIN}/r.date ${lkn_ref_datetime})

  cat << EOF > ./dir_lkn
 desire(-1,-1,-1,${lkn_ref_cmcdate},-1,-1)
 zap(-1,-1,-1,${cmcdate},-1,-1)
 stdcopi(-1)
EOF

  ${TASK_BIN}/editfst -s ${lkn_iref_src} ${lkn_tref_src} -d lkn_data_file -i ./dir_lkn

  current_datetime=$(${TASK_BIN}/r.date -V ${current_datetime} +${iperiod_lake_cor}H)
  current_hr=$(( current_hr + iperiod_lake_cor ))

done

# Now add the lake mask and coord info from the reference lake data
${TASK_BIN}/editfst -s ${lkn_tref_src} -d lkn_data_file -i ./dir_coord
${TASK_BIN}/editfst -s ${TASK_INPUT}/cslmamskfile -d lkn_data_file -i << EOF
 desire(-1,"AMSK",-1,-1,-1,-1,-1)
 end
EOF

