#!/bin/bash

set -e

if [[ x${ATM_MODEL_BNDL} == x ]] ; then
    cat <<EOF
ERROR: \${ATM_MODEL_BNDL} not defined or does not exists
       Cannot compile CanAM-GEM

       Please make sure you loaded the full GEM development environment
---- ABORT ----
EOF
    exit 1
fi

if [[ x${storage_model} == x ]] ; then
    cat <<EOF
ERROR: \${storage_model} not defined or does not exists
       Cannot compile CanAM-GEM

       Please make sure you have created and set the \${storage_model} path (most preferably under \$HOME or \$HOME_ords
---- ABORT ----
EOF
    exit 1
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
CANAM=${DIR}/../ ; export CANAM
rpnphy=${CANAM}/gem/interface ; export rpnphy

# First build CanAM-GEM library (libcccmaphy)
cd ${CANAM}/gem/interface
if [ -f Makefile ] ; then
  make distclean
fi
./ouv_exp_canam ; make -j8 cccmaphy >../canam_build.log 2>&1

# Now build the GEM executable
cd ${CANAM}/gem
if [ -f Makefile ] ; then
  make distclean
fi
ouv_exp_gem ; linkit ; make dep ; make -j obj ; make gemdm
