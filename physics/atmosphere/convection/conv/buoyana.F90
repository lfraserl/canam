!> \file
!> \brief The main purpose of this subroutine is to evaluate convective available potential energy (CAPE)
!! and define the location of the top of the layer that may be occupied by moist convection in a given
!! model column. This quantitiy is used in determining triggering conditions for moist convection and in
!! the closure conditions for deep (precipitating) convection.
!!
!! @author Norm McFarlane, John Scinocca
!
subroutine buoyana(z,p,pf,t,q,pressg,ilev,ilevm,ilg,il1,il2, &
                   msg,maxi,lcl,lnb,lpbl,tl,cape,capep, &
                   tc,qstc,hmn,pblt)
  !
  !     * nov 24/2006 - m.lazare.    - calls new tincld3 instead of tincld2.
  !     *                            - selective promotion of variables to
  !     *                              real(8) :: to support 32-bit mode.`
  !     * jun 15/2006 - m.lazare.    new version for gcm15f:
  !     *                            - now uses internal work arrays
  !     *                              instead of passed workspace.
  !     *                            - "ZERO" data constant added
  !     *                              and used in call to intrinsics.
  !     * may 04/2006 - m.lazare/    previous version buoyan9 for gcm15e:
  !     *               k.vonsalzen. - calls new tincld2.
  !     *                              this requires passing in eps2.
  !     * may 05/2001 - k.vonsalzen. previous version buoyan8 for gcm15d.
  !
  !     * calculates properties of undiluted air parcel ascending
  !     * from the pbl to its level of neutral buoyancy.
  !
  use phys_consts, only : grav, rgas, rgasv, cpres, hv, tvfa, eps1,eps2,a,b
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilevm  !< Number of vertical levels minus 1 \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: msg
  !
  !     * i/o fields:
  !

  real, intent(in)   , dimension(ilg,ilev) :: q !< Grid slice of model water vapour mixing ratio on model levels \f$[g/g]\f$
  real, intent(in)   , dimension(ilg,ilev) :: t !< Grid slice of model prognostic temperature on model levels \f$[K]\f$
  real, intent(inout), dimension(ilg,ilev) :: tc !< Parcel temperature \f$[K]\f$
  real, intent(inout), dimension(ilg,ilev) :: qstc !< Saturation mixing ratio for the parcel \f$[units]\f$
  real, intent(in)   , dimension(ilg,ilev) :: z !< Height above the local column surface of model half levels (centers of layers) \f$[m]\f$
  real, intent(in)   , dimension(ilg,ilev) :: pf !< Grid slice of pressure on model layer interface levels \f$[mb]\f$
  real, intent(in)   , dimension(ilg,ilev) :: p !< Grid slice of pressure on model levels \f$[mb]\f$
  real, intent(inout), dimension(ilg,ilev) :: hmn !< Moist static energy of the mean background air for the column \f$[units]\f$

  real, intent(inout), dimension(ilg) :: tl !< Parcel temperature at the LCL \f$[K]\f$
  real, intent(inout), dimension(ilg) :: cape !< Convective available potential energy defined to include the entire region between
                                 !! the MAXI level and the LNBVariable description \f$[J/kg]\f$
  real, intent(inout), dimension(ilg) :: capep !< The contribution to the value of CAPE associated with the region above the LCL \f$[J/kg]\f$
  real, intent(in)   , dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in)   , dimension(ilg) :: pblt !< Index of the level closest to the top of the boundary layer \f$[1]\f$

  integer, intent(inout), dimension(ilg) :: lcl !< The lifting condensation level for an ascending, undiluted parcel of air initiated
                                 !! at the MAXI level \f$[1]\f$
  integer, intent(inout), dimension(ilg) :: lnb !< The level of neutral buoyancy \f$[1]\f$
  integer, intent(inout), dimension(ilg) :: maxi !< The model level at which convection is initiated \f$[1]\f$
  integer, intent(inout), dimension(ilg) :: lpbl !< The level corresponding to the top of the planetary boundary layer \f$[1]\f$

  !
  !     * internal work fields (the first set are promoted to
  !     * integrate with the new tincld3, ie from shallow):
  !
  !==================================================================
  ! physical (adjustable) parameters
  !
  integer, parameter :: itrigg = 0

  !==================================================================

  real :: adef
  real :: atmp
  real :: dxdg
  real :: dz
  integer :: il
  integer :: l
  integer :: ldef
  real :: tpv
  real :: tv

  real*8, dimension(ilg,ilev) :: tc8
  real*8, dimension(ilg,ilev) :: tdry
  real*8, dimension(ilg,ilev) :: hmnc
  real*8, dimension(ilg,ilev) :: qc8
  real*8, dimension(ilg,ilev) :: qstc8
  real*8, dimension(ilg,ilev) :: z8
  real*8, dimension(ilg,ilev) :: p8
  real, dimension(ilg,ilev)   :: wc
  real, dimension(ilg,ilev)   :: buoy
  real, dimension(ilg,ilev)   :: fact
  real, dimension(ilg,ilev)   :: qc !< Variable description\f$[units]\f$
  real, dimension(ilg)        :: qm
  real, dimension(ilg)        :: hmnm
  real, dimension(ilg)        :: maxgr
  !
  integer,dimension(ilg)      :: iskip
  integer,dimension(ilg)      :: idoc
  integer,dimension(ilg)      :: ltm
  !
  real, parameter :: zero = 0.0

  !-----------------------------------------------------------------------
  do il = il1,il2
    lpbl (il) = nint(pblt(il))
    maxi (il) = ilev
    idoc (il) = 1
    iskip(il) = 0
    maxgr(il) = 9.e+20
    cape (il) = 0.
    capep(il) = 0.
  end do ! loop 20
  !
  do l = 1,ilev
    do il = il1,il2
      hmn(il,l) = cpres * t(il,l) + grav * z(il,l) + hv * q(il,l)
    end do
  end do ! loop 100
  !
  do il = il1,il2
    qm  (il) = q   (il,maxi(il))
    hmnm(il) = hmn (il,maxi(il))
  end do ! loop 120
  !
  !    * calculate temperature of undiluted air parcel.
  !
  do l = msg + 1,ilev
    do il = il1,il2
      qc  (il,l) = qm  (il)
      hmnc(il,l) = hmnm(il)
    end do
  end do ! loop 140
  !
  do l = 1,ilev
    do il = il1,il2
      z8   (il,l) = z   (il,l)
      p8   (il,l) = p   (il,l)
      qc8  (il,l) = qc  (il,l)
    end do
  end do
  !
  call tincld3(tc8,ilg,ilev,msg - 1,il1,il2,maxi,tdry,hmnc,qc8,qstc8, &
               z8,p8,hv,grav,cpres,eps1,eps2,a,b,iskip)
  !
  do l = 1,ilev
    do il = il1,il2
      tc  (il,l) = tc8  (il,l)
      qstc(il,l) = qstc8(il,l)
    end do
  end do
  !
  !    * buoyancy of undiulted air parcel.
  !
  do l = msg + 1,ilev
    do il = il1,il2
      buoy(il,l) = - 1.e-10
    end do
  end do ! loop 200
  !
  l = ilev
  do il = il1,il2
    if (l <= maxi(il) ) then
      tv = t(il,l) * (1. + tvfa * q(il,l))
      tpv = tc(il,l) * (1. + tvfa * qc(il,l) &
            - (1. + tvfa) * max(qc(il,l) - qstc(il,l),zero))
      buoy(il,l) = (tpv - tv)/tv
      fact(il,l) = rgas * buoy(il,l) * t(il,l) &
                   * log(0.01 * pressg(il)/pf(il,l))
      buoy(il,l) = grav * buoy(il,l)
    end if
  end do ! loop 210
  do l = msg + 1,ilevm
    do il = il1,il2
      if (l <= maxi(il) ) then
        tv = t(il,l) * (1. + tvfa * q(il,l))
        tpv = tc(il,l) * (1. + tvfa * qc(il,l) &
              - (1. + tvfa) * max(qc(il,l) - qstc(il,l),zero))
        buoy(il,l) = (tpv - tv)/tv
        fact(il,l) = rgas * buoy(il,l) * t(il,l) &
                     * log(pf(il,l + 1)/pf(il,l))
        buoy(il,l) = buoy(il,l) * grav
      end if
    end do
  end do ! loop 220
  !
  !    * determine cloud base as lifting condensation level (lcl).
  !
  do il = il1,il2
    lcl(il) = msg + 1
    ltm(il) = 0
  end do ! loop 240
  do l = ilevm,msg + 1, - 1
    do il = il1,il2
      if (l < maxi(il) &
          .and. qc(il,l) >= qstc(il,l) &
          .and. ltm(il) == 0) then
        ltm(il) = 1
        lcl(il) = l
      end if
    end do
  end do ! loop 250
  !
  !     * determine cloud top as level of neutral buoyancy (lnb).
  !
  !
  !     * vertical velocity of parcel.
  !
  adef = - 9.
  do l = msg + 1,ilev
    do il = il1,il2
      wc(il,l) = adef
    end do
  end do ! loop 295
  do il = il1,il2
    l = lcl(il)
    tl(il) = tc(il,l)
    wc(il,l) = 0.4
  end do ! loop 300
  do l = ilevm,msg + 1, - 1
    do il = il1,il2
      if (l < lcl(il) .and. wc(il,l + 1) >= 0.            ) then
        dz = z(il,l) - z(il,l + 1)
        dxdg = ( buoy(il,l) - buoy(il,l + 1) )/dz
        atmp = (buoy(il,l + 1) - dxdg * z(il,l + 1)) * dz &
               + .5 * dxdg * (z(il,l) ** 2 - z(il,l + 1) ** 2)
        atmp = wc(il,l + 1) ** 2 + 2. * atmp
        wc(il,l) = sign(sqrt(abs(atmp)),atmp)
      end if
    end do
  end do ! loop 310
  !
  !     * assign top of cloud layer to level of neutral buoyancy (lnb).
  !     * the lnb is defined as the first level at which the cloudy air
  !     * is neutrally buoyant below that level at which the mean vertical
  !     * velocity in the cloud drops to zero.
  !
  ldef = ilev
  do il = il1,il2
    lnb(il) = ldef
    ltm(il) = ldef
  end do ! loop 500
  do l = ilevm,msg + 1, - 1
    do il = il1,il2
      if (l < lcl(il) .and. lnb(il) == ldef) then
        if (wc(il,l) <= 0. .and. wc(il,l + 1) > 0. ) then
          if (ltm(il) /= ldef) then
            lnb(il) = ltm(il)
          else
            lnb(il) = l
          end if
        else if (buoy(il,l) <= 0. .and. buoy(il,l + 1) > 0. ) then
          ltm(il) = l
        end if
      end if
    end do
  end do ! loop 520
  do il = il1,il2
    if (lnb(il) == ldef) then
      if (ltm(il) /= ldef) then
        lnb(il) = ltm(il)
      else
        lnb(il) = ilev + 1
      end if
    end if
  end do ! loop 540
  !
  !     * calculate convective available potential energy (cape).
  !
  do l = msg + 1,ilev
    do il = il1,il2
      if (idoc(il) /= 0 .and. l < maxi(il) .and. l >= lnb(il)) then
        cape(il) = cape(il) + fact(il,l)
        if (l <= lcl(il) ) capep(il) = capep(il) + fact(il,l)
      end if
    end do
  end do ! loop 600
  !
  return
end subroutine buoyana

!> \file
!! \section sec_theory Theoretical formulation
!! \n
!! The basic definition of CAPE is as follows:
!! \n
!! \f{equation}{
!! CAPE=\intop_{zb}^{zt}\frac{g(T_{v}^{(p)}-\bar{T_{v})}}{\bar{T_{v}}}dz \tag{1}
!! \f}
!! \n
!! The integrand in this formula is the buoyancy (\f$B^{(p)})\f$of a parcel
!! of air which undergoes undiluted adiabatic ascent between the base
!! level (\f$z_{b})\f$ and uppermost level (\f$z_{t})\f$ may be reached by
!! moists onvection. This level is typically the level of nuetral buoyancy
!! (LNB) for conditionally unstable atmospheric column.
!! \n
!! The virtual temperature (\f$T_{v})\f$ is defined using a standard approximation
!! as \f$T_{v}\simeq T[1+(\frac{1}{\epsilon}-1)r_{v}-r_{c}]\simeq T(1+.61r_{v}-r_{c})\f$,
!! where \f$\epsilon\simeq.622\f$ is the ratio of the gas constants for
!! dry air (\f$R_{d})\f$ and water vapour (\f$R_{v})\f$. (ref: \textbf{AMS
!! Glossary of Meteorology; http://glossary.ametsoc.org/wiki/Virtual\_temperature}).
!! The remaining quantities in the above formulae are defined as follows:
!! \n
!! \f$r_{v},r_{c}=r_{l}+r_{i}\f$: respectively mixing ratios of water vapour
!! and condensed [liquid (\f$r_{l})\f$ plus solid (\f$r_{i})\f$] water.
!! \n
!! \f$g(=9.8m/s^{2}):\f$ acceleration due to gravity.
!! \n
!! \f$z_{b},z_{c}:\f$ respectively the base and top levels of the convective
!! layer.
!! \n
!! \f$T_{v}^{(p)}\f$: the virtual temperature associated with a parcel of
!! air lifted adiabatically from the base level (\f$z_{b})\f$ taking into
!! accout the effects of condensation and latent heat release. The ascent
!! is assumed to be reversible (i.e. all of the condensate generated
!! in the ascent is carried upward with the parcel).
!! \n
!! \f$\bar{T_{v}}\f$: the virtual temperature of the background (mean state)
!! air (evuated from input values of the background temperature and water
!! vapour mixing ratio).
!! \n
!! \subsection ssec_conservation Conservation assumptions and determination of the parcel properties
!! \n
!! In the operational formulation it is assumed that the moist static
!! energy (\f$h^{(p)}=c_{p}T^{(p)}+L_{v}r_{v}^{(p)}+gz)\f$ and total water
!! (vapour plus condensate: \f$r_{t}=r_{v}+r_{c}\f$), of the parcel are
!! conserved during the lifting process, where \f$c_{p}\f$ is the specifice
!! heat at constant pressure, \f$L_{v}\f$ is the latent heat of vapourization.
!! Conservation of moist static energy is a \textit{simplifying assumption}
!! (see \textbf{Romps, 2015, DOI: 10.1175/JAS-D-15-0054.1} ). Note that
!! the moist static energy can be decomposed as the sum of the dry static
!! energy (\f$s^{(p)}=c_{p}T^{(p)}+gz)\f$ and the latent heat term (\f$L_{v}r_{v}^{(p)})\f$.
!! It is assumed that these quantities are separately conserved for adiabatic
!! ascent of unsaturated air.
!! \n
!! In the operational formulation these quantities are initialized to
!! the background values at the base level, i.e.
!! \n
!! \f{equation}{
!! h^{(p)}=\bar{h}(z_{b}) \tag{2}
!! \f}
!! \n
!! \f{equation}{
!! r_{t}^{(p)}=\bar{r}(z_{b}) \tag{3}
!! \f}
!! \n
!! where the overbar denotes environmental values. These quatitied are
!! independant of height under the conservationnassumption. However the
!! parcel properties (temperature, water vapour mixing ratio, and condensed
!! water mixing ratio) do vary in the vertical. The parcel is typically
!! unsaturated at the base level but becomes saturated at the lifting
!! condensation level (LCL) and posotovely buoyant (\f$T_{v}^{(p)}>\bar{T}_{v}\f$
!! ) at the level of free convetion (LFC). At any level the temperature,
!! specific humidity, and water content of the parcel are evaluated in tincld3.f.
!! \n
!! \subsection ssec_lnb Level of nuetral buoyancy, cloud base, cloud top, parcel vertical velocity}
!! \n
!! Once the parcel properties have been determined via the subroutine
!! TINCLD3, the parcel buoyancy is readily determined. This quantitly
!! will typically be close to zero within a dry daytime planetery boundary
!! layer (PBL) where the background temperature is structure is close
!! to dry-adiabatic and the water vapour mixing ratio nearly homogeneous
!! (i.e. characteristic of the well mixed turbulent part of the PBL),
!! and negative within the stable stratified region between the well-mixed
!! part and the LFC. In general, however the backgound atmosphere is
!! stable stratified in the region above the LFC in suvh a way that the
!! parcel buoyancy eventually becomes negative again and remains so on
!! further lifting. The level at which the parcel buoyancy becomes negative
!! and remains so is usually referred to as the level of nuetral buoynancy
!! (LNB).
!! \n
!! \n
!! However pecause the background virtual temperature varies with heigh
!! there may be layers of limited vertical extent where the parcel buoyancy
!! is negative but flanked by layers where it is positive. In principle
!! a positively buoyant ascending parcel that enters sugh a layer from
!! below may have sufficient vertical momentum to ascend through the
!! negatively buoyant region. To account for this the following simple
!! equation for the vertical velocity is used:
!! \n
!! \f{equation}{
!! \frac{\partial}{\partial z}\left(\frac{w_{(p)}^{2}}{2}\right)=B^{(p)} \tag{4}
!! \f}
!! \n
!! This equation is intergrated upward level by level numerically between
!! the LCL and higher levels. The parcel vertical velocity (\f$w_{(p)})\f$
!! is set to an arbitrary positive value ( default value = 0.4 m/s)
!! at the LCL. All levels above this level where the vertical velocity
!! remins positive are regarded as potentially part of the region within
!! the atmospheric column where moist convection may be active. The operational
!! LNB (\f$z_{t})\f$is assumed to lie below the level where the \f$w_{(p)}\f$
!! becomes negative. It is defined to be located at the top of the highest
!! layer that is nuetrally or positivly buoyant below this level.

