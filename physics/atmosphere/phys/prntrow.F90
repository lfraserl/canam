!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine prntrow (fld,name,loc,nlev,loff,ilg,il1,il2,jlat)
  !
  !     * m.lazare. - aug 13/90. add second dimension to use of "FLD" for
  !     *                        single-level fields, as should be.
  !     * m.lazare. - oct 20/88.
  !
  !     * prints out row values (between il1 and il2) of array fld,
  !     * for each of nlev levels at "JLAT" latitude row inside routine.
  !     *
  !     *      fld  = array to be printed out.
  !     *      name = name of array to be included in printout,
  !     *             i.e. "TH" for throw.
  !     *      loc  = location where printout is occurring, i.e.
  !     *             "AFTER VRTDFS".
  !     *             being displayed.
  !     *      nlev = number of levels for variable.
  !     *      loff = number of levels (including moon layer) before
  !     *             variable has values, i.e. loff=msg+1 for estg,
  !     *             loff=0 for throw, loff=1 for urow. for single-
  !     *             level fields, some level indicator (i.e. could be
  !     *             done in a vertical level loop).
  !     *             moon layer referenced to as level "0" in printout.
  !     *      ilg  = number of equally-spaced longitudes to be printed out.
  !     *      il1  = starting longitude number for printout.
  !     *      il2  = finishing longitude number for printout.
  !     *      jlat = current latitude row for printout.
  !
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: jlat
  integer, intent(in) :: loff
  integer, intent(in) :: nlev

  real, intent(in), dimension(ilg,nlev) :: fld !< Variable description\f$[units]\f$
  character(len=12), intent(in) :: loc          !< Variable description\f$[units]\f$
  character(len=5), intent(in) :: name          !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: il
  integer :: l
  integer :: lind
  !-----------------------------------------------------------------------
  !
  !     * distinguish between single-level and multi-level fields.
  !
  if (nlev == 1) then
    write(6,6100) loc,loff,name,jlat,(fld(il,1),il = il1,il2)
6100 format ('0',a14,', LEVEL',i3,' FOR VARIABLE ', &
                a7,' AT LATITUDE ROW',i3,' :',/,(5(1x,1pe24.16)))
  else
    lind = loff - 1
    write(6,6200)
6200 format ('1')
    do l = 1,nlev
      write(6,6300) loc,l + lind,name,jlat,(fld(il,l),il = il1,il2)
6300  format ('0',a14,', LEVEL',i3,' FOR VARIABLE ', &
                a7,' AT LATITUDE ROW',i3,' :',/,(5(1x,1pe24.16)))
    end do ! loop 200
  end if
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
