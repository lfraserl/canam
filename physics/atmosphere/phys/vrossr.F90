!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine vrossr(p, a, b, c, d, ilg, il1, il2, m)

  !     * ccrn mars 18/85 - b.dugas. (vectoriser...)
  !     * programmer  andrew staniforth, rpn.
  !     * solve the tri-diagonal matrix problem below.

  !  *****                                      ****  ****     ***    ****
  !  *****                                      ****  ****     ***    ****
  !  ** b(1),c(1), 0  , 0  , 0  , - - - -  ,  0   **  **  p(1)  **    **
  !  ** a(2),b(2),c(2), 0  , 0  , - - - -  ,  0   **  **  p(2)  **    **
  !  **  0  ,a(3),b(3),c(3), 0  , - - - -  ,  0   **  **  p(3)  **    **
  !  **  0  , 0  ,a(4),b(4),c(4), - - - -  ,  0   **  **  p(4)  **    **
  !  **  0  , 0  , 0  ,a(5),b(5), - - - -  ,  0   **  **  p(5)  ** -- **
  !  **  -                                    -   **  **    -   ** -- **
  !  **  -                                    -   **  **    -   **    **
  !  **  0  , - - , 0 ,a(m-2),b(m-2),c(m-2),  0   **  ** p(m-2) **    ** d
  !  **  0  , - - , 0 ,  0   ,a(m-1),b(m-1),c(m-1)**  ** p(m-1) **    ** d
  !  **  0  , - - , 0 ,  0   ,  0   , a(m) , b(m) **  **  p(m)  **    **
  !  ****                                       ****  ****    ****    ****
  !  ****                                       ****  ****    ****    ****


  !     * the routine solves il2-il1+1 of these problems simultaniously,
  !     * that is, it does one latitude circle at a time.
  !     * delta is a working array of dimension ilg x m.

  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: m

  real, intent(inout), dimension(ilg,m) :: p !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,m) :: a !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,m) :: b !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,m) :: c !< Variable description\f$[units]\f$
  real, intent(in),    dimension(ilg,m) :: d !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  integer :: i
  integer :: l
  real :: x
  real, dimension(ilg,m) :: delta   !< Timestep for atmospheric model \f$[seconds]\f$
  !-----------------------------------------------------------------------
  do i = il1,il2
    c(i,m) = 0.
  end do ! loop 10

  do i = il1,il2
    x         = 1./b(i,1)
    p(i,1)    = - c(i,1) * x
    delta(i,1) = d(i,1) * x
  end do ! loop 30

  do l = 2,m
    do i = il1,il2
      x         = 1./(b(i,l) + a(i,l) * p(i,l - 1))
      p(i,l)    = - c(i,l) * x
      delta(i,l) = (d(i,l) - a(i,l) * delta(i,l - 1)) * x
    end do
  end do ! loop 50

  do i = il1,il2
    p(i,m) = delta(i,m)
  end do ! loop 60

  do l = m - 1,1, - 1
    do i = il1,il2
      p(i,l) = p(i,l) * p(i,l + 1) + delta(i,l)
    end do
  end do ! loop 70

  return
end subroutine vrossr
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
