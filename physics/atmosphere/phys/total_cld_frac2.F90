!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine total_cld_frac2(cldt,cldo,clwt,cict, & ! output
                           clw, cic, rel, rei, dz, & ! input
                           cldwatmin, ilg, il1, il2, ilev, nxloc)

  !     apr 29/2012 - m. lazare.  separate cloud water/ice calculation.
  !     feb 5, 2009 - jason cole. original version total_cld_frac for gcm15g/h/i.
  !
  ! subroutine to compute the total cloud fraction using subcolumns from
  ! the stochastic cloud generator (cld_gen_driver2).  done in two ways:
  ! using all cloudy subcolumns containing at least cldwatmin cloud water (cldt)
  ! and using only cloudy subcolumns with vertically integrated visible optical
  ! thickness of 0.2 (cldo) which is a bit more consistent with isccp and ceres
  ! observations

  implicit none

  !
  ! output
  !

  real, dimension(ilg),intent(out) :: cldt !< TOTAL CLOUD FRACTION (USING CLDWATMIN THRESHOLD)\f$[units]\f$
  real, dimension(ilg),intent(out) :: cldo !< TOTAL CLOUD FRACTION (USING TAU > 0.2 THRESHOLD)\f$[units]\f$
  real, dimension(ilg),intent(out) :: clwt !< TOTAL CLOUD LIQUID WATER PATH (KG/M2)\f$[units]\f$
  real, dimension(ilg),intent(out) :: cict !< TOTAL CLOUD ICE PATH (KG/M2)\f$[units]\f$

  !
  ! input
  !

  real, dimension(ilg,ilev,nxloc),intent(in) :: clw !< CLOUD LIQUID WATER                 (UNITS)\f$[units]\f$
  real, dimension(ilg,ilev,nxloc),intent(in) :: cic !< CLOUD ICE WATER                     (UNITS)\f$[units]\f$
  real, dimension(ilg,ilev,nxloc),intent(in) :: rel !< CLOUD LIQUID WATER EFFECTIVE RADIUS (MICRONS)\f$[units]\f$
  real, dimension(ilg,ilev,nxloc),intent(in) :: rei !< CLOUD ICE WATER EFFECTIVE DIAMETER  (MICRONS)\f$[units]\f$

  real, dimension(ilg,ilev),intent(in) :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$

  real, intent(in) :: cldwatmin   !< Minimum threshold for cloud water content \f$[kg/kg]\f$

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: nxloc   !< Number of cloud subcolumns in an atmospheric column \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !=================================================================

  !
  ! local
  !

  real, parameter :: min_tau = 0.2 ! threshold to compute cldo
  real, parameter :: r_one   = 1.0
  real, parameter :: r_zero  = 0.0

  real, dimension(ilg) :: cumtau ! accumulate cloud optical thickness
  real, dimension(ilg) :: count_cldy

  real :: invrel
  real :: rei_loc
  real :: invrei
  real :: tau_vis
  real :: tauliqvis
  real :: tauicevis
  real :: r_nxloc
  real :: clw_path
  real :: cic_path

  integer :: i
  integer :: l
  integer :: icol

  ! initialize variables

  do i = il1, il2
    cldt(i) = r_zero
    cldo(i) = r_zero
    clwt(i) = r_zero
    cict(i) = r_zero
  end do

  r_nxloc = real(nxloc)

  do icol = 1, nxloc

    do i = il1, il2
      count_cldy(i) = r_zero
      cumtau(i)     = r_zero
    end do

    do l = 1, ilev
      do i = il1, il2

        tau_vis  = r_zero
        clw_path = clw(i,l,icol) * dz(i,l)
        cic_path = cic(i,l,icol) * dz(i,l)

        ! calculation for cldt
        if (clw_path > cldwatmin) then
          count_cldy(i) = r_one
          clwt(i) = clwt(i) + clw_path/1000.
        end if
        if (cic_path > cldwatmin) then
          count_cldy(i) = r_one
          cict(i) = cict(i) + cic_path/1000.
        end if

        ! calculation for cldo

        if ((clw_path > cldwatmin  .or. &
            cic_path > cldwatmin) .and. &
            cumtau(i) < min_tau) then

          ! compute cloud optical thickness for this volume

          invrel = r_one/rel(i,l,icol)

          rei_loc = 1.5396 * rei(i,l,icol) ! convert to generalized dimension
          invrei  = r_one/rei_loc

          if (clw_path > cldwatmin) then
            tauliqvis = clw(i,l,icol) &
                        * (4.483e-04 + invrel * (1.501 + invrel &
                        * (7.441e-01 - invrel * 9.620e-01)))
          else
            tauliqvis = r_zero
          end if

          if (cic_path > cldwatmin) then
            tauicevis = cic(i,l,icol) &
                        * ( - 0.303108e-04 + 0.251805e+01 * invrei)
          else
            tauicevis = r_zero
          end if

          tau_vis = (tauliqvis + tauicevis) * dz(i,l)
        end if

        cumtau(i) = cumtau(i) + tau_vis

      end do ! i
    end do ! l

    do i = il1, il2
      cldt(i) = cldt(i) + count_cldy(i)

      if (cumtau(i) > min_tau) then
        cldo(i) = cldo(i) + r_one
      end if
    end do ! i

  end do ! icol

  do i = il1, il2
    cldt(i) = cldt(i)/r_nxloc
    cldo(i) = cldo(i)/r_nxloc
    clwt(i) = clwt(i)/r_nxloc
    cict(i) = cict(i)/r_nxloc
  end do

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
