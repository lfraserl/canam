!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cld_generator3(cic_sub, clw_sub, ncldy, &
                          zf, avg_cf, avg_qlw, avg_qiw, &
                          rlc_cf, rlc_cw, sigma_qcw, &
                          ilg, il1, il2, lay, nx_loc, &
                          alpha, rcorr, iseedrow, &
                          i_loc, l_cld, ipph, ioverlap)

  !      * apr 30/2012 - jason cole.   new version for gcm16:
  !      *                             - remove {x,x1,x2,y,y1,y2} in
  !      *                               call (from cld_gen_driver_)
  !      *                               since only used internally
  !      *                               as already declared.
  !      * dec 12/2007 - jason cole.   previous version cld_generator2
  !      *                             for gcm15g/h/i:
  !      *                             - handle 3 different overlap
  !      *                               methods (defined using ioverlap).
  !      *                             - remove code related to setting
  !      *                               lpph and imaxran since now using
  !      *                               ipph and ioverlap.
  !      * jan 09/2007 - jason cole.   previous version cld_generator for gcm15f.
  ! --------------------------------------------------------------------
  ! --------------------------------------------------------------------
  ! generate a nx column by lay layer cloud field.  a
  ! method and model evaluation is described in:
  ! raisanen et al, 2004, stochastic generation of subgrid-scale cloudy
  ! columns for large-scale models,
  ! quart. j. roy. meteorol. soc., 2004 , 130 , 2047-2068
  ! input profiles must be from top of the model to the bottom as is the output.
  ! --------------------------------------------------------------------
  ! this version only generates the cloud overlap using the mro used in the cccma
  ! shortwave code
  !
  use xcw_data, only: n1, n2, xcw
  implicit none

  ! note: lay    => number of layers in gcm
  !       ilg    => number of gcm columns
  !       nx_loc => number of columns to generate

  !
  ! parameters
  !

  real, parameter :: cut = 0.001

  !
  ! input data
  !

  real, intent(in) , dimension(ilg,lay) :: zf !< Full-level (layer midpoint) altitude (km)\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: avg_cf !< Cloud amount for each layer\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: avg_qlw !< Cloud mean liquid water mixing ratio (g/m^3)\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: avg_qiw !< Cloud mean ice mixing ratio          (g/m^3)\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: sigma_qcw !< Normalized cloud condensate std. dev.\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: rlc_cf !< Cloud fraction decorrelation length\f$[units]\f$
  real, intent(in) , dimension(ilg,lay) :: rlc_cw !< Cloud condensate decorrelation length\f$[units]\f$

  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: nx_loc   !< Number of cloud subcolumns in an atmospheric column \f$[unitless]\f$

  integer, intent(in) :: ioverlap !< Overlap flag\f$[units]\f$
  integer, intent(in) :: ipph !< Plane-parallel homogeneous flag\f$[units]\f$

  !
  ! output data
  !

  real,intent(out) , dimension(ilg,lay,nx_loc) :: cic_sub !< Column ice water mixing ratio profile    (g/m^3)\f$[units]\f$
  real,intent(out) , dimension(ilg,lay,nx_loc) :: clw_sub !< Column liquid water mixing ratio profile (g/m^3)\f$[units]\f$

  integer, intent(out) , dimension(ilg) :: ncldy !< Number of cloudy subcolumns\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! work arrays
  !

  real(8), dimension(ilg) :: x
  real(8), dimension(ilg) :: y
  real(8), dimension(ilg) :: x1
  real(8), dimension(ilg) :: y1
  real(8), dimension(ilg) :: x2
  real(8), dimension(ilg) :: y2

  real, intent(inout), dimension(ilg,lay) :: alpha ! fraction of maximum/random cloud overlap
  real, intent(inout), dimension(ilg,lay) :: rcorr ! fraction of maximum/random cloud condensate overlap

  integer(4), intent(inout) , dimension(ilg,4) :: iseedrow ! seed for kissvec rng

  integer, intent(inout), dimension(ilg) :: i_loc ! place the new subcolumns into the arrays starting from the front

  logical, intent(inout), dimension(ilg) :: l_cld

  !
  ! local data (scalars)
  !

  integer :: il ! counter over ilg gcm columns
  integer :: i ! counter
  integer :: k ! counter over lay vertical layers
  integer :: k_top ! index of top most cloud layer
  integer :: k_base ! index of lowest cloud layer
  integer :: ind1 ! index in variability calculation
  integer :: ind2 ! index in variability calculation

  real :: rind1 ! real :: index in variability calculation
  real :: rind2 ! real :: index in variability calculation
  real :: zcw ! ratio of cloud condensate miximg ratio for
  ! this cell to its layer cloud-mean value

  logical :: l_top
  logical :: l_bot

  ! initialize the arrays

  do il = il1, il2
    i_loc(il) = 1
    l_cld(il) = .false.
  end do

  ! find uppermost cloudy layer

  l_top = .false.
  k_top = 0
  do k = 1,lay
    do il = il1, il2
      k_top = k
      if (avg_cf(il,k) > cut) l_top = .true.
    end do ! il
    if (l_top) exit
  end do ! k

  ! if no cloudy layers in any gcm column in this group, exit

  if (k_top == 0) then
    return
  end if

  ! find lowermost cloudy layer

  l_bot = .false.

  k_base = 0
  do k = lay,1, - 1
    do il = il1, il2
      k_base = k
      if (avg_cf(il,k) >= cut) l_bot = .true.
    end do ! il
    if (l_bot) exit
  end do ! k

  ! calculate overlap factors alpha for cloud fraction and rcorr for cloud
  ! condensate based on layer midpoint distances and decorrelation depths

  if (ioverlap == 2) then   ! using generalized overlap
    do k = k_top, k_base - 1
      do il = il1, il2
        if (rlc_cf(il,k) > 0.0) then
          alpha(il,k) = exp( - (zf(il,k) - zf(il,k + 1)) &
                        / rlc_cf(il,k))
        else
          alpha(il,k) = 0.0
        end if
        if (rlc_cw(il,k) > 0.0) then
          rcorr(il,k) = exp( - (zf(il,k) - zf(il,k + 1)) &
                        / rlc_cw(il,k))
        else
          rcorr(il,k) = 0.0
        end if
      end do ! il
    end do ! k
  end if

  ! although it increases the size of the code i will use an if-then-else appraoch to
  ! do the different version of cloud overlap
  ! it should make it easier for people to follow what is going on.
  ! ioverlap = 0, the overlap is maximum-random
  ! ioverlap = 1, the overlap is maximum-random following the definition given in geleyn and hollingsworth
  ! ioverlap = 2, the overlap is generalized overlap

  if (ioverlap == 0) then

    do i = 1,nx_loc

      ! generate all subcolumns for latitude chain

      do k = k_top, k_base

        if (k == k_top) then
          call kissvec(iseedrow, x, il1, il2, ilg)
          call kissvec(iseedrow, y, il1, il2, ilg)

        end if

        call kissvec(iseedrow, x1, il1, il2, ilg)
        call kissvec(iseedrow, y1, il1, il2, ilg)

        do il = il1, il2

          if (avg_cf(il,k - 1) == 0.0) then ! it is clear above
            x(il) = x1(il)
            y(il) = y1(il)
          end if
          ! treatment of cloudy cells

          if (x(il) < avg_cf(il,k)) then ! generate cloud in this layer

            if (ipph == 0) then ! homogeneous clouds
              zcw = 1.0
            else
              ! horizontally variable clouds:
              ! determine zcw = ratio of cloud condensate miximg ratio qc for this cell to
              ! its mean value for all cloudy cells in this layer.
              ! use bilinear interpolation of zcw tabulated in array xcw as a function of
              !    * cumulative probability y
              !    * relative standard deviation sigma
              ! take care that the definition of rind2 is consistent with subroutine
              ! tabulate_xcw

              rind1 = y(il) * (n1 - 1) + 1.0
              ind1  = max(1, min(int(rind1), n1 - 1))
              rind1 = rind1 - ind1
              rind2 = 40.0 * sigma_qcw(il,k) - 3.0
              ind2  = max(1, min(int(rind2), n2 - 1))
              rind2 = rind2 - ind2

              zcw = (1.0 - rind1) * (1.0 - rind2) * xcw(ind1,ind2) &
                    + (1.0 - rind1) * rind2       * xcw(ind1,ind2 + 1) &
                    + rind1 * (1.0 - rind2)       * xcw(ind1 + 1,ind2) &
                    + rind1 * rind2             * xcw(ind1 + 1,ind2 + 1)
            end if

            ! a horizontally constant iwc/lwc ratio is assumed for each layer so far
            l_cld(il)             = .true.
            clw_sub(il,k,i_loc(il)) = zcw * avg_qlw(il,k)
            cic_sub(il,k,i_loc(il)) = zcw * avg_qiw(il,k)
          end if

        end do           ! il
      end do              ! k

      ! need to check if a cloudy subcolumn was generated
      do il = il1, il2
        if (l_cld(il)) then
          i_loc(il) = i_loc(il) + 1
          l_cld(il) = .false.
        end if
      end do
    end do                 ! i
  else if (ioverlap == 1) then
    do i = 1,nx_loc

      ! generate all subcolumns for latitude chain

      do k = k_top, k_base

        if (k == k_top) then
          call kissvec(iseedrow, x, il1, il2, ilg)
          call kissvec(iseedrow, y, il1, il2, ilg)
        end if

        call kissvec(iseedrow, x1, il1, il2, ilg)
        call kissvec(iseedrow, y1, il1, il2, ilg)

        do il = il1, il2
          if (x(il) < 1.0 - avg_cf(il,k - 1)) then ! it is clear above
            x(il) = x1(il) * (1.0 - avg_cf(il,k - 1))
            y(il) = y1(il)
          end if

          ! treatment of cloudy cells

          if (x(il) < avg_cf(il,k)) then ! generate cloud in this layer

            if (ipph == 0) then ! homogeneous clouds
              zcw = 1.0
            else
              ! horizontally variable clouds:
              ! determine zcw = ratio of cloud condensate miximg ratio qc for this cell to
              ! its mean value for all cloudy cells in this layer.
              ! use bilinear interpolation of zcw tabulated in array xcw as a function of
              !    * cumulative probability y
              !    * relative standard deviation sigma
              ! take care that the definition of rind2 is consistent with subroutine
              ! tabulate_xcw

              rind1 = y(il) * (n1 - 1) + 1.0
              ind1  = max(1, min(int(rind1), n1 - 1))
              rind1 = rind1 - ind1
              rind2 = 40.0 * sigma_qcw(il,k) - 3.0
              ind2  = max(1, min(int(rind2), n2 - 1))
              rind2 = rind2 - ind2

              zcw = (1.0 - rind1) * (1.0 - rind2) * xcw(ind1,ind2) &
                    + (1.0 - rind1) * rind2       * xcw(ind1,ind2 + 1) &
                    + rind1 * (1.0 - rind2)       * xcw(ind1 + 1,ind2) &
                    + rind1 * rind2             * xcw(ind1 + 1,ind2 + 1)
            end if

            ! a horizontally constant iwc/lwc ratio is assumed for each layer so far
            l_cld(il)             = .true.
            clw_sub(il,k,i_loc(il)) = zcw * avg_qlw(il,k)
            cic_sub(il,k,i_loc(il)) = zcw * avg_qiw(il,k)
          end if

        end do           ! il
      end do              ! k

      ! need to check if a cloudy subcolumn was generated
      do il = il1, il2
        if (l_cld(il)) then
          i_loc(il) = i_loc(il) + 1
          l_cld(il) = .false.
        end if
      end do
    end do                 ! i
  else if (ioverlap == 2) then
    do i = 1,nx_loc

      ! generate all subcolumns for latitude chain

      do k = k_top, k_base

        if (k == k_top) then
          call kissvec(iseedrow, x, il1, il2, ilg)
          call kissvec(iseedrow, y, il1, il2, ilg)
        end if

        call kissvec(iseedrow, x1, il1, il2, ilg)
        call kissvec(iseedrow, y1, il1, il2, ilg)

        call kissvec(iseedrow, x2, il1, il2, ilg)
        call kissvec(iseedrow, y2, il1, il2, ilg)

        do il = il1, il2

          if (x1(il) > alpha(il,k - 1)) x(il) = x2(il)
          if (y1(il) > rcorr(il,k - 1)) y(il) = y2(il)

          ! treatment of cloudy cells

          if (x(il) < avg_cf(il,k)) then ! generate cloud in this layer

            if (ipph == 0) then ! homogeneous clouds
              zcw = 1.0
            else
              ! horizontally variable clouds:
              ! determine zcw = ratio of cloud condensate miximg ratio qc for this cell to
              ! its mean value for all cloudy cells in this layer.
              ! use bilinear interpolation of zcw tabulated in array xcw as a function of
              !    * cumulative probability y
              !    * relative standard deviation sigma
              ! take care that the definition of rind2 is consistent with subroutine
              ! tabulate_xcw

              rind1 = y(il) * (n1 - 1) + 1.0
              ind1  = max(1, min(int(rind1), n1 - 1))
              rind1 = rind1 - ind1
              rind2 = 40.0 * sigma_qcw(il,k) - 3.0
              ind2  = max(1, min(int(rind2), n2 - 1))
              rind2 = rind2 - ind2

              zcw = (1.0 - rind1) * (1.0 - rind2) * xcw(ind1,ind2) &
                    + (1.0 - rind1) * rind2       * xcw(ind1,ind2 + 1) &
                    + rind1 * (1.0 - rind2)       * xcw(ind1 + 1,ind2) &
                    + rind1 * rind2             * xcw(ind1 + 1,ind2 + 1)
            end if

            ! a horizontally constant iwc/lwc ratio is assumed for each layer so far
            l_cld(il)             = .true.
            clw_sub(il,k,i_loc(il)) = zcw * avg_qlw(il,k)
            cic_sub(il,k,i_loc(il)) = zcw * avg_qiw(il,k)
          end if

        end do           ! il
      end do              ! k

      ! need to check if a cloudy subcolumn was generated
      do il = il1, il2
        if (l_cld(il)) then
          i_loc(il) = i_loc(il) + 1
          l_cld(il) = .false.
        end if
      end do
    end do                 ! i
  else
    write( * , * ) "Invalid value for IOVERLAP = ",ioverlap,"."
    write( * , * ) "Valid values are 0, 1, and 2."
    write( * , * ) "Stopping program in CLD_GENERATOR."
    stop
  end if ! ioverlap

  ! record the number of cloudy subcolumns generated
  do il = il1, il2
    ncldy(il) = i_loc(il) - 1
  end do ! il

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
