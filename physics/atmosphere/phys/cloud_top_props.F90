!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine cloud_top_props(reliq,reice,cldliq, & ! output
                           cldice,cldtoplnc, &
                           col_lnc, &
                           clw_sub, cic_sub, rel_sub, & ! input
                           rei_sub, cdd, dz, &
                           cldwatmin,ilg,il1,il2,ilev,nxloc)

  ! june 02, 2013 - jason cole.
  ! this subroutine computes the cloud top effective radius for liquid and ice.
  ! this subroutine only works when the stochastic cloud generator is turned on.
  ! the code is very simple and works subcolumn-by-subcolumn
  !  - first it finds the uppermost cloud by going downward from toa until
  !    encountering a layer with ice or liquid
  !  - a decision needs to be made whether the layer is ice or liquid (it can
  !    have both present) the one with the higher concentration is used to
  !    select the dominant cloud phase.
  !  - once this this decided we can accumulate things to compute the
  !    mean cloud radius and cloud fraction for liquid and ice clouds.
  !  - we also use this to compute the particle number concentrations.  they
  !    request it to be computed in two manners.  one is to use the concentration
  !    in the uppermost cloud and the second is to provide the vertical.

  implicit none

  !
  ! input data
  !

  real, intent(in), dimension(ilg,ilev,nxloc) :: clw_sub !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,nxloc) :: cic_sub !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,nxloc) :: rel_sub !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev,nxloc) :: rei_sub !< Variable description\f$[units]\f$

  real, intent(in), dimension(ilg,ilev)       :: cdd !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev)       :: dz   !< Layer thickness for thermodynamic layers \f$[m]\f$

  real, intent(in)                            :: cldwatmin   !< Minimum threshold for cloud water content \f$[kg/kg]\f$
  integer, intent(in)                         :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)                         :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)                         :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)                         :: ilev   !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in)                         :: nxloc   !< Number of cloud subcolumns in an atmospheric column \f$[unitless]\f$

  !
  ! output data
  !

  real, intent(out), dimension(ilg) :: reliq !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: reice !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: cldliq !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: cldice !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: cldtoplnc !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: col_lnc !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  !
  ! local data
  !

  integer                       :: il
  integer                       :: l
  integer                       :: icol
  real                          :: cwt
  real                          :: r_nxloc
  real, dimension(ilg,nxloc)    :: nconc_liq
  logical, dimension(ilg,nxloc) :: l_cld_top
  integer, dimension(ilg,nxloc) :: i_cld_top
  integer, dimension(ilg,nxloc) :: phase_cld_top ! 1 = liquid, 2 = ice

  !
  ! units
  !

  r_nxloc = 1.0/real(nxloc)

  ! initialize fields

  do icol = 1, nxloc
    do il = il1, il2
      l_cld_top(il,icol)     = .false.
      i_cld_top(il,icol)     = - 999
      phase_cld_top(il,icol) = - 999
      nconc_liq(il,icol)     = 0.0
    end do ! il
  end do ! icol

  do il = il1, il2
    reliq(il)     = 0.0
    reice(il)     = 0.0
    cldliq(il)    = 0.0
    cldice(il)    = 0.0
    cldtoplnc(il) = 0.0
    col_lnc(il)   = 0.0
  end do ! il

  ! work our way down from model top and look for the uppermost cloud and
  ! assign a cloud phase based on cloud water content.
  ! while working through the loop compute the vertical integral of cloud
  ! particle number concentration.

  do icol = 1, nxloc
    do l = 1,ilev
      do il = il1, il2
        cwt = clw_sub(il,l,icol) + cic_sub(il,l,icol)
        if (.not. l_cld_top(il,icol)) then ! no cloud top found
          if (cwt > cldwatmin) then
            l_cld_top(il,icol) = .true.
            i_cld_top(il,icol) = l

            if ((cic_sub(il,l,icol) <= 0.0) .or. &
                (clw_sub(il,l,icol) >= cic_sub(il,l,icol))) then ! liquid
              phase_cld_top(il,icol) = 1
            else if ((clw_sub(il,l,icol) <= 0.0) .or. &
                                            (cic_sub(il,l,icol) > clw_sub(il,l,icol))) then ! ice
              phase_cld_top(il,icol) = 2
            end if

          end if
        end if

        ! compute vertical integral
        if (cwt > cldwatmin) then
          if ((cic_sub(il,l,icol) <= 0.0) .or. &
              (clw_sub(il,l,icol) >= cic_sub(il,l,icol))) then ! liquid
            nconc_liq(il,icol) = nconc_liq(il,icol) &
                                 + cdd(il,l) * dz(il,l)
          end if
        end if
      end do ! il
    end do ! l
  end do ! icol

  ! now loop over columns and accumulate cloud properties

  do icol = 1, nxloc
    do il = il1, il2
      if (l_cld_top(il,icol)) then
        l = i_cld_top(il,icol)
        if (phase_cld_top(il,icol) == 1) then ! liquid
          reliq(il)     = reliq(il) + rel_sub(il,l,icol)
          cldtoplnc(il) = cldtoplnc(il) + cdd(il,l)
          col_lnc(il)   = col_lnc(il) + nconc_liq(il,icol)
          cldliq(il) = cldliq(il) + 1.0
        end if
        if (phase_cld_top(il,icol) == 2) then ! ice
          reice(il)    = reice(il) + rei_sub(il,l,icol)
          cldice(il)   = cldice(il) + 1.0
        end if
      end if
    end do ! il
  end do ! icol

  ! compute the appropriate means (radius is cloud-mean, cloud-fraction is grid-mean)

  do il = il1, il2
    if (cldliq(il) > 0.0) then
      reliq(il)     = reliq(il)/cldliq(il)
      cldtoplnc(il) = cldtoplnc(il)/cldliq(il)
      col_lnc(il)   = col_lnc(il)/cldliq(il)
      cldliq(il)    = cldliq(il) * r_nxloc
    else
      reliq(il)     = 0.0
      cldtoplnc(il) = 0.0
      col_lnc(il)   = 0.0
      cldliq(il)    = 0.0
    end if
    if (cldice(il) > 0.0) then
      reice(il)     = reice(il)/cldice(il)
      cldice(il)    = cldice(il) * r_nxloc
    else
      reice(il)     = 0.0
      cldice(il)    = 0.0
    end if
  end do ! il

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
