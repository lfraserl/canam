!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine intgtio2(gtrow,  gtrol, gcrow, &
                    il1,il2,ilg,delt,gmt,iday,mday,imask)
  !
  !     * apr 29/03 - m.lazare. 1,lon -> il1,il2.
  !     * oct 16/97 - m.lazare. previous version intgtio.
  !
  !     * gtrow  = ground temperature    for previous timestep.
  !     * gtrol  = ground temperature    at next physics time.
  !     * gcrow  = ground cover (-1.,0,+1.).
  !
  !     * il1    = start longitude index.
  !     * il2    = end   longitude index.
  !     * ilg    = dimension of longitude arrays.
  !     * delt   = model timestep in seconds.
  !     * gmt    = number of seconds in current day.
  !     * iday   = current julian day.
  !     * mday   = date of target sst.
  !     * imask  = switch to only do based on gcrow mask (imask=1) or
  !     *          to not use as mask, i.e. for tracer sources (imask=0).
  !
  implicit none
  real :: day
  real :: daysm
  real, intent(inout) :: delt  !< Timestep for atmospheric model \f$[seconds]\f$
  real :: fmday
  real, intent(inout) :: gmt  !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  integer :: i
  integer, intent(inout) :: iday  !< Julian calendar day of year \f$[day]\f$
  integer, intent(inout) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(inout) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(inout) :: imask
  integer, intent(inout) :: mday  !< Julian calendar day of next mid-month \f$[day]\f$
  real :: stepsm

  real, intent(inout), dimension(ilg) :: gtrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gtrol !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: gcrow !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !--------------------------------------------------------------------
  !     * compute the number of timesteps from here to mday.
  !
  day = real(iday) + gmt/86400.
  fmday = real(mday)
  if (fmday < day) fmday = fmday + 365.
  daysm = fmday - day
  stepsm = daysm * 86400./delt
  !
  !     * interpolate new gt only over sea (where gc=0.) for imask=1.
  !     * general interplation for imask=0.
  !
  do i = il1,il2
    if (imask == 1) then
      if (gcrow(i) == 0.) then
        gtrow  (i) = ((stepsm - 1.) * gtrow  (i) + gtrol  (i)) / stepsm
      end if
    else
      gtrow  (i)  = ((stepsm - 1.) * gtrow  (i) + gtrol  (i)) / stepsm
    end if
  end do ! loop 210
  !
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
