!> \file
!>\brief Compute the downward solar flux from model top to 1 hPa
!!
!! @author Jiangnan Li
!
subroutine strandn3(trant, attn, attntop, rmu, dp, dt, o3, o2, &
                    rmu3,itile, &
                    ib, ig, lev1, il1, il2, ilg, lay, lev, ntile)
  !
  !     * feb 09,2009 - j.li.     new version for gcm15h:
  !     *                         - o2 passed directly, thus no need
  !     *                           for "trace" common block.
  !     * apr 21,2008 - l.solheim/ previous version strandn2 for gcm15g:
  !     *               j.li.      - cosmetic change to add threadprivate
  !     *                            for common block "trace", in support
  !     *                            of "radforce" model option.
  !     *                          - more accuate treatment of o3
  !     * apr 25,2003 - j.li.  previous version strandn for gcm15e/f.
  !----------------------------------------------------------------------c
  !     calculation of the downward flux from top level to 1 mb, no      c
  !     scattering effect is considered                                  c
  !                                                                      c
  !     tran:    transmisivity                                           c
  !     attn:    attenuation factor for reducing solar flux from model   c
  !              top level to 1 mb                                       c
  !     attntop: attenuation factor for reducing solar flux from toa to  c
  !              model top level                                         c
  !     rmu:     cosine of solar zenith angle                            c
  !     dp:      air mass path for a model layer (exlained in raddriv).  c
  !     o3:      o3 mass mixing ratio                                    c
  !----------------------------------------------------------------------c
  use ckdsw4, only : cs1o21, cs1o3
  implicit none
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay  !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: lev1  !< Vertical level at which pressure becomes greater than 1 hPa \f$[unitless]\f$
  integer, intent(in) :: ntile  !< Number of surface tiles in an atmospheric column \f$[unitless]\f$
  !
  real, intent(inout), dimension(ilg,ntile,2,lev) :: trant !< Transmisivity for each tile \f$[W/m^2]\f$
  real, intent(inout), dimension(ilg) :: attn !< Attenuation due to O3 in layer between model top and top of atmosphere \f$[W/m^2]\f$
  real, intent(inout), dimension(ilg) :: attntop !< Attenuation due to O3 in layer between model top and top of atmosphere \f$[W/m^2]\f$
  real, intent(in), dimension(ilg) :: rmu !< Factor to adjust solar zenith due to curvature and refraction \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dp !< Airmass path of a layer \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< Layer temperature - 250 K \f$[K]\f$
  real, intent(in), dimension(ilg,lay) :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: o2 !< O2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg) :: rmu3 !< Factor to adjust solar zenith due to curvature and refraction \f$[1]\f$
  integer, intent(in), dimension(ilg,ntile) :: itile !< Index of surface tile number \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real, dimension(ilg,2,lev) :: tran !< Transmisivity \f$[W/m^2]\f$
  real :: dto3
  integer :: i
  integer :: it
  integer :: k
  integer :: kp1
  integer :: lev1m1
  real :: tau
  real :: x
  !=======================================================================
  lev1m1 =  lev1 - 1
  !
  if (ib == 1) then
    !
    !----------------------------------------------------------------------c
    !     band (14500 - 50000 cm-1), nongray gaseous absorption of o3     c
    !     ans o2.                                                          c
    !----------------------------------------------------------------------c
    !
    do i = il1, il2
      tran(i,1,1)       =  attntop(i)
      tran(i,2,1)       =  attntop(i)
    end do ! loop 10
    !
    if (ig == 1) then
      do k = 1, lev1m1
        kp1 = k + 1
        do i = il1, il2
          dto3          =  dt(i,k) + 23.13
          x             = (cs1o21 - 0.881e-05 * rmu3(i)) * o2(i,k)
          tau           = ((cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                          dto3 * cs1o3(3,ig))) * o3(i,k) + x) * &
                          dp(i,k)
          tran(i,1,kp1) =  tran(i,1,k) * exp( - tau / rmu(i))
          tran(i,2,kp1) =  tran(i,1,kp1)
        end do ! loop 100
      end do ! loop 150
      !
    else
      do k = 1, lev1m1
        kp1 = k + 1
        do i = il1, il2
          dto3          =  dt(i,k) + 23.13
          tau           = (cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                          dto3 * cs1o3(3,ig))) * o3(i,k) * dp(i,k)
          tran(i,1,kp1) =  tran(i,1,k) * exp( - tau / rmu(i))
          tran(i,2,kp1) =  tran(i,1,kp1)
        end do ! loop 200
      end do ! loop 250
    end if
    !
    !----------------------------------------------------------------------c
    !     flux adjustment for region below 1 mb                            c
    !----------------------------------------------------------------------c
    !
    do i = il1, il2
      attn(i)           =  tran(i,1,lev1)
    end do ! loop 400
    !
  else
    !
    do k = 1, lev1
      do i = il1, il2
        tran(i,1,k)       =  1.0
        tran(i,2,k)       =  1.0
      end do
    end do ! loop 500
    !
    do i = il1, il2
      attn(i)           =  1.0
    end do ! loop 600
  end if
  do k = 1, lev1
    do it = 1, ntile
      do i = il1, il2
        if (itile(i,it) > 0) then
          trant(i,it,1,k) =  tran(i,1,k)
          trant(i,it,2,k) =  tran(i,2,k)
        end if
      end do ! i
    end do ! it
  end do ! k
  !
  return
end subroutine strandn3
!> \file
!>  Calculate the downward solar flux from top of atmosphere to 1 hPa. No
!! scattering effect is considered.
