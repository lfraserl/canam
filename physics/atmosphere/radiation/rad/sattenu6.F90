!> \file
!>\brief Attenuation of solar radiation between the top of model and top of atmosphere
!!
!! @author Jiangnan Li
!
subroutine sattenu6 (atten, ib, ig, rmu, o3, co2, ch4, o2, dp, &
                     dip, dt, dt0, inpt, gh, il1, il2, ilg, lay)
  !
  !     * jun 22,2013 - j.cole.   set the radiative effect of the moon
  !     *                         layer to zero at end (atten=1.),
  !     *                         so that balt=balx (full transmission).
  !     * may 01,2012 - m.lazare. previous version sattenu5 for gcm16:
  !     *                         - calls attenue5 instead of attenue4.
  !     * feb 09,2009 - j.li.     previous version sattenu4 for gcm15h/i:
  !     *                         - 3d ghg implemented, thus no need
  !     *                           for "trace" common block or
  !     *                           temporary work arrays to hold
  !     *                           mixing ratios of ghg depending on
  !     *                           a passed, specified option.
  !     *                         - calls attenue4 instead of attenue3.
  !     * apr 21,2008 - l.solheim/ previous version sattenu3 for gcm15g:
  !     *               m.lazare/  - cosmetic change to add threadprivate
  !     *               j.li.        for common block "trace", in support
  !     *                            of "radforce" model option.
  !     *                          - calls attenue3 instead of attenue2.
  !     *                          - update o3 and add ch4 effect.
  !     * may 05,2006 - m.lazare. previous version sattenu2 for gcm15e/f:
  !     *                         - pass integer :: variables "init" and
  !     *                           "nit" instead of actual integer
  !     *                           values, to "attenue" routines.
  !     * original version sattenu by jiangnan li.
  !----------------------------------------------------------------------c
  !     calculation of solar attenuation above the model top level. for  c
  !     band1 only o3 and o2 are considered, the contribution of other   c
  !     gases is small. for band 3 and 4, co2 is considered for gh       c
  !                                                                      c
  !     atten: attenuation factor for downward flux from toa to the      c
  !            model top level                                           c
  !     o3:    averged o3 mass mixing ratio above the model top level    c
  !     co2:   co2 mass mixing ratio at model top layer                  c
  !     o2:    o2 mass mixing ratio at model top layer                   c
  !     dp:    here dp is only the pressure difference, different from   c
  !            that defined in raddriv. so there is a factor 1.02        c
  !     dip:   interpretation factor for pressure                        c
  !     dt:    layer temperature - 250 k                                 c
  !     dt0:   temperature in moon layer - 250 k                         c
  !----------------------------------------------------------------------c
  use ckdsw4, only : cs1o21, cs1o2gh3, cs1o3, cs1o3gh, &
                     cs2o2gh, cs3co2gh, cs4ch4gh, cs4co2gh, ntl
  implicit none
  integer, intent(in) :: ib
  integer, intent(in) :: ig
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay    !< Number of vertical layers \f$[unitless]\f$
  !
  real, intent(inout), dimension(ilg)  :: atten !< Attenuated transmission factor for solar \f$[1]\f$
  real, intent(in), dimension(ilg)     :: rmu !< Cosine of zenith angle \f$[1]\f$
  real, intent(in), dimension(ilg)     :: o3 !< O3 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: co2 !< CO2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: ch4 !< CH4 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg,lay) :: o2 !< O2 mixing ratio \f$[gram/gram]\f$
  real, intent(in), dimension(ilg)     :: dp !< Airmass path of layer layer between model top and top of atmosphere \f$[gram/cm^2]\f$
  real, intent(in), dimension(ilg)     :: dip !< Interpretation between two neighboring standard input pressure levels \f$[1]\f$
  real, intent(in), dimension(ilg,lay) :: dt !< Layer temperature - 250 K \f$[K]\f$
  real, intent(in), dimension(ilg)     :: dt0 !< Temperature in layer between model top and top of atmosphere - 250 K \f$[K]\f$
  integer, intent(in), dimension(ilg)  :: inpt !< Level number of the slected standard input pressures \f$[1]\f$
  logical, intent(in) :: gh !< If true use optically thick gas group \f$[1]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  real :: dto3
  integer :: i
  integer :: im
  integer :: isl
  real :: tau
  real, dimension(ilg) :: mmr !< Placeholder for gas concentration (mixing ratio) at model top
  real, dimension(ilg) :: tt0 !< Temperature in layer between model top and top of atmosphere - 250K
  real, pointer, dimension(:, :) :: coeff
  !
  !=======================================================================
  !
  !     * set the radiative effect of the moon layer to one (full
  !     * transmission).
  !
  do i = il1, il2
    atten(i) =  1.0
  end do

  return
  !
  if (ib == 1) then
    if (gh) then
      if (ig == 3) then
        do i = il1, il2
          dto3     =  dt(i,1) + 23.13
          tau      =  1.02 * ((cs1o3gh(1,ig) + dto3 * &
                     (cs1o3gh(2,ig) + dto3 * cs1o3gh(3,ig))) * &
                     o3(i) +  cs1o2gh3 * o2(i,1)) * dp(i)
          atten(i) =  exp( - tau / rmu(i))
        end do ! loop 100
      else
        do i = il1, il2
          dto3     =  dt(i,1) + 23.13
          tau      =  1.02 * (cs1o3gh(1,ig) + dto3 * &
                     (cs1o3gh(2,ig) + dto3 * cs1o3gh(3,ig))) * o3(i) * dp(i)
          atten(i) =  exp( - tau / rmu(i))
        end do ! loop 110
      end if
    else
      !
      if (ig == 1) then
        do i = il1, il2
          dto3     =  dt(i,1) + 23.13
          tau      =  1.02 * ((cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                     dto3 * cs1o3(3,ig))) * o3(i) + &
                     cs1o21 * o2(i,1)) * dp(i)
          atten(i) =  exp( - tau / rmu(i))
        end do ! loop 120
      else
        do i = il1, il2
          dto3     =  dt(i,1) + 23.13
          tau      =  1.02 * (cs1o3(1,ig) + dto3 * (cs1o3(2,ig) + &
                     dto3 * cs1o3(3,ig))) * o3(i) * dp(i)
          atten(i) =  exp( - tau / rmu(i))
        end do ! loop 130
      end if
    end if
    !
  else if (ib == 2) then
    if (ig == 1) then
      do i = il1, il2
        atten(i)   =  1.0
      end do ! loop 200
    else
      im = ig - 1
      isl = 1
      coeff => cs2o2gh(:, :, im)
      mmr = o2(:, 1)
      tt0 = dt(:, 1)
      call attenue5 (atten, coeff, mmr, dp, dip, tt0, &
                     dt0, rmu, inpt, ntl, isl, il1, il2, ilg)
    end if
    !
  else if (ib == 3) then
    isl = 1
    mmr = co2(:, 1)
    tt0 = dt(:, 1)
    coeff => cs3co2gh(:, :, ig)
    call attenue5 (atten, coeff, mmr, dp, dip, tt0, dt0, &
                   rmu, inpt, ntl, isl, il1, il2, ilg)
    !
  else if (ib == 4) then
    isl = 1
    if (ig /= 4 .and. ig /= 6 .and. ig /= 8) then
      if (ig <= 3)  im =  ig
      if (ig == 5)  im =  ig - 1
      if (ig == 7)  im =  ig - 2
      if (ig == 9)  im =  ig - 3
      coeff => cs4co2gh(:, :, im)
      mmr = co2(:, 1)
      tt0 = dt(:, 1)
      call attenue5 (atten, coeff, mmr, dp, dip, tt0, &
                     dt0, rmu, inpt, ntl, isl, il1, il2, ilg)
    else if (ig == 4) then
      mmr = ch4(:, 1)
      tt0 = dt(:, 1)
      call attenue5 (atten, cs4ch4gh, mmr, dp, dip, tt0, dt0, rmu, &
                     inpt, ntl, isl, il1, il2, ilg)
    else
      do i = il1, il2
        atten(i)   =  1.0
      end do ! loop 400
    end if
  end if
  !
  return
end subroutine sattenu6
!> \file
!>  Calculation of solar attenuation above the model top level. for
!! band1 only o3 and o2 are considered, the contribution of other
!! gases is small. for band 3 and 4, co2 is considered for gh.
