!> \file
!>\brief Compute cloud optical properties at thermal wavelengths
!!
!! @author Jiangnan Li
!
subroutine lw_cld_props_4str(tauci, omci, gci, psi, f2,     & ! output
                         cld, wclw, wcli, radeqvw, radeqvi, & ! input
                         cut, il1, il2, ilg, lay, ib, nstrm)  ! control
  !
  !     * feb 10/2015 - j.li.     new version for gcm18+:
  !     *                         - revised ice cloud optical properties
  !     *                           from yang et al (2012).
  !     * feb 10/2009 - m.lazare. previous version lw_cld_props2 for gcm15h:
  !     *                         - calculate optical properties regardless
  !     *                           of cloud layer water content, it no
  !     *                           cutoff to zero below certain threshold.
  !     * oct 22/2007 - j.cole/   previous version lw_cld_props for gcm15g:
  !     *               m.lazare. compute cloud l/w optical properties
  !     *                         required for radiation. it is composed
  !     *                         of three parts:
  !     *                          1. assigns "cld" to "cldg".
  !     *                          2. calculation of {taucl,omcl,gcl}
  !     *                             migrated from clouds15 routines in
  !     *                             physics (separated into s/w and l/w
  !     *                             separate components).
  !     *                          3. calculation of resulting optical
  !     *                             properties arrays required for
  !     *                             radiation.
  !     *                         note that this means {taucl,omcl,gcl}
  !     *                         merely become scalars.
  !
  use datcldop4, only :  awl => awl_4str, bwl => bwl_4str, cwl => cwl_4str, &
                         ail, bil, cil
  implicit none
  !
  !     * output data.
  !
  real, intent(out) , dimension(ilg,lay) :: tauci !< Cloud optical depth \f$[1]\f$
  real, intent(out) , dimension(ilg,lay) :: omci !< Cloud single scattering albedo \f$[1]\f$
  real, intent(out) , dimension(ilg,lay) :: gci !< Cloud asymmetry factor \f$[1]\f$
  real, intent(out) , dimension(ilg,lay,6) :: psi !< !<6 psi function for 4stream \f$[0]\f$
  real, intent(out) , dimension(ilg,lay) :: f2 !< Delta-Edington scaling factor, GCI\f$^2\f$ \f$[1]\f$
  !
  !     * input data.
  !
  real, intent(in) , dimension(ilg,lay) :: cld !< Input cloud fraction \f$[1]\f
  real, intent(in) , dimension(ilg,lay) :: wclw !< Cloud liquid water path \f$[gram/m^2]\f$
  real, intent(in) , dimension(ilg,lay) :: wcli !< Cloud ice water path \f$[gram/m^2]\f$
  real, intent(in) , dimension(ilg,lay) :: radeqvw !< Liquid cloud effective radius \f$[um]\f$
  real, intent(in) , dimension(ilg,lay) :: radeqvi !< Ice cloud effective radius \f$[um]\f$

  real, intent(in) :: cut !< Minimum threshold for input cloud to be considered for radaitive transfer \f$[1]\f$

  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: lay   !< Number of vertical layers \f$[unitless]\f$
  integer, intent(in) :: ib !< Longwave band number \f$[1]\f$
  integer, intent(in) :: nstrm !< Switch to 2 or 4 stream radiative transfer \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !
  !     * local data (scalar)
  real, parameter :: uu3 = 1.1036383  !< Factor for perturbation method in longwave radiative transfer solution \f$[1]\f$
  !
  real  :: rew1
  real  :: rew2
  real  :: rew3
  real  :: dg
  real  :: dg2
  real  :: dg3
  real  :: taulw
  real  :: omlw
  real  :: glw
  real  :: glw2
  real  :: glw3
  real  :: glw4
  real  :: tauli
  real  :: omli
  real  :: gli
  real  :: gli2
  real  :: gli3
  real  :: gli4
  real  :: taucl
  real  :: omcl
  real  :: gcl
  real  :: gcl2
  real  :: gcl3
  real  :: gcl4
  real  :: y1
  real  :: y2
  real  :: y12
  real  :: absli
  real  :: xinv
  real  :: x1
  real  :: x2
  real  :: x3
  integer :: i
  integer :: k
  !
  !----------------------------------------------------------------------c
  !     cloud radiative properties for radiation.                        c
  !     taucs, omcs, gcs (taucl, omcl, gcl): optical depth, single       c
  !     scattering albedo, asymmetry factor for solar (infrared).        c
  !     radeqvw: effective radius(in micrometer) for water cloud         c
  !     radeqvi: effective radius(in micrometer) for ice cloud           c
  !     dg: geometry length for ice cloud                                c
  !     wcdw (wcdi): liquid water (ice) content (in gram / m^3)          c
  !     wclw (wcli): liquid water (ice) path length (in gram / m^2)      c
  !     ccld: cloud fraction                                             c
  !     parameterization for water cloud:                                c
  !     dobbie, etc. 1999, jgr, 104, 2067-2079                           c
  !     lindner, t. h. and j. li., 2000, j. clim., 13, 1797-1805.        c
  !     parameterization for ice cloud:                                  c
  !     ice cloud optical property based on yang et al. (2012) jas       c
  !     and a serirs papers by group of u a &m and u wisconsin            c
  !     dg is the effective diameter                                     c

  !----------------------------------------------------------------------c
  !
  do k = 1, lay
    do i = il1, il2
      if (cld(i,k) <= cut) then
        tauci(i,k) = 0.0
        omci(i,k)  = 0.0
        gci(i,k)   = 0.0
        f2(i,k)    = 0.0
        psi(i,k,:) = 0.0
      else
        rew1 = radeqvw(i,k)
        rew2 = rew1 * rew1
        rew3 = rew2 * rew1
        dg   = 2.0 * min (max (radeqvi(i,k), 5.0), 60.0)
        dg2  = dg  * dg
        dg3  = dg2 * dg
        !
        taulw = wclw(i,k) * (awl(1,ib) + awl(2,ib) * rew1 + &
                awl(3,ib) / rew1 + awl(4,ib) / rew2  + &
                awl(5,ib) / rew3)
        omlw  = 1.0 - (bwl(1,ib) + bwl(2,ib) / rew1 + &
                bwl(3,ib) * rew1 + bwl(4,ib) * rew2)
        glw   = cwl(1,ib,1) + cwl(2,ib,1) / rew1 + &
                cwl(3,ib,1) * rew1 + cwl(4,ib,1) * rew2
        glw2  = cwl(1,ib,2) + cwl(2,ib,2) / rew1 + &
                cwl(3,ib,2) * rew1 + cwl(4,ib,2) * rew2
        glw3  = cwl(1,ib,3) + cwl(2,ib,3) / rew1 + &
                cwl(3,ib,3) * rew1 + cwl(4,ib,3) * rew2
        glw4  = cwl(1,ib,4) + cwl(2,ib,4) / rew1 + &
                cwl(3,ib,4) * rew1 + cwl(4,ib,4) * rew2
        !
        !----------------------------------------------------------------------c
        !     since in yang, it is a param for absorptance depth
        !----------------------------------------------------------------------c
        !
        absli = ail(1,ib) + ail(2,ib) / dg + ail(3,ib) / dg2
        omli  = 1.0 - (bil(1,ib) + bil(2,ib) * dg + &
                bil(3,ib) * dg2 + bil(4,ib) * dg3)
        tauli = wcli(i,k) * (absli / (1.0 - omli))
        gli   = cil(1,ib,1) + cil(2,ib,1) * dg + cil(3,ib,1) * dg2 + &
                cil(4,ib,1) * dg3
        gli2  = cil(1,ib,2) + cil(2,ib,2) * dg + cil(3,ib,2) * dg2 + cil(4,ib,2) * dg3
        gli3  = cil(1,ib,3) + cil(2,ib,3) * dg + cil(3,ib,3) * dg2 + cil(4,ib,3) * dg3
        gli4  = cil(1,ib,4) + cil(2,ib,4) * dg + cil(3,ib,4) * dg2 + cil(4,ib,4) * dg3
        !
        taucl    =  taulw + tauli
        if (taucl > 0.) then
          y1    = omlw * taulw
          y2    = omli * tauli
          y12   = y1 + y2
          omcl  = y12 / taucl
          gcl   = (glw * y1 + gli * y2) / y12
          gcl2  = (glw2 * y1 + gli2 * y2) / y12
          gcl3  = (glw3 * y1 + gli3 * y2) / y12
          gcl4  = (glw4 * y1 + gli4 * y2) / y12
       else
          omcl  = 0.
          gcl   = 0.
          gcl2  = 0.0
          gcl3  = 0.0
          gcl4  = 0.0
        end if
        !
        tauci(i,k) = taucl
        omci(i,k)  = omcl * tauci(i,k)

        if (nstrm == 2) then
           f2(i,k) = gcl2
           psi(i,k,:) = 0.0

        else if (nstrm == 4) then
!...  aerosol scattering in longwave is not considered, uu3 see li (2002)
!     psi1 = psi(11), psi2=psi(12)=psi(21), psi3=psi(22), psi4 = psi(1-1),
!     psi5=psi(1-2)=psi(2-1), psi6=psi(2-2)
!
           f2(i,k)    =  gcl4
           xinv       = 1.0 / (1.0 - f2(i,k))
           x1         =  3.0 * (gcl - f2(i,k)) * xinv
           x2         =  5.0 * (gcl2 - f2(i,k)) * xinv
           x3         =  7.0 * (gcl3 - f2(i,k)) * xinv
!
!  in psi(i,k,1) and psi(i,k,3), factor of 2 because the weight = 0.5
!
           psi(i,k,1) =  2.0 - 0.5 * (1.0 + 0.0446582 * x1 + 0.1875000 * x2 + 0.0860800 * x3)
           psi(i,k,2) = -0.5 * (1.0 + 0.1666667 * x1 - 0.1875000 * x2 - 0.0127315 * x3)
           psi(i,k,3) =  2.0 - 0.5 * (1.0 + 0.6220084 * x1 + 0.1875000 * x2 + 0.0018830 * x3)
           psi(i,k,4) = -0.5 * (1.0 - 0.0446582 * x1 + 0.1875000 * x2 - 0.0860800 * x3)
           psi(i,k,5) = -0.5 * (1.0 - 0.1666667 * x1 - 0.1875000 * x2 + 0.0127315 * x3)
           psi(i,k,6) = -0.5 * (1.0 - 0.6220084 * x1 + 0.1875000 * x2 - 0.0018830 * x3)
        end if

        gci(i,k)   = (gcl - f2(i,k)) / (1.0 - f2(i,k))
        gci(i,k)   =  - 0.5 * (1.0 - uu3 * gci(i,k))
      end if
    end do
  end do

  return
end subroutine lw_cld_props_4str
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
