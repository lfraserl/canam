!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine precl2(pdr,pcd,idr,icd, &
                        xvf,n)

  !*********************************************************************
  !*                                                                   *
  !*            subroutine precl2                                       *
  !*                                                                   *
  !*********************************************************************
  !
  !       calculate the interpolation coefficients which link
  !  temperature and cooling rate grids. second order interpolation is used.
  !                  v.fomichev, may 1996.
  !                 - modified for ckd: v. fomichev, november, 2004.
  !                 - modified by m. lazare for gcm15j, april, 2011.
  !
  ! called by driver (after thermdat2, before any model loops)
  ! calls detint
  !

  implicit none
  integer :: i
  integer :: k
  real :: xc
  real :: xr
  real :: z
  real :: z1
  real :: z2

  !  input: n     - number of model levels
  !         xvf   - model levels (mid-layer, defined in thermdat2)

  real, intent(in), dimension(n) :: xvf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(3,n) :: pcd !< Variable description\f$[units]\f$
  real, intent(inout), dimension(3,67) :: pdr !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(n) :: icd !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(67) :: idr !< Variable description\f$[units]\f$
  integer, intent(in) :: n !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  ! output: (eventually stored in common block /vfclpr/):
  !    pdr(3,67)- coefficients for interpolation from x(n) to xr(67) grid
  !    idr(67)- the indexes of levels (for x(n)) for second order
  !             interpolation from x(n) to xr(67) grid
  !    pcd(3,n),icd(n) - coefficients and indexes of levels for
  !                      interpolation from xc(59) to x(n)
  !
  common /pirgrd/ xr(67), xc(59)
  !-------------------------------------------------------------
  !     to form pdr(3,67),idr(67) arrays

  do i=1,67
    if (xr(i)<=xvf(1)) then
      pdr(1,i) = 1.
      pdr(2,i) = 0.
      pdr(3,i) = 0.
      idr(i) = 1
    else
      call detint(xr(i),xvf,n,z,z1,z2,k)
      pdr(1,i)=z
      pdr(2,i)=z1
      pdr(3,i)=z2
      idr(i)=k
    end if
  end do ! loop 3

  !     to form the pcd(3,n),icd(n) arrays

  do i=1,n
    call detint(xvf(i),xc,59,z,z1,z2,k)
    pcd(1,i)=z
    pcd(2,i)=z1
    pcd(3,i)=z2
    icd(i)=k
  end do ! loop 4

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
