!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
function a18lin(x,xn,yn,m,n)

  !****************************************************************************
  !*                                                                          *
  !*                            function a18lin                               *
  !*                                                                          *
  !****************************************************************************
  !
  ! linear interpolation
  !
  ! called by cco2gr
  ! calls nothing
  !
  ! input:
  !  x - argument for which a value of function should be found
  !  xn(n),yn(n) - values of function yn(n) at xn(n) grid. x(n) should be
  !                ordered so that x(i-1) < x(i).
  ! output:
  !  a18lin - value of function for x


  implicit none
  real :: a18lin
  integer :: i
  integer :: k
  integer, intent(in) :: m
  integer, intent(in) :: n
  real, intent(in) :: x
  real, intent(in), dimension(n) :: xn !< Variable description\f$[units]\f$
  real, intent(in), dimension(n) :: yn !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================


  k=m-1
  do i=m,n
    k=k+1
    if ((x-xn(i)) .le. 0) exit
  end do
  if (k==1) k=2

  ! k has been found so that xn(k)<=x<xn(k+1)

  a18lin=(yn(k)-yn(k-1))/(xn(k)-xn(k-1))*(x-xn(k))+yn(k)
  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
