!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine spheric1(heat,pmb,t,cmo3,qof,rmu0, &
                          xvf,o2vf,amuvf,cpvf,gvf,effh,effsrc,to2, &
                          kmx,kmxp,lmx,il1,il2, &
                          to3,alt,hmto3,hmto2,indx)
  !***********************************************************************
  !*                                                                     *
  !*              subroutine spheric1                                    *
  !*                                                                     *
  !***********************************************************************
  !
  ! to calculate the solar heating due to o2 and o3 taking into account
  ! effect of sphericity (sun below the horizont).
  ! used parameterizations:
  ! - strobel (jrg, vol 83, p 6225, 1978) for
  !   o3 bands: hartley (242.5-277.5 nm), huggins (277.5-360 nm),
  !             herzberg (206-242.5 nm)
  !   o2 spectral regions: herzberg contiuum ((206-242.5 nm),
  !                        schumann-runge bands (175-205) & continuum (125-175)
  ! - shine and rickaby (in "Ozone in the Atmosphere", edited by r.d. bojkov and
  !                    p. fabian, 597--600, a. deepak, hampton, virginia, 1989
  !   for o3 chappuis band (407.5-852.5 nm)
  ! - mlynczack &solomon (jgr, vol 98, p 10517, 1993) for
  !   heating efficiency in o2 src and o3 region shorter than 305.5 nm
  ! - rodgers formula for atmospheric mass
  !                                               v fomichev, october, 1999

  ! called by mamrad
  ! calls nothing

  implicit none
  real :: am
  real :: ch
  real :: effha
  real :: ehu1
  real :: ehz
  real :: go2
  real :: go3
  real :: grav
  real :: ha
  real :: heato2
  real :: heato3
  real :: hm
  real :: hscal
  real :: hu
  real :: hvsp
  real :: hz_o2
  real :: hz_o3
  integer :: i
  integer :: il
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer :: k
  integer :: km
  integer :: kmax
  integer :: kmin
  integer, intent(in) :: kmx
  integer, intent(in) :: kmxp
  integer :: kp
  integer :: kr
  integer :: krp
  integer, intent(in) :: lmx
  real :: srb
  real :: src
  real :: src1
  real :: src2
  real :: srfact

  ! output array (heating in k/s)

  real, intent(inout), dimension(lmx,kmx) :: heat !< Variable description\f$[units]\f$

  ! input arrays: pressure (mb) and temperature (K) at layer's interface,
  !               cosine of solar zenith angle, o3 mass mixing ratio at
  !               mid-layer and o3 content in a layer (cm-atm).

  real, intent(in), dimension(lmx,kmxp) :: pmb !< Variable description\f$[units]\f$
  real, intent(in), dimension(lmx,kmx) :: t !< Variable description\f$[units]\f$
  real, intent(in), dimension(lmx,kmx) :: cmo3 !< Variable description\f$[units]\f$
  real, intent(in), dimension(lmx,kmx) :: qof !< Variable description\f$[units]\f$
  real, intent(in), dimension(lmx) :: rmu0 !< Variable description\f$[units]\f$

  real, intent(in), dimension(kmx) :: xvf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: o2vf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: amuvf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: cpvf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: gvf !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: effh !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: effsrc !< Variable description\f$[units]\f$
  real, intent(in), dimension(kmx) :: to2 !< Variable description\f$[units]\f$

  !     * internal arrays

  real, intent(inout), dimension(lmx,kmx) :: to3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lmx,kmx) :: alt !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lmx,kmx) :: hmto3 !< Variable description\f$[units]\f$
  real, intent(inout), dimension(lmx,kmx) :: hmto2 !< Variable description\f$[units]\f$
  integer, intent(inout), dimension(lmx,kmx) :: indx !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================
  !------------------------------------------------------------------
  ! calculate geometric altitude (in meters) from the center of earth up
  !
  do i=il1,il2
    hscal=8.31441e3*t(i,1)/(amuvf(1)*gvf(1))
    alt(i,1)=hscal*0.5*log(pmb(i,1)/pmb(i,2))+6.37e6
  end do ! loop 10
  !
  do k=2,kmx
    am=0.5*(amuvf(k)+amuvf(k-1))
    grav=0.5*(gvf(k)+gvf(k-1))
    km=k-1
    kp=k+1
    do i=il1,il2
      hscal=8.31441e3*t(i,k)/(am*grav)
      alt(i,k)=alt(i,km)+hscal*0.5*log(pmb(i,km)/pmb(i,kp))
    end do ! loop 20
  end do ! loop 30
  !
  ! calculate o3 column amount (in cm-2) from bottom up
  !
  do i=il1,il2
    to3(i,kmx)=2.687e19*0.5*qof(i,kmx)
  end do ! loop 40
  !
  do k=2,kmx
    kr=kmxp-k
    krp=kr+1
    do i=il1,il2
      to3(i,kr)=to3(i,krp)+2.687e19*0.5*(qof(i,kr)+qof(i,krp))
    end do ! loop 50
  end do ! loop 60
  !
  !     * calculate necessary arrays to determine column o2,o3 amounts
  !     * above the tangent height.
  !     * THIS LOOP WILL NOT VECTORIZE BECAUSE OF COMPUTED GO TO'S.
  !     * perhaps look at revising this for next version.
  !
  do k=1,kmx
    do il=il1,il2
      !
      !       * tangent height.
      !
      hm=alt(il,k)*sqrt(1.-rmu0(il)*rmu0(il))

      !       * calculations are to be done only if tangent height above the
      !       * surface by 10km

      if (hm>6.38e6) then
        indx(il,k)=1

        !         * to find the column o2 and o3 amount above the tangent height
        !         * linear interpolation.

        kmin=1
        kmax=k
        km=int((kmin+kmax)/2.)
        do while (km /= kmin)
          if (hm-alt(il,km) < 0) then
            kmax=km
          else
            kmin=km
          end if
          km=int((kmin+kmax)/2.)
        end do

        !         * 'km' has been found so that ALT(IL,km)<= hm <ALT(IL,km+1).

        hvsp=(hm-alt(il,km+1))/(alt(il,km+1)-alt(il,km))
        hmto3(il,k)=(to3(il,km+1)-to3(il,km))*hvsp + to3(il,km+1)
        hmto2(il,k)=(to2(km+1)-to2(km))*hvsp + to2(km+1)
      else
        indx(il,k)=0
      end if
    end do
  end do ! loop 70

  ! calculate solar o3 and o2 heating

  ! first, initialize factor multiplying srb and sr continuum heatings
  ! depending on where model top is.

  if (xvf(kmx)>8.3) then
    srfact=1.
  else
    srfact=0.
  end if
  !
  do k = 1, kmx

    ! efficiency for o3 heating at wavelength shorther than 305.5 nm

    effha=(effh(k)-0.26)/0.74

    do il = il1, il2

      if (rmu0(il) > 0.0) then

        ! **************** sun above the horizont ****************

        ! o3 and o2 column amount: downward looking paths
        go3=to3(il,k)*35./sqrt(1224.0*rmu0(il)*rmu0(il)+1.)
        go2=to2(k)*35./sqrt(1224.0*rmu0(il)*rmu0(il)+1.)

        ! o3 bands (ch - after shine & rickaby, others - after strobel):

        ch=4.1847e8*exp(-3.157e-21*go3)*(0.4967+ &
                                    0.5033)              ! chem.pot
        ehu1=exp(-1.7658e-19*go3)
        hu=(1.6935e27*(1-ehu1)*(0.75+ &
                           0.25) & ! chem.pot
      +1.1443e27*(ehu1-exp(-4.2249e-18*go3))*(0.75*effha+ &
                                          0.25))/go3     ! chem.pot
        ha=1.4707e10*exp(-8.8e-18*go3)*(0.7903*effha+ &
                                   0.2097)               ! chem.pot
        ehz=exp(-6.6e-24*go2-4.9e-18*go3)
        hz_o3=2.1363e9*ehz*(0.8079*effha+ &
                       0.1921)                           ! chem.pot

        heato3=(ch+hu+ha+hz_o3)*cmo3(il,k)

        ! o2: srb, and sr & hz continuums (after strobel, only net heating for sr):
        ! note: no src and srb in the stratosphere since conditions for wavelengths < 205nm
        !       do not exist !

        hz_o2=4.7695e3*ehz*(0.0633+ &
                       0.9367)                           ! chem.pot
        src1=2.716e6*exp(-1.e-17*go2)
        src2=(5.902e23*exp(-2.9e-19*go2)-3.312e23*exp(-1.7e-18*go2) &
                                -2.590e23*exp(-1.15e-17*go2))/go2
        src = (src1+src2)*effsrc(k)
        if (go2>1.e18) then
          srb=1./(1.113e-24*go2+5.712e-15*sqrt(go2))
        else
          srb=1.463e5
        end if

        heato2=(srfact*(src+srb)+hz_o2)*o2vf(k)

        heat(il,k)=(heato3+heato2)/(amuvf(k)*cpvf(k))

      else

        !       **************** sun below the horizont ****************


        !       * calculations are to be done only if tangent height above
        !       * the surface by 10km (see where "indx" calculated in earlier
        !       * loop)

        if (indx(il,k)==1) then


          ! o3 and o2 amount along the path from atm.lid to a given point

          go3=35.*(2.*hmto3(il,k)-to3(il,k))
          go2=35.*(2.*hmto2(il,k)-to2(k))

          ! o3 bands (ch - after shine & rickaby, others - after strobel):

          ch=4.1847e8*exp(-3.157e-21*go3)*(0.4967+ &
                                      0.5033)            ! chem.pot
          ehu1=exp(-1.7658e-19*go3)
          hu=(1.6935e27*(1-ehu1)*(0.75+ &
                             0.25) & ! chem.pot
        +1.1443e27*(ehu1-exp(-4.2249e-18*go3))*(0.75*effha+ &
                                            0.25))/go3   ! chem.pot
          ha=1.4707e10*exp(-8.8e-18*go3)*(0.7903*effha+ &
                                     0.2097)             ! chem.pot
          ehz=exp(-6.6e-24*go2-4.9e-18*go3)
          hz_o3=2.1363e9*ehz*(0.8079*effha+ &
                         0.1921)                         ! chem.pot

          heato3=(ch+hu+ha+hz_o3)*cmo3(il,k)

          ! o2: srb, and sr & hz continuums (after strobel, only net heating for sr):
          ! note: no src and srb in the stratosphere since conditions for wavelengths < 205nm
          !       do not exist !

          hz_o2=4.7695e3*ehz*(0.0633+ &
                         0.9367)                         ! chem.pot
          src1=2.716e6*exp(-1.e-17*go2)
          src2=(5.902e23*exp(-2.9e-19*go2)-3.312e23*exp(-1.7e-18*go2) &
                                  -2.590e23*exp(-1.15e-17*go2))/go2
          src = (src1+src2)*effsrc(k)
          if (go2>1.e18) then
            srb=1./(1.113e-24*go2+5.712e-15*sqrt(go2))
          else
            srb=1.463e5
          end if

          heato2=(srfact*(src+srb)+hz_o2)*o2vf(k)

          heat(il,k)=(heato3+heato2)/(amuvf(k)*cpvf(k))

        end if                                                 ! indx
      end if                                                   ! rmu0

    end do ! loop 80
  end do ! loop 90

  return
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
