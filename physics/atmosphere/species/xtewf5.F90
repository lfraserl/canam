!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xtewf5(xemis,ebwarow,eowarow,eswarow,dshj,pressg,zf, &
                  levwf, ntrac, ztmst, ilev, lev, il1, il2, ilg, &
                  ioco,iocy,ibco,ibcy,idms,iso2)
  !
  !     * jun 17/2013 - k.vonsalzen/ new version for gcm17+:
  !     *               m.lazare.    - add condition to set wgt=1. for
  !     *                              special case where hso2=lso2.
  !     * apr 28/2012 - k.vonsalzen/ previous version xtewf4 for gcm16:
  !     *               m.lazare.    - initialize lso2 and hso2 to
  !     *                              ilev instead of zero, to
  !     *                              avoid possible invalid
  !     *                              memory references.
  !     *                            - bugfix for incorrect memory
  !     *                              reference: lso2(l)->lso2(il).
  !     *                            - eliminate resetting of lso2 to
  !     *                              ilev if rlh is not positive.
  !     * apr 26/2010 - k.vonsalzen. previous version xtewf3 for gcm15i:
  !     *                            - zf and pf calculated one-time at
  !     *                              beginning of physics and zf
  !     *                              passed in, with internal calculation
  !     *                              thus removed and no need to pass
  !     *                              in shbj,t.
  !     *                            - emissions accumulated into general
  !     *                              "xemis" array rather than updating
  !     *                              xrow (xrow updated from xemis in
  !     *                              the physics).
  !     *                            - removal of diagnostic fields
  !     *                              calculation, so no need to pass
  !     *                              in "saverad" or "isvchem".
  !     *                            - wgt=0 (ie no emissions in layer)
  !     *                              if it is above top or below base
  !     *                             of emissions.
  !     * mar 30/2009 - k.vonsalzen.  previous version xtewf2 for gcm15h:
  !     *                             - correct units for oc emissions
  !     *                             - add diagnostic fields
  !     * mar 06/07 - x.ma.           previous version xtewf for gcm15g:
  !     *                             interpolate the emissions to the
  !     *                             model levels.
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(in) :: ibco  !< Tracer index for black carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: ibcy  !< Tracer index for black carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: idms  !< Tracer index for dimethyl sulfide \f$[unitless]\f$
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ioco  !< Tracer index for organic carbon (hydrophobic) \f$[unitless]\f$
  integer, intent(in) :: iocy  !< Tracer index for organic carbon (hydrophylic) \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: lev  !< Number of vertical levels plus 1 \f$[unitless]\f$
  integer, intent(in) :: levwf      !< Number of vertical layers for wildfire emissions input data \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  real, intent(in) :: ztmst
  !
  !     * input fields
  !
  real, intent(in), dimension(ilg,levwf) :: ebwarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levwf) :: eowarow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,levwf) :: eswarow !< Variable description\f$[units]\f$
  !
  !     * i/o fields.
  !
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev) :: zf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  !
  !     * internal work fields.
  !
  real :: dul
  real :: fact
  integer :: il
  integer :: jt
  integer :: k
  integer :: l
  real :: pom2oc
  real :: rlh
  real :: ruh
  real :: wgt
  real  , dimension(ilg,levwf)  :: eowa
  real  ,  dimension(ilg) :: t_ebwa
  real  ,  dimension(ilg) :: t_bwa
  real  ,  dimension(ilg) :: t_eowa
  real  ,  dimension(ilg) :: t_owa
  real  ,  dimension(ilg) :: t_eswa
  real  ,  dimension(ilg) :: t_swa
  integer, dimension(ilg) :: lso2
  integer, dimension(ilg) :: hso2
  !==================================================================
  ! physical (adjustable) parameters
  !
  !
  !     * local array...
  !     * height levels of input field used for interpolation.
  !
  real, dimension(6), parameter :: zref = [100., 500., 1000., 2000., 3000., 6000.]
  !==================================================================
  !
  !----------------------------------------------------------------------
  pom2oc = 1.4
  do k = 1,levwf
    do il = il1,il2
      eowa(il,k) = eowarow(il,k)/pom2oc
    end do
  end do
  !
  ! * for diagnostic purpose: to make sure the total emissions
  !        after interpolation are same as before.
  ! * t_e*wa: total from values before interpolation
  ! * t_*wa:  total from values after  interpolation
  !
  do il = il1, il2
    t_ebwa(il) = 0.
    t_eowa(il) = 0.
    t_eswa(il) = 0.
    t_bwa (il) = 0.
    t_owa (il) = 0.
    t_swa (il) = 0.
  end do ! loop 90
  !
  do k = 1, levwf

    ruh = zref(k)
    if (k == 1) then
      rlh = 0.
    else
      rlh = zref(k - 1)
    end if
    dul = ruh - rlh
    !
    !     * determine layer corresponding to so2 emissions.
    !
    do il = il1, il2
      lso2(il) = ilev
      hso2(il) = ilev
    end do ! loop 100

    do l = ilev, 1, - 1
      do il = il1, il2
        if (zf(il,l) <= rlh)   lso2(il) = l
        if (zf(il,l) <= ruh)   hso2(il) = l
      end do
    end do ! loop 105
    !
    !   * ebwa
    !
    do il = il1, il2
      t_ebwa(il) = t_ebwa(il) + ebwarow(il,k)
      t_eowa(il) = t_eowa(il) + eowa(il,k)
      t_eswa(il) = t_eswa(il) + eswarow(il,k)
    end do ! loop 108
    !
    do l = 1, ilev
      do il = il1, il2
        fact                   = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == hso2(il) .and. l == lso2(il)) then
          wgt = 1.
        else if (l == hso2(il)) then
          wgt = ( ruh - zf(il,l) )/dul
        else if (l > hso2(il) .and. l < lso2(il) ) then
          wgt = (zf(il,l - 1) - zf(il,l))/dul
        else if (l == lso2(il) ) then
          wgt = ( zf(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        t_bwa(il) = t_bwa(il) + wgt * ebwarow(il,k)
        xemis(il,l,ibco) = xemis(il,l,ibco) + 0.8 * wgt * ebwarow(il,k) * fact
        xemis(il,l,ibcy) = xemis(il,l,ibcy) + 0.2 * wgt * ebwarow(il,k) * fact
      end do
    end do ! loop 110
    !
    !   * eowa
    !
    do l = 1, ilev
      do il = il1, il2
        fact                 = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == hso2(il) .and. l == lso2(il)) then
          wgt = 1.
        else if (l == hso2(il)) then
          wgt = ( ruh - zf(il,l) )/dul
        else if (l > hso2(il) .and. l < lso2(il) ) then
          wgt = (zf(il,l - 1) - zf(il,l))/dul
        else if (l == lso2(il) ) then
          wgt = ( zf(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        t_owa(il) = t_owa(il) + wgt * eowa(il,k)
        xemis(il,l,ioco) = xemis(il,l,ioco) + 0.5 * wgt * eowa(il,k) * fact
        xemis(il,l,iocy) = xemis(il,l,iocy) + 0.5 * wgt * eowa(il,k) * fact
      end do
    end do ! loop 120
    !
    !   * eswa
    !
    jt = iso2
    !
    do l = 1, ilev
      do il = il1, il2
        fact                 = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == hso2(il) .and. l == lso2(il)) then
          wgt = 1.
        else if (l == hso2(il)) then
          wgt = ( ruh - zf(il,l) )/dul
        else if (l > hso2(il) .and. l < lso2(il) ) then
          wgt = (zf(il,l - 1) - zf(il,l))/dul
        else if (l == lso2(il) ) then
          wgt = ( zf(il,l - 1) - rlh)/dul
        else
          wgt = 0.
        end if
        t_swa(il) = t_swa(il) + wgt * eswarow(il,k)
        xemis(il,l,jt) = xemis(il,l,jt) + wgt * eswarow(il,k) * fact &
                         * 32.064/64.059
      end do
    end do ! loop 130
  end do ! loop 180
  !
  !   --------------------------------------------------------------
  !
  return
end subroutine xtewf5
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
