!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine xtevolc4(xemis, pressg, dshj, zf, &
                    escvrow, ehcvrow, esevrow, ehevrow, esvcrow, &
                    esverow, dsu, psuef, iso2, lsvchem, lpam, &
                    ilg, il1, il2, ilev, ntrac, ztmst)
  !
  !     * jun 18/2013 - k.vonsalzen/ new version for gcm17+:
  !     *               m.lazare.    - add condition to set wgt=1. for
  !     *                              special case where hso2=lso2.
  !     *                            - add support for the pla option
  !     *                              "doc" array, "ipam" switch now passed.
  !     *                            - remove "ehcvrow" and "ehevrow"
  !     *                              from if conditions.
  !     * apr 28/2012 - k.vonsalzen/ previous version xtevolc3 for gcm16:
  !     *               m.lazare.    - initialize lso2 and hso2 to
  !     *                              ilev instead of zero, to
  !     *                              avoid possible invalid
  !     *                              memory references.
  !     *                            - bugfix to include ratio of
  !     *                              molecular weights in
  !     *                              calculation of continuous
  !     *                              volcanic emissions.
  !     * apr 26/2010 - k.vonsalzen/ previous version xtevolc2 for gcm15i:
  !     *               m.lazare.    - zf passed in (calculated in
  !     *                              physics) rather than calculated
  !     *                              internally (no need to pass in
  !     *                              t or shbj).
  !     *                            - explosive volcanoes not controlled
  !     *                              by passed-in "iexplvol" switch
  !     *                              rather a new logical :: internal
  !     *                              variable "volcexp" is used
  !     *                              default "off") which can be
  !     *                              updated to be activated. we
  !     *                              do explosive volcanoes externally
  !     *                              to this routine generally now.
  !     *                            - emissions now stored into general
  !     *                              "emis" array, instead of updating
  !     *                              xrow at this point.
  !     *                            - bugfix to vertical loop 205 where
  !     *                              should start from "ilev" and not
  !     *                              "lev" (otherwise memory out of
  !     *                              bounds).
  !     *                            - more accurate calculation of "wgt".
  !     * feb 07,2009 - k.vonsalzen/ previous routine xtevolc for gcm15h:
  !     *               m.lazare:    - apply emissions from volcanoes.
  !     *                              this used to be in old xteaero.
  !     *               **note**:      we pass in and use integer :: switch
  !     *                              "iexplvol" to control whether
  !     *                              explosive volcano code in this
  !     *                              routine is activated (iexplvol=0)
  !     *                              or is handled else where in the
  !     *                              code (iexplvol=1).
  !
  use phys_consts, only : grav
  implicit none
  integer, intent(in) :: il1  !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2  !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: ilev  !< Number of vertical levels \f$[unitless]\f$
  integer, intent(in) :: ilg  !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: iso2  !< Tracer index for sulfur dioxide \f$[unitless]\f$
  integer, intent(in) :: ntrac  !< Total number of tracers in atmospheric model \f$[unitless]\f$
  logical, intent(in) :: lpam  !< Switch to use PAM aerosol scheme \f$[unitless]\f$
  logical, intent(in) :: lsvchem  !< Switch to save extra chemistry diagnostics \f$[unitless]\f$
  real, intent(in) :: psuef
  real, intent(in) :: ztmst
  !
  !     * i/o fields.
  !
  real, intent(inout), dimension(ilg,ilev,ntrac) :: xemis !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg,ilev) :: dshj   !< Thickness of thermodynamic layers in eta coordinates \f$[unitless]\f$
  real, intent(in), dimension(ilg) :: pressg   !< Surface pressure \f$[Pa]\f$
  real, intent(in), dimension(ilg,ilev)  :: zf !< Variable description\f$[units]\f$
  real, intent(inout), dimension(ilg,ilev) :: dsu !< Variable description\f$[units]\f$
  !
  real, intent(in), dimension(ilg) :: escvrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ehcvrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: esevrow !< Variable description\f$[units]\f$
  real, intent(in), dimension(ilg) :: ehevrow !< Variable description\f$[units]\f$
  !
  real, intent(out), dimension(ilg) :: esvcrow !< Variable description\f$[units]\f$
  real, intent(out), dimension(ilg) :: esverow !< Variable description\f$[units]\f$
  !
  !     * internal work fields.
  !
  real :: elcv
  real :: elev
  real :: eucv
  real :: euev
  real :: fact
  integer :: il
  integer :: jt
  integer :: l
  real :: wgt
  integer, dimension(ilg) :: lso2
  integer, dimension(ilg) :: hso2
  !==================================================================
  ! physical (adjustable) parameters
  !
  real, parameter :: wso2 = 64.059
  real, parameter :: wh2so4 = 98.073
  real, parameter :: wnh3 = 17.030
  real, parameter :: wamsul = wh2so4 + 2. * wnh3
  real, parameter :: wrat = wamsul/wso2
  real, parameter :: psueft = 0.
  !
  !     *         local flag to control explosive climatological
  !     *         (non-transient) volcanoes.
  !
  logical, parameter :: volcexp = .false. !< local flag to control explosive climatological
                                          !! (non-transient) volcanoes\f$[unitless]\f$
  !==================================================================
  !
  !     * escv: continous volcanic emissions
  !
  do il = il1,il2
    lso2(il) = ilev
    hso2(il) = ilev
    esvcrow(il) = 0.0
    esverow(il) = 0.0
  end do ! loop 200
  !
  do l = ilev,1, - 1
    do il = il1,il2
      if (zf(il,l) <= 0.67 * ehcvrow(il))   lso2(il) = l
      if (zf(il,l) <= 1.00 * ehcvrow(il))   hso2(il) = l
    end do
  end do ! loop 205
  !
  jt = iso2
  do l = 1,ilev
    do il = il1,il2
      if (escvrow(il) > 0.) then
        elcv = 0.67 * ehcvrow(il)
        eucv = 1.00 * ehcvrow(il)
        fact = ztmst * grav/(dshj(il,l) * pressg(il))
        if (l == hso2(il) .and. l == lso2(il)) then
          wgt = 1.
        else if (l == hso2(il)) then
          wgt = (eucv - zf(il,l))/(eucv - elcv)
        else if (l > hso2(il) .and. l < lso2(il)) then
          wgt = (zf(il,l - 1) - zf(il,l))/(eucv - elcv)
        else if (l == lso2(il)) then
          wgt = (zf(il,l - 1) - elcv)/(eucv - elcv)
        else
          wgt = 0.
        end if
        xemis(il,l,jt) = xemis(il,l,jt) + wgt * escvrow(il) * fact &
                         * 32.064/64.059 * (1. - psueft)
        if (lpam) dsu(il,l) = dsu(il,l) &
            + wgt * escvrow(il) * fact * wrat * psueft/ztmst
        if (lsvchem) then
          esvcrow(il) = wgt * escvrow(il) * 32.064/64.059
        end if
      end if
    end do
  end do ! loop 220
  !
  !     * esev: eruptive volcanic emissions
  !     * note ! these are only done if the local switch "volcexp" is .true.
  !
  if (volcexp) then
    do il = il1,il2
      lso2(il) = ilev
      hso2(il) = ilev
    end do ! loop 300
    !
    do l = ilev,1, - 1
      do il = il1,il2
        if (zf(il,l) <= ehevrow(il) + 500. )   lso2(il) = l
        if (zf(il,l) <= ehevrow(il) + 1500.)   hso2(il) = l
      end do
    end do ! loop 305
    !
    do l = 1,ilev
      do il = il1,il2
        if (esevrow(il) > 0.) then
          elev = ehevrow(il) + 500.
          euev = ehevrow(il) + 1500.
          fact = ztmst * grav/(dshj(il,l) * pressg(il))
          if (l == hso2(il) .and. l == lso2(il)) then
            wgt = 1.
          else if (l == hso2(il)) then
            wgt = (euev - zf(il,l))/(euev - elev)
          else if (l > hso2(il) .and. l < lso2(il)) then
            wgt = (zf(il,l - 1) - zf(il,l))/(euev - elev)
          else if (l == lso2(il)) then
            wgt = (zf(il,l - 1) - elev)/(euev - elev)
          else
            wgt = 0.
          end if
          xemis(il,l,jt) = xemis(il,l,jt) + wgt * esevrow(il) * fact &
                           * 32.064/64.059 * (1. - psueft)
          if (lpam) dsu(il,l) = dsu(il,l) &
              + wgt * esevrow(il) * fact * wrat * psueft/ztmst
          if (lsvchem) then
            esverow(il) = wgt * esevrow(il) * 32.064/64.059
          end if
        end if
      end do
    end do ! loop 320
  end if
  !
  return
end subroutine xtevolc4
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
