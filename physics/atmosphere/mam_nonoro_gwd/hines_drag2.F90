!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine hines_drag2 (flux_u,flux_v,drag_u,drag_v,alt,density, &
                        densb,m_alpha,specfac,dragil,m_min,slope, &
                        naz,il1,il2,lev1,lev2,nlons,nlevs,nazmth)
  !
  !  calculate zonal and meridional components of the vertical flux
  !  of horizontal momentum and corresponding wave drag (force per unit mass)
  !  on a longitude by altitude grid needed for the hines doppler spread
  !  gravity wave parameterization scheme.
  !
  !  aug. 6/95 - c. mclandress
  !
  !  modifications:
  !  --------------
  !  aug. 8/99 - c. mclandress (nonzero m_min for all other values of slope)
  !  feb. 2/96 - c. mclandress (added: minimum cutoff wavenumber m_min
  !                             12 and 16 azimuths; logical :: flags)
  !
  !  output arguements:
  !  ------------------
  !
  !     * flux_u  = zonal component of vertical momentum flux (pascals)
  !     * flux_v  = meridional component of vertical momentum flux (pascals)
  !     * drag_u  = zonal component of drag (m/s^2).
  !     * drag_v  = meridional component of drag (m/s^2).
  !
  !  input arguements:
  !  -----------------
  !
  !     * alt     = altitudes (m).
  !     * density = background density (kg/m^3).
  !     * densb   = background density at bottom level (kg/m^3).
  !     * m_alpha = cutoff vertical wavenumber (1/m).
  !     * specfac = ak_alpha * k_alpha.
  !     * dragil  = longitudes and levels where drag to be calculated.
  !     * dragil  = logical :: flag indicating longitudes and levels where
  !     *           calculations to be performed.
  !     * m_min   = minimum allowable cutoff vertical wavenumber.
  !     * slope   = slope of incident vertical wavenumber spectrum.
  !     * naz     = number of horizontal azimuths used (4, 8, 12 or 16).
  !     * il1     = first longitudinal index to use (il1 >= 1).
  !     * il2     = last longitudinal index to use (il1 <= il2 <= nlons).
  !     * lev1    = first altitude level to use (lev1 >=1).
  !     * lev2    = last altitude level to use (lev1 < lev2 <= nlevs).
  !     * nlons   = number of longitudes.
  !     * nlevs   = number of vertical levels.
  !     * nazmth  = azimuthal array dimension (nazmth >= naz).
  !
  !  constants in data statement.
  !  ----------------------------
  !
  !     * cos45 = cosine of 45 degrees.
  !     * cos30 = cosine of 30 degrees.
  !     * sin30 = sine of 30 degrees.
  !     * cos22 = cosine of 22.5 degrees.
  !     * sin22 = sine of 22.5 degrees.
  !
  !  subroutine arguements.
  !  ----------------------
  !
  implicit none
  integer, intent(in)  :: naz !< Variable description\f$[units]\f$
  integer, intent(in)  :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in)  :: lev1   !< Variable description\f$[units]\f$ 
  integer, intent(in)  :: lev2   !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlons !< Variable description\f$[units]\f$
  integer, intent(in)  :: nlevs !< Variable description\f$[units]\f$
  integer, intent(in)  :: nazmth !< Variable description\f$[units]\f$
  ! ccc      logical  :: dragil(nlons,nlevs)
  integer, intent(in), dimension(nlons,nlevs) :: dragil !< Variable description\f$[units]\f$
  real, intent(in)    :: m_min !< Variable description\f$[units]\f$
  real, intent(in)    :: slope !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: flux_u !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: flux_v !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: drag_u !< Variable description\f$[units]\f$
  real, intent(inout), dimension(nlons,nlevs) :: drag_v !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: alt !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs) :: density !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons) :: densb !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nlevs,nazmth) :: m_alpha !< Variable description\f$[units]\f$
  real, intent(in), dimension(nlons,nazmth) :: specfac !< Variable description\f$[units]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  !  internal variables.
  !  -------------------
  !
  integer  :: i
  integer  :: l
  integer  :: lev1p
  integer  :: lev2m
  real    :: cos45
  real    :: cos30
  real    :: sin30
  real    :: cos22
  real    :: sin22
  real    :: ddz
  real    :: ddz2
  real    :: zero
  real    :: mms
  real    :: flux2
  real    :: flux3
  real    :: flux4
  real    :: flux5
  real    :: flux6
  real    :: flux7
  real    :: flux8
  real    :: flux9
  real    :: flux10
  real    :: flux11
  real    :: flux12
  real    :: flux14
  real    :: flux15
  real    :: flux16
  data  cos45 / 0.7071068 /
  data  cos30 / 0.8660254 /, sin30 / 0.5       /
  data  cos22 / 0.9238795 /, sin22 / 0.3826834 /
  data  zero / 0. /
  !-----------------------------------------------------------------------
  !
  lev1p = lev1 + 1
  lev2m = lev2 - 1
  !
  !  sum over azimuths for case where slope = 1 (note: this is the only
  !  value of the slope for which a nonzero m_min is used).
  !
  if (slope == 1.) then
    !
    !  case with 4 azimuths.
    !
    if (naz == 4) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux_u(i,l) = densb(i) * ( &
                          specfac(i,1) * (m_alpha(i,l,1) - m_min) &
                          - specfac(i,3) * (m_alpha(i,l,3) - m_min) )
            flux_v(i,l) = densb(i) * ( &
                          specfac(i,2) * (m_alpha(i,l,2) - m_min) &
                          - specfac(i,4) * (m_alpha(i,l,4) - m_min) )
          end if
        end do
      end do ! loop 10
    end if
    !
    !  case with 8 azimuths.
    !
    if (naz == 8) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux2 = specfac(i,2) * ( m_alpha(i,l,2) - m_min)
            flux4 = specfac(i,4) * ( m_alpha(i,l,4) - m_min)
            flux6 = specfac(i,6) * ( m_alpha(i,l,6) - m_min)
            flux8 = specfac(i,8) * ( m_alpha(i,l,8) - m_min)
            flux_u(i,l) = densb(i) * ( &
                          specfac(i,1) * (m_alpha(i,l,1) - m_min) &
                          - specfac(i,5) * (m_alpha(i,l,5) - m_min) &
                          + cos45 * (flux2 - flux4 - flux6 + flux8) )
            flux_v(i,l) = densb(i) * ( &
                          specfac(i,3) * (m_alpha(i,l,3) - m_min) &
                          - specfac(i,7) * (m_alpha(i,l,7) - m_min) &
                          + cos45 * (flux2 + flux4 - flux6 - flux8) )
          end if
        end do
      end do ! loop 20
    end if
    !
    !  case with 12 azimuths.
    !
    if (naz == 12) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux2  = specfac(i,2)  * ( m_alpha(i,l,2)  - m_min)
            flux3  = specfac(i,3)  * ( m_alpha(i,l,3)  - m_min)
            flux5  = specfac(i,5)  * ( m_alpha(i,l,5)  - m_min)
            flux6  = specfac(i,6)  * ( m_alpha(i,l,6)  - m_min)
            flux8  = specfac(i,8)  * ( m_alpha(i,l,8)  - m_min)
            flux9  = specfac(i,9)  * ( m_alpha(i,l,9)  - m_min)
            flux11 = specfac(i,11) * ( m_alpha(i,l,11) - m_min)
            flux12 = specfac(i,12) * ( m_alpha(i,l,12) - m_min)
            flux_u(i,l) = densb(i) * ( &
                          specfac(i,1) * (m_alpha(i,l,1) - m_min) &
                          - specfac(i,7) * (m_alpha(i,l,7) - m_min) &
                          + cos30 * (flux2 - flux6 - flux8 + flux12) &
                          + sin30 * (flux3 - flux5 - flux9 + flux11) )
            flux_v(i,l) = densb(i) * ( &
                          specfac(i,4)  * (m_alpha(i,l,4)  - m_min) &
                          - specfac(i,10) * (m_alpha(i,l,10) - m_min) &
                          + cos30 * (flux3 + flux5 - flux9 - flux11) &
                          + sin30 * (flux2 + flux6 - flux8 - flux12) )
          end if
        end do
      end do ! loop 30
    end if
    !
    !  case with 16 azimuths.
    !
    if (naz == 16) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux2  = specfac(i,2)  * ( m_alpha(i,l,2)  - m_min)
            flux3  = specfac(i,3)  * ( m_alpha(i,l,3)  - m_min)
            flux4  = specfac(i,4)  * ( m_alpha(i,l,4)  - m_min)
            flux6  = specfac(i,6)  * ( m_alpha(i,l,6)  - m_min)
            flux7  = specfac(i,7)  * ( m_alpha(i,l,7)  - m_min)
            flux8  = specfac(i,8)  * ( m_alpha(i,l,8)  - m_min)
            flux10 = specfac(i,10) * ( m_alpha(i,l,10) - m_min)
            flux11 = specfac(i,11) * ( m_alpha(i,l,11) - m_min)
            flux12 = specfac(i,12) * ( m_alpha(i,l,12) - m_min)
            flux14 = specfac(i,14) * ( m_alpha(i,l,14) - m_min)
            flux15 = specfac(i,15) * ( m_alpha(i,l,15) - m_min)
            flux16 = specfac(i,16) * ( m_alpha(i,l,16) - m_min)
            flux_u(i,l) = densb(i) * ( &
                          specfac(i,1) * (m_alpha(i,l,1) - m_min) &
                          - specfac(i,9) * (m_alpha(i,l,9) - m_min) &
                          + cos22 * (flux2 - flux8 - flux10 + flux16) &
                          + cos45 * (flux3 - flux7 - flux11 + flux15) &
                          + sin22 * (flux4 - flux6 - flux12 + flux14) )
            flux_v(i,l) = densb(i) * ( &
                          specfac(i,5)  * (m_alpha(i,l,5)  - m_min) &
                          - specfac(i,13) * (m_alpha(i,l,13) - m_min) &
                          + cos22 * (flux4 + flux6 - flux12 - flux14) &
                          + cos45 * (flux3 + flux7 - flux11 - flux15) &
                          + sin22 * (flux2 + flux8 - flux10 - flux16) )
          end if
        end do
      end do ! loop 40
    end if
    !
  end if
  !
  !  sum over azimuths for case where slope not equal to 1.
  !
  if (slope /= 1.) then

    mms = m_min ** slope
    !
    !  case with 4 azimuths.
    !
    if (naz == 4) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux_u(i,l) = densb(i) / slope * ( &
                          specfac(i,1) * (m_alpha(i,l,1) ** slope - mms) &
                          - specfac(i,3) * (m_alpha(i,l,3) ** slope - mms) )
            flux_v(i,l) = densb(i) / slope * ( &
                          specfac(i,2) * (m_alpha(i,l,2) ** slope - mms) &
                          - specfac(i,4) * (m_alpha(i,l,4) ** slope - mms) )
          end if
        end do
      end do ! loop 50
    end if
    !
    !  case with 8 azimuths.
    !
    if (naz == 8) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux2 = specfac(i,2) * ( m_alpha(i,l,2) ** slope - mms)
            flux4 = specfac(i,4) * ( m_alpha(i,l,4) ** slope - mms)
            flux6 = specfac(i,6) * ( m_alpha(i,l,6) ** slope - mms)
            flux8 = specfac(i,8) * ( m_alpha(i,l,8) ** slope - mms)
            flux_u(i,l) = densb(i) / slope * ( &
                          specfac(i,1) * (m_alpha(i,l,1) ** slope - mms) &
                          - specfac(i,5) * (m_alpha(i,l,5) ** slope - mms) &
                          + cos45 * (flux2 - flux4 - flux6 + flux8) )
            flux_v(i,l) =  densb(i) / slope * ( &
                          specfac(i,3) * (m_alpha(i,l,3) ** slope - mms) &
                          - specfac(i,7) * (m_alpha(i,l,7) ** slope - mms) &
                          + cos45 * (flux2 + flux4 - flux6 - flux8) )
          end if
        end do
      end do ! loop 60
    end if
    !
    !  case with 12 azimuths.
    !
    if (naz == 12) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux2  = specfac(i,2)  * ( m_alpha(i,l,2) ** slope - mms)
            flux3  = specfac(i,3)  * ( m_alpha(i,l,3) ** slope - mms)
            flux5  = specfac(i,5)  * ( m_alpha(i,l,5) ** slope - mms)
            flux6  = specfac(i,6)  * ( m_alpha(i,l,6) ** slope - mms)
            flux8  = specfac(i,8)  * ( m_alpha(i,l,8) ** slope - mms)
            flux9  = specfac(i,9)  * ( m_alpha(i,l,9) ** slope - mms)
            flux11 = specfac(i,11) * ( m_alpha(i,l,11) ** slope - mms)
            flux12 = specfac(i,12) * ( m_alpha(i,l,12) ** slope - mms)
            flux_u(i,l) = densb(i) / slope * ( &
                          specfac(i,1) * (m_alpha(i,l,1) ** slope - mms) &
                          - specfac(i,7) * (m_alpha(i,l,7) ** slope - mms) &
                          + cos30 * (flux2 - flux6 - flux8 + flux12) &
                          + sin30 * (flux3 - flux5 - flux9 + flux11) )
            flux_v(i,l) =  densb(i) / slope * ( &
                          specfac(i,4)  * (m_alpha(i,l,4) ** slope - mms) &
                          - specfac(i,10) * (m_alpha(i,l,10) ** slope - mms) &
                          + cos30 * (flux3 + flux5 - flux9 - flux11) &
                          + sin30 * (flux2 + flux6 - flux8 - flux12) )
          end if
        end do
      end do ! loop 70
    end if
    !
    !  case with 16 azimuths.
    !
    if (naz == 16) then
      do l = lev1,lev2
        do i = il1,il2
          if (dragil(i,l) == 1) then
            flux2  = specfac(i,2)  * ( m_alpha(i,l,2) ** slope - mms)
            flux3  = specfac(i,3)  * ( m_alpha(i,l,3) ** slope - mms)
            flux4  = specfac(i,4)  * ( m_alpha(i,l,4) ** slope - mms)
            flux6  = specfac(i,6)  * ( m_alpha(i,l,6) ** slope - mms)
            flux7  = specfac(i,7)  * ( m_alpha(i,l,7) ** slope - mms)
            flux8  = specfac(i,8)  * ( m_alpha(i,l,8) ** slope - mms)
            flux10 = specfac(i,10) * ( m_alpha(i,l,10) ** slope - mms)
            flux11 = specfac(i,11) * ( m_alpha(i,l,11) ** slope - mms)
            flux12 = specfac(i,12) * ( m_alpha(i,l,12) ** slope - mms)
            flux14 = specfac(i,14) * ( m_alpha(i,l,14) ** slope - mms)
            flux15 = specfac(i,15) * ( m_alpha(i,l,15) ** slope - mms)
            flux16 = specfac(i,16) * ( m_alpha(i,l,16) ** slope - mms)
            flux_u(i,l) = densb(i) / slope * ( &
                          specfac(i,1) * (m_alpha(i,l,1) ** slope - mms) &
                          - specfac(i,9) * (m_alpha(i,l,9) ** slope - mms) &
                          + cos22 * (flux2 - flux8 - flux10 + flux16) &
                          + cos45 * (flux3 - flux7 - flux11 + flux15) &
                          + sin22 * (flux4 - flux6 - flux12 + flux14) )
            flux_v(i,l) =  densb(i) / slope * ( &
                          specfac(i,5)  * (m_alpha(i,l,5) ** slope - mms) &
                          - specfac(i,13) * (m_alpha(i,l,13) ** slope - mms) &
                          + cos22 * (flux4 + flux6 - flux12 - flux14) &
                          + cos45 * (flux3 + flux7 - flux11 - flux15) &
                          + sin22 * (flux2 + flux8 - flux10 - flux16) )
          end if
        end do
      end do ! loop 80
    end if
    !
  end if
  !
  !  calculate drag at intermediate levels using centered differences.
  !
  do l = lev1p,lev2m
    do i = il1,il2
      if (dragil(i,l) == 1) then
        ddz2 = density(i,l) * ( alt(i,l + 1) - alt(i,l - 1) )
        drag_u(i,l) = - ( flux_u(i,l + 1) - flux_u(i,l - 1) ) / ddz2
        drag_v(i,l) = - ( flux_v(i,l + 1) - flux_v(i,l - 1) ) / ddz2
      end if
    end do
  end do ! loop 90
  !
  !  drag at first and last levels using one-side differences.
  !
  do i = il1,il2
    if (dragil(i,lev1) == 1) then
      ddz = density(i,lev1) * ( alt(i,lev1p) - alt(i,lev1) )
      drag_u(i,lev1) = - ( flux_u(i,lev1p) - flux_u(i,lev1) ) / ddz
      drag_v(i,lev1) = - ( flux_v(i,lev1p) - flux_v(i,lev1) ) / ddz
    end if
  end do ! loop 100
  do i = il1,il2
    if (dragil(i,lev2) == 1) then
      ddz = density(i,lev2) * ( alt(i,lev2) - alt(i,lev2m) )
      drag_u(i,lev2) = - ( flux_u(i,lev2) - flux_u(i,lev2m) ) / ddz
      drag_v(i,lev2) = - ( flux_v(i,lev2) - flux_v(i,lev2m) ) / ddz
    end if
  end do ! loop 110
  !
  return
  !-----------------------------------------------------------------------
end
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
