!>\file
!>\brief Purpose: Chemistry solver
!!
!! @author David Plummer, J.de Grandpre, S.R.Beagley
!
module mamchmf2_mod

implicit none ; private

public :: mamchmf2

contains

  subroutine mamchmf2 (arj, ark, chltrox, chlsrox, chdgrox, csadrow, &
                       wflxrox, emisln, acemphs, sfemrow, sfsaphs,   &
                       qtemp, airrow, delm, dep_wrk, oxlfr, indxtrp, &
                       indxlst, dtadv, kount, il1, il2, msg, xrow)

  ! ---------------------------------------------------------------------------
  ! Authors (original routine): J.de Grandpre, S.R.Beagley
  ! Modified for GCM/MAM use (March - September 1994).
  !
  ! Chemical solver: D.Plummer, D.Chartrand, J.C. McConnell
  !
  ! Purpose: A troposphere / stratosphere / mesosphere chemistry solver.
  !
  ! Method : Full-newton solver
  !          Provides advected species tendencies for time T+1 and
  !          non-advected VMRs for time T+1 to the GCM.
  !
  ! New chemical solver:
  !
  ! Purpose : Forward integration in time of chemical set
  ! Revision: David Plummer, Edna Templeton, York University 1995
  !
  ! Called by: chem3.F90.
  ! Calls    : setrk05, n2o5hydro, coefsetv2, dgetrf, dgetrs,
  !            semimpv2, chdiag, hetchem
  ! ----------------------------------------------------------------------------

  use msizes, only          : ilg, ilev, ntrac
  use chem_params_mod, only : jpntjv, jpnrks, nsfcem, nvdep, nchdg, nchl, nwflx, &
                              jpspcs, jpnewt, jphnewt, jpsrxn, jppsc2, vppi,  &
                              jphcsta_hi, jphcsto_hi, jphcsta_lo, jphcsto_lo, &
                              vpbltz, rc
  use mam_chem_mod
  use chemistry_mod, only   : hetchem
  use radcons_mod, only     : co2_ppm
  use tracers_info_mod, only: itbga1, itshno, itsh2o, itt6

  implicit none

  integer, intent(in) :: il1, il2, kount, msg
  integer, dimension(ilg), intent(in) :: indxlst, indxtrp

  real(kind=rc), intent(in) :: dtadv
  real, dimension(ilg,ilev),       intent(in) :: acemphs !< aircraft NOx emissions (molecules/cm^3/sec)
  real, dimension(ilg,ilev),       intent(in) :: emisln  !< lightning NOx emission field (molecules/sec/molecule air)
  real, dimension(ilg,ilev),       intent(in) :: sfsaphs !< sulphate (SO4) emission field
  real, dimension(ilg,nsfcem),     intent(in) :: sfemrow  !< nsfcem different emission fields for CO and NOx arranged in a preassumed order

  real(kind=rc), dimension(ilg,nwflx),      intent(inout) :: wflxrox
  real(kind=rc), dimension(ilg,nchl),       intent(inout) :: chltrox, chlsrox
  real(kind=rc), dimension(ilg,ilev,nchdg), intent(inout) :: chdgrox
  real, dimension(ilg,ilev,2),     intent(out)   :: csadrow
  real, dimension(ilg,ilev,ntrac), intent(inout) :: xrow
  real(kind=rc), dimension(ilg,7),    intent(in)  :: dep_wrk
  real(kind=rc), dimension(ilg,ilev), intent(in)  :: airrow, delm, qtemp
  real(kind=rc), dimension(ilg,ilev), intent(out) :: oxlfr
  real(kind=rc), dimension(jpntjv,ilg,ilev), target, intent(inout) :: arj
  real(kind=rc), dimension(jpnrks,ilg,ilev), target, intent(out)   :: ark

  ! Non-advected gas-phase chemistry species
  real(kind=rc), dimension(ilg,ilev) :: qAE1, qSHNO3, qSH2O

  ! Local parameters
  integer, parameter :: maxitr = 50    !< maximum number of iterations
  real(kind=rc), parameter :: qcpmin = 1.0e-35_rc  !< minimum mixing ratio
  real(kind=rc), parameter :: xtol = 5.0e-5_rc     !< convergence criteria
  real(kind=rc), parameter :: eps = 1.0e-30_rc     !< minimum diagonal element of Jacobian

  ! Locally defined work arrays
  integer, dimension(100) :: nleft
  integer, dimension(ilg,ilev) :: iconv
  integer :: ii

  real(kind=rc), dimension(jpspcs,ilg,ilev), target :: an0, an2   !< gas-phase species arrays
  real(kind=rc), dimension(ilg,ilev,12)             :: an3  !< heterogeneous chemistry species array
  real(kind=rc), dimension(jpnewt,jpnewt+1), target :: fb
  real(kind=rc), dimension(ilg,ilev)        :: h2ocpy
  real(kind=rc), dimension(2,ilg,ilev)      :: csadwrk
  real(kind=rc), dimension(ilg,ilev)        :: emiswrk

  ! Variables for BLAS/LAPACK routines. The function being called depends on the
  ! precision of the model run, eg: Dgetrf for 64-bit, Sgetrf for 32-bit
  integer :: ijacdim, info, inrhs, ipiv(100)
  !
  integer :: jphcsta, jphcsto  !< top and bottom layers of the heterogen chem
  integer :: i, iclcl, idamp, it, j, k, n, nrep
  real(kind=rc) :: hcmin, hno3cpy
  real(kind=rc), dimension(ilg,ilev) :: xmin

  real(kind=rc) :: xf1, xf2
  real(kind=rc) :: co2_ppm_rc

  real(kind=rc), dimension(:), pointer :: an0_ptr, an2_ptr, fb_ptr
  real(kind=rc), dimension(:), pointer :: ark_ptr, arj_ptr
  logical :: is_64bit
! ------------------------------------------------------------------------------

  ! Assign incoming "xrow" to orderred solver species array an2;
  call mam_assign_species(xrow, an2, airrow, kount, il1, il2, ilg, ilev, ntrac, -1)

  co2_ppm_rc = real(co2_ppm, kind=rc)

  do k = 1, ilev
    do i = il1, il2
  ! Take a copy of the incoming H2O field (in mixing ratio) to use
  ! in deciding when to run HETCHEM and when to use the tropospheric N2O5
  ! hydrolysis parameterization.
      h2ocpy(i,k) = real(xrow(i,k,itt6), kind=rc)

      qSHNO3(i,k) = real(xrow(i,k,itshno), kind=rc)
      qSH2O (i,k) = real(xrow(i,k,itsh2o), kind=rc)
        qAE1(i,k) = real(xrow(i,k,itbga1), kind=rc)
    enddo
  enddo

  if (kount == 0 .or. kount == 1) then
     qSHNO3(:,:) = 0.0_rc
     qSH2O(:,:)  = 0.0_rc
  end if
  !
  ! work term for near-surface emission flux
  do i = il1, il2
    do k = 1, ilev
      emiswrk(i, k) = airrow(i,k) * 0.5_rc * dtadv / delm(i,k)
    enddo
  enddo

  ! Output array to save PSC surface area density diagnostics
  csadrow(:,:,:) = 0.0

  ! For the range of model levels where heterogeneous chemistry is active,
  ! account for partitioning into the aerosol phase.
  ! First, determine whether the low-top or high-top settings of jphcsta/o
  ! should be used. These are the limiting levels of heterogeneous chemistry.
  if (ilev == 49) then
    jphcsta = jphcsta_lo
    jphcsto = jphcsto_lo
  elseif (ilev == 80) then
    jphcsta = jphcsta_hi
    jphcsto = jphcsto_hi
  else
    write(6,*) "mamchmf2: ILEV must be 49 or 80 in order to set the right"
    write(6,*) "          values for jphcsta/o. Stopping. ILEV is", ilev
    stop
  endif

  do k = jphcsta, jphcsto
    do i = il1, il2
      qSHNO3(i,k) = max(qSHNO3(i,k),0.0_rc)
      qSHNO3(i,k) = min(qSHNO3(i,k),1.0_rc)
      qSH2O (i,k) = max(qSH2O (i,k),0.0_rc)
      qSH2O (i,k) = min(qSH2O (i,k),1.0_rc)
    enddo
  enddo

  an3(:, :, 11:12) = qcpmin   ! Initialize

  do k = jphcsta, jphcsto
    do i = il1, il2
      hno3cpy = an2(28,i,k)

      an3(i,k,11) = hno3cpy * qSHNO3(i,k)
      an3(i,k,12) = h2ocpy(i,k) * qSH2O(i,k)
      an2(27,i,k) = h2ocpy(i,k) * (1.0_rc-qSH2O(i,k))
      an2(28,i,k) = hno3cpy * (1.0_rc-qSHNO3(i,k))
    enddo
  enddo

  do k = 1, ilev
    do i = il1, il2
      xmin(i,k) = qcpmin * airrow(i,k)

      do n=1,jpspcs
        an2(n,i,k) = max(an2(n,i,k), xmin(i,k))
      enddo
      an3(i,k,11) = max(an3(i,k,11), xmin(i,k))
      an3(i,k,12) = max(an3(i,k,12), xmin(i,k))
    enddo
  enddo

  ! HALF TIMESTEP OF EMISSIONS

  ! lightning emissions
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + &
            real(emisln(i,k), kind=rc) * airrow(i,k) * 0.5_rc * dtadv
    enddo
  enddo

  ! aircraft emissions
  xf1 = 0.95_rc * 0.5_rc * dtadv
  xf2 = 0.05_rc * 0.5_rc * dtadv
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + xf1 * real(acemphs(i,k), kind=rc)
      an2(9,i,k) = an2(9,i,k) + xf2 * real(acemphs(i,k), kind=rc)
    enddo
  enddo

  ! emissions restricted to the surface and put into the bottom two levels
  xf1 = 0.5_rc

  do k = ilev-1, ilev
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k) + &
             xf1 * 0.95_rc * real(sfemrow(i,5), kind=rc) * emiswrk(i, k)
      an2( 9,i,k) = an2( 9,i,k) + &
             xf1 * 0.05_rc * real(sfemrow(i,5), kind=rc) * emiswrk(i, k)
      an2(34,i,k) = an2(34,i,k) + &
             xf1 * real(sfemrow(i,2), kind=rc) * emiswrk(i, k)
    enddo
  enddo

  ! emissions from open biomass burning put into the lowest ~ 1 km.
  ! For the standard 71-level model, this is the bottom 7 levels.
  k = ilev
  do i = il1, il2
    an2( 8,i,k) = an2( 8,i,k) + &
            0.095_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
    an2( 9,i,k) = an2( 9,i,k) + &
            0.005_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
    an2(34,i,k) = an2(34,i,k) + &
            0.100_rc * real(sfemrow(i,1), kind=rc) * emiswrk(i, k)
  enddo

  xf1 = 1.0_rc / 6.0_rc
  do k = ilev-6, ilev-1
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k) + &
              xf1 * 0.855_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
      an2( 9,i,k) = an2( 9,i,k) + &
              xf1 * 0.045_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
      an2(34,i,k) = an2(34,i,k) + &
              xf1 * 0.900_rc * real(sfemrow(i,1), kind=rc) * emiswrk(i, k)
    enddo
  enddo

  ! Special treatment for emissions from International Shipping:
  ! put them into the bottom two layers, but assume that 50% of NOx is already
  ! oxidized to HNO3 to account for non-linearities in plume.
  xf1 = 0.5_rc
  do k = ilev-1, ilev
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k) + &
            xf1 * 0.475_rc * real(sfemrow(i,6), kind=rc) * emiswrk(i, k)
      an2( 9,i,k) = an2( 9,i,k) + &
            xf1 * 0.025_rc * real(sfemrow(i,6), kind=rc) * emiswrk(i, k)
      an2(28,i,k) = an2(28,i,k) + &
            xf1 * 0.500_rc * real(sfemrow(i,6), kind=rc) * emiswrk(i, k)
      an2(34,i,k) = an2(34,i,k) + &
            xf1 * real(sfemrow(i,3), kind=rc) * emiswrk(i, k)
    enddo
  enddo

  ! HALF TIMESTEP OF DRY DEPOSITION
  !

  ! Diagnostic calculation of dry deposition flux (units of moles/m^2/sec)
  xf1 = 1.0e4_rc / (6.0221415E23_rc * dtadv)
  k = ilev

  do i = il1, il2
    xf2 = xf1 * delm(i,k) / airrow(i,k)
    wflxrox(i,3) = xf2 * an2(28,i,k) * (1.0_rc - dep_wrk(i,3))
    wflxrox(i,4) = xf2 * an2( 2,i,k) * (1.0_rc - dep_wrk(i,1))
    wflxrox(i,5) = xf2 * an2( 9,i,k) * (1.0_rc - dep_wrk(i,2))
    wflxrox(i,6) = xf2 * an2( 2,i,k) * (1.0_rc - dep_wrk(i,7))
  enddo

  ! Add dry deposition of HBr and HCl, assuming deposition velocity is equal
  ! to that of HNO3.
  k = ilev

  do i = il1, il2
    an2( 2,i,k) = an2( 2,i,k) * dep_wrk(i,1)
    an2( 9,i,k) = an2( 9,i,k) * dep_wrk(i,2)
    an2(16,i,k) = an2(16,i,k) * dep_wrk(i,3)
    an2(24,i,k) = an2(24,i,k) * dep_wrk(i,3)
    an2(28,i,k) = an2(28,i,k) * dep_wrk(i,3)
    an2(29,i,k) = an2(29,i,k) * dep_wrk(i,4)
    an2(32,i,k) = an2(32,i,k) * dep_wrk(i,5)
    an2(33,i,k) = an2(33,i,k) * dep_wrk(i,6)
  enddo

  ! Calculate gas-phase reaction rates
  call setrk05 (ark, qtemp, airrow, qAE1, h2ocpy, il1, il2, jphcsta)

  ! Separate subroutine to calculate N2O5 hydrolysis on tropospheric aerosols
  call n2o5hydro (ark, chdgrox, qtemp, airrow, h2ocpy, sfsaphs, &
                  il1, il2, jphcsta)

  ! Prevent halocarbons from decaying below the hole-filling minimum
  ! by turning off the decay as the concentration gets very small
  do k = 1, ilev
    do i = il1, il2
      hcmin = 1.0e-14_rc * airrow(i,k)

      if (an2(30,i,k) < hcmin) then
        ark(4,i,k)  = 1.0e-04_rc * ark(4,i,k)
        ark(75,i,k) = 1.0e-04_rc * ark(75,i,k)
        arj(28,i,k) = 1.0e-04_rc * arj(28,i,k)
      endif
      if (an2(36,i,k) < hcmin) then
        ark(107,i,k) = 1.0e-04_rc * ark(107,i,k)
        arj(31,i,k)  = 1.0e-04_rc * arj(31,i,k)
      endif
      if (an2(37,i,k) < hcmin) then
        ark(108,i,k) = 1.0e-04_rc * ark(108,i,k)
        arj(32,i,k)  = 1.0e-04_rc * arj(32,i,k)
      endif
      if (an2(38,i,k) < hcmin) then
        ark(101,i,k) = 1.0e-04_rc * ark(101,i,k)
        arj(38,i,k)  = 1.0e-04_rc * arj(38,i,k)
      endif
      if (an2(39,i,k) < hcmin) then
        ark(102,i,k) = 1.0e-04_rc * ark(102,i,k)
        arj(39,i,k)  = 1.0e-04_rc * arj(39,i,k)
      endif
      if (an2(40,i,k) < hcmin) then
        ark(103,i,k) = 1.0e-04_rc * ark(103,i,k)
        ark(104,i,k) = 1.0e-04_rc * ark(104,i,k)
        arj(41,i,k)  = 1.0e-04_rc * arj(41,i,k)
      endif
      if (an2(41,i,k) < hcmin) then
        ark(105,i,k) = 1.0e-04_rc * ark(105,i,k)
        ark(106,i,k) = 1.0e-04_rc * ark(106,i,k)
        arj(40,i,k)  = 1.0e-04_rc * arj(40,i,k)
      endif
      if (an2(42,i,k) < hcmin) then
        ark(91,i,k) = 1.0e-04_rc * ark(91,i,k)
        ark(92,i,k) = 1.0e-04_rc * ark(92,i,k)
        arj(37,i,k) = 1.0e-04_rc * arj(37,i,k)
      endif
      if (an2(43,i,k) < hcmin) then
        ark(111,i,k) = 1.0e-04_rc * ark(111,i,k)
        ark(112,i,k) = 1.0e-04_rc * ark(112,i,k)
        arj(42,i,k)  = 1.0e-04_rc * arj(42,i,k)
      endif
      if (an2(44,i,k) < hcmin) then
        ark(113,i,k) = 1.0e-04_rc * ark(113,i,k)
        ark(114,i,k) = 1.0e-04_rc * ark(114,i,k)
        arj(43,i,k)  = 1.0e-04_rc * arj(43,i,k)
      endif
    enddo
  enddo
  !
  ! --- Newton's method iterative solution for chemistry

  ! --- diagnostic counter
  nleft(:) = 0

  ! --- convergence flag
  iconv(:,:) = 0

  an0(1:jpspcs,il1:il2,1:ilev) = an2(1:jpspcs,il1:il2,1:ilev)
  is_64bit = (rc == kind(1.0d0))

  do k = 1, ilev
    idamp = 0

    do i = il1, il2
      an0_ptr => an0(1:jpspcs,i,k)
      an2_ptr => an2(1:jpspcs,i,k)
      ark_ptr => ark(1:jpnrks,i,k)
      arj_ptr => arj(1:jpntjv,i,k)
      do it = 1, maxitr

        nleft(it) = nleft(it) + 1
        fb(:,:) = 0.0_rc

        call coefsetv2 (fb, an2_ptr, an0_ptr, arj_ptr, ark_ptr, &
                        airrow(i,k), dtadv, co2_ppm_rc )

        ijacdim = jpnewt
        if (is_64bit) then
           call dgetrf (ijacdim, ijacdim, fb, ijacdim, ipiv, info)
        else
           call sgetrf (ijacdim, ijacdim, fb, ijacdim, ipiv, info)
        end if

        if (info /= 0) then
          write(6,*) "mamchmf2 (getrF):"
          write(6,*) "SINGULAR MATRIX - stopping", k, i, it
          flush(6)
          call xit ('MAMCHMF2',-16)
          stop
        endif

        inrhs = 1
        fb_ptr => fb(1:jpnewt,jpnewt+1)
        if (is_64bit) then
           call dgetrs ('N', ijacdim, inrhs, fb, ijacdim, ipiv, fb_ptr, ijacdim, info)
        else
           call sgetrs ('N', ijacdim, inrhs, fb, ijacdim, ipiv, fb_ptr, ijacdim, info)
        end if
        if (info /= 0) then
          write(6,*) "mamchmf2 (getrS):"
          write(6,*) "SINGULAR MATRIX - stopping", k, i, it
          flush(6)
          call xit ('MAMCHMF',-16)
          stop
        endif

        ! Update the species concentrations
        do j = 1, jpnewt
          an2(j,i,k) = max(an2(j,i,k)+fb(j,jpnewt+1), xmin(i,k))
        enddo

        iclcl=1
        do j=1,jpnewt
          if (abs(fb(j,jpnewt+1)/an2(j,i,k)) > xtol   &
             .and.  an2(j,i,k) > xmin(i,k)) iclcl = 0
        enddo

        ! Damp any oscillating solutions
        if (it == 15 .and. iclcl == 0) then
          idamp = idamp + 1
          do j = 1, jpnewt
            an2(j,i,k) = max(an2(j,i,k) - 0.2_rc*fb(j,jpnewt+1), xmin(i,k))
          enddo
        endif

        ! Update the species not included in the Jacobian
        call semimpv2 (an2_ptr, an0_ptr, arj_ptr, ark_ptr, &
                       airrow(i,k), dtadv, co2_ppm_rc )

        if (iclcl == 1) then
          iconv(i,k) = 1
          exit
        endif

      enddo  ! end loop 250 over it=1,maxitr
    enddo    ! end loop 225 over i=il1,il2
  enddo      ! end loop 200 over k=1,ilev

  nrep = 0
  do k = 1, ilev
    do i = il1, il2
      if (iconv(i,k) == 0) nrep = nrep + 1
    enddo
  enddo

 ! if (nrep > 0) then
 !   write(6,'(a48,i6)') "mamchmf2: WARNING: Failed to converge all points",nrep
 ! endif

  !***********************************************************************
  ! Calculation of chemical diagnostics at the end of chemistry
  !
  call chdiag (chltrox, chlsrox, chdgrox, oxlfr, an2, an0, airrow, &
               emiswrk, arj, ark, co2_ppm_rc, dtadv, indxtrp,      &
               indxlst, il1, il2, ilg, ilev, jpspcs)

  ! Supplementary diagnostic output of 3-D lightning NOx emission
  ! with units of molecules/cm^3/sec
  do k = msg+1, ilev
    do i = il1, il2
      chdgrox(i,k,13) = real(emisln(i,k), kind=rc) * airrow(i,k)
    enddo
  enddo

  !***********************************************************************

  ! Pull out species required for heterogeneous chemistry

  do k = 1, ilev
    do i = il1, il2
      an3(i,k,1)  = an2(14,i,k)
      an3(i,k,2)  = an2(15,i,k)
      an3(i,k,3)  = an2(11,i,k)
      an3(i,k,4)  = an2(18,i,k)
      an3(i,k,5)  = an2(23,i,k)
      an3(i,k,6)  = an2(25,i,k)
      an3(i,k,7)  = an2(22,i,k)
      an3(i,k,8)  = an2(27,i,k)
      an3(i,k,9)  = an2(28,i,k)
      an3(i,k,10) = an2(16,i,k)
    enddo
  enddo

  call hetchem (an3, qtemp, airrow, qAE1, h2ocpy, csadwrk, dtadv,   &
                il1, il2, ilg, ilev, jphcsta, jphcsto)

  do k = 1, ilev
    do i = il1, il2
      an2(14,i,k) = an3(i,k,1)
      an2(15,i,k) = an3(i,k,2)
      an2(11,i,k) = an3(i,k,3)
      an2(18,i,k) = an3(i,k,4)
      an2(23,i,k) = an3(i,k,5)
      an2(25,i,k) = an3(i,k,6)
      an2(22,i,k) = an3(i,k,7)
      an2(27,i,k) = an3(i,k,8)
      an2(28,i,k) = an3(i,k,9)
      an2(16,i,k) = an3(i,k,10)
    enddo
  enddo

  ! Diagnostic output of stratospheric sulphate aerosol surface area
  ! density in units of cm^2/cm^3
  do k = 1, jphcsta-1
    do i = il1, il2
      csadrow(i,k,1) = 1.0e-08 * real(qAE1(i,k))
    enddo
  enddo

  do k = jphcsta, jphcsto
    do i = il1, il2
      csadrow(i,k,1) = real(csadwrk(1,i,k))
      csadrow(i,k,2) = real(csadwrk(2,i,k))
    enddo
  enddo
  !
  ! HALF TIMESTEP OF DRY DEPOSITION
  !
  ! Diagnostic calculation of dry deposition flux
  xf1 = 1.0e4_rc / (6.0221415e23_rc * dtadv)
  k = ilev

  do i = il1, il2
    xf2 = xf1 * delm(i,k) / airrow(i,k)
    wflxrox(i,3) = xf2 * an2(28,i,k) * (1.0_rc - dep_wrk(i,3))
    wflxrox(i,4) = xf2 * an2( 2,i,k) * (1.0_rc - dep_wrk(i,1))
    wflxrox(i,5) = xf2 * an2( 9,i,k) * (1.0_rc - dep_wrk(i,2))
    wflxrox(i,6) = xf2 * an2( 2,i,k) * (1.0_rc - dep_wrk(i,7))
  enddo

  k = ilev
  do i = il1, il2
    an2( 2,i,k) = an2( 2,i,k) * dep_wrk(i,1)
    an2( 9,i,k) = an2( 9,i,k) * dep_wrk(i,2)
    an2(16,i,k) = an2(16,i,k) * dep_wrk(i,3)
    an2(24,i,k) = an2(24,i,k) * dep_wrk(i,3)
    an2(28,i,k) = an2(28,i,k) * dep_wrk(i,3)
    an2(29,i,k) = an2(29,i,k) * dep_wrk(i,4)
    an2(32,i,k) = an2(32,i,k) * dep_wrk(i,5)
    an2(33,i,k) = an2(33,i,k) * dep_wrk(i,6)
  enddo

  ! HALF TIMESTEP OF EMISSIONS
  !
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + &
              real(emisln(i,k), kind=rc) * airrow(i,k) * 0.5_rc * dtadv
    enddo
  enddo

  xf1 = 0.95_rc * 0.5_rc * dtadv
  xf2 = 0.05_rc * 0.5_rc * dtadv
  do k = msg+1, ilev
    do i = il1, il2
      an2(8,i,k) = an2(8,i,k) + xf1 * real(acemphs(i,k), kind=rc)
      an2(9,i,k) = an2(9,i,k) + xf2 * real(acemphs(i,k), kind=rc)
    enddo
  enddo

  ! Collected surface emission sources
  xf1 = 0.5_rc
  do k=ilev-1,ilev
    do i=il1,il2
      an2( 8,i,k) = an2( 8,i,k) + &
            xf1 * 0.95_rc * real(sfemrow(i,5), kind=rc) * emiswrk(i, k)
      an2( 9,i,k) = an2( 9,i,k) + &
            xf1 * 0.05_rc * real(sfemrow(i,5), kind=rc) * emiswrk(i, k)
      an2(34,i,k) = an2(34,i,k) + &
            xf1 * real(sfemrow(i,2), kind=rc) * emiswrk(i, k)
    enddo
  enddo

  ! Open biomass burning
  k=ilev
  do i=il1,il2
    an2( 8,i,k) = an2( 8,i,k) + 0.095_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
    an2( 9,i,k) = an2( 9,i,k) + 0.005_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
    an2(34,i,k) = an2(34,i,k) + 0.100_rc * real(sfemrow(i,1), kind=rc) * emiswrk(i, k)
  enddo

  xf1 = 1.0_rc / 6.0_rc
  do k=ilev-6,ilev-1
    do i=il1,il2
      an2( 8,i,k) = an2( 8,i,k) + &
             xf1 * 0.855_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
      an2( 9,i,k) = an2( 9,i,k) + &
             xf1 * 0.045_rc * real(sfemrow(i,4), kind=rc) * emiswrk(i, k)
      an2(34,i,k) = an2(34,i,k) + &
             xf1 * 0.900_rc * real(sfemrow(i,1), kind=rc) * emiswrk(i, k)
    enddo
  enddo

  ! International Shipping
  xf1 = 0.5_rc
  do k = ilev-1, ilev
    do i = il1, il2
      an2( 8,i,k) = an2( 8,i,k) + &
              xf1 * 0.475_rc * real(sfemrow(i,6), kind=rc) * emiswrk(i, k)
      an2( 9,i,k) = an2( 9,i,k) + &
              xf1 * 0.025_rc * real(sfemrow(i,6), kind=rc) * emiswrk(i, k)
      an2(28,i,k) = an2(28,i,k) + &
              xf1 * 0.500_rc * real(sfemrow(i,6), kind=rc) * emiswrk(i, k)
      an2(34,i,k) = an2(34,i,k) + &
              xf1 * real(sfemrow(i,3), kind=rc) * emiswrk(i, k)
    enddo
  enddo

  ! For the range of model levels where heterogeneous chemistry is active,
  ! account for partitioning into the aerosol phase
  do k = jphcsta, jphcsto
    do i = il1, il2
      an2(28,i,k) = an2(28,i,k) + an3(i,k,11)
      an2(27,i,k) = an2(27,i,k) + an3(i,k,12)
      qSHNO3(i,k) = an3(i,k,11) / an2(28,i,k)
      qSH2O(i,k)  = an3(i,k,12) / an2(27,i,k)
    enddo
  enddo

  ! Returned the orderred solver species array an2 back into "xrow";
  call mam_assign_species(xrow, an2, airrow, kount, il1, il2, ilg, ilev, ntrac, +1)
  !
  do k = 1, ilev
     do i = il1, il2
        xrow(i,k,itshno) = real(max(qSHNO3(i,k), qcpmin))
        xrow(i,k,itsh2o) = real(max(qSH2O (i,k), qcpmin))
    enddo
  enddo

  return
  end subroutine mamchmf2


  subroutine mam_assign_species(xrow, an2, airrow, kount, il1, il2, ilg, ilev, ntrac, imode)
  use chem_params_mod, only : jpspcs, rc
  use tracers_info_mod, only: itox, itnx, itc9, itbx, itn5, ithx, itn7, itt5, &
                              ithb, itmm, itco, itha, itmh, ithd, itc7, itc4, &
                              itf1, itf2, itf3, itf4, itf5, itf6, itcb, itbf, &
                              itdb, itny, itcy, itcz, itbz, itta, ittb, itx1, &
                              itx2, itx3, ito1, itoo, ito3, itn1, itn2, itn3, &
                              itn4, itc1, itc6, itoc, itc3, itc5, itbr, itbo, &
                              itob, itbc, itbn, itt6, ith0, ithh, ith1, ith2, &
                              itm2, itc2

  implicit none
  integer, intent(in) :: il1, il2, kount, ilg, ilev ,ntrac, imode

  real, dimension(ilg,ilev,ntrac),           intent(inout) :: xrow
  real(kind=rc), dimension(ilg,ilev),        intent(in)    :: airrow
  real(kind=rc), dimension(jpspcs,ilg,ilev), intent(inout) :: an2   !< gas-phase species arrays

  ! Advected gas-phase chemistry species/families
  real(kind=rc), dimension(ilg,ilev) :: qOx, qNOx, qClOx, qBrOx, qHNO3
  real(kind=rc), dimension(ilg,ilev) :: qHOx, qHNO4, qN2O, qHBr, qCH4, qCO
  real(kind=rc), dimension(ilg,ilev) :: qCH2O, qCH3OOH, qH2, qHCl, qClONO2
  real(kind=rc), dimension(ilg,ilev) :: qCFC11, qCFC12, qCCl4, qCH3CCl3
  real(kind=rc), dimension(ilg,ilev) :: qHCFC22, qCH3Cl, qCH3Br, qCHBr3
  real(kind=rc), dimension(ilg,ilev) :: qCH2Br2

  ! Non-advected gas-phase chemistry species
  real(kind=rc), dimension(ilg,ilev) :: qO1D, qO3P, qO3, qNO, qNO2, qNO3
  real(kind=rc), dimension(ilg,ilev) :: qN2O5, qCl, qHOCl, qOClO, qClO
  real(kind=rc), dimension(ilg,ilev) :: qCl2O2, qBr, qBrO, qHOBr, qBrCl
  real(kind=rc), dimension(ilg,ilev) :: qBrONO2, qH2O, qH, qH2O2,qOH,qHO2
  real(kind=rc), dimension(ilg,ilev) :: qCH3O2, qCl2

  real(kind=rc) :: cOxold, cHOxold, cNOxold, cClOxold, cBrOxold
  real(kind=rc), parameter :: qcpmin = 1.0e-33_rc  !< minimum mixing ratio
  integer :: i, k

  if (imode < 0) then
     ! Assign incoming "xrow" to local gas specie arrays;
     ! while converting from volume mixing ratio to molecules /cm**3:
     do k = 1, ilev
        do i = il1, il2
           qOx     (i,k) = real(xrow(i,k,itox), kind=rc) * airrow(i,k)
           qNOx    (i,k) = real(xrow(i,k,itnx), kind=rc) * airrow(i,k)
           qClOx   (i,k) = real(xrow(i,k,itc9), kind=rc) * airrow(i,k)
           qBrOx   (i,k) = real(xrow(i,k,itbx), kind=rc) * airrow(i,k)
           qHNO3   (i,k) = real(xrow(i,k,itn5), kind=rc) * airrow(i,k)
           qHOx    (i,k) = real(xrow(i,k,ithx), kind=rc) * airrow(i,k)
           qHNO4   (i,k) = real(xrow(i,k,itn7), kind=rc) * airrow(i,k)
           qN2O    (i,k) = real(xrow(i,k,itt5), kind=rc) * airrow(i,k)
           qHBr    (i,k) = real(xrow(i,k,ithb), kind=rc) * airrow(i,k)
           qCH4    (i,k) = real(xrow(i,k,itmm), kind=rc) * airrow(i,k)
           qCO     (i,k) = real(xrow(i,k,itco), kind=rc) * airrow(i,k)
           qCH2O   (i,k) = real(xrow(i,k,itha), kind=rc) * airrow(i,k)
           qCH3OOH (i,k) = real(xrow(i,k,itmh), kind=rc) * airrow(i,k)
           qH2     (i,k) = real(xrow(i,k,ithd), kind=rc) * airrow(i,k)
           qHCl    (i,k) = real(xrow(i,k,itc7), kind=rc) * airrow(i,k)
           qClONO2 (i,k) = real(xrow(i,k,itc4), kind=rc) * airrow(i,k)
           qCFC11  (i,k) = real(xrow(i,k,itf1), kind=rc) * airrow(i,k)
           qCFC12  (i,k) = real(xrow(i,k,itf2), kind=rc) * airrow(i,k)
           qCCl4   (i,k) = real(xrow(i,k,itf3), kind=rc) * airrow(i,k)
           qCH3CCl3(i,k) = real(xrow(i,k,itf4), kind=rc) * airrow(i,k)
           qHCFC22 (i,k) = real(xrow(i,k,itf5), kind=rc) * airrow(i,k)
           qCH3Cl  (i,k) = real(xrow(i,k,itf6), kind=rc) * airrow(i,k)
           qCH3Br  (i,k) = real(xrow(i,k,itcb), kind=rc) * airrow(i,k)
           qCHBr3  (i,k) = real(xrow(i,k,itbf), kind=rc) * airrow(i,k)
           qCH2Br2 (i,k) = real(xrow(i,k,itdb), kind=rc) * airrow(i,k)
           qO1D    (i,k) = real(xrow(i,k,ito1), kind=rc) * airrow(i,k)
           qO3P    (i,k) = real(xrow(i,k,itoo), kind=rc) * airrow(i,k)
           qO3     (i,k) = real(xrow(i,k,ito3), kind=rc) * airrow(i,k)
           qNO     (i,k) = real(xrow(i,k,itn1), kind=rc) * airrow(i,k)
           qNO2    (i,k) = real(xrow(i,k,itn2), kind=rc) * airrow(i,k)
           qNO3    (i,k) = real(xrow(i,k,itn3), kind=rc) * airrow(i,k)
           qN2O5   (i,k) = real(xrow(i,k,itn4), kind=rc) * airrow(i,k)
           qCl     (i,k) = real(xrow(i,k,itc1), kind=rc) * airrow(i,k)
           qHOCl   (i,k) = real(xrow(i,k,itc6), kind=rc) * airrow(i,k)
           qOClO   (i,k) = real(xrow(i,k,itoc), kind=rc) * airrow(i,k)
           qClO    (i,k) = real(xrow(i,k,itc3), kind=rc) * airrow(i,k)
           qCl2O2  (i,k) = real(xrow(i,k,itc5), kind=rc) * airrow(i,k)
           qBr     (i,k) = real(xrow(i,k,itbr), kind=rc) * airrow(i,k)
           qBrO    (i,k) = real(xrow(i,k,itbo), kind=rc) * airrow(i,k)
           qHOBr   (i,k) = real(xrow(i,k,itob), kind=rc) * airrow(i,k)
           qBrCl   (i,k) = real(xrow(i,k,itbc), kind=rc) * airrow(i,k)
           qBrONO2 (i,k) = real(xrow(i,k,itbn), kind=rc) * airrow(i,k)
           qH2O    (i,k) = real(xrow(i,k,itt6), kind=rc) * airrow(i,k)
           qH      (i,k) = real(xrow(i,k,ith0), kind=rc) * airrow(i,k)
           qH2O2   (i,k) = real(xrow(i,k,ithh), kind=rc) * airrow(i,k)
           qOH     (i,k) = real(xrow(i,k,ith1), kind=rc) * airrow(i,k)
           qHO2    (i,k) = real(xrow(i,k,ith2), kind=rc) * airrow(i,k)
           qCH3O2  (i,k) = real(xrow(i,k,itm2), kind=rc) * airrow(i,k)
           qCl2    (i,k) = real(xrow(i,k,itc2), kind=rc) * airrow(i,k)
        end do
     end do

     if (kount == 0 .or. kount == 1) then
       do k = 1, ilev
         do i = il1, il2
           qO1D(i,k)   = 0.0_rc
           qO3P(i,k)   = 0.0_rc
           qO3(i,k)    = qOx(i,k)
           qNO(i,k)    = 0.0_rc
           qNO2(i,k)   = qNOx(i,k)
           qNO3(i,k)   = 0.0_rc
           qN2O5(i,k)  = 0.0_rc
           qH2O2(i,k)  = 0.0_rc
           qHO2(i,k)   = 0.0_rc
           qH(i,k)     = 0.0_rc
           qOH(i,k)    = qHOx(i,k)
           qHOCl(i,k)  = 0.0_rc
           qCl(i,k)    = 0.0_rc
           qOClO(i,k)  = qClOx(i,k)
           qCl2O2(i,k) = 0.0_rc
           qCl2(i,k)   = 0.0_rc
           qClO(i,k)   = 0.0_rc
           qBrCl(i,k)  = 0.0_rc
           qBrONO2(i,k)= 0.0_rc
           qBr(i,k)    = 0.0_rc
           qHOBr(i,k)  = 0.0_rc
           qBrO(i,k)   = qBrOx(i,k)
           qCH3O2(i,k) = 0.0_rc
         enddo
       enddo
     endif

     do k = 1, ilev
       do i = il1, il2
         cOxold   = qO3(i,k) + qO3P(i,k) + qO1D(i,k)
         cHOxold  = qH(i,k) + qOH(i,k) + qHO2(i,k) + 2.0_rc*qH2O2(i,k)
         cNOxold  = qNO(i,k) + qNO2(i,k) + qNO3(i,k) + 2.0_rc*qN2O5(i,k)
         cClOxold = qClO(i,k) + qHOCl(i,k) + qCl(i,k) + qOClO(i,k)   &
                  + 2.0_rc*(qCl2(i,k) + qCl2O2(i,k))
         cBrOxold = qBrO(i,k) + qHOBr(i,k) + qBr(i,k) + qBrCl(i,k) + qBrONO2(i,k)

         ! Add conditionals to avoid possible division by zero
         if (cOxold > 0.0_rc) then
            cOxold   = qOx(i,k) / cOxold
         else
            cOxold   = 1.0_rc
         end if
         if (cHOxold > 0.0_rc) then
            cHOxold  = qHOx(i,k) / cHOxold
         else
            cHOxold   = 1.0_rc
         end if
         if (cNOxold > 0.0_rc) then
            cNOxold  = qNOx(i,k) / cNOxold
         else
            cNOxold   = 1.0_rc
         end if
         if (cClOxold > 0.0_rc) then
            cClOxold = qClOx(i,k) / cClOxold
         else
            cClOxold   = 1.0_rc
         end if
         if (cBrOxold > 0.0_rc) then
            cBrOxold = qBrOx(i,k) / cBrOxold
         else
            cBrOxold   = 1.0_rc
         end if

         an2(1,i,k)  = qO3P(i,k)*cOxold
         an2(2,i,k)  = qO3(i,k)*cOxold
         an2(3,i,k)  = qO1D(i,k)*cOxold
         an2(4,i,k)  = qH(i,k)*cHOxold
         an2(5,i,k)  = qOH(i,k)*cHOxold
         an2(6,i,k)  = qHO2(i,k)*cHOxold
         an2(7,i,k)  = qHNO4(i,k)
         an2(8,i,k)  = qNO(i,k)*cNOxold
         an2(9,i,k)  = qNO2(i,k)*cNOxold
         an2(10,i,k) = qNO3(i,k)*cNOxold
         an2(11,i,k) = qN2O5(i,k)*cNOxold
         an2(12,i,k) = qCl(i,k)*cClOxold
         an2(13,i,k) = qClO(i,k)*cClOxold
         an2(14,i,k) = qClONO2(i,k)
         an2(15,i,k) = qHOCl(i,k)*cClOxold
         an2(16,i,k) = qHCl(i,k)
         an2(17,i,k) = qCl2O2(i,k)*cClOxold
         an2(18,i,k) = qCl2(i,k)*cClOxold
         an2(19,i,k) = qOClO(i,k)*cClOxold
         an2(20,i,k) = qBr(i,k)*cBrOxold
         an2(21,i,k) = qBrO(i,k)*cBrOxold
         an2(22,i,k) = qBrCl(i,k)*cBrOxold
         an2(23,i,k) = qBrONO2(i,k)*cBrOxold
         an2(24,i,k) = qHBr(i,k)
         an2(25,i,k) = qHOBr(i,k)*cBrOxold
         an2(26,i,k) = qCH3O2(i,k)
         an2(27,i,k) = qH2O(i,k)
         an2(28,i,k) = qHNO3(i,k)
         an2(29,i,k) = qH2O2(i,k)*cHOxold
         an2(30,i,k) = qN2O(i,k)
         an2(31,i,k) = qCH4(i,k)
         an2(32,i,k) = qCH2O(i,k)
         an2(33,i,k) = qCH3OOH(i,k)
         an2(34,i,k) = qCO(i,k)
         an2(35,i,k) = qH2(i,k)
         an2(36,i,k) = qCFC11(i,k)
         an2(37,i,k) = qCFC12(i,k)
         an2(38,i,k) = qCCl4(i,k)
         an2(39,i,k) = qCH3CCl3(i,k)
         an2(40,i,k) = qHCFC22(i,k)
         an2(41,i,k) = qCH3Cl(i,k)
         an2(42,i,k) = qCH3Br(i,k)
         an2(43,i,k) = qCHBr3(i,k)
         an2(44,i,k) = qCH2Br2(i,k)
       enddo
     enddo

  else

     do k = 1, ilev
       do i = il1, il2
         qO3P(i,k)     = an2(1,i,k)
         qO3(i,k)      = an2(2,i,k)
         qO1D(i,k)     = an2(3,i,k)
         qH(i,k)       = an2(4,i,k)
         qOH(i,k)      = an2(5,i,k)
         qHO2(i,k)     = an2(6,i,k)
         qHNO4(i,k)    = an2(7,i,k)
         qNO(i,k)      = an2(8,i,k)
         qNO2(i,k)     = an2(9,i,k)
         qNO3(i,k)     = an2(10,i,k)
         qN2O5(i,k)    = an2(11,i,k)
         qCL(i,k)      = an2(12,i,k)
         qCLO(i,k)     = an2(13,i,k)
         qClONO2(i,k)  = an2(14,i,k)
         qHOCl(i,k)    = an2(15,i,k)
         qHCl(i,k)     = an2(16,i,k)
         qCL2O2(i,k)   = an2(17,i,k)
         qCl2(i,k)     = an2(18,i,k)
         qOClO(i,k)    = an2(19,i,k)
         qBr(i,k)      = an2(20,i,k)
         qBrO(i,k)     = an2(21,i,k)
         qBrCl(i,k)    = an2(22,i,k)
         qBrONO2(i,k)  = an2(23,i,k)
         qHBr(i,k)     = an2(24,i,k)
         qHOBr(i,k)    = an2(25,i,k)
         qCH3O2(i,k)   = an2(26,i,k)
         qH2O(i,k)     = an2(27,i,k)
         qHNO3(i,k)    = an2(28,i,k)
         qH2O2(i,k)    = an2(29,i,k)
         qN2O(i,k)     = an2(30,i,k)
         qCH4(i,k)     = an2(31,i,k)
         qCH2O(i,k)    = an2(32,i,k)
         qCH3OOH(i,k)  = an2(33,i,k)
         qCO(i,k)      = an2(34,i,k)
         qH2(i,k)      = an2(35,i,k)
         qCFC11(i,k)   = an2(36,i,k)
         qCFC12(i,k)   = an2(37,i,k)
         qCCl4(i,k)    = an2(38,i,k)
         qCH3CCl3(i,k) = an2(39,i,k)
         qHCFC22(i,k)  = an2(40,i,k)
         qCH3Cl(i,k)   = an2(41,i,k)
         qCH3Br(i,k)   = an2(42,i,k)
         qCHBr3(i,k)   = an2(43,i,k)
         qCH2Br2(i,k)  = an2(44,i,k)

         qOx(i,k)   = qO3(i,k) + qO3P(i,k) + qO1D(i,k)
         qHOx(i,k)  = qH(i,k) + qOH(i,k) + qHO2(i,k) + 2.0_rc*qH2O2(i,k)
         qNOx(i,k)  = qNO(i,k) + qNO2(i,k) + qNO3(i,k) + 2.0_rc*qN2O5(i,k)
         qClOx(i,k) = qCl(i,k) + qClO(i,k) + 2.0_rc*(qCl2O2(i,k) + qCl2(i,k)) &
                    + qHOCl(i,k) + qOClO(i,k)
         qBrOx(i,k) = qBr(i,k) + qBrO(i,k) + qBrONO2(i,k) + qBrCl(i,k) + qHOBr(i,k)

       enddo   ! end loop over i=il1,il2
     enddo  ! end loop over k=1,ilev

     ! Convert species molecules/cm^3 back to volume mixing ratio, while re-assigning back to xrow
     do k = 1, ilev
        do i = il1, il2
           xrow(i,k,itox)   = real(max(qOx     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itnx)   = real(max(qNOx    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc9)   = real(max(qClOx   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itbx)   = real(max(qBrOx   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itn5)   = real(max(qHNO3   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ithx)   = real(max(qHOx    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itn7)   = real(max(qHNO4   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itt5)   = real(max(qN2O    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ithb)   = real(max(qHBr    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itmm)   = real(max(qCH4    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itco)   = real(max(qCO     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itha)   = real(max(qCH2O   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itmh)   = real(max(qCH3OOH (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ithd)   = real(max(qH2     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc7)   = real(max(qHCl    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc4)   = real(max(qClONO2 (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itf1)   = real(max(qCFC11  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itf2)   = real(max(qCFC12  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itf3)   = real(max(qCCl4   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itf4)   = real(max(qCH3CCl3(i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itf5)   = real(max(qHCFC22 (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itf6)   = real(max(qCH3Cl  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itcb)   = real(max(qCH3Br  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itbf)   = real(max(qCHBr3  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itdb)   = real(max(qCH2Br2 (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ito1)   = real(max(qO1D    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itoo)   = real(max(qO3P    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ito3)   = real(max(qO3     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itn1)   = real(max(qNO     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itn2)   = real(max(qNO2    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itn3)   = real(max(qNO3    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itn4)   = real(max(qN2O5   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc1)   = real(max(qCl     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc6)   = real(max(qHOCl   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itoc)   = real(max(qOClO   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc3)   = real(max(qClO    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc5)   = real(max(qCl2O2  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itbr)   = real(max(qBr     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itbo)   = real(max(qBrO    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itob)   = real(max(qHOBr   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itbc)   = real(max(qBrCl   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itbn)   = real(max(qBrONO2 (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itt6)   = real(max(qH2O    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ith0)   = real(max(qH      (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ithh)   = real(max(qH2O2   (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ith1)   = real(max(qOH     (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,ith2)   = real(max(qHO2    (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itm2)   = real(max(qCH3O2  (i,k) / airrow(i,k), qcpmin))
           xrow(i,k,itc2)   = real(max(qCl2    (i,k) / airrow(i,k), qcpmin))
        end do
     end do
  end if

  return
  end subroutine mam_assign_species


end module mamchmf2_mod
