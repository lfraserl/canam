subroutine tmcalc(tbar,thliq,thice,hcp,tpond,zpond,tsnow,zsnow, &
                        albsno,rhosno,hcpsno,tbase,ovrflw,tovrfl, &
                        runoff,trunof,hmfg,htc,htcs,wtrs,wtrg, &
                        fi,tbarw,gzero,g12,g23,ggeo,ta,wsnow, &
                        tctop,tcbot,gflux,zplim,thpor,thlmin,hcps,hcpice, &
                        delzw,delzz,delz,isand,iwf,ig,ilg,il1,il2,jl,n)
  !
  !     * feb 22/08 - d.verseghy. streamline some calculations.
  !     * nov 20/06 - d.verseghy. add geothermal heat flux.
  !     * apr 03/06 - d.verseghy. allow for presence of water in snow,
  !     * dec 07/05 - d.verseghy. revised heat flux calculation between
  !     *                         soil and tbase in layer 3.
  !     * oct 06/05 - d.verseghy. modify for cases where ig>3.
  !     * mar 30/05 - d.verseghy. add runoff temperature calculation.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * may 16/03 - y.delage/d.verseghy. bugfix in freezing/
  !     *                                  thawing calculations
  !     *                                  (present since v.2.7)
  !     * jun 17/02 - d.verseghy. remove incorporation of ponded water
  !     *                         into first layer soil moisture
  !     *                         update subroutine call; shortened
  !     *                         class4 common block.
  !     * dec 12/01 - d.verseghy. pass in switch to do surface flow
  !     *                         calculation only if watflood routines
  !     *                         are not being run.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics; introduce calculation
  !     *                         of overland flow.
  !     * aug 30/95 - d.verseghy. class - version 2.4.
  !     *                         variable surface detention capacity
  !     *                         implemented.
  !     * aug 18/95 - d.verseghy. revisions to allow for inhomogeneity
  !     *                         between soil layers and fractional
  !     *                         organic matter content.
  !     * aug 16/95 - d.verseghy. two new arrays to complete water
  !     *                         balance diagnostics.
  !     * dec 22/94 - d.verseghy. class - version 2.3.
  !     *                         revise calculations of tbar and htc
  !     *                         allow specification of limiting pond
  !     *                         depth "PNDLIM" (parallel changes
  !     *                         made simultaneously in classw).
  !     * nov 01/93 - d.verseghy. class - version 2.2.
  !     *                         revised version with in-lined code
  !     *                         from twcalc and tfreez to permit
  !     *                         freezing and thawing of soil layers
  !     *                         at the end of each time step.
  !     * jul 30/93 - d.verseghy/m.lazare. new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. store ponded water into first
  !     *                         soil layer liquid water; step
  !     *                         ahead soil layer temperatures
  !     *                         using conduction heat flux
  !     *                         calculated at top and bottom
  !     *                         of each layer.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, hcpw, hcpsnd, rhow, rhoice, clhmlt
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: iwf !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: n !<
  !
  ! Pass in the parameters as an argument from classw, to maintain bit-identity
  real, intent(in) :: hcpice 
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: tbar  (ilg,ig) !<
  real, intent(inout) :: thliq (ilg,ig) !<
  real, intent(inout) :: thice (ilg,ig) !<
  real, intent(inout) :: hcp   (ilg,ig) !<
  real, intent(inout) :: hmfg  (ilg,ig) !<
  real, intent(inout) :: htc   (ilg,ig) !<
  !
  real, intent(inout) :: tpond (ilg) !<
  real, intent(inout) :: zpond (ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: albsno(ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(inout) :: tbase (ilg) !<
  real, intent(inout) :: ovrflw(ilg) !<
  real, intent(inout) :: tovrfl(ilg) !<
  real, intent(inout) :: runoff(ilg) !<
  real, intent(inout) :: trunof(ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  real, intent(inout) :: wtrs  (ilg) !<
  real, intent(inout) :: wtrg  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: tbarw (ilg,ig) !<
  real, intent(inout) :: gzero (ilg) !<
  real, intent(in) :: g12   (ilg) !<
  real, intent(in) :: g23   (ilg) !<
  real, intent(in) :: ggeo  (ilg) !<
  real, intent(in) :: ta    (ilg) !<
  real, intent(in) :: wsnow (ilg) !<
  real, intent(in) :: tctop (ilg,ig) !<
  real, intent(in) :: tcbot (ilg,ig) !<
  real, intent(in) :: zplim (ilg) !<
  !
  !     * soil information arrays.
  !
  real, intent(in) :: thpor (ilg,ig) !<
  real, intent(in) :: thlmin(ilg,ig) !<
  real, intent(in) :: hcps  (ilg,ig) !<
  real, intent(in) :: delzw (ilg,ig) !<
  real, intent(in) :: delzz (ilg,ig) !<
  real, intent(in) :: delz  (ig) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * temporary variables.
  !
  real, intent(inout), dimension(ilg,ig) :: gflux !<
  !
  integer :: i !<
  integer :: j !<
  real :: gp1 !<
  real :: zfrez !<
  real :: hadd !<
  real :: hconv !<
  real :: ttest !<
  real :: tlim !<
  real :: hexces !<
  real :: thfrez !<
  real :: thmelt !<
  real :: g3b !<
  !
  !-----------------------------------------------------------------------
  !
  !     * calculate subsurface and overland runoff terms; adjust
  !     * surface ponding depth.
  !
  do i=il1,il2
    if (fi(i)>0. .and. isand(i,1)>-4 .and. iwf==0 .and. &
        (zpond(i)-zplim(i))>1.0e-8) then
      trunof(i)=(trunof(i)*runoff(i)+(tpond(i)+tfrez)* &
                   (zpond(i)-zplim(i)))/(runoff(i)+ &
                   (zpond(i)-zplim(i)))
      runoff(i)=runoff(i)+(zpond(i)-zplim(i))
      tovrfl(i)=(tovrfl(i)*ovrflw(i)+(tpond(i)+tfrez)* &
                   fi(i)*(zpond(i)-zplim(i)))/(ovrflw(i)+ &
                   fi(i)*(zpond(i)-zplim(i)))
      ovrflw(i)=ovrflw(i)+fi(i)*(zpond(i)-zplim(i))
      zpond(i)=min(zpond(i),zplim(i))
    end if
  end do ! loop 100
  !
  !     * update soil temperatures after ground water movement.
  !
  do j=1,ig
    do i=il1,il2
      if (fi(i)>0. .and. delzw(i,j)>0. .and. isand(i,1)>-4) &
          then
        htc(i,j)=htc(i,j)+fi(i)*((tbarw(i,j)+tfrez)* &
                  hcpw*thliq(i,j)+(tbar(i,j)+tfrez)* &
                  hcpice*thice(i,j))*delzw(i,j)/delt
        hcp(i,j)=hcpw*thliq(i,j)+hcpice*thice(i,j)+ &
                  hcps(i,j)*(1.-thpor(i,j))
        tbar(i,j)=((tbarw(i,j)+tfrez)*hcpw*thliq(i,j)* &
                   delzw(i,j)+(tbar(i,j)+tfrez)*((hcpice* &
                   thice(i,j)+hcps(i,j)*(1.-thpor(i,j)))* &
                   delzw(i,j)+hcpsnd*(delzz(i,j)-delzw(i,j))))/ &
                   (hcp(i,j)*delzw(i,j)+hcpsnd*(delzz(i,j)- &
                   delzw(i,j)))-tfrez
      end if
    end do
  end do ! loop 200
  !
  !     * step ahead pond temperature; check for freezing, and add
  !     * frozen water to snow pack.
  !
  do i=il1,il2
    if (fi(i)>0. .and. isand(i,1)>-4 .and. zpond(i)>0.0) &
        then
      htc(i,1)=htc(i,1)+fi(i)*hcpw*(tpond(i)+tfrez)* &
             zpond(i)/delt
      gp1=zpond(i)*(g12(i)-gzero(i))/(zpond(i)+delzz(i,1))+ &
             gzero(i)
      tpond(i) =tpond(i)+(gzero(i)-gp1)*delt/(hcpw*zpond(i))
      gzero(i)=gp1
    end if
  end do ! loop 300
  !
  do i=il1,il2
    if (fi(i)>0. .and. isand(i,1)>-4 .and. zpond(i)>0. &
        .and. tpond(i)<0.) &
        then
      htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                zsnow(i)/delt
      zfrez=0.0
      hadd=-tpond(i)*hcpw*zpond(i)
      tpond(i)=0.0
      hconv=clhmlt*rhow*zpond(i)
      htc(i,1)=htc(i,1)-fi(i)*hcpw*tfrez*zpond(i)/delt
      if (hadd<=hconv) then
        zfrez=hadd/(clhmlt*rhow)
        zpond(i)=zpond(i)-zfrez
        zfrez=zfrez*rhow/rhoice
        if (.not.(zsnow(i)>0.0)) albsno(i)=0.50
        tsnow(i)=tsnow(i)*hcpsno(i)*zsnow(i)/(hcpsno(i)*zsnow(i) &
                   +hcpice*zfrez)
        rhosno(i)=(rhosno(i)*zsnow(i)+rhoice*zfrez)/(zsnow(i) &
                    +zfrez)
        if (zsnow(i)>0.0) then
          hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                    (rhow*zsnow(i))
        else
          hcpsno(i)=hcpice*rhosno(i)/rhoice
        end if
        zsnow(i)=zsnow(i)+zfrez
      else
        hadd=hadd-hconv
        zfrez=zpond(i)*rhow/rhoice
        ttest=-hadd/(hcpice*zfrez)
        if (zsnow(i)>0.0) then
          tlim=min(tsnow(i),tbar(i,1))
        else
          tlim=min(ta(i)-tfrez,tbar(i,1))
        end if
        if (ttest<tlim) then
          hexces=hadd+tlim*hcpice*zfrez
          gzero(i)=gzero(i)-hexces/delt
          htc(i,1)=htc(i,1)+fi(i)*(hadd-hexces)/delt
          tsnow(i)=(tsnow(i)*hcpsno(i)*zsnow(i)+ &
                       tlim*hcpice*zfrez) &
                       /(hcpsno(i)*zsnow(i)+hcpice*zfrez)
        else
          tsnow(i)=(tsnow(i)*hcpsno(i)*zsnow(i)+ttest*hcpice* &
                        zfrez)/(hcpsno(i)*zsnow(i)+hcpice*zfrez)
          htc(i,1)=htc(i,1)+fi(i)*hadd/delt
        end if
        if (.not.(zsnow(i)>0.0)) albsno(i)=0.50
        rhosno(i)=(rhosno(i)*zsnow(i)+rhoice*zfrez)/(zsnow(i)+ &
                      zfrez)
        if (zsnow(i)>0.0) then
          hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                    (rhow*zsnow(i))
        else
          hcpsno(i)=hcpice*rhosno(i)/rhoice
        end if
        zsnow(i)=zsnow(i)+zfrez
        zpond(i)=0.0
      end if
      htc (i,1)=htc (i,1)+fi(i)*hcpw*tfrez*zpond(i)/delt
      hmfg(i,1)=hmfg(i,1)-fi(i)*clhmlt*rhoice*zfrez/delt
      wtrs(i)=wtrs(i)+fi(i)*zfrez*rhoice/delt
      wtrg(i)=wtrg(i)-fi(i)*zfrez*rhoice/delt
      htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)*zsnow(i)/ &
                delt
    end if
  end do ! loop 400
  !
  !     * step ahead soil layer temperatures; check for freezing or
  !     * thawing.
  !
  if (ig>3) then
    do j=4,ig
      do i=il1,il2
        if (fi(i)>0. .and. isand(i,1)>-4) then
          gflux(i,j)=(tcbot(i,j-1)+tctop(i,j))*(tbar(i,j-1)- &
                        tbar(i,j))/(delz(j-1)+delz(j))
        end if
      end do
    end do ! loop 500
  end if
  !
  do i=il1,il2
    if (fi(i)>0. .and. isand(i,1)>-4) then
      tbar(i,1)=tbar(i,1)+(gzero(i)-g12(i))*delt/ &
                   (hcp(i,1)*delzw(i,1)+hcpsnd*(delzz(i,1)- &
                   delzw(i,1)))
      tbar(i,2)=tbar(i,2)+(g12  (i)-g23(i))*delt/ &
                   (hcp(i,2)*delzw(i,2)+hcpsnd*(delzz(i,2)- &
                   delzw(i,2)))
      if (ig==3) then
        if (delzz(i,3)>0.0) then
          if (delzz(i,ig)<(delz(ig)-1.0e-5)) then
            g3b=(tcbot(i,3)+tctop(i,3))*(tbar(i,3)- &
                         tbase(i))/delz(3)
            tbar(i,3)=tbar(i,3)+(g23(i)-g3b)*delt/ &
                         (hcp(i,3)*delzz(i,3))
            tbase(i)=tbase(i)+(g3b-ggeo(i))*delt/ &
                         (hcpsnd*(delz(3)-delzz(i,3)))
          else
            tbar(i,3)=tbar(i,3)+(g23(i)-ggeo(i))*delt/ &
                         (hcp(i,3)*delzw(i,3))
          end if
        else
          tbase(i)=tbase(i)+(g23(i)-ggeo(i))*delt/ &
                     (hcpsnd*delz(3))
        end if
        htc(i,3)=htc(i,3)-fi(i)*ggeo(i)
      else
        tbar(i,3)=tbar(i,3)+g23(i)*delt/ &
                       (hcp(i,3)*delzw(i,3)+hcpsnd*(delz(3)- &
                       delzw(i,3)))
      end if
      gflux(i,1)=gzero(i)
      gflux(i,2)=g12(i)
      gflux(i,3)=g23(i)
    end if
  end do ! loop 550
  !
  do j=3,ig
    do i=il1,il2
      if (fi(i)>0. .and. isand(i,1)>-4 .and. ig>3) then
        htc(i,j)=htc(i,j)-fi(i)*(tbar(i,j)+tfrez)*(hcp(i,j)* &
                     delzw(i,j)+hcpsnd*(delzz(i,j)- &
                     delzw(i,j)))/delt
        if (j==3) then
          tbar(i,j)=tbar(i,j)-gflux(i,j+1)*delt/ &
                       (hcp(i,j)*delzw(i,j)+hcpsnd*(delzz(i,j)- &
                       delzw(i,j)))
        else if (j==ig) then
          tbar(i,j)=tbar(i,j)+(gflux(i,j)-ggeo(i))*delt/ &
                       (hcp(i,j)*delzw(i,j)+hcpsnd*(delzz(i,j)- &
                       delzw(i,j)))
        else
          tbar(i,j)=tbar(i,j)+(gflux(i,j)-gflux(i,j+1))*delt/ &
                   (hcp(i,j)*delzw(i,j)+hcpsnd*(delzz(i,j)- &
                   delzw(i,j)))
        end if
        htc(i,j)=htc(i,j)+fi(i)*(tbar(i,j)+tfrez)*(hcp(i,j)* &
                     delzw(i,j)+hcpsnd*(delzz(i,j)- &
                     delzw(i,j)))/delt
      end if
    end do
  end do ! loop 600
  !
  do j=1,ig
    do i=il1,il2
      if (fi(i)>0. .and. delzw(i,j)>0. .and. isand(i,1)>-4) &
          then
        htc(i,j)=htc(i,j)-fi(i)*(tbar(i,j)+tfrez)*(hcp(i,j)* &
                     delzw(i,j)+hcpsnd*(delzz(i,j)- &
                     delzw(i,j)))/delt
        if (tbar(i,j)<0. .and. thliq(i,j)>thlmin(i,j)) &
            then
          thfrez=-(hcp(i,j)*delzw(i,j)+hcpsnd*(delzz(i,j)- &
                     delzw(i,j)))*tbar(i,j)/(clhmlt*rhow* &
                     delzw(i,j))
          if (thfrez<=(thliq(i,j)-thlmin(i,j))) then
            hmfg(i,j)=hmfg(i,j)-fi(i)*thfrez*clhmlt* &
                           rhow*delzw(i,j)/delt
            htc(i,j)=htc(i,j)-fi(i)*thfrez*clhmlt* &
                           rhow*delzw(i,j)/delt
            thliq(i,j)=thliq(i,j)-thfrez
            thice(i,j)=thice(i,j)+thfrez*rhow/rhoice
            hcp(i,j)=hcpw*thliq(i,j)+hcpice*thice(i,j)+ &
                            hcps(i,j)*(1.-thpor(i,j))
            tbar (i,j)=0.0
          else
            hmfg(i,j)=hmfg(i,j)-fi(i)*(thliq(i,j)- &
                     thlmin(i,j))*clhmlt*rhow*delzw(i,j)/delt
            htc(i,j)=htc(i,j)-fi(i)*(thliq(i,j)- &
                     thlmin(i,j))*clhmlt*rhow*delzw(i,j)/delt
            hadd=(thfrez-(thliq(i,j)-thlmin(i,j)))*clhmlt* &
                      rhow*delzw(i,j)
            thice(i,j)=thice(i,j)+(thliq(i,j)- &
                            thlmin(i,j))*rhow/rhoice
            thliq(i,j)=thlmin(i,j)
            hcp(i,j)=hcpw*thliq(i,j)+hcpice*thice(i,j)+ &
                            hcps(i,j)*(1.-thpor(i,j))
            tbar (i,j)=-hadd/(hcp(i,j)*delzw(i,j)+hcpsnd* &
                            (delzz(i,j)-delzw(i,j)))
          end if
        end if
        !
        if (tbar(i,j)>0. .and. thice(i,j)>0.) then
          thmelt=(hcp(i,j)*delzw(i,j)+hcpsnd*(delzz(i,j)- &
                     delzw(i,j)))*tbar(i,j)/(clhmlt*rhoice* &
                     delzw(i,j))
          if (thmelt<=thice(i,j)) then
            hmfg(i,j)=hmfg(i,j)+fi(i)*thmelt*clhmlt* &
                           rhoice*delzw(i,j)/delt
            htc(i,j)=htc(i,j)+fi(i)*thmelt*clhmlt* &
                           rhoice*delzw(i,j)/delt
            thice(i,j)=thice(i,j)-thmelt
            thliq(i,j)=thliq(i,j)+thmelt*rhoice/rhow
            hcp(i,j)=hcpw*thliq(i,j)+hcpice*thice(i,j)+ &
                            hcps(i,j)*(1.-thpor(i,j))
            tbar (i,j)=0.0
          else
            hmfg(i,j)=hmfg(i,j)+fi(i)*thice(i,j)*clhmlt* &
                           rhoice*delzw(i,j)/delt
            htc(i,j)=htc(i,j)+fi(i)*thice(i,j)*clhmlt* &
                           rhoice*delzw(i,j)/delt
            hadd=(thmelt-thice(i,j))*clhmlt*rhoice* &
                           delzw(i,j)
            thliq(i,j)=thliq(i,j)+thice(i,j)*rhoice/rhow
            thice(i,j)=0.0
            hcp(i,j)=hcpw*thliq(i,j)+hcpice*thice(i,j)+ &
                            hcps(i,j)*(1.-thpor(i,j))
            tbar (i,j)=hadd/(hcp(i,j)*delzw(i,j)+hcpsnd* &
                            (delzz(i,j)-delzw(i,j)))
          end if
        end if
        htc(i,j)=htc(i,j)+fi(i)*(tbar(i,j)+tfrez)*(hcp(i,j)* &
                     delzw(i,j)+hcpsnd*(delzz(i,j)- &
                     delzw(i,j)))/delt
      end if
    end do
  end do ! loop 700
  !
  return
end subroutine tmcalc
