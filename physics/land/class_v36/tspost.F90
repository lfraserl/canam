subroutine tspost(gsnow,tsnow,wsnow,rhosno,qmeltg, &
                        gzero,tsnbot,htcs,hmfn, &
                        gconsts,gcoeffs,gconst,gcoeff,tbar, &
                        tsurf,zsnow,tcsnow,hcpsno,qtrans, &
                        fi,delz,ilg,il1,il2,jl,ig)
  !
  !     * aug 16/06 - d.verseghy. major revision to implement thermal
  !     *                         separation of snow and soil.
  !     * mar 23/06 - d.verseghy. add calculations to allow for water
  !     *                         freezing in snowpack.
  !     * oct 04/05 - d.verseghy. modify 300 loop for cases where ig>3.
  !     * nov 04/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jun 17/02 - d.verseghy. reset ponded water temperature
  !     *                         using calculated ground heat flux
  !     *                         shortened class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         incorporate explicitly calculated
  !     *                         thermal conductivities at tops and
  !     *                         bottoms of soil layers, and
  !     *                         modifications to allow for variable
  !     *                         soil permeable depth.
  !     * sep 27/96 - d.verseghy. class - version 2.6.
  !     *                         fix bug in calculation of fluxes
  !     *                         between soil layers (present since
  !     *                         release of class version 2.5).
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * dec 22/94 - d.verseghy. class - version 2.3.
  !     *                         revise calculation of tbarpr(i,1).
  !     * apr 10/92 - m.lazare.   class - version 2.2.
  !     *                         divide previous subroutine "T4LAYR" into
  !     *                         "TSPREP" and "TSPOST" and vectorize.
  !     * apr 11/89 - d.verseghy. calculate heat fluxes between snow/soil
  !     *                         layers; consistency check on calculated
  !     *                         surface latent heat of melting/
  !     *                         freezing; step ahead snow layer
  !     *                         temperature and assign excess heat to
  !     *                         melting if necessary; disaggregate
  !     *                         first soil layer temperature into
  !     *                         ponded water and soil temperatures
  !     *                         add shortwave radiation transmitted
  !     *                         through snowpack to heat flux at top
  !     *                         of first soil layer; convert layer
  !     *                         temperatures to degrees c.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, hcpw, hcpice, rhow, rhoice, clhmlt
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ig !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: gzero (ilg) !<
  real, intent(inout) :: tsnbot(ilg) !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: gsnow (ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: wsnow (ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: qmeltg(ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  real, intent(inout) :: hmfn  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: tsurf (ilg) !<
  real, intent(in) :: zsnow (ilg) !<
  real, intent(in) :: tcsnow(ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(in) :: qtrans(ilg) !<
  real, intent(in) :: gconst(ilg) !<
  real, intent(in) :: gcoeff(ilg) !<
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: gconsts(ilg) !<
  real, intent(in) :: gcoeffs(ilg) !<
  real, intent(in) :: tbar(ilg,ig) !<
  real, intent(in) :: delz  (ig) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: j !<
  real :: gsnold !<
  real :: hadd !<
  real :: hconv !<
  real :: wfrez !<
  !-----------------------------------------------------------------------
  !
  do i=il1,il2
    if (fi(i)>0.) then
      gsnold=gcoeffs(i)*tsurf(i)+gconsts(i)
      tsnbot(i)=(zsnow(i)*tsnow(i)+delz(1)*tbar(i,1))/ &
              (zsnow(i)+delz(1))
      !              tsnbot(i)=0.90*tsnow(i)+0.10*tbar(i,1)
      !              tsnbot(i)=tsurf(i)-gsnold*zsnow(i)/(2.0*tcsnow(i))
      tsnbot(i)=min(tsnbot(i),tfrez)
      gzero(i)=gcoeff(i)*tsnbot(i)+gconst(i)
      if (qmeltg(i)<0.) then
        gsnow(i)=gsnow(i)+qmeltg(i)
        qmeltg(i)=0.
      end if
      tsnow(i)=tsnow(i)+(gsnow(i)-gzero(i))*delt/ &
                           (hcpsno(i)*zsnow(i))-tfrez
      if (tsnow(i)>0.) then
        qmeltg(i)=qmeltg(i)+tsnow(i)*hcpsno(i)*zsnow(i)/delt
        gsnow(i)=gsnow(i)-tsnow(i)*hcpsno(i)*zsnow(i)/delt
        tsnow(i)=0.
      end if
      gzero(i)=gzero(i)+qtrans(i)
    end if
  end do ! loop 100
  !
  do i=il1,il2
    if (fi(i)>0. .and. tsnow(i)<0. .and. wsnow(i)>0.) &
        then
      htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)*zsnow(i)/ &
                delt
      hadd=-tsnow(i)*hcpsno(i)*zsnow(i)
      hconv=clhmlt*wsnow(i)
      if (hadd<=hconv) then
        wfrez=hadd/clhmlt
        hadd=0.0
        wsnow(i)=max(0.0,wsnow(i)-wfrez)
        tsnow(i)=0.0
        rhosno(i)=rhosno(i)+wfrez/zsnow(i)
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                (rhow*zsnow(i))
      else
        hadd=hadd-hconv
        wfrez=wsnow(i)
        wsnow(i)=0.0
        rhosno(i)=rhosno(i)+wfrez/zsnow(i)
        hcpsno(i)=hcpice*rhosno(i)/rhoice
        tsnow(i)=-hadd/(hcpsno(i)*zsnow(i))
      end if
      hmfn(i)=hmfn(i)-fi(i)*clhmlt*wfrez/delt
      htcs(i)=htcs(i)-fi(i)*clhmlt*wfrez/delt
      htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)*zsnow(i)/ &
                delt
    end if
  end do ! loop 200
  !
  return
end subroutine tspost
