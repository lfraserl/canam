subroutine snovap(rhosno,zsnow,hcpsno,tsnow,evap,qfn,qfg,htcs, &
                        wlost,trunof,runoff,tovrfl,ovrflw, &
                        fi,r,s,rhosni,wsnow,ilg,il1,il2,jl)
  !
  !     * aug 25/11 - d.verseghy. correct calculation of trunof
  !     *                         and tovrfl.
  !     * feb 22/07 - d.verseghy. new accuracy limits for r and s.
  !     * mar 24/06 - d.verseghy. allow for presence of water in snow.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 26/02 - d.verseghy. change rhosni from constant to
  !     *                         variable.
  !     * apr 11/01 - m.lazare.   check for existence of snow before
  !     *                         performing calculations.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         pass in new "CLASS4" common block.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * aug 16/95 - d.verseghy. class - version 2.4.
  !     *                         incorporate diagnostic array "WLOST".
  !     * dec 22/94 - d.verseghy. class - version 2.3.
  !     *                         additional diagnostic calculation -
  !     *                         update htcs.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. sublimation from snowpack.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, hcpw, hcpice, rhow, rhoice, clhmlt, clhvap
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * input/output arrays.
  !
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  real, intent(inout) :: tsnow (ilg) !<
  real, intent(inout) :: evap  (ilg) !<
  real, intent(inout) :: qfn   (ilg) !<
  real, intent(inout) :: qfg   (ilg) !<
  real, intent(inout) :: htcs  (ilg) !<
  real, intent(inout) :: wlost (ilg) !<
  real, intent(inout) :: trunof(ilg) !<
  real, intent(inout) :: runoff(ilg) !<
  real, intent(inout) :: tovrfl(ilg) !<
  real, intent(inout) :: ovrflw(ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: r     (ilg) !<
  real, intent(in) :: s     (ilg) !<
  real, intent(in) :: rhosni(ilg) !<
  real, intent(inout) :: wsnow (ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  real :: zadd !<
  real :: zlost !<
  real :: zrem !<
  !-----------------------------------------------------------------------
  do i=il1,il2
    if (fi(i)>0. .and. (s(i)<1.0e-11 .or. r(i)<1.0e-11) &
        .and. zsnow(i)>0.) then
      htcs(i)=htcs(i)-fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                 zsnow(i)/delt
      if (evap(i)<0.) then
        zadd=-evap(i)*delt*rhow/rhosni(i)
        rhosno(i)=(zsnow(i)*rhosno(i)+zadd*rhosni(i))/ &
                       (zsnow(i)+zadd)
        zsnow (i)=zsnow(i)+zadd
        hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                 (rhow*zsnow(i))
        evap  (i)=0.0
      else
        zlost=evap(i)*delt*rhow/rhosno(i)
        if (zlost<=zsnow(i)) then
          zsnow(i)=zsnow(i)-zlost
          evap (i)=0.0
          hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
                     (rhow*zsnow(i))
        else
          zrem=(zlost-zsnow(i))*rhosno(i)/rhow
          zsnow(i)=0.0
          hcpsno(i)=0.0
          evap(i)=zrem*(clhmlt+clhvap)/(clhvap*delt)
          wlost(i)=wlost(i)-zrem*rhow*clhmlt/clhvap
          if (runoff(i)>0. .or. wsnow(i)>0.) &
              trunof(i)=(trunof(i)*runoff(i)+(tsnow(i)+tfrez)* &
              wsnow(i)/rhow)/(runoff(i)+wsnow(i)/rhow)
          runoff(i)=runoff(i)+wsnow(i)/rhow
          if (ovrflw(i)>0. .or. wsnow(i)>0.) &
              tovrfl(i)=(tovrfl(i)*ovrflw(i)+(tsnow(i)+tfrez)* &
              fi(i)*wsnow(i)/rhow)/(ovrflw(i)+fi(i)* &
              wsnow(i)/rhow)
          ovrflw(i)=ovrflw(i)+fi(i)*wsnow(i)/rhow
          tsnow(i)=0.0
          wsnow(i)=0.0
          qfn(i)=qfn(i)-fi(i)*zrem*rhow/delt
          qfg(i)=qfg(i)+fi(i)*evap(i)*rhow
        end if
      end if
      htcs(i)=htcs(i)+fi(i)*hcpsno(i)*(tsnow(i)+tfrez)* &
                 zsnow(i)/delt
    end if
  end do ! loop 100
  !
  return
end subroutine snovap
