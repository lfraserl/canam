subroutine snoalbw(albsno,rhosno,zsnow,hcpsno,tsnow, &
                         fi,s,rmelt,wsnow,rhomax,isand, &
                         ilg,ig,il1,il2,jl)
  !
  !     * feb 09/15 - d.verseghy. new version for gcm18 and class 3.6:
  !     *                         - change limiting condition on
  !     *                           existance of snow for snow albedo
  !     *                           calculation to be that of snowfall
  !     *                           during current timestep rather than
  !     *                           snowfall rate.
  !     * mar 07/07 - d.verseghy. streamline some calculations.
  !     * mar 24/06 - d.verseghy. allow for presence of water in snow.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * sep 10/04 - r.harvey/d.verseghy. increase snow albedo
  !     *                         refreshment and wetting thresholds.
  !     * aug 04/04 - y.delage/d.verseghy. protect sensitive
  !     *                         calculations agaist roundoff
  !     *                         errors.
  !     * apr 21/04 - f.seglenieks/d.verseghy. bug fix in snow
  !     *                         temperature comparisons.
  !     * jul 26/02 - d.verseghy. shortened class4 common block.
  !     * oct 20/00 - r.brown/d.verseghy. modified snow density
  !     *                                 calculations, accounting
  !     *                                 for settling in warm and
  !     *                                 cold snow.
  !     * jun 05/97 - d.verseghy. class - version 2.7.
  !     *                         specify location of ice sheets
  !     *                         by soil texture array rather
  !     *                         than by soil colour index.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * mar 13/92 - m.lazare.   class - version 2.1.
  !     *                         code for model version gcm7 -
  !     *                         divide previous subroutine
  !     *                         "SNOALB" into "SNOALBA" and
  !     *                         "SNOALBW" and vectorize.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. calculate decrease in snow albedo
  !     *                         and increase in density due to
  !     *                         aging. (assign different lower
  !     *                         snow albedo limits for dry and
  !     *                         melting snow.)
  !
  use times_mod, only : delt
  use hydcon, only : hcpw, hcpice, rhow, rhoice
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: ig !<
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: albsno(ilg) !<
  real, intent(inout) :: rhosno(ilg) !<
  real, intent(inout) :: zsnow (ilg) !<
  real, intent(inout) :: hcpsno(ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: tsnow (ilg) !<
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: s     (ilg) !<
  real, intent(in) :: rmelt (ilg) !<
  real, intent(in) :: wsnow (ilg) !<
  !
  integer, intent(in), dimension(ilg,ig) :: isand !<
  !
  !     * work array.
  !
  real, intent(inout), dimension(ilg) :: rhomax !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: iptbad !<
  real :: timfac !<
  real :: rhoold !<
  !----------------------------------------------------------------------
  iptbad=0
  do i=il1,il2
    if (zsnow(i)>0. .and. &
        fi  (i)>0. .and. s(i)*delt<1.0e-4) then
      if (albsno(i)>0.5001 .and. (rmelt(i)>1.0e-7 .or. &
          tsnow(i)>=-0.01)) then
        albsno(i)=(albsno(i)-0.50)*exp(-0.01*delt/3600.0)+ &
                 0.50
      else if (albsno(i)>0.7001 .and. rmelt(i)<=1.0e-7) &
 then
        albsno(i)=(albsno(i)-0.70)*exp(-0.01*delt/3600.0)+ &
                 0.70
      end if
    end if
    !
    if (fi(i)>0. .and. zsnow(i)>0.0001) then
      if (tsnow(i)<-0.01) then
        rhomax(i)=450.0-(204.7/zsnow(i))* &
                 (1.0-exp(-zsnow(i)/0.673))
      else
        rhomax(i)=700.0-(204.7/zsnow(i))* &
                 (1.0-exp(-zsnow(i)/0.673))
      end if
    end if
    !
    if (fi(i)>0. .and. zsnow(i)>0.0001 .and. &
        rhosno(i)<(rhomax(i)-0.01)) then
      rhoold=rhosno(i)
      rhosno(i)=(rhosno(i)-rhomax(i))*exp(-0.01*delt/3600.0)+ &
             rhomax(i)
      zsnow(i)=zsnow(i)*rhoold/rhosno(i)
      hcpsno(i)=hcpice*rhosno(i)/rhoice+hcpw*wsnow(i)/ &
             (rhow*zsnow(i))
    end if
    if ((albsno(i)<0.49 .or. albsno(i)>1.0) .and. &
        zsnow (i)>0. .and. fi(i)>0.)               iptbad=i
  end do ! loop 100
  !
  if (iptbad/=0) then
    write(6,6100) iptbad,jl,albsno(iptbad)
    6100    format('0AT (I,J)= (',i3,',',i3,'), ALBSNO = ',f10.5)
    call xit('SNOALBW',-1)
  end if
  !
  return
end subroutine snoalbw
