subroutine wflow(wmove,tmove,lzf,ninf,trmdr,tpond,zpond, &
                       r,tr,evap,psif,grkinf,thlinf,thliqx,tbarwx, &
                       delzx,zbotx,fmax,zf,dzf,dtflow,thlnlz, &
                       thlqlz,dzdisp,wdisp,wabs,iter,nend,isimp, &
                       igrn,ig,igp1,igp2,ilg,il1,il2,jl,n)

  !     * feb 09/09 - j.p.blanchette. increase limiting value of nend.
  !     * jan 06/09 - d.verseghy. checks on wetting front location
  !     *                         in 500 loop.
  !     * aug 07/07 - d.verseghy. increase iteration counter nend
  !     *                         move calculation of fmax from
  !     *                         grinfl to this routine.
  !     * may 17/06 - d.verseghy. protect against divisions by zero.
  !     * sep 13/05 - d.verseghy. replace hard-coded 4 with igp1.
  !     * sep 23/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 30/04 - d.verseghy/y.delage. protect sensitive
  !     *                         calculations against roundoff errors.
  !     * jun 21/02 - d.verseghy. update subroutine call.
  !     * mar 04/02 - d.verseghy. define "NEND" for all cases.
  !     * dec 16/94 - d.verseghy. bug fix - specify tmove behind
  !     *                         wetting front after any full-layer
  !     *                         jump downward.
  !     * apr 24/92 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. code for model version gcm7u -
  !     *                         class version 2.0 (with canopy).
  !     * apr 11/89 - d.verseghy. saturated flow of water through soil.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ig !<
  integer, intent(in) :: igp1 !<
  integer, intent(in) :: igp2 !<
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: n !<
  !
  !     * input/output fields.
  !
  real, intent(inout) :: wmove (ilg,igp2) !<
  real, intent(inout) :: tmove (ilg,igp2) !<
  !
  integer, intent(inout)                  :: lzf   (ilg) !<
  integer, intent(inout)                  :: ninf  (ilg) !<
  !
  real, intent(inout) :: trmdr (ilg) !<
  real, intent(inout) :: tpond (ilg) !<
  real, intent(inout) :: zpond (ilg) !<
  !
  !     * input fields.
  !
  real, intent(in) :: r     (ilg) !<
  real, intent(in) :: tr    (ilg) !<
  real, intent(in) :: evap  (ilg) !<
  real, intent(in) :: psif  (ilg,igp1) !<
  real, intent(in) :: grkinf(ilg,igp1) !<
  real, intent(in) :: thlinf(ilg,igp1) !<
  real, intent(in) :: thliqx(ilg,igp1) !<
  real, intent(in) :: tbarwx(ilg,igp1) !<
  real, intent(in) :: delzx (ilg,igp1) !<
  real, intent(in) :: zbotx (ilg,igp1) !<
  real, intent(inout) :: fmax  (ilg) !<
  real, intent(inout) :: zf    (ilg) !<
  !
  integer, intent(in), dimension(ilg) :: igrn !<
  !
  !     * internal work fields.
  !
  real, intent(inout) :: dzf   (ilg) !<
  real, intent(inout) :: dtflow(ilg) !<
  real, intent(inout) :: thlnlz(ilg) !<
  real, intent(inout) :: thlqlz(ilg) !<
  real, intent(inout) :: dzdisp(ilg) !<
  real, intent(inout) :: wdisp (ilg) !<
  real, intent(inout) :: wabs  (ilg) !<
  !
  integer, intent(inout)                  :: iter  (ilg) !<
  integer, intent(inout)                  :: nend  (ilg) !<
  integer, intent(inout)                  :: isimp (ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: j !<
  integer :: npnts !<
  integer :: nit !<
  real :: resid !<
  real :: finf !<
  real :: zptest !<
  real :: winf !<
  !-----------------------------------------------------------------------
  !
  !     * calculate iteration endpoint "NEND", and set switch "ITER" to 1
  !     * for points over which this subroutine is to be performed.
  !
  do i=il1,il2
    if (igrn(i)>0 .and. trmdr(i)>0.0) then
      resid=mod(trmdr(i),120.)
      if (resid>0.) then
        nend(i)=nint(trmdr(i)/120.+0.5)+100
      else
        nend(i)=nint(trmdr(i)/120.)+100
      end if
      iter(i)=1
      fmax(i)=999999.
    else
      nend(i)=0
      iter(i)=0
    end if
  end do ! loop 50
  nit=1
  !
  do j=1,igp1
    do i=il1,il2
      if (iter(i)>0 .and. lzf(i)>1 .and. j<lzf(i)) then
        fmax(i)=min(grkinf(i,j),fmax(i))
      end if
    end do
  end do ! loop 75
  !
100 continue

  !     * beginning of iteration sequence.
  !     * set or reset number of points to be processed on the current
  !     * latitude circle(s).
  !
  npnts=0
  !
  !     * if the water content of the current soil layer equals or exceeds
  !     * the water content behind the wetting front, instantaneously
  !     * relocate wetting front to bottom of current soil layer
  !     * re-evaluate infiltration parameters, update water movement
  !     * matrix, set switch "ISIMP" to 1 and drop to end of iteration
  !     * loop.
  !     * (some arrays are gathered (on lzf) to avoid multiple indirect-
  !     * addressing references in the ensuing loops.)
  !
  do i=il1,il2
    if (iter(i)==1) then
      thlnlz(i)=thlinf(i,lzf(i))
      thlqlz(i)=thliqx(i,lzf(i))
      if (thlqlz(i)>(thlnlz(i)-1.0e-6) .and. lzf(i)<igp1 &
          .and. thlqlz(i)>0.0001) then
        zf(i)=zbotx(i,lzf(i))
        wmove(i,ninf(i))=thlqlz(i)*delzx(i,lzf(i))
        tmove(i,ninf(i))=tbarwx(i,lzf(i))
        finf=grkinf(i,lzf(i))*(zf(i)+zpond(i)+psif (i,lzf(i)))/ &
                  zf(i)
        fmax(i)=min(fmax(i),finf)
        lzf(i)=lzf(i)+1
        ninf(i)=ninf(i)+1
        isimp(i)=1
      else
        isimp(i)=0
      end if
    else
      isimp(i)=0
    end if
  end do ! loop 200
  !
  !     * infiltration calculations taking finite time. set timestep of
  !     * current iteration pass and check hydraulic conductivity of
  !     * current soil layer. if zero, recalculate pond depth and pond
  !     * temperature and set "ISIMP" to -1; else set "ISIMP" to -2.
  !
  do i=il1,il2
    if (iter(i)==1 .and. isimp(i)/=1) then
      dtflow(i)=min(trmdr(i),120.)
      if (grkinf(i,lzf(i))>1.0e-12) then
        isimp(i)=-2
      else
        tpond(i)=(zpond(i)*tpond(i)+r(i)*dtflow(i)*tr(i))/ &
                      (zpond(i)+r(i)*dtflow(i))
        if (abs(r(i)-evap(i))>1.0e-11) then
          zptest=zpond(i)+(r(i)-evap(i))*dtflow(i)
        else
          zptest=zpond(i)
        end if
        if (zptest<0.) then
          dtflow(i)=zpond(i)/(evap(i)-r(i))
          zpond(i)=0.0
        else
          zpond(i)=zptest
        end if
        isimp(i)=-1
      end if
    end if
  end do ! loop 300
  !
  !     * "ISIMP"=-2: normal saturated infiltration under piston-flow
  !     * conditions. calculate current infiltration rate (finf); water
  !     * infiltrating during current iteration pass (winf); soil water
  !     * displaced into empty pores ahead of wetting front (wdisp)
  !     * and soil water overtaken by displaced soil water (wabs).
  !     * re-evaluate pond temperature and pond depth; update water
  !     * movement matrix; adjust current position of wetting front.
  !
  do i=il1,il2
    if (iter(i)==1 .and. isimp(i)==-2) then
      if (lzf(i)<igp1) then
        if (zf(i)>1.0e-7) then
          finf=grkinf(i,lzf(i))*(zf(i)+zpond(i)+ &
                      psif (i,lzf(i)))/zf(i)
        else
          finf=grkinf(i,1)
        end if
      else
        finf=grkinf(i,lzf(i))*(zf(i)+zpond(i))/zf(i)
      end if
      if (zpond(i)<1.0e-8 .and. finf>r(i))      finf=r(i)
      if (finf>fmax(i)) finf=fmax(i)
      winf=finf*dtflow(i)
      if (lzf(i)<igp1) then
        dzf(i)=winf/thlnlz(i)
        wdisp(i)=dzf(i)*thlqlz(i)
        dzdisp(i)=wdisp(i)/(thlnlz(i)-thlqlz(i))
        wabs(i)=dzdisp(i)*thlqlz(i)
        if ((zf(i)+dzf(i)+dzdisp(i))>zbotx(i,lzf(i))) then
          dtflow(i)=(zbotx(i,lzf(i))-zf(i))/ &
                          (finf/thlnlz(i)+ &
                          (finf*thlqlz(i))/ &
                          (thlnlz(i)* &
                          (thlnlz(i)-thlqlz(i))))
          winf=finf*dtflow(i)
          dzf(i)=winf/thlnlz(i)
          wdisp(i)=dzf(i)*thlqlz(i)
          dzdisp(i)=wdisp(i)/(thlnlz(i)-thlqlz(i))
          wabs(i)=dzdisp(i)*thlqlz(i)
        end if
      end if
      if (zpond(i)+r(i)*dtflow(i)>1.0e-8) then
        tpond(i)=(zpond(i)*tpond(i)+r(i)*dtflow(i)*tr(i))/ &
                      (zpond(i)+r(i)*dtflow(i))
      end if
      if (abs(r(i)-finf-evap(i))>1.0e-11) then
        zptest=zpond(i)+r(i)*dtflow(i)-winf-evap(i)* &
                    dtflow(i)
      else
        zptest=zpond(i)
      end if
      if (zptest<0.) then
        dtflow(i)=zpond(i)/(finf+evap(i)-r(i))
        winf=finf*dtflow(i)
        zpond(i)=0.0
        if (lzf(i)<igp1) then
          dzf(i)=winf/thlnlz(i)
          wdisp(i)=dzf(i)*thlqlz(i)
          dzdisp(i)=wdisp(i)/(thlnlz(i)-thlqlz(i))
          wabs(i)=dzdisp(i)*thlqlz(i)
        end if
      else
        zpond(i)=zptest
      end if
      if ((wmove(i,1)+winf)>0.) then
        tmove(i,1)=(wmove(i,1)*tmove(i,1)+winf*tpond(i))/ &
                       (wmove(i,1)+winf)
      end if
      wmove(i,1)=wmove(i,1)+winf
    end if
  end do ! loop 400
  !
  !     * (this portion of the above do-loop was split off on the cray
  !     * because it would not vectorize. one might try and re-combine
  !     * it on the sx-3 (goes in first part of if block)).
  !
  do i=il1,il2
    if (iter(i)==1 .and. isimp(i)==-2 .and. &
        lzf(i)<igp1) then
      wmove(i,ninf(i))=wmove(i,ninf(i))+wdisp(i)+wabs(i)
      zf(i)=zf(i)+dzf(i)+dzdisp(i)
    end if
  end do ! loop 450
  !
  !     * calculate remaining iteration time; re-evaluate infiltration
  !     * parameters.
  !
  do i=il1,il2
    if (iter(i)==1 .and. isimp(i)/=1) then
      trmdr(i)=trmdr(i)-dtflow(i)
      if (abs(zf(i)-zbotx(i,lzf(i)))<1.0e-6 .and. &
          trmdr(i)>0. .and. lzf(i)<igp1 .and. &
          thlqlz(i)>0.0001) then
        finf=grkinf(i,lzf(i))*(zbotx(i,lzf(i))+zpond(i)+ &
                  psif (i,lzf(i)))/zbotx(i,lzf(i))
        fmax(i)=min(fmax(i),finf)
        lzf(i)=lzf(i)+1
        ninf(i)=ninf(i)+1
        tmove(i,ninf(i))=tbarwx(i,lzf(i))
      end if
    end if
  end do ! loop 500
  !
  !     * increment iteration counter ("NIT") and see if any points still
  !     * remain to be done (using "NPNTS"). if so, return to beginning
  !     * to complete these remaining points.
  !
  nit=nit+1
  !
  do i=il1,il2
    if (igrn(i)>0) then
      if (nit<=nend(i) .and. iter(i)==1 .and. &
          (zpond(i)>1.0e-8 .or. r(i)>0.)  .and. &
          trmdr(i)>0.) then
        npnts=npnts+1
      else
        iter(i)=0
      end if
    end if
  end do ! loop 600
  !
  if (npnts>0)                                         go to 100
  !
  return
end subroutine wflow
