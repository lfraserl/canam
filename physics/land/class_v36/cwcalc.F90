subroutine cwcalc(tcan,raican,snocan,frainc,fsnowc,chcap, &
                        hmfc,htcc,fi,cmass,ilg,il1,il2,jl)
  !
  !     * mar 25/08 - d.verseghy. update frainc and fsnowc.
  !     * sep 24/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jun 20/02 - d.verseghy. cosmetic rearrangement of
  !     *                         subroutine call; shortened
  !     *                         class4 common block.
  !     * jun 20/97 - d.verseghy. class - version 2.7.
  !     *                         pass in new "CLASS4" common block.
  !     * jan 02/96 - d.verseghy. class - version 2.5.
  !     *                         completion of energy balance
  !     *                         diagnostics.
  !     * jul 30/93 - d.verseghy/m.lazare. class - version 2.2.
  !     *                                  new diagnostic fields.
  !     * mar 17/92 - d.verseghy/m.lazare. class - version 2.1.
  !     *                                  revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 13/91 - d.verseghy. adjust canopy temperature and
  !     *                         intercepted liquid/frozen
  !     *                         moisture stores for freezing/
  !     *                         thawing.
  !
  use times_mod, only : delt
  use hydcon, only : tfrez, sphw, sphice, sphveg, clhmlt, clhvap
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  !
  !     * i/o arrays.
  !
  real, intent(inout) :: tcan  (ilg) !<
  real, intent(inout) :: raican(ilg) !<
  real, intent(inout) :: snocan(ilg) !<
  real, intent(inout) :: frainc(ilg) !<
  real, intent(inout) :: fsnowc(ilg) !<
  real, intent(inout) :: chcap (ilg) !<
  real, intent(inout) :: hmfc  (ilg) !<
  real, intent(inout) :: htcc  (ilg) !<
  !
  !     * input arrays.
  !
  real, intent(in) :: fi    (ilg) !<
  real, intent(in) :: cmass (ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  real :: hfrez !<
  real :: hconv !<
  real :: rconv !<
  real :: hcool !<
  real :: hmelt !<
  real :: sconv !<
  real :: hwarm !<
  !---------------------------------------------------------------------
  do i=il1,il2
    if (fi(i)>0.) then
      htcc  (i)=htcc(i)-fi(i)*tcan(i)*chcap(i)/delt
      if (raican(i)>0. .and. tcan(i)<tfrez) then
        hfrez=chcap(i)*(tfrez-tcan(i))
        hconv=raican(i)*clhmlt
        if (hfrez<=hconv) then
          rconv=hfrez/clhmlt
          fsnowc(i)=fsnowc(i)+frainc(i)*rconv/raican(i)
          frainc(i)=frainc(i)-frainc(i)*rconv/raican(i)
          snocan(i)=snocan(i)+rconv
          raican(i)=raican(i)-rconv
          tcan  (i)=tfrez
          hmfc  (i)=hmfc(i)-fi(i)*clhmlt*rconv/delt
          htcc  (i)=htcc(i)-fi(i)*clhmlt*rconv/delt
        else
          hcool=hfrez-hconv
          snocan(i)=snocan(i)+raican(i)
          fsnowc(i)=fsnowc(i)+frainc(i)
          frainc(i)=0.0
          tcan  (i)=-hcool/(sphveg*cmass(i)+sphice* &
                          snocan(i))+tfrez
          hmfc  (i)=hmfc(i)-fi(i)*clhmlt*raican(i)/delt
          htcc  (i)=htcc(i)-fi(i)*clhmlt*raican(i)/delt
          raican(i)=0.0
        end if
      end if
      if (snocan(i)>0. .and. tcan(i)>tfrez) then
        hmelt=chcap(i)*(tcan(i)-tfrez)
        hconv=snocan(i)*clhmlt
        if (hmelt<=hconv) then
          sconv=hmelt/clhmlt
          frainc(i)=frainc(i)+fsnowc(i)*sconv/snocan(i)
          fsnowc(i)=fsnowc(i)-fsnowc(i)*sconv/snocan(i)
          snocan(i)=snocan(i)-sconv
          raican(i)=raican(i)+sconv
          tcan(i)=tfrez
          hmfc  (i)=hmfc(i)+fi(i)*clhmlt*sconv/delt
          htcc  (i)=htcc(i)+fi(i)*clhmlt*sconv/delt
        else
          hwarm=hmelt-hconv
          raican(i)=raican(i)+snocan(i)
          frainc(i)=frainc(i)+fsnowc(i)
          fsnowc(i)=0.0
          tcan(i)=hwarm/(sphveg*cmass(i)+sphw*raican(i))+ &
                        tfrez
          hmfc  (i)=hmfc(i)+fi(i)*clhmlt*snocan(i)/delt
          htcc  (i)=htcc(i)+fi(i)*clhmlt*snocan(i)/delt
          snocan(i)=0.0
        end if
      end if
      chcap(i)=sphveg*cmass(i)+sphw*raican(i)+sphice*snocan(i)
      htcc (i)=htcc(i)+fi(i)*tcan(i)*chcap(i)/delt
    end if
  end do ! loop 100
  !
  return
end subroutine cwcalc
