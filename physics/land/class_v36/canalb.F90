subroutine canalb(alvscn,alircn,alvscs,alircs,trvscn,trircn, &
                        trvscs,trircs,rc,rcs, &
                        alvsc,alirc,rsmin,qa50,vpda,vpdb,psiga,psigb, &
                        fc,fcs,fsnow,fsnowc,fsnocs,fcan,fcans,pai,pais, &
                        ail,psignd,fcloud,coszs,qswinv,vpd,ta, &
                        acvdat,acidat,alvsgc,alirgc,alvssc,alirsc, &
                        ilg,il1,il2,jl,ic,icp1,ig,ialc, &
                        cxteff,trvs,trir,rcacc,rcg,rcv,rct,gc)
  !
  !     * aug 04/15 - d.verseghy/m.lazare. remove flag value of rc for
  !     *                         very dry soils.
  !     * sep 05/14 - p.bartlett. increased albedo values for snow-
  !     *                         covered canopy.
  !     * jun 27/13 - d.verseghy/ use lower bound of 0.01 instead of
  !     *             m.lazare.   0. in loop 900 to avoid crash
  !     *                         in extreme case of low visible
  !     *                         incident sun.
  !     * dec 21/11 - m.lazare.   define constants "EXPMAX1", expmax2",
  !     *                         "EXPMAX3" to avoid redundant exp
  !     *                         calculations.
  !     * oct 16/08 - r.harvey.   add large limit for effective
  !     *                         extinction coefficient (cxteff) in
  !     *                         (rare) cases when canopy transmissivity
  !     *                         in the visible is zero exactly.
  !     * mar 25/08 - d.verseghy. distinguish between leaf area index
  !     *                         and plant area index.
  !     * oct 19/07 - d.verseghy. simplify albedo calculations for
  !     *                         snow-free crops and grass; correct
  !     *                         bug in calculation of rc.
  !     * apr 13/06 - p.bartlett/d.verseghy. correct overall canopy
  !     *                                    albedo, introduce separate
  !     *                                    ground and snow albedos for
  !     *                                    open or canopy-covered areas.
  !     * mar 21/06 - p.bartlett. protect rc calculation against division
  !     *                         by zero.
  !     * sep 26/05 - d.verseghy. remove hard coding of ig=3 in 600 loop.
  !     * nov 03/04 - d.verseghy. add "IMPLICIT NONE" command.
  !     * jul 05/04 - d.verseghy. protect sensitive calculations against
  !     *                         roundoff errors.
  !     * jan 24/02 - p.bartlett/d.verseghy. refine calculation of new
  !     *                                    stomatal resistances.
  !     * jul 30/02 - p.bartlett/d.verseghy. new stomatal resistance
  !     *                                    formulation incorporated.
  !     * mar 18/02 - d.verseghy. allow for assignment of specified time-
  !     *                         varying values of vegetation snow-free
  !     *                         albedo.
  !     * nov 29/94 - m.lazare. class - version 2.3.
  !     *                       call abort changed to call xit to enable
  !     *                       RUNNING ON PC'S.
  !     * may 06/93 - d.verseghy. extensive modifications to canopy
  !     *                         albedo loops.
  !     * mar 03/92 - d.verseghy/m.lazare. revised and vectorized code
  !     *                                  for model version gcm7.
  !     * aug 12/91 - d.verseghy. canopy albedos and transmissivities.
  !
  use hydcon, only : canext
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: jl !<
  integer, intent(in) :: ic !<
  integer, intent(in) :: icp1 !<
  integer, intent(in) :: ig !<
  integer, intent(in) :: ialc !<
  !
  !     * output arrays.
  !
  real, intent(inout) :: alvscn(ilg) !<
  real, intent(inout) :: alircn(ilg) !<
  real, intent(inout) :: alvscs(ilg) !<
  real, intent(inout) :: alircs(ilg) !<
  real, intent(inout) :: trvscn(ilg) !<
  real, intent(inout) :: trircn(ilg) !<
  real, intent(inout) :: trvscs(ilg) !<
  real, intent(inout) :: trircs(ilg) !<
  real, intent(inout) :: rc    (ilg) !<
  real, intent(inout) :: rcs   (ilg) !<
  !
  !     * 2-d input arrays.
  !
  real, intent(in) :: alvsc (ilg,icp1) !<
  real, intent(in) :: alirc (ilg,icp1) !<
  real, intent(in) :: rsmin (ilg,ic) !<
  real, intent(in) :: qa50  (ilg,ic) !<
  real, intent(in) :: vpda  (ilg,ic) !<
  real, intent(in) :: vpdb  (ilg,ic) !<
  real, intent(in) :: psiga (ilg,ic) !<
  real, intent(in) :: psigb (ilg,ic) !<
  real, intent(in) :: fcan  (ilg,ic) !<
  real, intent(in) :: fcans (ilg,ic) !<
  real, intent(in) :: pai   (ilg,ic) !<
  real, intent(in) :: pais  (ilg,ic) !<
  real, intent(in) :: acvdat(ilg,ic) !<
  real, intent(in) :: acidat(ilg,ic) !<
  real, intent(in) :: ail   (ilg,ic) !<
  !
  !     * 1-d input arrays.
  !
  real, intent(in) :: fc    (ilg) !<
  real, intent(in) :: fcs   (ilg) !<
  real, intent(in) :: fsnow (ilg) !<
  real, intent(in) :: fsnowc(ilg) !<
  real, intent(in) :: fsnocs(ilg) !<
  real, intent(in) :: psignd(ilg) !<
  real, intent(in) :: fcloud(ilg) !<
  real, intent(in) :: coszs (ilg) !<
  real, intent(in) :: qswinv(ilg) !<
  real, intent(in) :: vpd   (ilg) !<
  real, intent(in) :: ta    (ilg) !<
  real, intent(in) :: alvsgc(ilg) !<
  real, intent(in) :: alirgc(ilg) !<
  real, intent(in) :: alvssc(ilg) !<
  real, intent(in) :: alirsc(ilg) !<
  !
  !     * work arrays.
  !
  real, intent(inout) :: cxteff(ilg,ic) !<
  real, intent(inout) :: rcacc (ilg,ic) !<
  real, intent(inout) :: rcv   (ilg,ic) !<
  real, intent(inout) :: rcg   (ilg,ic) !<
  real, intent(inout) :: rct   (ilg) !<
  real, intent(inout) :: gc    (ilg) !<
  real, intent(inout) :: trvs  (ilg) !<
  real, intent(inout) :: trir  (ilg) !<
  !
  !     * temporary variables.
  !
  integer :: i !<
  integer :: j !<
  integer :: iptbad !<
  integer :: jptbad !<
  integer :: jptbdi !<
  real :: svf !<
  real :: alvscx !<
  real :: alircx !<
  real :: alvsn !<
  real :: alirn !<
  real :: alvss !<
  real :: alirs !<
  real :: trtot !<
  real :: expmax1 !<
  real :: expmax2 !<
  real :: expmax3 !<
  !
  real :: trclrv !<
  real :: trcldv !<
  real :: trclrt !<
  real :: trcldt !<
  !
  real, parameter :: alvswc = 0.27
  real, parameter :: alirwc = 0.38
  real, parameter :: cxtlrg = 1.0e20
  !----------------------------------------------------------------------
  !
  !     * assign constant exponentiation terms: expmax1=exp(-0.4/0.9659),
  !     * expmax2=exp(-0.4/0.7071),expmax3=exp(-0.4/0.2588)
  !
  expmax1=0.6609
  expmax2=0.5680
  expmax3=0.2132
  !
  !     * initialize work arrays.
  !
  do i=il1,il2
    rct(i)=0.0
    gc(i)=0.0
    rc(i)=0.0
  end do ! loop 50
  do j=1,ic
    do i=il1,il2
      cxteff(i,j)=0.0
      rcacc(i,j)=0.0
      rcg(i,j)=0.0
      rcv(i,j)=0.0
    end do
  end do ! loop 100
  !
  !     * albedo and transmissivity calculations for canopy over
  !     * bare soil.
  !
  !     * needleleaf trees.
  !
  j=1
  do i=il1,il2
    if (coszs(i)>0. .and. fcan(i,j)>0.) then
      trclrv=exp(-0.4*pai(i,j)/coszs(i))
      trcldv=0.30*exp(-0.4*pai(i,j)/0.9659)+0.50*exp(-0.4* &
                pai(i,j)/0.7071)+0.20*exp(-0.4*pai(i,j)/0.2588)
      trclrt=exp(-0.3*pai(i,j)/coszs(i))
      trcldt=0.30*exp(-0.3*pai(i,j)/0.9659)+0.50*exp(-0.3* &
                pai(i,j)/0.7071)+0.20*exp(-0.3*pai(i,j)/0.2588)
      trvs(i)=fcloud(i)*trcldv+(1.0-fcloud(i))*trclrv
      if (trvs(i)>0.0001) then
        cxteff(i,j)=-log(trvs(i))/max(pai(i,j),1.0e-5)
      else
        cxteff(i,j)=cxtlrg
      end if
      trtot =fcloud(i)*trcldt+(1.0-fcloud(i))*trclrt
      trir(i)= 2.*trtot-trvs(i)
      trvscn(i)=trvscn(i)+fcan(i,j)*trvs(i)
      trircn(i)=trircn(i)+fcan(i,j)*trir(i)
    end if
  end do ! loop 150
  !
  do i=il1,il2
    if (coszs(i)>0. .and. fcan(i,j)>0.) then
      svf=exp(canext(j)*pai(i,j))
      if (ialc==0) then
        alvscx=fsnowc(i)*alvswc+(1.0-fsnowc(i))*alvsc(i,j)
        alircx=fsnowc(i)*alirwc+(1.0-fsnowc(i))*alirc(i,j)
        alvsn=(1.0-svf)*alvscx+svf*trvs(i)*alvsgc(i)
        alirn=(1.0-svf)*alircx+svf*trir(i)*alirgc(i)
      else
        alvscx=fsnowc(i)*alvswc+(1.0-fsnowc(i))*acvdat(i,j)
        alircx=fsnowc(i)*alirwc+(1.0-fsnowc(i))*acidat(i,j)
        alvsn=(1.0-svf)*alvscx+svf*acvdat(i,j)
        alirn=(1.0-svf)*alircx+svf*acidat(i,j)
      end if
      alvscn(i)=alvscn(i)+fcan(i,j)*alvsn
      alircn(i)=alircn(i)+fcan(i,j)*alirn
    end if
  end do ! loop 200
  !
  !     * broadleaf trees.
  !
  j=2
  do i=il1,il2
    if (coszs(i)>0. .and. fcan(i,j)>0.) then
      trclrv=min(exp(-0.7*pai(i,j)),exp(-0.4/coszs(i)))
      trcldv=0.30*min(exp(-0.7*pai(i,j)),expmax1) &
               +0.50*min(exp(-0.7*pai(i,j)),expmax2) &
               +0.20*min(exp(-0.7*pai(i,j)),expmax3)
      trclrt=min(exp(-0.4*pai(i,j)),exp(-0.4/coszs(i)))
      trcldt=0.30*min(exp(-0.4*pai(i,j)),expmax1)+ &
                0.50*min(exp(-0.4*pai(i,j)),expmax2)+ &
                0.20*min(exp(-0.4*pai(i,j)),expmax3)
      trvs(i)=fcloud(i)*trcldv+(1.0-fcloud(i))*trclrv
      if (trvs(i)>0.0001) then
        cxteff(i,j)=-log(trvs(i))/max(pai(i,j),1.0e-5)
      else
        cxteff(i,j)=cxtlrg
      end if
      trtot =fcloud(i)*trcldt+(1.0-fcloud(i))*trclrt
      trir(i)= 2.*trtot-trvs(i)
      trvscn(i)=trvscn(i)+fcan(i,j)*trvs(i)
      trircn(i)=trircn(i)+fcan(i,j)*trir(i)
    end if
  end do ! loop 250
  !
  do i=il1,il2
    if (coszs(i)>0. .and. fcan(i,j)>0.) then
      svf=exp(canext(j)*pai(i,j))
      if (ialc==0) then
        alvscx=fsnowc(i)*alvswc+(1.0-fsnowc(i))*alvsc(i,j)
        alircx=fsnowc(i)*alirwc+(1.0-fsnowc(i))*alirc(i,j)
        alvsn=(1.0-svf)*alvscx+svf*trvs(i)*alvsgc(i)
        alirn=(1.0-svf)*alircx+svf*trir(i)*alirgc(i)
      else
        alvscx=fsnowc(i)*alvswc+(1.0-fsnowc(i))*acvdat(i,j)
        alircx=fsnowc(i)*alirwc+(1.0-fsnowc(i))*acidat(i,j)
        alvsn=(1.0-svf)*alvscx+svf*acvdat(i,j)
        alirn=(1.0-svf)*alircx+svf*acidat(i,j)
      end if
      alvscn(i)=alvscn(i)+fcan(i,j)*alvsn
      alircn(i)=alircn(i)+fcan(i,j)*alirn
    end if
  end do ! loop 300
  !
  !     * crops and grass.
  !
  do j=3,ic
    do i=il1,il2
      if (coszs(i)>0. .and. fcan(i,j)>0.) then
        trclrv=exp(-0.5*pai(i,j)/coszs(i))
        trcldv=0.30*exp(-0.5*pai(i,j)/0.9659)+0.50*exp(-0.5* &
                pai(i,j)/0.7071)+0.20*exp(-0.5*pai(i,j)/0.2588)
        trclrt=exp(-0.4*pai(i,j)/coszs(i))
        trcldt=0.30*exp(-0.4*pai(i,j)/0.9659)+0.50*exp(-0.4* &
                pai(i,j)/0.7071)+0.20*exp(-0.4*pai(i,j)/0.2588)
        trvs(i)=fcloud(i)*trcldv+(1.0-fcloud(i))*trclrv
        if (trvs(i)>0.0001) then
          cxteff(i,j)=-log(trvs(i))/max(pai(i,j),1.0e-5)
        else
          cxteff(i,j)=cxtlrg
        end if
        trtot =fcloud(i)*trcldt+(1.0-fcloud(i))*trclrt
        trir(i)= 2.*trtot-trvs(i)
        trvscn(i)=trvscn(i)+fcan(i,j)*trvs(i)
        trircn(i)=trircn(i)+fcan(i,j)*trir(i)
      end if
    end do
  end do ! loop 350
  !
  do j=3,ic
    do i=il1,il2
      if (coszs(i)>0. .and. fcan(i,j)>0.) then
        svf=exp(canext(j)*pai(i,j))
        if (ialc==0) then
          alvscx=fsnowc(i)*alvswc+(1.0-fsnowc(i))*alvsc(i,j)
          alircx=fsnowc(i)*alirwc+(1.0-fsnowc(i))*alirc(i,j)
          alvsn=(1.0-svf)*alvscx+svf*trvs(i)*alvsgc(i)
          alirn=(1.0-svf)*alircx+svf*trir(i)*alirgc(i)
        else
          alvscx=fsnowc(i)*alvswc+(1.0-fsnowc(i))*acvdat(i,j)
          alircx=fsnowc(i)*alirwc+(1.0-fsnowc(i))*acidat(i,j)
          alvsn=(1.0-svf)*alvscx+svf*acvdat(i,j)
          alirn=(1.0-svf)*alircx+svf*acidat(i,j)
        end if
        alvscn(i)=alvscn(i)+fcan(i,j)*alvsn
        alircn(i)=alircn(i)+fcan(i,j)*alirn
      end if
    end do
  end do ! loop 400
  !
  !     * total albedos.
  !
  iptbad=0
  do i=il1,il2
    if (fc(i)>0. .and. coszs(i)>0.) then
      alvscn(i)=alvscn(i)/fc(i)
      alircn(i)=alircn(i)/fc(i)
    end if
    if (alvscn(i)>1. .or. alvscn(i)<0.) iptbad=i
    if (alircn(i)>1. .or. alircn(i)<0.) iptbad=i
  end do ! loop 450
  !
  if (iptbad/=0) then
    write(6,6100) iptbad,jl,alvscn(iptbad),alircn(iptbad)
    6100     format('0AT (I,J)= (',i3,',',i3,'), ALVSCN,ALIRCN = ',2f10.5)
    call xit('CANALB',-1)
  end if
  !
  !     * total transmissivities.
  !
  iptbad=0
  do i=il1,il2
    if (fc(i)>0. .and. coszs(i)>0.) then
      trvscn(i)=trvscn(i)/fc(i)
      trircn(i)=trircn(i)/fc(i)
      trvscn(i)=min( trvscn(i), 0.90*(1.0-alvscn(i)) )
      trircn(i)=min( trircn(i), 0.90*(1.0-alircn(i)) )
    end if
    if (trvscn(i)>1. .or. trvscn(i)<0.) iptbad=i
    if (trircn(i)>1. .or. trircn(i)<0.) iptbad=i
  end do ! loop 475
  !
  if (iptbad/=0) then
    write(6,6300) iptbad,jl,trvscn(iptbad),trircn(iptbad)
    6300     format('0AT (I,J)= (',i3,',',i3,'), TRVSCN,TRIRCN = ',2f10.5)
    call xit('CANALB',-3)
  end if
  !----------------------------------------------------------------------
  !
  !     * albedo and transmissivity calculations for canopy over snow.
  !
  !     * needleleaf trees.
  !
  j=1
  do i=il1,il2
    if (coszs(i)>0. .and. fcans(i,j)>0.) then
      trclrv=exp(-0.4*pais(i,j)/coszs(i))
      trcldv=0.30*exp(-0.4*pais(i,j)/0.9659)+0.50*exp(-0.4* &
                pais(i,j)/0.7071)+0.20*exp(-0.4*pais(i,j)/0.2588)
      trclrt=exp(-0.3*pais(i,j)/coszs(i))
      trcldt=0.30*exp(-0.3*pais(i,j)/0.9659)+0.50*exp(-0.3* &
                pais(i,j)/0.7071)+0.20*exp(-0.3*pais(i,j)/0.2588)
      trvs(i)=fcloud(i)*trcldv+(1.0-fcloud(i))*trclrv
      trtot =fcloud(i)*trcldt+(1.0-fcloud(i))*trclrt
      trir(i)= 2.*trtot-trvs(i)
      trvscs(i)=trvscs(i)+fcans(i,j)*trvs(i)
      trircs(i)=trircs(i)+fcans(i,j)*trir(i)
    end if
  end do ! loop 500
  !
  do i=il1,il2
    if (coszs(i)>0. .and. fcans(i,j)>0.) then
      if (ialc==0) then
        alvscx=fsnocs(i)*alvswc+(1.0-fsnocs(i))*alvsc(i,j)
        alircx=fsnocs(i)*alirwc+(1.0-fsnocs(i))*alirc(i,j)
      else
        alvscx=fsnocs(i)*alvswc+(1.0-fsnocs(i))*acvdat(i,j)
        alircx=fsnocs(i)*alirwc+(1.0-fsnocs(i))*acidat(i,j)
      end if
      svf=exp(canext(j)*pais(i,j))
      alvss=(1.0-svf)*alvscx+svf*trvs(i)*alvssc(i)
      alirs=(1.0-svf)*alircx+svf*trir(i)*alirsc(i)
      alvscs(i)=alvscs(i)+fcans(i,j)*alvss
      alircs(i)=alircs(i)+fcans(i,j)*alirs
    end if
  end do ! loop 550
  !
  !     * broadleaf trees.
  !
  j=2
  do i=il1,il2
    if (coszs(i)>0. .and. fcans(i,j)>0.) then
      trclrv=min(exp(-0.7*pais(i,j)),exp(-0.4/coszs(i)))
      trcldv=0.30*min(exp(-0.7*pais(i,j)),expmax1) &
               +0.50*min(exp(-0.7*pais(i,j)),expmax2) &
               +0.20*min(exp(-0.7*pais(i,j)),expmax3)
      trclrt=min(exp(-0.4*pais(i,j)),exp(-0.4/coszs(i)))
      trcldt=0.30*min(exp(-0.4*pais(i,j)),expmax1)+ &
                0.50*min(exp(-0.4*pais(i,j)),expmax2)+ &
                0.20*min(exp(-0.4*pais(i,j)),expmax3)
      trvs(i)=fcloud(i)*trcldv+(1.0-fcloud(i))*trclrv
      trtot =fcloud(i)*trcldt+(1.0-fcloud(i))*trclrt
      trir(i)= 2.*trtot-trvs(i)
      trvscs(i)=trvscs(i)+fcans(i,j)*trvs(i)
      trircs(i)=trircs(i)+fcans(i,j)*trir(i)
    end if
  end do ! loop 600
  !
  do i=il1,il2
    if (coszs(i)>0. .and. fcans(i,j)>0.) then
      if (ialc==0) then
        alvscx=fsnocs(i)*alvswc+(1.0-fsnocs(i))*alvsc(i,j)
        alircx=fsnocs(i)*alirwc+(1.0-fsnocs(i))*alirc(i,j)
      else
        alvscx=fsnocs(i)*alvswc+(1.0-fsnocs(i))*acvdat(i,j)
        alircx=fsnocs(i)*alirwc+(1.0-fsnocs(i))*acidat(i,j)
      end if
      svf=exp(canext(j)*pais(i,j))
      alvss=(1.0-svf)*alvscx+svf*trvs(i)*alvssc(i)
      alirs=(1.0-svf)*alircx+svf*trir(i)*alirsc(i)
      alvscs(i)=alvscs(i)+fcans(i,j)*alvss
      alircs(i)=alircs(i)+fcans(i,j)*alirs
    end if
  end do ! loop 650
  !
  !     * crops and grass.
  !
  do j=3,ic
    do i=il1,il2
      if (coszs(i)>0. .and. fcans(i,j)>0.) then
        trclrv=exp(-0.5*pais(i,j)/coszs(i))
        trcldv=0.30*exp(-0.5*pais(i,j)/0.9659)+0.50*exp(-0.5* &
                pais(i,j)/0.7071)+0.20*exp(-0.5*pais(i,j)/0.2588)
        trclrt=exp(-0.4*pais(i,j)/coszs(i))
        trcldt=0.30*exp(-0.4*pais(i,j)/0.9659)+0.50*exp(-0.4* &
                pais(i,j)/0.7071)+0.20*exp(-0.4*pais(i,j)/0.2588)
        trvs(i)=fcloud(i)*trcldv+(1.0-fcloud(i))*trclrv
        trtot =fcloud(i)*trcldt+(1.0-fcloud(i))*trclrt
        trir(i)= 2.*trtot-trvs(i)
        trvscs(i)=trvscs(i)+fcans(i,j)*trvs(i)
        trircs(i)=trircs(i)+fcans(i,j)*trir(i)
      end if
    end do
  end do ! loop 700
  !
  do j=3,ic
    do i=il1,il2
      if (coszs(i)>0. .and. fcans(i,j)>0.) then
        if (ialc==0) then
          alvscx=fsnocs(i)*alvswc+(1.0-fsnocs(i))*alvsc(i,j)
          alircx=fsnocs(i)*alirwc+(1.0-fsnocs(i))*alirc(i,j)
        else
          alvscx=fsnocs(i)*alvswc+(1.0-fsnocs(i))*acvdat(i,j)
          alircx=fsnocs(i)*alirwc+(1.0-fsnocs(i))*acidat(i,j)
        end if
        svf=exp(canext(j)*pais(i,j))
        alvss=(1.0-svf)*alvscx+svf*trvs(i)*alvssc(i)
        alirs=(1.0-svf)*alircx+svf*trir(i)*alirsc(i)
        alvscs(i)=alvscs(i)+fcans(i,j)*alvss
        alircs(i)=alircs(i)+fcans(i,j)*alirs
      end if
    end do
  end do ! loop 750
  !
  !     * total albedos and consistency checks.
  !
  iptbad=0
  do i=il1,il2
    if (fcs(i)>0. .and. coszs(i)>0.) then
      alvscs(i)=alvscs(i)/fcs(i)
      alircs(i)=alircs(i)/fcs(i)
    end if
    if (alvscs(i)>1. .or. alvscs(i)<0.) iptbad=i
    if (alircs(i)>1. .or. alircs(i)<0.) iptbad=i
  end do ! loop 775
  !
  !     * total transmissivities and consistency checks.
  !
  iptbad=0
  jptbad=0
  do i=il1,il2
    if (fcs(i)>0. .and. coszs(i)>0.) then
      trvscs(i)=trvscs(i)/fcs(i)
      trircs(i)=trircs(i)/fcs(i)
      trvscs(i)=min( trvscs(i), 0.90*(1.0-alvscs(i)) )
      trircs(i)=min( trircs(i), 0.90*(1.0-alircs(i)) )
    end if
    if (trvscs(i)>1. .or. trvscs(i)<0.) iptbad=i
    if (trircs(i)>1. .or. trircs(i)<0.) iptbad=i
    if ((1.-alvscn(i)-trvscn(i))<0.) then
      jptbad=1000+i
      jptbdi=i
    end if
    if ((1.-alvscs(i)-trvscs(i))<0.) then
      jptbad=2000+i
      jptbdi=i
    end if
    if ((1.-alircn(i)-trircn(i))<0.) then
      jptbad=3000+i
      jptbdi=i
    end if
    if ((1.-alircs(i)-trircs(i))<0.) then
      jptbad=4000+i
      jptbdi=i
    end if
  end do ! loop 800
  !
  if (iptbad/=0) then
    write(6,6400) iptbad,jl,trvscs(iptbad),trircs(iptbad)
    6400     format('0AT (I,J)= (',i3,',',i3,'), TRVSCS,TRIRCS = ',2f10.5)
    call xit('CANALB',-4)
  end if
  !
  if (iptbad/=0) then
    write(6,6200) iptbad,jl,alvscs(iptbad),alircs(iptbad)
    6200     format('0AT (I,J)= (',i3,',',i3,'), ALVSCS,ALIRCS = ',2f10.5)
    call xit('CANALB',-2)
  end if
  !
  if (jptbad/=0) then
    write(6,6500) jptbdi,jl,jptbad
    6500     format('0AT (I,J)= (',i3,',',i3,'), JPTBAD =  ',i5)
    call xit('CANALB',-5)
  end if
  !-----------------------------------------------------------------------
  !
  !     * bulk stomatal resistances for canopy overlying snow and canopy
  !     * overlying bare soil.
  !
  do i=il1,il2
    if ((fcs(i)+fc(i))>0.0) then
      if (ta(i)<=268.15) then
        rct(i)=250.
      else if (ta(i)<278.15) then
        rct(i)=1./(1.-(278.15-ta(i))*.1)
      else if (ta(i)>313.15) then
        if (ta(i)>=323.15) then
          rct(i)=250.
        else
          rct(i)=1./(1.-(ta(i)-313.15)*0.1)
        end if
      else
        rct(i)=1.
      end if
    end if
  end do ! loop 850
  !
  do j=1,ic
    do i=il1,il2
      if (fcan(i,j)>0.) then
        if (vpd(i)>0. .and. vpda(i,j)>0.0) then
          if (abs(vpdb(i,j))>1.0e-5) then
            rcv(i,j)=max(1.,((vpd(i)/10.)**vpdb(i,j))/ &
                          vpda(i,j))
          else
            rcv(i,j)=1./exp(-vpda(i,j)*vpd(i)/10.)
          end if
        else
          rcv(i,j)=1.0
        end if
        if (psiga(i,j)>0.0) then
          rcg(i,j)=1.+(psignd(i)/psiga(i,j))**psigb(i,j)
        else
          rcg(i,j)=1.0
        end if
        if (qswinv(i)>0.01 .and. coszs(i)>0. .and. &
            cxteff(i,j)>1.0e-5 .and. rcg(i,j)<1.0e5) then
          rcacc(i,j)=min(cxteff(i,j)*rsmin(i,j)/log((qswinv(i)+ &
             qa50(i,j)/cxteff(i,j))/(qswinv(i)*exp(-cxteff(i,j)* &
             pai(i,j))+qa50(i,j)/cxteff(i,j)))*rcv(i,j)*rcg(i,j)* &
             rct(i),5000.)
          rcacc(i,j)=max(rcacc(i,j),10.0)
        else
          rcacc(i,j)=5000.
        end if
        rc(i)=rc(i)+fcan(i,j)/rcacc(i,j)
      end if
    end do
  end do ! loop 900
  !
  do i=il1,il2
    if ((fcs(i)+fc(i))>0.) then
      if (qswinv(i)<2.0) then
        rcs(i)=5000.0
        rc(i)=5000.0
      else
        rcs(i)=5000.0
        if (rc(i)>0) then
          rc(i)=fc(i)/rc(i)
        else
          rc(i)=5000.0
        end if
      end if
    else
      rc(i)=0.0
      rcs(i)=0.0
    end if
  end do ! loop 950
  !
  return
end subroutine canalb
