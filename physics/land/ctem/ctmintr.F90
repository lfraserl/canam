subroutine ctmintr(datain, ctmin,  ntin, &
                         datout, ctmout, ntout, imcjmc, noption)

  !     * november   6, 2002     w.g. lee
  !                              bug fix. dr discovered a problem if there was an interpolation
  !                              time that exactly matches the input data. his correction was
  !                              implemented.
  !
  !     * september 30, 2002     w.g. lee
  !
  !     * ctmintr reads input data arrays and their associated
  !     * compact timestamp. it then uses linear interpolation to
  !     * determine an output array, given a set of output compact
  !     * timestamps.
  !
  !
  !     * assumptions:
  !     *
  !     * - there must be at least 2 time levels of input data.
  !     * - input timestamps/data must be order from lowest timestamp to
  !     *   largest timestamp.
  !
  !
  !     * output options:
  !     *
  !     *    noption = 0    linearly interpolate, but if requested
  !     *                   data has a timestamp outside of the range
  !     *                   of input data, output the closest values.
  !     *    noption = 1    linearly interpolate, but if requested
  !     *                   data has a timestamp outside of the
  !     *                   range of input data, abort.

  !     ------------------------------------------------------------------

  implicit none

  integer  :: ij !<
  integer  :: nin !<
  integer  :: n !<
  integer, intent(in)  :: imcjmc !<
  integer, intent(in)  :: ntin !<
  integer, intent(in)  :: ntout !<
  integer, intent(in)  :: noption !<

  real, intent(in)     :: datain(imcjmc,ntin) !<
  real, intent(in)     :: ctmin(3,ntin) !<
  real, intent(inout)  :: datout(imcjmc,ntout) !<
  real, intent(in)     :: ctmout(3,ntout) !<

  real*8, dimension(1000) :: tin !<
  real*8, dimension(1000) :: tout !<
  real :: deltat !<
  real :: facta !<
  real :: factb !<


  do n = 1,ntin
    call ctm2sec(ctmin(1,n),ctmin(2,n),ctmin(3,n),tin(n))
  end do ! loop 10

  do n = 2,ntin
    if (tin(n)<=tin(n-1)) call    xit('ctmintr',-20)
  end do ! loop 20

  do n = 1,ntout

    call ctm2sec(ctmout(1,n),ctmout(2,n),ctmout(3,n),tout(n))

    if (tout(n)<tin(1)) then

      if (noption==1) then
        call     xit('ctmintr',-1)
      else
        do ij = 1,imcjmc
          datout(ij,n) = datain(ij,1)
        end do ! loop 110
      end if

    else

      if (tout(n)>tin(ntin)) then

        if (noption==1) then
          call     xit('ctmintr',-2)
        else
          do ij = 1,imcjmc
            datout(ij,n) = datain(ij,ntin)
          end do ! loop 210
        end if

      else

        do nin = 2, ntin
          if ((tout(n)>=tin(nin-1)) .and. &
              (tout(n)<=tin(nin))) then
            deltat =  tin(nin) - tin(nin-1)
            facta  = (tin(nin) - tout(n))    / deltat
            factb  = (tout(n)  - tin(nin-1)) / deltat
            do ij = 1,imcjmc
              datout(ij,n) = facta * datain(ij,nin-1) &
                                       + factb * datain(ij,nin)
            end do ! loop 310
            exit
          end if
        end do ! loop 380

      end if
    end if
  end do ! loop 900

  !     ------------------------------------------------------------------

  return
end subroutine ctmintr
