subroutine ctemg ( &
                        co2cg1gat,co2cg2gat,co2cs1gat,co2cs2gat, &
                        cfluxcsgat,cfluxcggat,co2gat,fsfgat, &
                        lghtgat,wetfgat,wetsgat, &
                        soilcgat,litrcgat,rootcgat,stemcgat, &
                        gleafcgat,bleafcgat,fallhgat,posphgat, &
                        leafsgat,growtgat,lastrgat,lastsgat, &
                        thisylgat,stemhgat,roothgat,tempcgat, &
                        prefgat,newfgat,fcolgat,areagat, &
                        sandgat,claygat,sdepgat, &
                        tagtl,fsnowgtl,tcanogtl,tcansgtl, &
                        tbargtl,tbarcgtl,tbarcsgtl,tbarggtl, &
                        tbargsgtl,thliqcgtl,thliqggtl,thicecgtl, &
                        ancsgtl,ancggtl,rmlcsgtl,rmlcggtl, &
                        ilmos,jlmos, &
                        nml,nl,nt,nm,ilg,ig,ic,icp1, &
                        ictem,ictemp1,kount,gmt, &
                        co2cg1rot,co2cg2rot,co2cs1rot,co2cs2rot, &
                        cfluxcsrot,cfluxcgrot,co2row,fsfrot, &
                        lghtrow,wetfrow,wetsrow, &
                        soilcrot,litrcrot,rootcrot,stemcrot, &
                        gleafcrot,bleafcrot,fallhrot,posphrot, &
                        leafsrot,growtrot,lastrrot,lastsrot, &
                        thisylrot,stemhrot,roothrot,tempcrot, &
                        prefrot,newfrot,fcolrot,arearow, &
                        sandrot,clayrot,sdeprot, &
                        tartl,fsnowrtl,tcanortl,tcansrtl, &
                        tbarrtl,tbarcrtl,tbarcsrtl,tbargrtl, &
                        tbargsrtl,thliqcrtl,thliqgrtl,thicecrtl, &
                        ancsrtl,ancgrtl,rmlcsrtl,rmlcgrtl)
  !
  !     * oct 02/2018 - m.lazare.   - add fcol to ctemg so that exists on timesteps
  !     *                             where ctem is not called, so garbage is not
  !     *                             put into fcolrot from fcolgat in the call to
  !     *                             ctems at those timesteps.
  !     * aug 23/2018 - vivek arora - remove pfhc and pexf
  !     * feb 20/16 - m.lazare. new version for gcm19+:
  !     *                       - add all necessary extra arrays to
  !     *                         support moving ctem into agcm.
  !     * jan 15/15 - m.lazare. previous version for gcm18:
  !     *                       remove fieldsm,wiltsm (replaced by
  !     *                       thfc,thlw used for both ctem and class).
  !     * jan 26/13 - m.lazare. add paicrot/paicgat and slaicrot/slaicgat.
  !     * nov 10/11 - m.lazare. gather routine for ctem based on classg.
  !
  implicit none
  !
  !     * integer :: constants.
  !
  integer, intent(in)  :: nml !<
  integer, intent(in)  :: nl !<
  integer, intent(in)  :: nt !<
  integer, intent(in)  :: nm !<
  integer, intent(in)  :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in)  :: ig !<
  integer, intent(in)  :: ic !<
  integer, intent(in)  :: icp1 !<
  integer, intent(in)  :: ictem !<
  integer, intent(in)  :: ictemp1 !<
  integer  :: j !<
  integer  :: k !<
  integer  :: l !<
  integer  :: m !<
  integer, intent(in)  :: kount   !< Current model timestep \f$[unitless]\f$
  real, intent(in)     :: gmt   !< Real value of number of elapsed seconds in current day \f$[seconds]\f$
  !
  !     * ctem variables.
  !
  real, intent(in) :: co2cg1rot(nl,nt,ictem) !<
  real, intent(in) :: co2cg2rot(nl,nt,ictem) !<
  real, intent(in) :: co2cs1rot(nl,nt,ictem) !<
  real, intent(in) :: co2cs2rot(nl,nt,ictem) !<

  real, intent(in) :: cfluxcsrot(nl,nt) !<
  real, intent(in) :: cfluxcgrot(nl,nt) !<
  !
  !     * invariant ctem fields.
  !
  real, intent(in) :: lghtrow (nl) !<
  real, intent(in) :: wetfrow (nl) !<
  real, intent(in) :: wetsrow (nl) !<
  !
  !     * input fields to main ctem routine from average over
  !     * coupling period of previous coupling cycle.
  !     * these are not gathered at gmt=0. since the "ROT"
  !     * FIELDS DON'T EXIST AT THIS POINT UNTIL THEY ARE INITIALIZED.
  !
  real, intent(in) :: tbarrtl  (nl,nt,ig) !<
  real, intent(in) :: tbarcrtl (nl,nt,ig) !<
  real, intent(in) :: tbarcsrtl(nl,nt,ig) !<
  real, intent(in) :: tbargrtl (nl,nt,ig) !<
  real, intent(in) :: tbargsrtl(nl,nt,ig) !<
  real, intent(in) :: thliqcrtl(nl,nt,ig) !<
  real, intent(in) :: thliqgrtl(nl,nt,ig) !<
  real, intent(in) :: thicecrtl(nl,nt,ig) !<
  !
  real, intent(in) :: ancsrtl  (nl,nt,ictem) !<
  real, intent(in) :: ancgrtl  (nl,nt,ictem) !<
  real, intent(in) :: rmlcsrtl (nl,nt,ictem) !<
  real, intent(in) :: rmlcgrtl (nl,nt,ictem) !<
  !
  real, intent(in) :: tartl    (nl,nt) !<
  real, intent(in) :: fsnowrtl (nl,nt) !<
  real, intent(in) :: tcanortl (nl,nt) !<
  real, intent(in) :: tcansrtl (nl,nt) !<
  !
  !     * input/output fields from main ctem routine.
  !
  real, intent(in) :: soilcrot (nl,nt,ictemp1) !<
  real, intent(in) :: litrcrot (nl,nt,ictemp1) !<
  real, intent(in) :: rootcrot (nl,nt,ictem) !<
  real, intent(in) :: stemcrot (nl,nt,ictem) !<
  real, intent(in) :: gleafcrot(nl,nt,ictem) !<
  real, intent(in) :: bleafcrot(nl,nt,ictem) !<
  real, intent(in) :: fallhrot (nl,nt,ictem) !<
  real, intent(in) :: posphrot (nl,nt,ictem) !<
  real, intent(in) :: leafsrot (nl,nt,ictem) !<
  real, intent(in) :: growtrot (nl,nt,ictem) !<
  real, intent(in) :: lastrrot (nl,nt,ictem) !<
  real, intent(in) :: lastsrot (nl,nt,ictem) !<
  real, intent(in) :: thisylrot(nl,nt,ictem) !<
  real, intent(in) :: stemhrot (nl,nt,ictem) !<
  real, intent(in) :: roothrot (nl,nt,ictem) !<
  real, intent(in) :: tempcrot (nl,nt,2) !<
  !
  !     * ctem vegegation class fraction fields (3 time levels).
  !
  real, intent(in) :: prefrot  (nl,nt,ictem) !<
  real, intent(in) :: newfrot  (nl,nt,ictem) !<
  !
  !     * co2 land flux.
  !
  real, intent(in), dimension(nl,nt) :: fcolrot !<
  !
  !     * fraction area of land.
  !
  real, intent(in), dimension(nl) :: arearow !<
  !
  !     * invariant class input fields.
  !
  real, intent(in) :: sandrot (nl,nt,ig) !<
  real, intent(in) :: clayrot (nl,nt,ig) !<
  real, intent(in), dimension(nl,nt) :: sdeprot !<
  !-------------------------------------------------------------
  real, intent(inout) :: co2cg1gat(ilg,ictem) !<
  real, intent(inout) :: co2cg2gat(ilg,ictem) !<
  real, intent(inout) :: co2cs1gat(ilg,ictem) !<
  real, intent(inout) :: co2cs2gat(ilg,ictem) !<

  real, intent(inout) :: cfluxcsgat(ilg) !<
  real, intent(inout) :: cfluxcggat(ilg) !<
  !
  !     * invariant ctem fields.
  !
  real, intent(inout) :: lghtgat  (ilg) !<
  real, intent(inout) :: wetfgat  (ilg) !<
  real, intent(inout) :: wetsgat  (ilg) !<
  !
  !     * input fields to main ctem routine from average over
  !     * coupling period of previous coupling cycle.
  !     * these are not gathered at gmt=0. since the "ROT"
  !     * FIELDS DON'T EXIST AT THIS POINT UNTIL THEY ARE INITIALIZED.
  !
  real, intent(inout) :: tbargtl  (ilg,ig) !<
  real, intent(inout) :: tbarcgtl (ilg,ig) !<
  real, intent(inout) :: tbarcsgtl(ilg,ig) !<
  real, intent(inout) :: tbarggtl (ilg,ig) !<
  real, intent(inout) :: tbargsgtl(ilg,ig) !<
  real, intent(inout) :: thliqcgtl(ilg,ig) !<
  real, intent(inout) :: thliqggtl(ilg,ig) !<
  real, intent(inout) :: thicecgtl(ilg,ig) !<
  !
  real, intent(inout) :: ancsgtl  (ilg,ictem) !<
  real, intent(inout) :: ancggtl  (ilg,ictem) !<
  real, intent(inout) :: rmlcsgtl (ilg,ictem) !<
  real, intent(inout) :: rmlcggtl (ilg,ictem) !<
  !
  real, intent(inout) :: tagtl    (ilg) !<
  real, intent(inout) :: fsnowgtl (ilg) !<
  real, intent(inout) :: tcanogtl (ilg) !<
  real, intent(inout) :: tcansgtl (ilg) !<
  !
  !     * input/output fields from main ctem routine.
  !
  real, intent(inout)    :: soilcgat (ilg,ictemp1) !<
  real, intent(inout)    :: litrcgat (ilg,ictemp1) !<
  real, intent(inout)    :: rootcgat (ilg,ictem) !<
  real, intent(inout)    :: stemcgat (ilg,ictem) !<
  real, intent(inout)    :: gleafcgat(ilg,ictem) !<
  real, intent(inout)    :: bleafcgat(ilg,ictem) !<
  real, intent(inout)    :: fallhgat (ilg,ictem) !<
  real, intent(inout)    :: growtgat (ilg,ictem) !<
  real, intent(inout)    :: lastrgat (ilg,ictem) !<
  real, intent(inout)    :: lastsgat (ilg,ictem) !<
  real, intent(inout)    :: thisylgat(ilg,ictem) !<
  real, intent(inout)    :: stemhgat (ilg,ictem) !<
  real, intent(inout)    :: roothgat (ilg,ictem) !<
  integer, intent(inout) :: posphgat (ilg,ictem) !<
  integer, intent(inout) :: leafsgat (ilg,ictem) !<
  integer, intent(inout) :: tempcgat (ilg,2) !<
  !
  !     * ctem vegegation class fraction fields (3 time levels).
  !
  real, intent(inout)    :: prefgat  (ilg,ictem) !<
  real, intent(inout)    :: newfgat  (ilg,ictem) !<
  !
  !     * co2 land flux.
  !
  real, intent(inout), dimension(ilg) :: fcolgat !<
  !
  !     * fraction area of land.
  !
  real, intent(inout), dimension(ilg) :: areagat !<
  !
  !     * invariant class input fields.
  !
  real, intent(inout)    :: sandgat  (ilg,ig) !<
  real, intent(inout)    :: claygat  (ilg,ig) !<
  real, intent(inout), dimension(ilg) :: sdepgat !<
  !
  !     * atmospheric and grid-constant input variables.
  !
  real, intent(in)  :: co2row(nl) !<
  real, intent(in)  :: fsfrot(nl,nm) !<
  !
  real, intent(inout)  :: co2gat(ilg) !<
  real, intent(inout)  :: fsfgat(ilg) !<
  !
  !     * gather-scatter index arrays.
  !
  integer, intent(in)  :: ilmos (ilg) !<
  integer, intent(in)  :: jlmos  (ilg) !<
  !----------------------------------------------------------------------

  do k=1,nml
    cfluxcsgat(k)=cfluxcsrot(ilmos(k),jlmos(k))
    cfluxcggat(k)=cfluxcgrot(ilmos(k),jlmos(k))
    co2gat    (k)=co2row    (ilmos(k))
    fsfgat    (k)=fsfrot    (ilmos(k),jlmos(k))
    lghtgat   (k)=lghtrow   (ilmos(k))
    wetfgat   (k)=wetfrow   (ilmos(k))
    wetsgat   (k)=wetsrow   (ilmos(k))
    sdepgat   (k)=sdeprot   (ilmos(k),jlmos(k))
    fcolgat   (k)=fcolrot   (ilmos(k),jlmos(k))
    areagat   (k)=arearow   (ilmos(k))
  end do ! loop 100
  !
  do l=1,ictem
    do k=1,nml
      co2cg1gat(k,l)=co2cg1rot(ilmos(k),jlmos(k),l)
      co2cg2gat(k,l)=co2cg2rot(ilmos(k),jlmos(k),l)
      co2cs1gat(k,l)=co2cs1rot(ilmos(k),jlmos(k),l)
      co2cs2gat(k,l)=co2cs2rot(ilmos(k),jlmos(k),l)
      !
      rootcgat (k,l)=rootcrot (ilmos(k),jlmos(k),l)
      stemcgat (k,l)=stemcrot (ilmos(k),jlmos(k),l)
      gleafcgat(k,l)=gleafcrot(ilmos(k),jlmos(k),l)
      bleafcgat(k,l)=bleafcrot(ilmos(k),jlmos(k),l)
      fallhgat (k,l)=fallhrot (ilmos(k),jlmos(k),l)
      posphgat (k,l)=nint(posphrot(ilmos(k),jlmos(k),l))
      leafsgat (k,l)=nint(leafsrot(ilmos(k),jlmos(k),l))
      growtgat (k,l)=growtrot (ilmos(k),jlmos(k),l)
      lastrgat (k,l)=lastrrot (ilmos(k),jlmos(k),l)
      lastsgat (k,l)=lastsrot (ilmos(k),jlmos(k),l)
      thisylgat(k,l)=thisylrot(ilmos(k),jlmos(k),l)
      stemhgat (k,l)=stemhrot (ilmos(k),jlmos(k),l)
      roothgat (k,l)=roothrot (ilmos(k),jlmos(k),l)
      !
      prefgat  (k,l)=prefrot  (ilmos(k),jlmos(k),l)
      newfgat  (k,l)=newfrot  (ilmos(k),jlmos(k),l)
    end do
  end do ! loop 200
  !
  do l=1,ictemp1
    do k=1,nml
      soilcgat (k,l)=soilcrot (ilmos(k),jlmos(k),l)
      litrcgat (k,l)=litrcrot (ilmos(k),jlmos(k),l)
    end do
  end do ! loop 250
  !
  do l=1,2
    do k=1,nml
      tempcgat (k,l)=nint(tempcrot(ilmos(k),jlmos(k),l))
    end do
  end do ! loop 280
  !
  if (gmt>0. .and. kount>0) then
    do l=1,ig
      do k=1,nml
        tbargtl  (k,l)=tbarrtl  (ilmos(k),jlmos(k),l)
        tbarcgtl (k,l)=tbarcrtl (ilmos(k),jlmos(k),l)
        tbarcsgtl(k,l)=tbarcsrtl(ilmos(k),jlmos(k),l)
        tbarggtl (k,l)=tbargrtl (ilmos(k),jlmos(k),l)
        tbargsgtl(k,l)=tbargsrtl(ilmos(k),jlmos(k),l)
        thliqcgtl(k,l)=thliqcrtl(ilmos(k),jlmos(k),l)
        thliqggtl(k,l)=thliqgrtl(ilmos(k),jlmos(k),l)
        thicecgtl(k,l)=thicecrtl(ilmos(k),jlmos(k),l)
      end do
    end do ! loop 400
    !
    do l=1,ictem
      do k=1,nml
        ancsgtl  (k,l)=ancsrtl  (ilmos(k),jlmos(k),l)
        ancggtl  (k,l)=ancgrtl  (ilmos(k),jlmos(k),l)
        rmlcsgtl (k,l)=rmlcsrtl (ilmos(k),jlmos(k),l)
        rmlcggtl (k,l)=rmlcgrtl (ilmos(k),jlmos(k),l)
      end do
    end do ! loop 500
    !
    do k=1,nml
      tagtl     (k)=tartl     (ilmos(k),jlmos(k))
      fsnowgtl  (k)=fsnowrtl  (ilmos(k),jlmos(k))
      tcanogtl  (k)=tcanortl  (ilmos(k),jlmos(k))
      tcansgtl  (k)=tcansrtl  (ilmos(k),jlmos(k))
    end do ! loop 600
  end if
  !
  do l=1,ig
    do k=1,nml
      sandgat (k,l)  =sandrot (ilmos(k),jlmos(k),l)
      claygat (k,l)  =clayrot (ilmos(k),jlmos(k),l)
    end do
  end do ! loop 700

  return
end
