!> \file
!> \brief Brief description of the routine purpose.
!!
!! @author Routine author name(s)
!
subroutine drcoefl (cdm,z0_momentum,z0_heat,cdh,cdmn,va,t0,ta,qa,pres,zrefm,zrefh, &
                    fice,fls,ilg,il1,il2)
  !=======================================================================
  !     * dec 15/16 - m.lazare/   - nstep removed.
  !     *          less  d.verseghy. - bugfix in accounting for effect
  !     *                           of fice on drag coefficients.
  !     * jan 25/16 - m.mackay.   fractional ice cover included
  !     *                         bug fix for near sfc humidity
  !     *                         thermodynamic ref height added
  !     * may 22/15 - d.verseghy. weight drag coefficients for presence
  !     *                         of ice.
  !     * nov 27/07 - m.mackay.    turbulent transfer coefficients
  !     *
  !=======================================================================
  !
  use hydcon, only : grav, tfrez, vkc
  !
  implicit none
  !
  ! ----* input fields *------------------------------------------------
  !
  integer, intent(in) :: ilg   !< Total number of atmospheric columns \f$[unitless]\f$
  integer, intent(in) :: il1   !< Index of first atmospheric column for calculations \f$[unitless]\f$
  integer, intent(in) :: il2   !< Index of last atmospheric column for calculations \f$[unitless]\f$
  real, intent(in),dimension(ilg) :: va !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: t0 !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: ta !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: qa !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: pres !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: zrefm !< height of lowest model momentum level \f$[m]\f$
  real, intent(in),dimension(ilg) :: zrefh !< height of lowerst model thermodynamic level \f$[m]\f$
  real, intent(in),dimension(ilg) :: fice !< Variable description\f$[units]\f$
  real, intent(in),dimension(ilg) :: fls !< Variable description\f$[units]\f$
  !
  ! ----* output fields *------------------------------------------------
  !
  real, intent(inout),dimension(ilg) :: cdh !< drag coefficient for heat \f$[unitless]\f$
  real, intent(inout),dimension(ilg) :: cdm !< drag coefficient for momentum \f$[unitless]\f$
  real, intent(inout),dimension(ilg) :: z0_momentum !< surface roughness height for momentum \f$[m]\f$
  real, intent(inout),dimension(ilg) :: z0_heat !< surface roughnes height for heat \f$[m]\f$
  real, intent(inout),dimension(ilg) :: cdmn !< neutral drag coefficent for momentum \f$[unitless]\f$
  !==================================================================
  ! physical (adjustable) parameters
  !
  ! define and document here any adjustable parameters.
  ! this should be variable described using the doxygen format above as
  ! well as a description of its minimum/default/maximum.
  !
  ! here is an example,
  !
  ! real :: beta !< This is the adjustable factor for computing liquid cloud effective radius \f$[]\f$
  !           !! It is compute differently when using bulk or PAM aerosols.
  !           !! For bulk aerosols its minimum/default/maximum is (1.0/1.3/1.5).
  !==================================================================

  !
  ! ----* local variables *---------------------------------------------
  !
  integer :: i
  integer :: j
  integer :: k
  integer :: iter
  integer :: itmax
  real :: g
  real :: tol
  real :: charn
  real :: resid
  real :: vsq
  real :: cold
  real :: easat
  real :: qasat
  real :: ca
  real :: cb
  real :: shf
  real :: lhf
  real :: tvirt
  real :: mol
  real :: z_l
  real :: cdhn
  real :: x
  real :: psih
  real :: psim
  real :: pi
  real :: molold
  real :: tol2
  real :: cdhrat
  real :: cdmnw
  real :: cdhnw
  real :: cdmni
  real :: cdhni
  real :: denom
  real :: xice
  real :: z0_momentum_water  !< surface roughness for momentum calculated over water
  real :: z0_heat_water      !< surface roughness for heat calculated over water

  !
  ! ----* local parameters *--------------------------------------------
  !
  g = grav
  tol = 1.0e-8
  tol2 = 1.0e-2
  itmax = 100
  charn = 0.0175
  pi = 3.14159
  !
  !======================================================================
  do i = il1,il2
    !-----------------------------------------------------------------------
    ! neutral drag coefficients
    !  iterative routine to solve for cdn based on charnock relation for
    !  roughness
    !
    cdhnw = 1.35e-3       ! Carsten: heat coefficient for water is not dynamically derived - why? 
    cdmnw = 1.0e-3        ! initial trial value
    vsq = va(i) * va(i)
    resid = 999.0
    do while (abs(resid) > tol)
      cold = cdmnw
      cdmnw = vkc * vkc/ &
              (log(zrefm(i) * g/(charn * cold * vsq)) * log(zrefm(i) * g/ &
              (charn * cold * vsq)))
      resid = cdmnw - cold
    end do
    ! Calculation of the surface roughness
    z0_momentum_water = (charn * cold * vsq)/g
    z0_heat_water = (charn * cdhnw * vsq)/g
    !
    cdmni = (vkc/(log(zrefm(i)/0.002))) ** 2
    cdhni = (vkc/(log(zrefh(i)/0.00067))) ** 2
    if (fice(i) > (fls(i) + 0.001)) then
      xice = max(fice(i) - fls(i),0.0)
      cdmn(i) = (xice * cdmni + (1.0 - fice(i)) * cdmnw)/(1.0 - fls(i))
      cdhn = (xice * cdhni + (1.0 - fice(i)) * cdhnw)/(1.0 - fls(i))
      z0_momentum(i) = (xice * 0.002 + (1.0 - fice(i)) * z0_momentum_water)/(1.0-fls(i))
      z0_heat(i) = (xice * 0.00067 + (1.0 - fice(i)) * z0_heat_water )/(1.0-fls(i))
    else
      cdmn(i) = cdmnw
      cdhn = cdhnw
      z0_momentum(i) = z0_momentum_water
      z0_heat(i) = z0_heat_water
    end if
    !-----------------------------------------------------------------------
    ! initial trial values for transfer coefficients: set to neutral values
    !
    cdh(i) = cdhn
    cdm(i) = cdmn(i)
    !-----------------------------------------------------------------------
    ! iteratively compute drag coefficients until m.o. length converges
    !
    resid = 999.0
    mol = 9999.0
    iter = 0
    do while (abs(resid) > tol2 .and. iter < itmax)
      !-----------------------------------------------------------------------
      ! heat fluxes
      !----------------------------------------------------
      !     * calculation of easat consistent with classi
      !     * but constants differ from rogers &yau
      !     * rogers and yau values
      !         ca=17.67
      !         cb=29.65
      !----------------------------------------------------
      if (t0(i) >= tfrez) then
        ca = 17.269
        cb = 35.86
      else
        ca = 21.874
        cb = 7.66
      end if
      shf = cdh(i) * va(i) * (t0(i) - ta(i))
      easat = 611.0 * exp(ca * (t0(i) - tfrez)/(t0(i) - cb))
      qasat = 0.622 * easat/(pres(i) - 0.378 * easat)
      lhf = cdh(i) * va(i) * (qasat - qa(i))

      !-----------------------------------------------------------------------
      ! virtual temperature and m.-o. length
      !
      tvirt = ta(i) * (1.0 + 0.61 * qa(i))
      molold = mol
      mol = - va(i) * va(i) * va(i) * cdm(i) * sqrt(cdm(i)) * tvirt/ &
            (vkc * g * (shf + 0.61 * lhf * ta(i)) )
      z_l = zrefm(i)/mol
      resid = mol - molold

      !-----------------------------------------------------------------------
      ! stability corrections
      !
      !
      !- unstable case
      !---------------
      if (z_l < 0.0) then
        x = (1.0 - (16.0 * z_l)) ** 0.25
        psim = 2.0 * log((1.0 + x)/2.0) + log((1.0 + x * x)/2.0) &
               - 2.0 * atan(x) + pi/2.0
        psih = 2.0 * log((1.0 + x * x)/2.0)
        !
        !- stable case
        !-------------
      else if (z_l >= 0 .and. z_l < 0.5) then
        psim = - 5.0 * z_l
        psih = psim
      else if (z_l >= 0.5 .and. z_l < 10.0) then
        psim = (0.5/(z_l * z_l)) - (4.25/z_l) - 7.0 * log(z_l) - 0.852
        psih = psim
      else
        psim = log(z_l) - 0.76 * z_l - 12.093
        psih = psim
      end if

      !-----------------------------------------------------------------------
      ! recompute drag coefficients with stabilty corrections
      !
      denom = (1.0 + (cdmn(i)/(vkc * vkc)) * (psim * psim &
              - (2.0 * vkc * psim/sqrt(cdmn(i)))) )
      if (denom < 1.0e-6) denom = 1.0e-6
      !        if (abs(denom)<1.0e-6) denom=sign(1.0e-6,denom)
      cdm(i) = cdmn(i)/denom
      denom = (1.0 + (cdhn/(vkc * vkc)) * (psim * psih &
              - (vkc * psih/sqrt(cdmn(i))) - (vkc * psim * sqrt(cdmn(i))/cdhn)))
      if (denom < 1.0e-6) denom = 1.0e-6
      !        if (abs(denom)<1.0e-6) denom=sign(1.0e-6,denom)
      cdh(i) = cdhn/denom

      iter = iter + 1
    end do

    if (iter >= itmax) then
      cdm(i) = cdmn(i)
      cdh(i) = cdhn
    end if

    !     if (iter >= itmax) print*, "** max iters reached"

    if (cdh(i) < 0.0) then
      cdh(i) = cdhn
    end if

    if (cdm(i) < 0.0) then
      cdm(i) = cdmn(i)
    end if

    cdhrat = cdh(i)/cdhn
    if (cdhrat >= 8.0) then
      cdh(i) = cdhn
    end if

  end do ! loop 100

  return
end subroutine drcoefl
!> \file
!> This is an example of adding text at the end of the routine.
!! Your detailed description of the routine can be put here, including scientific
!! numerics, and another other important information.
!! Doxygen should be able to translate LaTeX and it is possible to include
!! references using "\cite", for example, \cite vonSalzen2013.
!! Equations can be included as well, as inline equations \f$ F=ma \f$,
!! or in the equation environment \n
!! (NOTE that HTML will not number the equation but it will in LaTeX),
!! \f{equation}{
!!  F_1=ma_1
!! \f}
